
state_query = "select distinct state from buisness_suite.final_view where state is not null order by state"

branch_query = "select distinct branch from buisness_suite.final_view order by branch"

function_query = "select distinct function_name from buisness_suite.final_view where function_name='M & M FINANCIAL SERVICES LTD'"

business_unit_query = 'select distinct business_unit from buisness_suite.final_view order by business_unit'

division_query = 'select distinct division from buisness_suite.final_view order by division'
department_query = 'select distinct department from buisness_suite.final_view order by department'
sub_department_query = 'select distinct sub_department from buisness_suite.final_view order by sub_department'
band_query = "select distinct band from buisness_suite.final_view"
cost_centre_query = 'select distinct cost_centre from buisness_suite.final_view order by cost_centre'
work_contract_query = 'select distinct work_contract from buisness_suite.final_view where work_contract is not null order by work_contract'
grade_query = 'select distinct employee_sub_group from buisness_suite.final_view'


new_query = """select count(distinct employee_id) as before_count,state
from buisness_suite.final_view_ where(((employee_status = 'Active' or employee_status ='Suspended'or employee_status = 'Unpaid Leave') and employment_details_date_of_hire <='2018-01-01') or 
((employment_details_last_working_date>'2018-01-31') and employment_details_date_of_hire < '2018-01-01' and employment_details_last_working_date is null)) and state in ('Maharashtra-Maharashtra') group by state
"""
source_of_recruitement_query_macro = '''select recruitement_data as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select recruitement_data,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by recruitement_data) Inner_query'''

business_service_function = """select business_service_function_cat as name,
case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select business_service_function_cat,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from
buisness_suite.final_view
where {clause} and business_service_function_cat in ('Service Function','Business Function') group by business_service_function_cat) Inner_query"""

business_service_function_macro = """select business_service_function as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select business_service_function,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended','Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from
buisness_suite.final_view
where {clause} group by business_service_function) Inner_query"""

major_service_function = """select major_service_function_cat as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select major_service_function_cat,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from
buisness_suite.final_view
where {clause} and major_service_function_cat in ('Major Function') group by major_service_function_cat) Inner_query"""

major_service_function_macro = """select major_function as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select major_function,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from
buisness_suite.final_view
where {clause} group by major_function) Inner_query"""



voluntary_involuntary_data_macro = '''with ctc1 as(select (coalesce(after_count,0)+before_count)/2 ::float ::numeric
as value from (
select count(distinct  case when  employment_details_date_of_hire<= '{start_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end
) as after_count ,
count(distinct  case when employment_details_last_working_date between '{start_date}'
AND  '{end_date}' then employee_id  else null end   ) as left_count
from
buisness_suite.final_view
where  {clause} ) Inner_query)
select reason_of_leaving as name,value,case when coalesce(count(distinct employee_id),0) = 0 then 0 else
count(distinct employee_id)/(value ::float)*100::numeric end  as
percentage from buisness_suite.final_view,ctc1 where  employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') and {clause} group by reason_of_leaving ,value
'''

new_query = """select count(distinct employee_id) as before_count,state
from buisness_suite.final_view_ where(((employee_status = 'Active' or employee_status ='Suspended'or employee_status = 'Unpaid Leave') and employment_details_date_of_hire <='2018-01-01') or 
((employment_details_last_working_date>'2018-01-31') and employment_details_date_of_hire < '2018-01-01' and employment_details_last_working_date is null)) and state in ('Maharashtra-Maharashtra') group by state
"""
employee_left_query = '''select count(distinct employee_id) as left_count from buisness_suite.final_view  where employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') and {clause}'''

attrition_days_query = '''select case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date  = '{end_date}' then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} ) Inner_query'''

attrition_percentage_query = '''select case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} ) Inner_query'''



gender_query = '''select gender as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select gender,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by gender) Inner_query'''

marital_status_query = '''select name, (percentage / SUM(percentage)OVER ())*100  AS "percentage" from(select marital_status as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select marital_status,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by marital_status) Inner_query)outer_query'''

source_of_recruitement_query = '''select recruitement_data_category as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select recruitement_data_category,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by recruitement_data_category) Inner_query'''

age_group_query = '''select age_group as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select age_group,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by age_group) Inner_query'''

training_data_query = '''select training_data as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select training_data,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by training_data) Inner_query'''

star_performer_query = '''select star_performer as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select star_performer,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by star_performer) Inner_query'''


gem_performer_query = '''select gem_performe as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select gem_performe,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by gem_performe) Inner_query'''

grade_performer_query = '''select employee_sub_group as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
value from (
select employee_sub_group,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by employee_sub_group) Inner_query '''

pip_performer_query = '''select pip_data as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select pip_data,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by pip_data) Inner_query'''

dic_performer_query = '''select dic_data as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select dic_data,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by dic_data) Inner_query'''

rating_data_query = '''select {rating} as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select {rating},count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by {rating}) Inner_query'''

avg_rating_query = """with ctc1 as(select count(distinct fv.employee_id) as before_count, COALESCE (k.rating::text,'NA')as rating from buisness_suite.final_view as fv
 left join(select fv.employee_id ,avg(n.rating::float) as rating  from buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n
on fv.employee_id = n.employee_id where
employment_details_date_of_hire <= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date >= '{start_date}' or employment_details_last_working_date is null ) and
{clause} and (n."year" in ({financial_year}) or n."year" is null)
group by fv.employee_id)k on fv.employee_id = k.employee_id where employment_details_date_of_hire <= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date >= '{start_date}' or employment_details_last_working_date is null ) and
{clause} group by rating),
ctc2 as(select count(distinct fv.employee_id) as after_count, COALESCE (s.rating::text,'NA')as rating from buisness_suite.final_view as fv
 left join(select distinct fv.employee_id ,avg(n.rating::float) as rating  from buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n
on fv.employee_id = n.employee_id where
employment_details_date_of_hire <= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date >= '{end_date}' or employment_details_last_working_date is null ) and
 {clause} and (n."year" in ({financial_year}) or n."year" is null)
group by fv.employee_id)s on fv.employee_id = s.employee_id where employment_details_date_of_hire <= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date >= '{end_date}' or employment_details_last_working_date is null ) and
{clause} group by rating) ,
ctc3 as(select  COALESCE (l.rating::text,'NA')as rating,count(distinct fv.employee_id) as left_count from buisness_suite.final_view as fv
 left join
(select fv.employee_id,avg(n.rating::float) as rating  from buisness_suite.final_view fv left join
buisness_suite.promotion_and_rating_master  n on fv.employee_id=n.employee_id where
{clause} and (n."year" in ({financial_year}) or n."year" is null ) and employment_details_last_working_date >'{start_date}'
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') group by fv.employee_id)l on fv.employee_id =l.employee_id where {clause}
and employment_details_last_working_date >'{start_date}'
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') group by l.rating)
select ctc3.rating as name,case when coalesce(ctc2.after_count,0)+coalesce(ctc1.before_count,0) = 0 then 0 else
ctc3.left_count/((coalesce(ctc2.after_count,0)+coalesce(ctc1.before_count,0))/2 ::float)*100::numeric end  as
percentage  from ctc3 left join ctc2 on ctc3.rating= ctc2.rating left join ctc1 on ctc1.rating = ctc3.rating
"""


previous_year_rating = """with ctc1 as (
select COALESCE (n.rating,'NA')as rating,count(distinct  case when  employment_details_date_of_hire <= '{start_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date >= '{start_date}' or employment_details_last_working_date is null ) then fv.employee_id else null end
) as before_count ,
count(distinct  case when  employment_details_date_of_hire <= '{end_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date >= '{end_date}' or employment_details_last_working_date is null ) then fv.employee_id else null end
) as after_count
from
buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n on fv.employee_id= n.employee_id and n."year" in ({financial_year})
where  {clause} group by n.rating),
ctc2 as(select COALESCE (n.rating,'NA')as rating,count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then fv.employee_id  else null end   ) as left_count from buisness_suite.final_view fv left join 
buisness_suite.promotion_and_rating_latest_view  n on fv.employee_id=n.employee_id where  
{clause} group by n.rating)
select ctc1.rating as name,case when coalesce(ctc1.after_count,0)+ctc1.before_count = 0 then 0 else
ctc2.left_count/((coalesce(ctc1.after_count,0)+ctc1.before_count)/2 ::float)*100::numeric end  as
percentage  from ctc1 left join ctc2 on ctc1.rating= ctc2.rating"""


tree_map_query = """select name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select {select} as name,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {where} group by {select}) Inner_query"""

annual_compensation_query = """select coalesce(annual_compensation_range) as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select annual_compensation_range,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by annual_compensation_range order by SUBSTRING(annual_compensation_range FROM '([0-9.]+)-')::numeric ASC) Inner_query"""

average_tenure_query = """select  avg(total_experience_months_in_mf::dec)/12 from buisness_suite.final_view  where employment_details_last_working_date between '{start_date}' \
AND  '{end_date}' and {clause}  """

attrition_trend_query = """with ctc1 as(select {date_part} as name,count(distinct employee_id) as before_count from buisness_suite.final_view where \
employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or \
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) {clause}\
group by {date_part}),\
ctc2 as(select  {date_part} as name,count(distinct employee_id) as after_count from buisness_suite.final_view where \
employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or \
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) {clause} group by {date_part})\
,\
ctc3 as(select {date_part} as name,count(employee_id) as left_count from buisness_suite.final_view \
where employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') {clause} group by {date_part}),\
ctc4 as(select ctc3.name,(left_count/((coalesce(ctc2.after_count,0)+ ctc1.before_count)/2 ::float)*100)::numeric as percentage from ctc3\
 left join ctc1 on ctc1.name = ctc3.name\
 left join ctc2 on ctc2.name = ctc3.name)select name,coalesce(percentage,0) as percentage from ctc4\
"""
cohert_analytics_query = """select coalesce(total_experience_months_in_mf_range,'N_A') as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select total_experience_months_in_mf_range,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by total_experience_months_in_mf_range order by SUBSTRING(total_experience_months_in_mf_range FROM '([0-9.]+)-')::numeric ASC) Inner_query"""

voluntary_involuntary_data = '''with ctc1 as(select (coalesce(after_count,0)+before_count)/2 ::float ::numeric
as value from (
select count(distinct  case when  employment_details_date_of_hire<= '{start_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from
buisness_suite.final_view
where  {clause}  ) Inner_query)
select reason_of_leaving_group as name,value,case when coalesce(count(distinct employee_id),0) = 0 then 0 else
count(distinct employee_id)/(value ::float)*100::numeric end  as
percentage from buisness_suite.final_view,ctc1 where  employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') and {clause} group by reason_of_leaving_group ,value
'''

higher_qualification_query = """select higher_qualification_category as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select higher_qualification_category,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by higher_qualification_category) Inner_query"""


reporting_manager_attrition_query = """with ctc1 as (select manager_user_sys_id,left_count,currently_working,voulunary_count,invoulunary_count,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select manager_user_sys_id,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count,
count(distinct  case when employment_details_last_working_date is null then employee_id  else null end   ) as currently_working,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group ='voluntary' then employee_id  else null end   ) as voulunary_count,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group = 'Involuntary' then employee_id  else null end   ) as invoulunary_count
from 
buisness_suite.final_view
where {clause} group by manager_user_sys_id) Inner_query )
select ct.manager_user_sys_id as id,ct.left_count,ct.currently_working,(ct.voulunary_count/left_count::float)*100::int as vol_per,(ct.invoulunary_count/left_count::float)*100 as invol_per,concat(fv.first_name ,' ',fv.last_name) as name,round(percentage::numeric,2) as percentage from ctc1 as ct left join buisness_suite.final_view as fv on fv.employee_id = ct.manager_user_sys_id  where percentage!=0"""


mcares_department_score = """
with ctc1 as (select cost_centre,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select cost_centre,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by cost_centre) Inner_query )
select ct.cost_centre as department,round(percentage::numeric,2) as percentage,'FY{year}' as year,fv.net_mcares_score as mcaresscore from ctc1 as ct
left join (select * from buisness_suite.mcares_score_department where year::text = '{year}')fv  on fv.department = ct.cost_centre where percentage!=0  order by year asc"""


mcares_manager_score = """ with ctc1 as (select manager_user_sys_id,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select manager_user_sys_id,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by manager_user_sys_id) Inner_query )
select ct.manager_user_sys_id as id ,concat(fv1.first_name ,' ',fv1.last_name) as name,'FY{year}' as year,fv.net_mcares_score as mcaresscore,round(percentage::numeric,2) as percentage from ctc1 as ct 
left join (select * from buisness_suite.mcares_score_manager where year in ('{year}'))fv  on fv.manager_id = ct.manager_user_sys_id
left join buisness_suite.final_view fv1 on fv1.employee_id = ct.manager_user_sys_id where percentage!=0 order by year asc"""





work_contrat_query = """select work_contract as name,case when coalesce(after_count,0)+before_count = 0 then 0 else 
left_count/((coalesce(after_count,0)+before_count)/2 ::float)*100::numeric end  as 
percentage from (
select work_contract,count(distinct  case when  employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end 
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from 
buisness_suite.final_view
where {clause} group by work_contract) Inner_query  where left_count!=0"""



functional_competency = """
with ctc1 as(select crm.name_of_the_functional_competency,count(distinct fv.employee_id) as before_count from 
buisness_suite.final_view fv left join buisness_suite.competency_report_master_year crm on fv.employee_id = crm.employee_id where employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) 
 and {clause} and crm.year <'{start_date_year}' and crm.score is not null group by crm.name_of_the_functional_competency  ),

ctc2 as(select  crm.name_of_the_functional_competency,count(distinct fv.employee_id) as after_count 
from buisness_suite.final_view fv left join buisness_suite.competency_report_master_year crm on fv.employee_id = crm.employee_id where employment_details_date_of_hire<= '{end_date}' 
and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) 
and {clause} and crm.year <'{end_date_year}' and crm.score is not null group by crm.name_of_the_functional_competency),

ctc3 as(select crm.name_of_the_functional_competency,count(distinct fv.employee_id) as left_count 
from buisness_suite.final_view fv left join buisness_suite.competency_report_master_year crm on fv.employee_id = crm.employee_id where employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement')  and {clause}  and crm.score is not null group by crm.name_of_the_functional_competency),

ctc4 as(select ctc3.name_of_the_functional_competency as name,(left_count/((coalesce(ctc2.after_count,0)+ ctc1.before_count)/2 ::float)*100)::numeric as 
percentage from ctc3 left join ctc1 on ctc1.name_of_the_functional_competency = ctc3.name_of_the_functional_competency left join ctc2 on ctc2.name_of_the_functional_competency = ctc3.name_of_the_functional_competency)
select name,round(coalesce(percentage,0),2) as percentage,round(avg(crm.score::int),2) as avg from ctc4 ct inner join buisness_suite.competency_report_master_year crm  on crm.name_of_the_functional_competency = ct.name
where crm.year in ({years}) group by ct.name,ct.percentage
"""

# csv_report_query = """
# select employee_id,concat(first_name ,' ',last_name) as name,{chart_name} from buisness_suite.final_view where employment_details_last_working_date between '{start_date}'
# AND  '{end_date}'  and {clause} group by  employee_id,name,{chart_name} order by {chart_name}
# """
#
# csv_report_query = """select distinct concat({chart_name},'_','startDateActive') as name,concat(first_name ,' ',last_name) as fullName, employee_id from buisness_suite.final_view where
# employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or
# employee_status = 'Unpaid Leave' or
# employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) and state in ('West Bengal')
# group by {chart_name},employee_id,fullName
# union all
# select  distinct concat({chart_name},'_','endDateActive') as name,concat(first_name ,' ',last_name) as fullName, employee_id from buisness_suite.final_view where
# employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or
# employee_status = 'Unpaid Leave' or
# employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) and state in ('West Bengal')
# group by {chart_name},employee_id,fullName
# union all
# select  distinct concat({chart_name},'_','leftEmployees') as name,concat(first_name ,' ',last_name) as fullName, employee_id from buisness_suite.final_view
# where employment_details_last_working_date between '{start_date}'
# AND  '{end_date}'  and state in ('West Bengal')  group by {chart_name},employee_id,fullName"""


csv_report_query = """select distinct concat({chart_name},'_','Open') as name,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view where 
employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or 
employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) and {clause}
group by {chart_name},employee_id,employeeName
union all
select  distinct concat({chart_name},'_','Close') as name,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view where 
employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or 
employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) and {clause}
group by {chart_name},employee_id,employeeName
union all
select  distinct concat({chart_name},'_','Left') as name,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view 
where employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement')  and {clause} group by {chart_name},employee_id,employeeName"""


vol_invol_csv_data = """select distinct 'Open' as name ,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view where 
employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or 
employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) and {clause}
group by employee_id,employeeName
union all
select  distinct 'Close' as name,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view where 
employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or 
employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) and {clause}
group by employee_id,employeeName
union all
select  distinct concat({chart_name},'_','Left') as name,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view 
where employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement')  and {chart_name} in ('Involuntary','voluntary') and {clause} group by {chart_name},employee_id,employeeName"""



consolidate_csv_query = """select distinct 'Consolidated_Open' as name ,{chart_name} as category,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view where 
employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or 
employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) and {clause}
group by {chart_name},employee_id,employeeName
union all
select  distinct 'Consolidated_Close' as name ,{chart_name} as category,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view where 
employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or 
employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) and  {clause}
group by {chart_name},employee_id,employeeName
union all
select  distinct 'Consolidated_Left' as name,{chart_name} as category,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view 
where employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement')  and  {clause} group by {chart_name},employee_id,employeeName
"""

consolidate_vol_invol_csv_query = """select distinct 'Consolidated_Open' as name ,{chart_name} as category,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view where 
employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or 
employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) and {clause}
group by {chart_name},employee_id,employeeName
union all
select  distinct 'Consolidated_Close' as name ,{chart_name} as category,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view where 
employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or 
employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) and  {clause}
group by {chart_name},employee_id,employeeName
union all
select  distinct 'Consolidated_Left' as name,{chart_name} as category,concat(first_name ,' ',last_name) as employeeName, employee_id from buisness_suite.final_view 
where employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement')  and  {clause} and {chart_name} in ('Involuntary','voluntary') group by {chart_name},employee_id,employeeName
"""

consolidated_avg_rating="""select  'Consolidated_Open' as name ,COALESCE (k.rating::text,'NA')as rating,fv.employee_id ,concat(first_name ,' ',last_name) as employeeName from buisness_suite.final_view as fv
 left join(select fv.employee_id ,avg(n.rating::float) as rating  from buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n
on fv.employee_id = n.employee_id where
employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) and
 {clause}and (n."year" in ({financial_year}) or n."year" is null)
group by fv.employee_id)k on fv.employee_id = k.employee_id where employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) and
 {clause} group by rating,fv.employee_id,employeeName
union all 
select  'Consolidated_Close' as name ,COALESCE (s.rating::text,'NA')as rating,fv.employee_id ,concat(first_name ,' ',last_name) as employeeName from buisness_suite.final_view as fv
 left join(select distinct fv.employee_id ,avg(n.rating::float) as rating  from buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n
on fv.employee_id = n.employee_id where
employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) and
{clause} and (n."year" in ({financial_year}) or n."year" is null)
group by fv.employee_id)s on fv.employee_id = s.employee_id where employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) and
{clause} group by rating,fv.employee_id,employeeName
 union all
select  'Consolidated_Left' as name ,COALESCE (l.rating::text,'NA')as rating,fv.employee_id ,concat(first_name ,' ',last_name) as employeeName from buisness_suite.final_view as fv
 left join
(select fv.employee_id,avg(n.rating::float) as rating  from buisness_suite.final_view fv left join
buisness_suite.promotion_and_rating_master  n on fv.employee_id=n.employee_id where
{clause} and (n."year" in ({financial_year}) or n."year" is null ) and employment_details_last_working_date >'{start_date}'
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') group by fv.employee_id)l on fv.employee_id =l.employee_id where {clause} 
and employment_details_last_working_date >'{start_date}'
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') group by rating,fv.employee_id,employeeName
"""

category_avg_rating = """select distinct fv.employee_id,concat(fv.first_name ,' ',fv.last_name) as employeeName,concat( COALESCE (rating::text,'NA'),'_','Open') as name from buisness_suite.final_view as fv
 left join(select fv.employee_id ,avg(n.rating::float) as rating  from buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n
on fv.employee_id = n.employee_id where
employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) and
{clause} and (n."year" in ({financial_year}) or n."year" is null)
group by fv.employee_id)k on fv.employee_id = k.employee_id where employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) and
{clause} group by rating,fv.employee_id,employeeName
union all 
select distinct fv.employee_id,concat(fv.first_name ,' ',fv.last_name) as employeeName,concat( COALESCE (rating::text,'NA'),'_','Close') as name from buisness_suite.final_view as fv
 left join(select distinct fv.employee_id ,avg(n.rating::float) as rating  from buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n
on fv.employee_id = n.employee_id where
employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) and
{clause} and (n."year" in ({financial_year}) or n."year" is null)
group by fv.employee_id)s on fv.employee_id = s.employee_id where employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) and
{clause} group by rating,fv.employee_id,employeeName
 union all
select fv.employee_id,concat(fv.first_name ,' ',fv.last_name) as employeeName,COALESCE (q.name::text,'NA_Left')as category from buisness_suite.final_view as fv
 left join
(select distinct employee_id,employeeName,concat( COALESCE (rating::text,'NA'),'_','Left') as name from (select fv.employee_id,concat(first_name ,' ',last_name) as employeeName,avg(n.rating::float) as rating  from buisness_suite.final_view fv left join 
buisness_suite.promotion_and_rating_master  n on fv.employee_id=n.employee_id where  
{clause} and (n."year" in ({financial_year}) or n."year" is null) and employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') group by fv.employee_id,fv.employee_id,employeeName,rating)inner_query  group by rating,employee_id,employeeName)q
on fv.employee_id =q.employee_id where  employment_details_last_working_date >'{start_date}'
and employment_details_last_working_date <'{end_date}' and {clause} and reason_of_leaving_group not in ('Death or retirement')

"""

category_last_year_ratig = """select distinct fv.employee_id,concat(first_name ,' ',last_name) as employeeName,concat( COALESCE (rating::text,'NA'),'_','Open') as name from buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n on fv.employee_id= n.employee_id and n."year" in ({financial_year})
where employment_details_date_of_hire<= '{start_date}' 
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) and {clause} group by n.rating,employeeName,fv.employee_id
union all
select distinct fv.employee_id,concat(first_name ,' ',last_name) as employeeName,concat( COALESCE (rating::text,'NA'),'_','Close') as name from buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n on fv.employee_id= n.employee_id and n."year" in ({financial_year})
where employment_details_date_of_hire<= '{end_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null )  and {clause} group by n.rating,employeeName,fv.employee_id
union all 
select distinct fv.employee_id,concat(first_name ,' ',last_name) as employeeName,concat( COALESCE (rating::text,'NA'),'_','Left') as name from buisness_suite.final_view fv left join 
buisness_suite.promotion_and_rating_latest_view  n on fv.employee_id=n.employee_id where  
{clause} and employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') group by n.rating, fv.employee_id,employeeName
"""
consolidated_last_year_rating ="""select distinct 'Consolidated_Open' as name ,COALESCE (n.rating::text,'NA')as category,fv.employee_id,concat(first_name ,' ',last_name) as employeeName from
buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n on fv.employee_id= n.employee_id and n."year" in {financial_year} 
where  employment_details_date_of_hire<= '{start_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{start_date}'  or employment_details_last_working_date is null )  and {clause}
union all
select distinct 'Consolidated_Close' as name ,COALESCE (n.rating::text,'NA')as category,fv.employee_id,concat(first_name ,' ',last_name) as employeeName from
buisness_suite.final_view fv left join buisness_suite.promotion_and_rating_master n on fv.employee_id= n.employee_id and n."year" in {financial_year} 
where  employment_details_date_of_hire<= '{end_date}' and(employee_status = 'Active' or  employee_status ='Suspended'or employee_status = 'Unpaid Leave' or 
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null )  and {clause}
union all
select distinct COALESCE (n.rating,'NA')as rating,fv.employee_id,concat(first_name ,' ',last_name) as employeeName from buisness_suite.final_view fv left join 
buisness_suite.promotion_and_rating_latest_view  n on fv.employee_id=n.employee_id where  
{clause} and employment_details_last_working_date>'{start_date}'
AND  employment_details_last_working_date<'{end_date}' and reason_of_leaving_group not in ('Death or retirement'))
"""
percentage_of_contribution = """with ctc1 as(select (coalesce(after_count,0)+before_count)/2 ::float ::numeric
as value from (
select count(distinct  case when  employment_details_date_of_hire<= '{start_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from
buisness_suite.final_view
where  {clause}  ) Inner_query)
select coalesce({chart_name},'NA') as category,case when coalesce(count(distinct employee_id),0) = 0 then 0 else
count(distinct employee_id)/(value ::float)*100::numeric end  as
percentage from buisness_suite.final_view,ctc1 where  employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') and {clause} 
group by {chart_name} ,value
"""

percentage_contribution_avg_rating = """with ctc1 as(select (coalesce(after_count,0)+before_count)/2 ::float ::numeric
as value from (
select count(distinct  case when  employment_details_date_of_hire<= '{start_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from
buisness_suite.final_view
where  {clause}  ) Inner_query) ,
ctc2 as(select  COALESCE (l.rating::text,'NA')as rating,count(distinct fv.employee_id) as left_count from buisness_suite.final_view as fv
 left join
(select fv.employee_id,avg(n.rating::float) as rating  from buisness_suite.final_view fv left join
buisness_suite.promotion_and_rating_master  n on fv.employee_id=n.employee_id where
{clause} and (n."year" in ({financial_year}) or n."year" is null ) and employment_details_last_working_date >'{start_date}'
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') group by fv.employee_id)l on fv.employee_id =l.employee_id where {clause}
and employment_details_last_working_date >'{start_date}'
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') group by l.rating)
select ctc2.rating as category,case when value = 0 then 0 else ctc2.left_count/(value ::float)*100::numeric end as
percentage from ctc1,ctc2

"""

percentage_contribution_previous_year_rating = """with ctc1 as(select (coalesce(after_count,0)+before_count)/2 ::float ::numeric
as value from (
select count(distinct  case when  employment_details_date_of_hire<= '{start_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{start_date}' or employment_details_last_working_date is null ) then employee_id else null end
) as before_count ,
count(distinct  case when  employment_details_date_of_hire<= '{end_date}'
and (employee_status in('Active' ,'Suspended''Unpaid Leave')or
employment_details_last_working_date>= '{end_date}' or employment_details_last_working_date is null ) then employee_id else null end
) as after_count ,
count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then employee_id  else null end   ) as left_count
from
buisness_suite.final_view
where  {clause}  ) Inner_query),
ctc2 as(select COALESCE (n.rating,'NA')as rating,count(distinct  case when employment_details_last_working_date >'{start_date}' 
and employment_details_last_working_date <'{end_date}' and reason_of_leaving_group not in ('Death or retirement') then fv.employee_id  else null end   ) as left_count from buisness_suite.final_view fv left join 
buisness_suite.promotion_and_rating_latest_view  n on fv.employee_id=n.employee_id where  
{clause} group by n.rating)
 select ctc2.rating as category,case when value = 0 then 0 else ctc2.left_count/(value ::float)*100::numeric end as
percentage from ctc1,ctc2
"""