"""
urls docstring
"""
from django.urls import path
from analytics.views import *

urlpatterns = [
    path('grade_graph/', get_grade_data),
    path('gem_performer_graph/',get_gem_performer_data ),
    path('star_performer_graph/',get_star_performer_data ),
    path('training_data_graph/', get_training_data),
    path('dic_data_graph/',get_dic_data),
    path('pip_data_graph/',get_pip_data),
    path('age_graph/', get_age_group_data),
    path('attrition_trend/', get_attrition_trend),
    path('tree_map/', get_tree_map_data),
    path('left_count', get_employee_left_count),
    path('attrition_percentage', get_attrition_percentage),
    path('cohert_analytics', get_cohert_analytics_data),
    path('get_marital_data', get_marital_status_data),
    path('compensation_data/', get_annual_compensation_data),
    path('drop_down_data/', get_drop_down_data),
    path('get_rating_data/', get_rating_data),
    path('get_cohert_analytics_data/', get_cohert_analytics_data),
    path('get_tenure_data/', get_average_tenure_data),
    path('get_recruitement_data/', get_source_of_recruitement_data),
    path('get_gender_data/', get_gender_data),
    path('get_work_contract_data/', get_work_contract_data),
    path('vol_invol_data/', get_voluntary_involuntary_data),
    path('higher_qualification_data/', get_higher_qualification_data),
    path('reporting_manager_data/', get_reporting_manager_data),
    path('mcares_manager/', get_manager_mcares_data),
    path('mcares_department/', get_department_mcares_data),
    path('get_functional_competency_score/', get_functional_competency_attrition_ratio),
    path('download_csv_chart_Data/', get_chart_csv_data),
    path('download_csv_consolidate_chart_Data/', get_chart_csv_consolidate_data),
    path('get_business_service_function/', get_business_service_function),
    path('get_major_service_function/', get_major_service_function),
    path('get_contribution_data/', get_chart_csv_contribution_data),
    path('get_toggle_contribution_data/', get_toggle_contribution_data),
    path('get_role_dropdown_data/', get_role_drop_down_data)
]


