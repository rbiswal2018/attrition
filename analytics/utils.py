from analytics.query import *
from authentication.util import get_cursor
import pdb
import datetime
import calendar
import fiscalyear
import pandas as pd
import math
import pdb
from authentication.models import UserProfile, CustomPermissions, Group

monthDict = {1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Aug', 9: 'Sep', 10: 'Oct',
             11: 'Nov', 12: 'Dec'}


def where_condition(request):
    user_id = request.pop("user_id")
    role_name = UserProfile.objects.get(user_id=user_id).role
    group_id = Group.objects.get(name=role_name).id
    custom_perm_list = CustomPermissions.objects.get(group_id=group_id).permission_list
    band_drop_down = set(["OBand", "MBand", "OBand(HO)", "DH(Excluding HO)"]) & set(custom_perm_list)
    params = {}
    params["clause"] = ""
    params["start_date"] = request.pop("start_date")
    params["end_date"] = request.pop("end_date")

    for j, i in enumerate(request.keys()):
        if i == "band":
            if len(band_drop_down) > 0:
                request[i] = str(request[i]).replace('Department Head', 'DH(Excluding HO)')
            if len(set(["OBand", "MBand", "OBand(HO)", "DH(Excluding HO)"]) & set(
                    request[i][2:-2].replace("'", '').split(','))) > 0:
                continue
            elif request[i] != "":

                if j == 0:
                    params["clause"] = params["clause"] + " {} in ('{}')".format(i, str(request[i])[2:-2])
                else:
                    params["clause"] = params["clause"] + "and {} in ('{}')".format(i, str(request[i])[2:-2])
        elif request[i] != "":
            if j == 0:
                params["clause"] = params["clause"] + " {} in ('{}')".format(i, str(request[i])[2:-2])
            else:
                params["clause"] = params["clause"] + "and {} in ('{}')".format(i, str(request[i])[2:-2])

    state_drop_down = set([str(i) for i in execute_function(state_query)]) & set(custom_perm_list)

    if 'state' in request.keys():
        if len(state_drop_down) > 0 and ("Maharashtra" in state_drop_down or "Maharashtra" in request["state"]):
            params["clause"] = params["clause"] + "and {} not in ('{}')".format('branch', 'HO-HEAD OFFICE')

    if 'state' not in request.keys() and len(state_drop_down) > 0:
        params["clause"] = params["clause"] + "and {} in ('{}')".format('state', str(state_drop_down)[2:-2])
        if len(state_drop_down) > 0 and "Maharashtra" in state_drop_down:
            params["clause"] = params["clause"] + "and {} not in ('{}')".format('branch', 'HO-HEAD OFFICE')
        else:
            pass
    if 'business_unit' not in request.keys():
        business_unit_drop_down = set([str(i) for i in execute_function(business_unit_query)]) & set(custom_perm_list)
        # params["clause"] = params["clause"] + "and {} in ('{}')".format('business_unit',
        #                                                                 str(business_unit_drop_down)[2:-2])
        if len(business_unit_drop_down) > 0:
            params["clause"] = params["clause"] + "and {} in ('{}')".format('business_unit',
                                                                            str(business_unit_drop_down)[2:-2])
        else:
            pass
    if band_drop_down and 'band' in request.keys():
        band_drop_down = request['band']

    if "OBand" in band_drop_down and "MBand" in band_drop_down and "DH(Excluding HO)" in band_drop_down:
        params['clause'] = params[
                               'clause'] + "and (band in ('OPERATIONAL','MANAGERIAL') or (band in ('DEPARTMENT HEAD') and state not in ('Head Office')))"
    elif "OBand" in band_drop_down and "MBand" in band_drop_down:
        params['clause'] = params['clause'] + "and band in ('OPERATIONAL','MANAGERIAL')"
    elif "OBand" in band_drop_down and "DH(Excluding HO)" in band_drop_down:
        params['clause'] = params[
                               'clause'] + "and (band in ('OPERATIONAL') or (band in ('DEPARTMENT HEAD') and state not in ('Head Office')))"
    elif "MBand" in band_drop_down and "DH(Excluding HO)" in band_drop_down:
        params['clause'] = params[
                               'clause'] + "and (band in ('MANAGERIAL') or (band in ('DEPARTMENT HEAD') and state not in ('Head Office')))"
    elif "OBand" in band_drop_down:
        params['clause'] = params['clause'] + "and band in ('OPERATIONAL')"
    elif "MBand" in band_drop_down:
        params['clause'] = params['clause'] + "and band in ('MANAGERIAL')"
    elif "DH(Excluding HO)" in band_drop_down:
        params['clause'] = params['clause'] + "and (band in ('DEPARTMENT HEAD') and state not in ('Head Office'))"

    if params['clause'][:3] == "and":
        params['clause'] = params['clause'][3:]

    return params


def fetch_dropdown_data(request):
    try:
        role_name = UserProfile.objects.get(user_id=request.user.id).role
        group_id = Group.objects.get(name=role_name).id
        custom_perm_list = CustomPermissions.objects.get(group_id=group_id).permission_list
        temp_dict = {}
        state_drop_down =set([str(i) for i in execute_function(state_query)])&set(custom_perm_list)
        business_unit_drop_down = set([str(i) for i in execute_function(business_unit_query)])&set(custom_perm_list)
        band_drop_down = set(["OBand", "MBand", "DH(Excluding HO)"])&set(custom_perm_list)

        if len(state_drop_down)>0:
            temp_dict["state_list"] =state_drop_down
        else:
            temp_dict["state_list"]= execute_function(state_query)
        if len(business_unit_drop_down)>0:
            temp_dict["business_unit"] = business_unit_drop_down
        else:
            temp_dict["business_unit"] = execute_function(business_unit_query)

        temp_dict["branch_list"] = execute_function(branch_query)
        temp_dict["function_name"] = execute_function(function_query)
        temp_dict["division_list"] = execute_function(division_query)
        # temp_dict["business_unit"] = execute_function(business_unit_query)
        temp_dict["department"] = execute_function(department_query)
        temp_dict["sub_department"] = execute_function(sub_department_query)
        temp_dict["cost_centre"] = execute_function(cost_centre_query)
        temp_dict["work_contract"] = execute_function(work_contract_query)
        temp_band_data= execute_function(band_query)
        temp_grade_data = execute_function(grade_query)
        grade_order_list =['L10C- Operational','L10B- Operational','L10A- Operational', 'L10-Operational','L9-Operational',  'L8-Operational','L7-Operational',
                           'L7-Managerial', 'L6-Managerial', 'L5-Managerial', 'L5-Department Head','L4-Department Head',  'L3-Department Head',
                           'L3-Executive','L2-Executive','L2-Strategic','L1-Strategic' ]
        band_order_list =['OPERATIONAL','MANAGERIAL','DEPATMENT HEAD','EXECUTIVE','STRATEGIC']

        temp_dict["employee_sub_group"] =order_band_grade_data(grade_order_list,temp_grade_data)
        if len(band_drop_down)>0:
            band_drop_down = ['Department Head' if i=="DH(Excluding HO)" else i for i in band_drop_down]
            temp_dict["band"] =band_drop_down
        else:
            temp_dict["band"] = order_band_grade_data(band_order_list,temp_band_data)
        return temp_dict
    except Exception as e:
        return e


def format_query_output(query, func):
    result = []
    query_obj = get_cursor(query)
    query_data = query_obj.fetchall()
    if func == "get_manager_mcares_data":
        temp_dict_tree_map = []
    query_columns = [col[0] for col in query_obj.description]
    if func == "get_employee_left_count" or func == "get_attrition_percentage" or func == "get_average_tenure_data" or func == "get_attrition_trend":
        result.append(query_data)
    elif func == "get_work_contract_data":
        for row in query_data:
            temp_disc = dict(set(zip(query_columns, row)))
            result.append(
                {"WorkContract": str(temp_disc['name']),
                 "AttritionPercentage": str(round(temp_disc['percentage'], 2))
                 })
    elif func == "get_reporting_manager_data":
        for row in query_data:
            temp_disc = dict(set(zip(query_columns, row)))
            result.append(
                {"ManagerId": temp_disc["id"],
                 "Name": str(temp_disc['name']),
                 "AttritionPercentage": round(temp_disc["percentage"], 2),
                 "LeftCount": str(temp_disc['left_count']),
                 "CurrentlyWorking": round(temp_disc["currently_working"], 2),
                 "Vol(%)": round(temp_disc['vol_per'], 2),
                 "Invol(%)": round(temp_disc["invol_per"], 2)

                 })
    elif func == "get_grade_data":
        temp_order_dict= {}
        grade_order_list = ['L1-Strategic','L2-Strategic','L2-Executive','L3-Executive','L3-Department Head','L4-Department Head','L5-Department Head',
        'L5-Managerial','L6-Managerial','L7-Managerial','L7-Operational','L8-Operational','L9-Operational','L10-Operational',
        'L10A- Operational','L10B- Operational','L10C- Operational']
        for row in query_data:
            temp_disc = dict(set(zip(query_columns, row)))
            temp_order_dict.update({temp_disc["name"]:temp_disc["value"]})
        for key in grade_order_list:
            if key in temp_order_dict.keys():
                temp = {}
                temp[key] = temp_order_dict.pop(key)
                result.append({"name":list(temp.keys())[0],"value":round(list(temp.values())[0],2)})
        for i in temp_order_dict.keys():
            result.append({"name":i,"value":temp_order_dict[i]})

    else:
        for row in query_data:
            temp_disc = dict(set(zip(query_columns, row)))
            result.append(
                {"name": str(temp_disc['name']),
                 "value": round(temp_disc["percentage"], 2),
                 })
    return result


def tree_map_output(request):
    """
    Return tree map detail.
    :param reporting_officer_sap_code:
    :param reporting_office_name:
    :param branch:
    :param oprn_cat_tree:
    :param oprn_category:
    :param year:
    :param quarter:
    :param func:
    :param circle:
    :param divisions:
    :param area:
    :return:
    """
    try:

        # oprn_categorly_clause = " oprn_category in ('{0}') and ".format(oprn_category)
        temp_dict_tree_map = {}

        condition = None
        select = None
        tree_map_key = 'Yes'
        filter_key = ""
        params = {}
        params["start_date"] = request.get("start_date", None)
        params["end_date"] = request.get("end_date", None)
        params["where"] = where_condition(request)["clause"]
        params["select"] = "concat("
        count = 0
        length = len(request.keys())

        for i in request.keys():
            count = count + 1
            if length == 1:
                params["select"] = params["select"] + i + ")"
            else:
                if count == length:
                    params["select"] = params["select"] + "{})".format(i)
                else:
                    params["select"] = params["select"] + "{},'-->',".format(i)

        temp_dict_tree_map["performer_by_state"] = []
        final_query = tree_map_query.format(**params)
        query_obj = get_cursor(final_query)
        query_data = query_obj.fetchall()
        query_columns = [col[0] for col in query_obj.description]
        for row in query_data:
            temp_disc = dict(set(zip(query_columns, row)))
            temp_dict_tree_map["performer_by_state"].append(temp_disc)

        return temp_dict_tree_map
    except Exception as e:
        return e


def format_attrition_trend(request, func):
    result = []
    start_date = request["start_date"]
    end_date = request["end_date"]
    x_axis_key = ""
    start_date_obj = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date_obj = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    params = where_condition(request)
    if start_date_obj.year != end_date_obj.year:
        temp_start_year = start_date_obj.year
        temp_end_year = end_date_obj.year
        delta = end_date_obj - start_date_obj
        if delta.days <= 366:
            x_axis_key = "Month"
            temp_start_month = start_date_obj.month
            temp_end_month = 12
            year = start_date_obj.year
            for i in range(start_date_obj.month, 13):
                temp_dict = {}
                if temp_start_month == i:
                    params["start_date"] = start_date
                    temp_date = calendar.monthrange(start_date_obj.year, i)
                    params["end_date"] = str(year) + "-" + str(i).zfill(2) + "-" + str(temp_date[1])
                elif temp_end_month == i:
                    params["start_date"] = str(year) + "-" + str(i).zfill(2) + '-01'
                    params["end_date"] = str(year) + '-12-31'
                else:
                    params["start_date"] = str(year) + "-" + str(i).zfill(2) + "-01"
                    temp_date = calendar.monthrange(start_date_obj.year, i)
                    params["end_date"] = str(year) + "-" + str(i).zfill(2) + "-" + str(temp_date[1])
                temp_dict["name"] = monthDict[i] + "-" + str(year)
                query = attrition_percentage_query.format(**params)
                temp_dict["value"] = format_query_output(query, func)[0][0][0]
                result.append(temp_dict)
            temp_end_month = end_date_obj.month
            year = end_date_obj.year
            for i in range(1, temp_end_month + 1):
                temp_dict = {}
                if i == 1:
                    params["start_date"] = str(end_date_obj.year) + "-01-01"
                    temp_date = calendar.monthrange(end_date_obj.year, i)
                    params["end_date"] = str(year) + "-" + str(i).zfill(2) + "-" + str(temp_date[1])
                elif temp_end_month == i:
                    params["start_date"] = str(year) + "-" + str(i).zfill(2) + '-01'
                    params["end_date"] = end_date
                else:
                    params["start_date"] = str(year) + "-" + str(i).zfill(2) + "-01"
                    temp_date = calendar.monthrange(end_date_obj.year, i)
                    params["end_date"] = str(year) + "-" + str(i).zfill(2) + "-" + str(temp_date[1])
                temp_dict["name"] = monthDict[i] + "-" + str(year)
                query = attrition_percentage_query.format(**params)
                temp_dict["value"] = format_query_output(query, func)[0][0][0]
                result.append(temp_dict)
        else:
            x_axis_key = "Year"
            for i in range(start_date_obj.year, end_date_obj.year + 1):
                if temp_start_year == i:
                    temp_dict = {}
                    params["start_date"] = start_date
                    params["end_date"] = str(i) + '-12-31'
                    temp_dict["name"] = i
                    query = attrition_percentage_query.format(**params)
                    temp_dict["value"] = format_query_output(query, func)[0][0][0]
                    result.append(temp_dict)
                elif temp_end_year == i:
                    temp_dict = {}
                    params["start_date"] = str(i) + '-01-01'
                    params["end_date"] = end_date
                    temp_dict["name"] = i
                    query = attrition_percentage_query.format(**params)
                    temp_dict["value"] = format_query_output(query, func)[0][0][0]
                    result.append(temp_dict)
                else:
                    temp_dict = {}
                    params["start_date"] = str(i) + "-01-01"
                    params["end_date"] = str(i) + '-12-31'
                    temp_dict["name"] = i
                    query = attrition_percentage_query.format(**params)
                    temp_dict["value"] = format_query_output(query, func)[0][0][0]
                    result.append(temp_dict)

    elif start_date_obj.month != end_date_obj.month:
        x_axis_key = "Month"
        temp_start_month = start_date_obj.month
        temp_end_month = end_date_obj.month
        year = start_date_obj.year
        for i in range(start_date_obj.month, end_date_obj.month + 1):
            temp_dict = {}
            if temp_start_month == i:
                params["start_date"] = start_date
                temp_date = calendar.monthrange(start_date_obj.year, i)
                params["end_date"] = str(year) + "-" + str(i).zfill(2) + "-" + str(temp_date[1])
            elif temp_end_month == i:
                params["start_date"] = str(year) + "-" + str(i).zfill(2) + '-01'
                params["end_date"] = end_date
            else:
                params["start_date"] = str(year) + "-" + str(i).zfill(2) + "-01"
                temp_date = calendar.monthrange(start_date_obj.year, i)
                params["end_date"] = str(year) + "-" + str(i).zfill(2) + "-" + str(temp_date[1])
            temp_dict["name"] = monthDict[i] + "-" + str(year)
            query = attrition_percentage_query.format(**params)
            temp_dict["value"] = format_query_output(query, func)[0][0][0]
            result.append(temp_dict)
    elif start_date_obj.month == end_date_obj.month:
        x_axis_key = "Day"
        month = start_date_obj.month
        temp_start_day = start_date_obj.day
        temp_end_day = end_date_obj.day
        year = start_date_obj.year
        for i in range(start_date_obj.day, end_date_obj.day + 1):
            temp_dict = {}
            if str(month).zfill(2) == '01' and str(i).zfill(2) == '01':
                params["start_date"] = str(year - 1) + "-" + str(12) + "-" + str(31)
            elif str(i).zfill(2) == '01':
                params["start_date"] = str(year) + "-" + str(month - 1).zfill(2) + "-" + str(
                    calendar.monthrange(start_date_obj.year, month - 1)[1])
            else:
                params["start_date"] = str(year) + "-" + str(month).zfill(2) + "-" + str(i - 1).zfill(2)
            params["end_date"] = str(year) + "-" + str(month).zfill(2) + "-" + str(i).zfill(2)
            temp_dict["name"] = str(year) + "-" + str(month).zfill(2) + "-" + str(i).zfill(2)
            temp_dict["name"] = str(year) + "-" + str(month).zfill(2) + "-" + str(i).zfill(2)
            query = attrition_days_query.format(**params)
            temp_dict["value"] = format_query_output(query, func)[0][0][0]
            result.append(temp_dict)

        params["date_part"] = "TO_DATE(employment_details_last_working_date, 'DD-MM-YYYY')"
    return result, x_axis_key


def execute_function(query):
    data = get_cursor(query)
    data = data.fetchall()
    data = list(map(lambda x: x[0], data))
    return data


def get_rating_financial_years(start_date,end_date):
    fiscalyear.START_MONTH = 4
    start_date_obj = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date_obj = datetime.datetime.strptime(end_date, "%Y-%m-%d")

    start_fy_year= fiscalyear.FiscalDate(start_date_obj.year,start_date_obj.month,start_date_obj.day)
    end_fy_year = fiscalyear.FiscalDate(end_date_obj.year,end_date_obj.month,end_date_obj.day)
    financial_year = ''
    for fyYear in range(start_fy_year.fiscal_year,end_fy_year.fiscal_year+1):
        if fyYear==end_fy_year.fiscal_year:
            financial_year =financial_year+ "'" + str(fyYear) + "'"
        else:
            financial_year = financial_year+"'"+str(fyYear)+"',"
    return financial_year


def format_mcares_score_manager(request, func):
    result = []
    start_date = request["start_date"]
    end_date = request["end_date"]
    start_date_obj = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date_obj = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    params = where_condition(request)

    if start_date_obj.year != end_date_obj.year:
        if start_date_obj.month in (1, 2, 3):
            params["start_date"] = start_date
            params["end_date"] = str(start_date_obj.year) + "-03-31"
            params["year"] = start_date_obj.year 
            final_query = mcares_manager_score.format(**params)
            query_obj = get_cursor(final_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_disc = dict(set(zip(query_columns, row)))
                result.append(
                    {"ManagerId": str(temp_disc['id']),
                     "Name": str(temp_disc['name']),
                     "Year": temp_disc["year"],
                     "McaresScore": temp_disc["mcaresscore"],
                     "AttritionPercentage": round(temp_disc["percentage"], 2),
                     })
        for i in range(start_date_obj.year, end_date_obj.year + 1):
            if i != end_date_obj.year:
                params["start_date"] = str(i) + "-04-01"
                params["end_date"] = str(i + 1) + "-03-31"
                params["year"] = i+1
                final_query = mcares_manager_score.format(**params)
                query_obj = get_cursor(final_query)
                query_data = query_obj.fetchall()
                query_columns = [col[0] for col in query_obj.description]
                for row in query_data:
                    temp_disc = dict(set(zip(query_columns, row)))
                    result.append(
                        {"ManagerId": str(temp_disc['id']),
                         "Name": str(temp_disc['name']),
                         "Year": temp_disc["year"],
                         "McaresScore": temp_disc["mcaresscore"],
                         "AttritionPercentage": round(temp_disc["percentage"], 2),
                         })
            if i == end_date_obj.year:
                if end_date_obj.month in (1, 2, 3):
                    pass
                else:
                    params["start_date"] = str(end_date_obj.year) + "-04-01"
                    params["end_date"] = end_date
                    params["year"] = i+1
                    final_query = mcares_manager_score.format(**params)
                    query_obj = get_cursor(final_query)
                    query_data = query_obj.fetchall()
                    query_columns = [col[0] for col in query_obj.description]
                    for row in query_data:
                        temp_disc = dict(set(zip(query_columns, row)))
                        result.append(
                            {"ManagerId": str(temp_disc['id']),
                             "Name": str(temp_disc['name']),
                             "Year": temp_disc["year"],
                             "McaresScore": temp_disc["mcaresscore"],
                             "AttritionPercentage": round(temp_disc["percentage"], 2),
                             })
    elif start_date_obj.year == end_date_obj.year:

        if start_date_obj.month not in (1, 2, 3):
            params["start_date"] = start_date
            params["end_date"] = end_date
            params["year"] = start_date_obj.year+1
            final_query = mcares_manager_score.format(**params)
            query_obj = get_cursor(final_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_disc = dict(set(zip(query_columns, row)))
                result.append(
                    {"ManagerId": str(temp_disc['id']),
                     "Name": str(temp_disc['name']),
                     "Year": temp_disc["year"],
                     "McaresScore": temp_disc["mcaresscore"],
                     "AttritionPercentage": round(temp_disc["percentage"], 2),
                     })
        if start_date_obj.month in (1, 2, 3) and end_date_obj.month in (1, 2, 3):
            params["start_date"] = start_date
            params["end_date"] = end_date
            params["year"] = start_date_obj.year 
            final_query = mcares_manager_score.format(**params)
            query_obj = get_cursor(final_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_disc = dict(set(zip(query_columns, row)))
                result.append(
                    {"ManagerId": str(temp_disc['id']),
                     "Name": str(temp_disc['name']),
                     "Year": temp_disc["year"],
                     "McaresScore": temp_disc["mcaresscore"],
                     "AttritionPercentage": round(temp_disc["percentage"], 2),
                     })
        if start_date_obj.month in (1, 2, 3) and end_date_obj.month not in (1, 2, 3):
            params["start_date"] = start_date
            params["end_date"] = str(start_date_obj.year) + "-03-31"
            params["year"] = start_date_obj.year 
            final_query = mcares_manager_score.format(**params)
            query_obj = get_cursor(final_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_disc = dict(set(zip(query_columns, row)))
                result.append(
                    {"ManagerId": str(temp_disc['id']),
                     "Name": str(temp_disc['name']),
                     "Year": temp_disc["year"],
                     "McaresScore": temp_disc["mcaresscore"],
                     "AttritionPercentage": round(temp_disc["percentage"], 2),
                     })
            params["start_date"] = str(start_date_obj.year) + "-04-01"
            params["end_date"] = end_date
            params["year"] = start_date_obj.year+1
            final_query = mcares_manager_score.format(**params)
            query_obj = get_cursor(final_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_disc = dict(set(zip(query_columns, row)))
                result.append(
                    {"ManagerId": str(temp_disc['id']),
                     "Name": str(temp_disc['name']),
                     "Year": temp_disc["year"],
                     "McaresScore": temp_disc["mcaresscore"],
                     "AttritionPercentage": round(temp_disc["percentage"], 2),
                     })
    # return result
    # df = pd.DataFrame(result)
    temp_factor_dict = []
    if len(result) == 0:
        temp_factor_dict.append({"ManagerId": "",
                                 "Name": "",
                                 "Year": "",
                                 "McaresScore": 0.0,
                                 "AttritionPercentage": 0.0,
                                 })
    else:
        temp_factor_dict = result

    df = pd.DataFrame(temp_factor_dict)
    cor_factor = df["McaresScore"].corr(df['AttritionPercentage'].astype('float'))
    if not math.isnan(cor_factor):
        cor_factor = round(cor_factor, 2)
    else:
        cor_factor = "Not Applicable"
    return result, cor_factor


def format_mcares_score_department(request, func):
    result = []
    start_date = request["start_date"]
    end_date = request["end_date"]
    start_date_obj = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date_obj = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    params = where_condition(request)
    if start_date_obj.year != end_date_obj.year:
        if start_date_obj.month in (1, 2, 3):
            params["start_date"] = start_date
            params["end_date"] = str(start_date_obj.year) + "-03-31"
            params["year"] = start_date_obj.year 
            final_query = mcares_department_score.format(**params)
            query_obj = get_cursor(final_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_disc = dict(set(zip(query_columns, row)))
                result.append(
                    {"Department": str(temp_disc['department']),
                     "Year": temp_disc["year"],
                     "McaresScore": temp_disc["mcaresscore"],
                     "AttritionPercentage": round(temp_disc["percentage"], 2),
                     })
        for i in range(start_date_obj.year, end_date_obj.year + 1):
            if i != end_date_obj.year:
                params["start_date"] = str(i) + "-04-01"
                params["end_date"] = str(i + 1) + "-03-31"
                params["year"] = i+1
                final_query = mcares_department_score.format(**params)
                query_obj = get_cursor(final_query)
                query_data = query_obj.fetchall()
                query_columns = [col[0] for col in query_obj.description]
                for row in query_data:
                    temp_disc = dict(set(zip(query_columns, row)))
                    result.append(
                        {"Department": str(temp_disc['department']),
                         "Year": temp_disc["year"],
                         "McaresScore": temp_disc["mcaresscore"],
                         "AttritionPercentage": round(temp_disc["percentage"], 2),
                         })
            if i == end_date_obj.year:
                if end_date_obj.month in (1, 2, 3):
                    pass
                else:
                    params["start_date"] = str(end_date_obj.year) + "-04-01"
                    params["end_date"] = end_date
                    params["year"] = i+1
                    final_query = mcares_department_score.format(**params)
                    query_obj = get_cursor(final_query)
                    query_data = query_obj.fetchall()
                    query_columns = [col[0] for col in query_obj.description]
                    for row in query_data:
                        temp_disc = dict(set(zip(query_columns, row)))
                        result.append(
                            {"Department": str(temp_disc['department']),
                             "Year": temp_disc["year"],
                             "McaresScore": temp_disc["mcaresscore"],
                             "AttritionPercentage": round(temp_disc["percentage"], 2),
                             })
    elif start_date_obj.year == end_date_obj.year:

        if start_date_obj.month not in (1, 2, 3):
            params["start_date"] = start_date
            params["end_date"] = end_date
            params["year"] = start_date_obj.year+1
            final_query = mcares_department_score.format(**params)
            query_obj = get_cursor(final_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_disc = dict(set(zip(query_columns, row)))
                result.append(
                    {"Department": str(temp_disc['department']),
                     "Year": temp_disc["year"],
                     "McaresScore": temp_disc["mcaresscore"],
                     "AttritionPercentage": round(temp_disc["percentage"], 2),
                     })
        if start_date_obj.month in (1, 2, 3) and end_date_obj.month in (1, 2, 3):
            params["start_date"] = start_date
            params["end_date"] = end_date
            params["year"] = start_date_obj.year 
            final_query = mcares_department_score.format(**params)
            query_obj = get_cursor(final_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_disc = dict(set(zip(query_columns, row)))
                result.append(
                    {"Department": str(temp_disc['department']),
                     "Year": temp_disc["year"],
                     "McaresScore": temp_disc["mcaresscore"],
                     "AttritionPercentage": round(temp_disc["percentage"], 2),
                     })
        if start_date_obj.month in (1, 2, 3) and end_date_obj.month not in (1, 2, 3):
            params["start_date"] = start_date
            params["end_date"] = str(start_date_obj.year) + "-03-31"
            params["year"] = start_date_obj.year 
            final_query = mcares_department_score.format(**params)
            query_obj = get_cursor(final_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_disc = dict(set(zip(query_columns, row)))
                result.append(
                    {"Department": str(temp_disc['department']),
                     "Year": temp_disc["year"],
                     "McaresScore": temp_disc["mcaresscore"],
                     "AttritionPercentage": round(temp_disc["percentage"], 2),
                     })
            params["start_date"] = str(start_date_obj.year) + "-04-01"
            params["end_date"] = end_date
            params["year"] = start_date_obj.year+1
            final_query = mcares_department_score.format(**params)
            query_obj = get_cursor(final_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_disc = dict(set(zip(query_columns, row)))
                result.append(
                    {"Department": str(temp_disc['department']),
                     "Year": temp_disc["year"],
                     "McaresScore": temp_disc["mcaresscore"],
                     "AttritionPercentage": round(temp_disc["percentage"], 2),
                     })
    # return result
    # df = pd.DataFrame(result)
    temp_factor_dict = []
    if len(result) == 0:
        temp_factor_dict.append({"ManagerId": "",
                                 "Name": "",
                                 "Year": "",
                                 "McaresScore": 0.0,
                                 "AttritionPercentage": 0.0,
                                 })
    else:
        temp_factor_dict = result
    df = pd.DataFrame(temp_factor_dict)
    cor_factor = df["McaresScore"].corr(df['AttritionPercentage'].astype('float'))
    if not math.isnan(cor_factor):
        cor_factor = round(cor_factor, 2)
    else:
        cor_factor = "Not Applicable"
    return result, cor_factor


def format_competency_report(request, func):
    result = []
    start_date = request["start_date"]
    end_date = request["end_date"]
    start_date_obj = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end_date_obj = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    params = where_condition(request)
    params["start_date_year"] = start_date_obj.year
    params["end_date_year"] = end_date_obj.year
    if start_date_obj.month not in (1, 2, 3):
        params["start_date_year"] = start_date_obj.year +1
    if end_date_obj.month not in (1, 2, 3):
        params["end_date_year"] = end_date_obj.year +1
    params['years'] = ''

    if end_date_obj.month in (1, 2, 3):
        temp_end_date_year = end_date_obj.year
    else:
        temp_end_date_year = end_date_obj.year + 1

    for i in range(start_date_obj.year, temp_end_date_year):
        if i != temp_end_date_year - 1:
            params['years'] = params['years'] + "'" + str(i) + "'" + ","
        else:
            params['years'] = params['years'] + "'" + str(i) + "'"
    if start_date_obj.month in (1, 2, 3):
        if params['years'] != "":
            params['years'] = params['years'] + ",'" + str(start_date_obj.year - 1) + "'"
        else:
            params['years'] = params['years'] + "'" + str(start_date_obj.year - 1) + "'"

    final_query = functional_competency.format(**params)
    query_obj = get_cursor(final_query)
    query_data = query_obj.fetchall()
    query_columns = [col[0] for col in query_obj.description]
    for row in query_data:
        temp_disc = dict(set(zip(query_columns, row)))
        result.append(
            {"FunctionalCompetency": str(temp_disc['name']),
             "AttritionPercentage": temp_disc["percentage"],
             "ProficiencyLevelScore": temp_disc["avg"]
             })
    return result


def order_band_grade_data(order_list,temp_data):
    temp_order_dict = []
    for key in order_list:
        if key in temp_data:
            temp_data.remove(key)
            temp_order_dict.append(key)
    temp_order_dict.extend(temp_data)
    return temp_order_dict

def fetch_role_dropdown():
    try:
        temp_dict = {}
        temp_dict["state_list"] = execute_function(state_query)
        temp_dict["business_unit"] = execute_function(business_unit_query)
        return temp_dict
    except Exception as e:
        return e