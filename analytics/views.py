import sys
import logging
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from analytics.query import *
import io, csv
from authentication.util import get_cursor
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import pdb
from django.db import connection
from io import BytesIO

import pandas as pd

from analytics.utils import *
from authentication.decorators import role_permission
from authentication.permissions import IsAuthenticatedUser
# from carrot.utilities import publish_message

logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_tree_map_data(request):
    try:
        result = {}
        request.data['user_id'] = request.user.id
        result = tree_map_output(request.data)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
# @role_permission
@permission_classes((IsAuthenticatedUser,))
def get_source_of_recruitement_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)

        if "recruitement_data_category" in request.data.keys():
            query = source_of_recruitement_query_macro.format(**params)
        else:
            query = source_of_recruitement_query.format(**params)


        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_marital_status_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = marital_status_query.format(**params)
        # print("#########_marital_status",query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
# @role_permission
@permission_classes((IsAuthenticatedUser,))
def get_gender_data(request):
    try:
        result = {}

        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = gender_query.format(**params)
        # print("tier graph query:: ", query)
        # print("#########_get_gender_data")
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)

    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_employee_left_count(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = employee_left_query.format(**params)
        # print("#########get_employee_left")
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_attrition_percentage(request):
    try:
        result = ""
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = attrition_percentage_query.format(**params)
        # print("#########get_attrition_percentage")
        # print(query)
        result = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_average_tenure_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = average_tenure_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_attrition_trend(request):
    try:

        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        result, x_axis_key = format_attrition_trend(request.data, current_function_name)
        return Response({"message": "success", "data": result, "axis_key": x_axis_key}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_cohert_analytics_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = cohert_analytics_query.format(**params)
        # print("#########get_cohert_analytics_data")
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_age_group_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = age_group_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_grade_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = grade_performer_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_pip_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = pip_performer_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_dic_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = dic_performer_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_training_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = training_data_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_star_performer_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = star_performer_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_gem_performer_data(request):
    try:
        result = {}

        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)

        query = gem_performer_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_annual_compensation_data(request):
    try:
        result = {}

        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = annual_compensation_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"messyage": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_rating_data(request):
    try:
        result = {}
        type_rating = request.data.pop("type_rating", None)
        current_function_name = sys._getframe().f_code.co_name
        start_date = request.data['start_date']
        end_date = request.data['end_date']
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        max_year_query = """select max(year) from buisness_suite.promotion_and_rating_master;"""
        query_obj_max = get_cursor(max_year_query)
        max_year = int(query_obj_max.fetchone()[0])
        # pdb.set_trace()
        if type_rating == "average_rating":
            params["financial_year"] = get_rating_financial_years(start_date,end_date)
            if len(params["financial_year"]) == 6:
                fy_year = int(params["financial_year"].strip("'"))
                if fy_year > max_year:
                    params["financial_year"] = "'" + str(max_year) + "'"
            query = avg_rating_query.format(**params)
        else:
            # params["financial_year"] = "'" + str(max_year) + "'"
            params["financial_year"] = get_rating_financial_years(end_date, end_date)
            fy_year = int(params["financial_year"].strip("'"))
            if fy_year > max_year:
                params["financial_year"] = "'" + str(max_year) + "'"
            query = previous_year_rating.format(**params)
        # print("#########get_rating_data")
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        print(".......error",e)
        logger.error("error in emp_detail_from_manager api %s" % e)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_voluntary_involuntary_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        if "reason_of_leaving_group" in request.data.keys():
            query = voluntary_involuntary_data_macro.format(**params)
        else:
            query = voluntary_involuntary_data.format(**params)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_higher_qualification_data(request):
    try:
        result = {}

        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = higher_qualification_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_work_contract_data(request):
    try:
        result = {}

        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = work_contrat_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_reporting_manager_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        query = reporting_manager_attrition_query.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
# @role_permission
@permission_classes((IsAuthenticatedUser,))
def get_manager_mcares_data(request):
    try:
        result = {}

        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        result,cor_factor = format_mcares_score_manager(request.data, current_function_name)
        return Response({"message": "success", "data": result, "cor_factor": cor_factor}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
# @role_permission
@permission_classes((IsAuthenticatedUser,))
def get_department_mcares_data(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        result,cor_factor = format_mcares_score_department(request.data, current_function_name)
        return Response({"message": "success", "data": result, "cor_factor": cor_factor}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
# @role_permission
@permission_classes((IsAuthenticatedUser,))
def get_functional_competency_attrition_ratio(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        result = format_competency_report(request.data, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
@role_permission
@permission_classes((IsAuthenticatedUser,))
def get_drop_down_data(request):
    try:
        result = fetch_dropdown_data(request)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
# @role_permission
@permission_classes((IsAuthenticatedUser,))
def get_role_drop_down_data(request):
    try:
        result =fetch_role_dropdown()
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)


@api_view(["POST"])
@permission_classes((IsAuthenticatedUser,))
@csrf_exempt
def get_chart_csv_data(request):
    try:
        params = {}
        chart_name = request.data.pop("chart_name")
        request.data['user_id'] = request.user.id
        start_date = request.data['start_date']
        end_date = request.data['end_date']
        params = where_condition(request.data)
        params["chart_name"] = chart_name
        if chart_name=='average_rating':
            params["financial_year"] = get_rating_financial_years(start_date,end_date)

            dbQuery =category_avg_rating.format(**params)
        elif chart_name=='previous_year_rating':
            params["financial_year"] = get_rating_financial_years(end_date,end_date)

            dbQuery =category_last_year_ratig.format(**params)
        elif chart_name=='reason_of_leaving_group':
            dbQuery = vol_invol_csv_data.format(**params)
        else:
            dbQuery =csv_report_query.format(**params)

        dataFrame = pd.read_sql(dbQuery, connection);
        data = dataFrame.groupby('name')
        with BytesIO() as b:
            # Use the StringIO object as the filehandle.
            writer = pd.ExcelWriter(b, engine='xlsxwriter')
            if chart_name == 'employment_details_last_working_date':
                dataFrame.to_excel(writer, sheet_name="employee_left_details", index=False)
                writer.save()
            else:
                for row, group in data:
                    row = row.replace('#DIV/0!', 'NA')
                    # if row == '#DIV/0!':
                        # continue
                    if len(row) > 31:
                        row = row.replace(row[14:-13], '...')
                    del group['name']
                    group.to_excel(writer, sheet_name=row.replace('/', '').replace('\\', ''), index=False)
                writer.save()
            return HttpResponse(b.getvalue(), content_type='application/vnd.ms-excel')
        # response = HttpResponse(writer, content_type='text/csv')
        # response['Content-Disposition'] = 'attachment; filename=stockitems_misuper.csv'
        # return response
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes((IsAuthenticatedUser,))
@csrf_exempt
def get_chart_csv_consolidate_data(request):
    try:
        params = {}
        chart_name = request.data.pop("chart_name")
        request.data['user_id'] = request.user.id
        start_date = request.data['start_date']
        end_date = request.data['end_date']
        params = where_condition(request.data)
        params["chart_name"] = chart_name
        if chart_name=='average_rating':
            params["financial_year"] = get_rating_financial_years(start_date,end_date)
            dbQuery =consolidated_avg_rating.format(**params)
        elif chart_name=='previous_year_rating':
            params["financial_year"] = get_rating_financial_years(end_date,end_date)
            dbQuery =consolidated_avg_rating.format(**params)

        elif chart_name=='reason_of_leaving_group':
            dbQuery = consolidate_vol_invol_csv_query.format(**params)
        else:
            dbQuery = consolidate_csv_query.format(**params)
        dataFrame = pd.read_sql(dbQuery, connection);
        data = dataFrame.groupby('name')
        with BytesIO() as b:
            # Use the StringIO object as the filehandle.
            writer = pd.ExcelWriter(b, engine='xlsxwriter')
            for row, group in data:
                if len(row)>31:
                    row = row.replace(row[14:-13],'...')
                if chart_name=='reason_of_leaving_group' or chart_name=='employment_details_last_working_date':
                    if row in ['Consolidated_Close', 'Consolidated_Open']:
                        del group['category']
                else:
                    pass
                del group["name"]
                group.to_excel(writer, sheet_name=row.replace('/','').replace('\\',''),index=False)
            writer.save()
            return  HttpResponse(b.getvalue(), content_type='application/vnd.ms-excel')
        # response = HttpResponse(writer, content_type='text/csv')
        # response['Content-Disposition'] = 'attachment; filename=stockitems_misuper.csv'
        # return response
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
# @role_permission
@permission_classes((IsAuthenticatedUser,))
def get_business_service_function(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        if "business_service_function_cat" in request.data.keys():
            query = business_service_function_macro.format(**params)
        else:
            query = business_service_function.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
# @role_permission
@permission_classes((IsAuthenticatedUser,))
def get_major_service_function(request):
    try:
        result = {}
        current_function_name = sys._getframe().f_code.co_name
        request.data['user_id'] = request.user.id
        params = where_condition(request.data)
        if "major_service_function_cat" in request.data.keys():
            query = major_service_function_macro.format(**params)
        else:
            query = major_service_function.format(**params)
        # print(query)
        result["data"] = format_query_output(query, current_function_name)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes((IsAuthenticatedUser,))
@csrf_exempt
def get_chart_csv_contribution_data(request):
    try:
        params = {}
        chart_name = request.data.pop("chart_name")
        request.data['user_id'] = request.user.id
        start_date = request.data['start_date']
        end_date = request.data['end_date']
        params = where_condition(request.data)
        params["chart_name"] = chart_name
        if chart_name =='previous_year_rating':
            dbQuery =percentage_contribution_previous_year_rating.format(**params)
        elif chart_name =="average_rating":
            params["financial_year"] = get_rating_financial_years(start_date,end_date)
            dbQuery = percentage_contribution_avg_rating.format(**params)
        else:
            dbQuery = percentage_of_contribution.format(**params)
        percentage_contribution_formulas = {"total_experience_months_in_mf_range": {"Average =":"(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
                        "Number of employees left =":"Count of employees left between  start Date and end date in particular Experience category",
                        "Percentage Contribution =":"(Number of employees left/Average)*100"}
        ,"annual_compensation_range" : {"Average =":"(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
                        "Number of employees left =":"Count of employees left between  start Date and end date in particular Compensation category",
                        "Percentage Contribution =":"(Number of employees left/Average)*100"}
        ,"recruitement_data" : {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Hiring Channel category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"age_group" : {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Age category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"gem_performe" : {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Reward category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"star_performer" :{
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Reward category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"gender" :{
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Gender category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"employee_sub_group" : {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Grade category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"pip_data" : {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular PIP category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"dic_data" : {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Dis category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"marital_status" : {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular marital status category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"training_data" : {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Training category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"higher_qualification_category" : {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Qualification category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"}
        ,"business_service_function_cat" :{
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Business/Service category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"},
        "previous_year_rating":{
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular rating category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"},
        "average_rating": {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular rating category",
            "Percentage Contribution =": "(Number of employees left/Average)*100"},
        "major_service_function_cat": {
            "Average =": "(Count of active employees as on start Date+ Count of active employees as on end Date) / 2",
            "Number of employees left =": "Count of employees left between  start Date and end date in particular Major Function",
            "Percentage Contribution =": "(Number of employees left/Average)*100"},
        }
        dataFrame = pd.read_sql(dbQuery, connection);
        # data = dataFrame.groupby('category')
        with BytesIO() as b:
            # Use the StringIO object as the filehandle.
            writer = pd.ExcelWriter(b, engine='xlsxwriter')
            dataFrame.to_excel(writer, sheet_name='PercentageContribution',index=False)
            per_formula = pd.DataFrame(data=percentage_contribution_formulas[chart_name], index=[0])
            per_formula.to_excel(writer, sheet_name='PercentageContributionFormula',index=False)
            writer.save()
            return  HttpResponse(b.getvalue(), content_type='application/vnd.ms-excel')
        # response = HttpResponse(writer, content_type='text/csv')
        # response['Content-Disposition'] = 'attachment; filename=stockitems_misuper.csv'
        # return response
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(["POST"])
@permission_classes((IsAuthenticatedUser,))
def get_toggle_contribution_data(request):
    try:
        params = {}
        chart_name = request.data.pop("chart_name")
        request.data['user_id'] = request.user.id
        start_date = request.data['start_date']
        end_date = request.data['end_date']
        params = where_condition(request.data)
        params["chart_name"] = chart_name
        if chart_name == 'previous_year_rating':
            dbQuery = percentage_contribution_previous_year_rating.format(**params)
        elif chart_name == "average_rating":
            params["financial_year"] = get_rating_financial_years(start_date, end_date)
            dbQuery = percentage_contribution_avg_rating.format(**params)
        else:
            dbQuery = percentage_of_contribution.format(**params)
        dataFrame = pd.read_sql(dbQuery, connection);
        dataFrame.rename(columns={'category': 'name'}, inplace=True)
        dataFrame.rename(columns={'percentage': 'value'}, inplace=True)
        # data = dataFrame.groupby('category')
        return Response({"message": "success", "data": dataFrame.T.to_dict().values()}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)
