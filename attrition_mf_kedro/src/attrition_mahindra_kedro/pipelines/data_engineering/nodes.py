# Copyright 2020 QuantumBlack Visual Analytics Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
# NONINFRINGEMENT. IN NO EVENT WILL THE LICENSOR OR OTHER CONTRIBUTORS
# BE LIABLE FOR ANY CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF, OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# The QuantumBlack Visual Analytics Limited ("QuantumBlack") name and logo
# (either separately or in combination, "QuantumBlack Trademarks") are
# trademarks of QuantumBlack. The License does not grant you any right or
# license to the QuantumBlack Trademarks. You may not use the QuantumBlack
# Trademarks or any confusingly similar mark as a trademark for your product,
#     or use the QuantumBlack Trademarks in any other manner that might cause
# confusion in the marketplace, including but not limited to in advertising,
# on websites, or on software.
#
# See the License for the specific language governing permissions and
# limitations under the License.
"""Example code for the nodes in the example pipeline. This code is meant
just for illustrating basic Kedro features.

PLEASE DELETE THIS FILE ONCE YOU START WORKING ON YOUR OWN PROJECT!
"""

from typing import Any, Dict
import pickle
from collections import Counter
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from datetime import datetime 

def str2percent(x):
    if type(x) is str:
        y = float(x.strip('%'))/100
    else:
        y = x
    return y
def select_col_data(data):
    req_data = data[['ï»¿"employee_id"', 'first_name', 'last_name', 'state', 'branch', 'division',
       'function_name', 'business_unit', 'department', 'sub_department','cost_centre', 'band', 'work_contract', 'employee_status',
       'employment_details_date_of_hire','employment_details_last_working_date', 'left_quarter_year',
       'left_quarter', 'last_working_date_year', 'total_experience_year_in_mf',
       'reporting_officer_total_experience_year_in_mf', 'age',
       'reporting_office_age', 'emp_gender', 'emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4',
       'repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3','repo_lang_known4', 'emp_location', 'emp_marital_status',
       'emp_highest_qualification', 'repo_highest_qualification', 'repo_location', 'grade_designation', 'repo_officer_fist_name',
       'repo_officer_last_name', 'reporting_officer_gender','reporting_officer_marital_status', 'manager_net_mcares_score', 'gem_prev_to_prev_quarter',
       'gem_prev_to_prev_prev_quarter','star_prev_to_prev_quarter', 'star_prev_to_prev_prev_quarter',
       'training_prev_to_prev_quarter','training_prev_to_prev_rev_quarter', 'dic_prev_to_prev_quarter',
       'monthly_compensation_gross_prev_to_prev_quarter','fixed_component_prev_to_prev_quarter',
       'annual_hike_percentage_prev_to_prev_quarter','no_of_days_prev_to_prev_prev_quarter','no_of_days_prev_to_prev_quarter',
       'pip_prev_to_prev_quarter', 'promotion_count_year']]

    req_data.columns = ['employee_id', 'first_name', 'last_name', 'state', 'branch', 'division',
       'function_name', 'business_unit', 'department', 'sub_department',
       'cost_centre', 'band', 'work_contract', 'employee_status',
       'employment_details_date_of_hire',
       'employment_details_last_working_date', 'left_quarter_year',
       'left_quarter', 'last_working_date_year', 'total_experience_year_in_mf',
       'reporting_officer_total_experience_year_in_mf', 'age',
       'reporting_office_age', 'emp_gender', 'emp_lang_known1',
       'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4',
       'repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3',
       'repo_lang_known4', 'emp_location', 'emp_marital_status',
       'emp_highest_qualification', 'repo_highest_qualification',
       'repo_location', 'grade_designation', 'repo_officer_fist_name',
       'repo_officer_last_name', 'reporting_officer_gender',
       'reporting_officer_marital_status', 'manager_net_mcares_score',
       'gem_previous_quarter', 'gem_prev_to_prev_quarter',
       'star_previous_quarter', 'star_prev_to_prev_quarter',
       'training_previous_quarter', 'training_prev_to_prev_quarter',
       'dic_previous_quarter','monthly_compensation_gross_previous_quarter',
       'fixed_component_previous_quarter',
       'annual_hike_percentage_previous_quarter',
       'no_of_days_prev_to_prev_quarter',
       'no_of_days_previous_quarter', 'pip_previous_quarter',
       'promotion_count_year']
    req_data['employee_status']=req_data.last_working_date_year.apply(lambda x:'Active' if pd.isna(x) else 'Separated')
    return req_data

def data_manip_simul_train(data):
    try:
        comp_cols = ['desig_name', 'reporting_office_name','divisions', 'circle', 'area', 'branch_name','emp_lang_known1','emp_lang_known2','emp_lang_known3',
                     'emp_lang_known4','repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4',
                     'state','grade_name','emp_location','repo_location','emp_highest_qualification','repo_highest_qualification','repo_department']
        under_grad = dict.fromkeys(list(map(str.capitalize,['Diploma in business administration','<xth','XIIth Standard / PUC','Puc','Xth Standard','ITI','< Xth','Diploma','Diploma in Computer Application','Diploma in Mechanical Engineering','Diploma in Electrical Engineering','Advance Diploma'])), 'under_grad')
        grad = dict.fromkeys(list(map(str.capitalize,['Hotel management','Mathematics','Ca (inter)','Business management','Cpa','C.i.a. - internal audit','Ba,bl','B.m.s.','B.arch.','B.SC','B.com','B.Com','BA','BA, BL', 'BIT' ,'BL','B.Pharm','Others','Armed Forces Services', 'B.A.','B.B.M', 'LLB','B.B.A','B.C.A', 'B.Ed.', 'B.P.E.','BCS','B.Tech','B.Tech.','Bachelor of Journalism','Bachelor of Social Works', 'BE', 'Graduate','BL','B.Sc. (Tech)', 'BHM','BA, BL' 'B.Pharm','Bachelor of Social, Legal Sciences','BDS'])),'grad')
        post_grad = dict.fromkeys(list(map(str.capitalize,['M.a.m.','M.p.m.','Pgpm','Pgdpe','Pg programme in media management','Mhrm','M.phil.','M.tech.','MA','MMM','MBA','MSW','MFM','PGDBA','LLM','Post Diploma', 'Executive Diploma in HR Management', 'ME', 'MCA','PG Diploma', 'M.Sc.', 'M.Com.', 'MscIT','ML','Post Graduate', 'PGDM','PGDHRM', 'PGDBM', 'ML','M.Ed.'])),'post_grad')
        
        f_data = dict(data[['employee_id', 'state', 'branch', 'division','function_name', 'business_unit', 'department', 'sub_department',
       'cost_centre', 'band','emp_location','repo_location', 'work_contract', 'total_experience_year_in_mf','reporting_officer_total_experience_year_in_mf', 
       'age', 'reporting_office_age','emp_gender', 'emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3',
       'emp_lang_known4', 'repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4',
       'emp_marital_status', 'emp_highest_qualification','repo_highest_qualification', 'manager_net_mcares_score', 
        'gem_previous_quarter','gem_prev_to_prev_quarter', 'star_previous_quarter','star_prev_to_prev_quarter', 'training_previous_quarter',
       'training_prev_to_prev_quarter','dic_previous_quarter', 'monthly_compensation_gross_previous_quarter','fixed_component_previous_quarter',
       'annual_hike_percentage_previous_quarter','no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter',
        'pip_previous_quarter', 'promotion_count_year','employee_status']])
        
        
        f_data['emp_qualif_cat'] = f_data['emp_highest_qualification'].apply(lambda x: x.capitalize() if not pd.isna(x) else x)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(under_grad)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(grad)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(post_grad)
        f_data['repo_qualif_cat'] = f_data['repo_highest_qualification'].apply(lambda x: x.capitalize() if not pd.isna(x) else x)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(under_grad)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(grad)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(post_grad)
        cleaned_df = pd.DataFrame(f_data)
        binInterval = [0, 24, 48, 108]
        binLabels   = ['<=2years','<=4years','<=9years']
        cleaned_df['promotion_category'] = pd.cut(cleaned_df['promotion_count_year'],bins=binInterval,labels=binLabels)
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].cat.add_categories('NA')
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].fillna('NA')
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].astype(str)
#         cleaned_df['emp_qualif_cat']=cleaned_df.apply(
#         lambda row: 'under_grad' if pd.isna(row['emp_qualif_cat']) and row['grade_name'] =="L10C- Operational" else row['emp_qualif_cat'],axis=1 )
        cleaned_df['emp_qualif_cat'].fillna(cleaned_df['emp_qualif_cat'].value_counts().index[0],inplace=True)
        cleaned_df['repo_qualif_cat'].fillna(cleaned_df['repo_qualif_cat'].value_counts().index[0],inplace=True)
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Divorcee','Single')
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Widower','Single')
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Widow','Single')
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Separated','Single')
        cleaned_df.loc[:,'employee_status'] = cleaned_df['employee_status'].str.replace('Suspended','Active')
        cleaned_df.loc[:,'employee_status'] = cleaned_df['employee_status'].str.replace('Unpaid Leave','Active')
        cleaned_df['location_emp_rep'] = np.where(cleaned_df['emp_location'].str.lower() == cleaned_df['repo_location'].str.lower(), 'same', 'different') 
        
        cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']] = cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']].replace(r'^\s*$', np.nan, regex=True)
        cleaned_df['emp_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']].values]
        cleaned_df[['repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4']] = cleaned_df[['repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4']].replace(r'^\s*$', np.nan, regex=True)
        cleaned_df['emp_lang_count'] = cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']].count(axis=1)
        cleaned_df['repo_lang_count'] = cleaned_df[['repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4']].count(axis=1)
        cleaned_df['repo_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in cleaned_df[['repo_lang_known1','repo_lang_known2','repo_lang_known3','repo_lang_known4']].values]
        cleaned_df['emp_lang_count'] = cleaned_df['emp_lang_count'].replace(0,1)
        cleaned_df['repo_lang_count'] = cleaned_df['repo_lang_count'].replace(0,1)
        cleaned_df['training_previous_quarter'] = cleaned_df['training_previous_quarter'].apply(lambda x:1 if x >= 1 else 0)
        cleaned_df['training_previous_quarter'].fillna(cleaned_df['training_previous_quarter'].value_counts().index[0],inplace=True)
        cleaned_df['training_prev_to_prev_quarter'] = cleaned_df['training_prev_to_prev_quarter'].apply(lambda x:1 if x >= 1 else 0)
        cleaned_df['training_prev_to_prev_quarter'].fillna(cleaned_df['training_prev_to_prev_quarter'].value_counts().index[0],inplace=True)
        
        cleaned_df.update(cleaned_df[['gem_previous_quarter','gem_prev_to_prev_quarter', 'star_previous_quarter','star_prev_to_prev_quarter', 'training_previous_quarter',
       'dic_previous_quarter','annual_hike_percentage_previous_quarter',
       'no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter','pip_previous_quarter']].fillna(0))
        cleaned_df['gem_prev_to_prev_quarter'] = cleaned_df['gem_prev_to_prev_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['star_prev_to_prev_quarter'] = cleaned_df['star_prev_to_prev_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['gem_previous_quarter'] = cleaned_df['gem_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['star_previous_quarter'] = cleaned_df['star_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['dic_previous_quarter'] = cleaned_df['dic_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['pip_previous_quarter'] = cleaned_df['pip_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['reporting_officer_total_experience_year_in_mf'] = cleaned_df['reporting_officer_total_experience_year_in_mf'].fillna(cleaned_df['reporting_officer_total_experience_year_in_mf'].mean())
        cleaned_df['manager_net_mcares_score'] = cleaned_df['manager_net_mcares_score'].fillna(cleaned_df['manager_net_mcares_score'].mean())
        cleaned_df['reporting_office_age'] = cleaned_df['reporting_office_age'].fillna(cleaned_df['reporting_office_age'].mean())
        cleaned_df['monthly_compensation_gross_previous_quarter'] = cleaned_df['monthly_compensation_gross_previous_quarter'].fillna(cleaned_df['monthly_compensation_gross_previous_quarter'].median())
        cleaned_df['annual_hike_percentage_previous_quarter'] = cleaned_df['annual_hike_percentage_previous_quarter'].apply(str2percent)
        cleaned_df['fixed_component_previous_quarter'] = cleaned_df['fixed_component_previous_quarter'].fillna(cleaned_df['fixed_component_previous_quarter'].median())
        cleaned_df['annual_hike_percentage_previous_quarter'] = cleaned_df['annual_hike_percentage_previous_quarter'].fillna(cleaned_df['annual_hike_percentage_previous_quarter'].median())
#         print (cleaned_df[['gem_previous_quarter','star_previous_quarter','pip_previous_quarter']])
        cat_cols = [ col for col in cleaned_df.columns if cleaned_df[col].dtypes == 'object' and col not in comp_cols ]
        cat_cols_df =  pd.get_dummies(cleaned_df[cat_cols])#,drop_first=True)
        cleaned_df.drop(labels=['emp_highest_qualification','repo_highest_qualification'],axis=1,inplace=True)
        final_data = pd.concat([cleaned_df,cat_cols_df],axis=1)
        cols_by_oprn = ['employee_id', 'band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL','band_OPERATIONAL', 'band_STRATEGIC', 
        'total_experience_year_in_mf','reporting_officer_total_experience_year_in_mf','manager_net_mcares_score', 
       'age', 'reporting_office_age','gem_prev_to_prev_quarter','gem_previous_quarter','star_prev_to_prev_quarter',\
        'star_previous_quarter','monthly_compensation_gross_previous_quarter','fixed_component_previous_quarter',\
        'annual_hike_percentage_previous_quarter','no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter',\
        'emp_lang_count', 'repo_lang_count','emp_marital_status_Married','emp_marital_status_Single', 'emp_gender_Female','emp_gender_Male',\
        'emp_qualif_cat_grad','emp_qualif_cat_post_grad','emp_qualif_cat_under_grad','repo_qualif_cat_grad', 'repo_qualif_cat_post_grad','repo_qualif_cat_under_grad',\
        'training_prev_to_prev_quarter','training_previous_quarter','dic_previous_quarter',\
        'pip_previous_quarter','location_emp_rep_same','location_emp_rep_different','promotion_category_<=2years', 'promotion_category_<=4years', 'promotion_category_<=9years',
       'promotion_category_NA','employee_status']
            
        clean_data = final_data[cols_by_oprn]
        # clean_data.to_csv()
        X =  clean_data.drop(labels=['employee_status','employee_id'],axis=1)
        y = clean_data['employee_status']
        X_train,X_test,y_train,y_test = train_test_split(X,y,random_state=101)
        
        return dict(
        train_x=X_train,
        train_y=y_train,
        test_x=X_test,
        test_y=y_test,)
    except Exception as e:
        print (e)


