# Copyright 2020 QuantumBlack Visual Analytics Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
# NONINFRINGEMENT. IN NO EVENT WILL THE LICENSOR OR OTHER CONTRIBUTORS
# BE LIABLE FOR ANY CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF, OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# The QuantumBlack Visual Analytics Limited ("QuantumBlack") name and logo
# (either separately or in combination, "QuantumBlack Trademarks") are
# trademarks of QuantumBlack. The License does not grant you any right or
# license to the QuantumBlack Trademarks. You may not use the QuantumBlack
# Trademarks or any confusingly similar mark as a trademark for your product,
#     or use the QuantumBlack Trademarks in any other manner that might cause
# confusion in the marketplace, including but not limited to in advertising,
# on websites, or on software.
#
# See the License for the specific language governing permissions and
# limitations under the License.

"""Example code for the nodes in the example pipeline. This code is meant
just for illustrating basic Kedro features.

Delete this when you start working on your own Kedro project.
"""

from kedro.pipeline import Pipeline, node

from .nodes import predict, train_mf_model, model_health,demograph_perf,survival_model,attrition_predictions


def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                train_mf_model,
                ['X_train','y_train'],
                'final_rf_model'
            ),
            node(
                predict,
                ['final_rf_model','X_test'],
                'predictions',
            ),
            # node(report_accuracy, ["predictions", "y_test"], None),
            node(
                model_health,
                ["final_rf_model","X_test", "y_test","X_train", "y_train"],
                 "model_health_save"
            ),
            node(
                demograph_perf,
                ["example_mf_data","final_rf_model"],
                ["treemap_save",'attrition_model_save'],
            ),
            node(
                survival_model,
                ["example_mf_data"],
                "survival_model_save",
            ),
            node(
                attrition_predictions,
                ["example_mf_data","final_rf_model"],
                'attrition_forecast_save',
            ),
            # node(
            #     train_model,
            #     ["example_train_x", "example_train_y", "parameters"],
            #     "example_model",
            # ),
            # node(
            #     predict,
            #     dict(model="example_model", test_x="example_test_x"),
            #     "example_predictions",
            # ),
            # node(report_accuracy, ["example_predictions", "example_test_y"], None),
        ]
    )
