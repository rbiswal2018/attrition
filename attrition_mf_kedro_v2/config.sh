#!/usr/bin/env bash
# result of ‘which conda’ ending in “anaconda3”

export MLFLOW_CONDA_HOME="/Users/baladitya_panda/AppData/Local/Continuum/anaconda3"
# result of ‘pwd’ in project root plus “mlruns”
export MLFLOW_TRACKING_URI="/Users/baladitya_panda/Project/attrition_mf_kedro/mlruns"
# export MLFLOW_TRACKING_URI="http://localhost:5000"
export MLFLOW_EXPERIMENT_NAME="kedro_env"
 