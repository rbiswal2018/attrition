# Copyright 2020 QuantumBlack Visual Analytics Limited
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
# NONINFRINGEMENT. IN NO EVENT WILL THE LICENSOR OR OTHER CONTRIBUTORS
# BE LIABLE FOR ANY CLAIM, DAMAGES, OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF, OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# The QuantumBlack Visual Analytics Limited ("QuantumBlack") name and logo
# (either separately or in combination, "QuantumBlack Trademarks") are
# trademarks of QuantumBlack. The License does not grant you any right or
# license to the QuantumBlack Trademarks. You may not use the QuantumBlack
# Trademarks or any confusingly similar mark as a trademark for your product,
#     or use the QuantumBlack Trademarks in any other manner that might cause
# confusion in the marketplace, including but not limited to in advertising,
# on websites, or on software.
#
# See the License for the specific language governing permissions and
# limitations under the License.

"""Example code for the nodes in the example pipeline. This code is meant
just for illustrating basic Kedro features.

Delete this when you start working on your own Kedro project.
"""
# pylint: disable=invalid-name

import logging
from typing import Any, Dict

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from imblearn.over_sampling import RandomOverSampler,SMOTE
import os
import mlflow
from mlflow import sklearn
from datetime import datetime
from sklearn.metrics import confusion_matrix,roc_auc_score, accuracy_score,recall_score,precision_score,\
f1_score,classification_report,roc_curve, auc
from sklearn import metrics
from datetime import datetime
from kedro.extras.datasets.pandas import CSVDataSet
from lifelines import CoxPHFitter,AalenAdditiveFitter

def train_mf_model(X_train,y_train):
    print(X_train.isnull().sum())
    ros = RandomOverSampler(random_state=42)
    X_res, y_res = ros.fit_resample(X_train,y_train)
    # mlflow.log_artifact(local_path=os.path.join("data", "01_raw", "data_to_predict_q1.csv"))
    params = {'n_estimators':8,'max_depth': 18, 'max_features': 'sqrt', 'min_samples_split': 8}
    rfr = RandomForestClassifier(**params)
    rfr.fit(X_res, y_res)
    # mlflow.log_params(params)
    # mlflow.sklearn.save_model(rfr, "model_"+datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M'))
    
    return rfr
    

def predict(model, test_x):
    """Node for making predictions given a pre-trained model and a test set."""
    predictions = model.predict(test_x)
    return predictions


def model_health(model,X_test, y_test,X_train, y_train):
    try:
        output_dict = dict()
        all_quarters = []
        y_train_pred = model.predict(X_train)
        y_test_pred = model.predict(X_test)
        
        output_dict['training_rows'] = X_train.shape[0]
        output_dict['testing_rows'] = X_test.shape[0]
        output_dict['train_accuracy'] = round(accuracy_score(y_train,y_train_pred),4)
        output_dict['test_accuracy'] = round(accuracy_score(y_test,y_test_pred),4)
        output_dict['train_precision'] = round(precision_score(y_train,y_train_pred,average='macro'),4)
        output_dict['test_precision'] = round(precision_score(y_test,y_test_pred,average='macro'),4)
        output_dict['train_recall'] = round(recall_score(y_train,y_train_pred,average='macro'),4)
        output_dict['test_recall'] = round(recall_score(y_test,y_test_pred,average='macro'),4)
        output_dict['train_f1'] = round(f1_score(y_train,y_train_pred,average='macro'),4)
        output_dict['test_f1'] = round(f1_score(y_test,y_test_pred,average='macro'),4)
        output_dict['auc_test'] = round(recall_score(y_test,y_test_pred,average='macro'),4)
        output_dict['auc_train'] = round(recall_score(y_train,y_train_pred,average='macro'),4)
        # log = logging.getLogger(__name__)
        # log.info("Model accuracy on test set: {0:.2f}%".format(output_dict['test_accuracy'] * 100))
        # mlflow.log_param("time of prediction", str(datetime.now()))
        # mlflow.set_tag("Attrition", 2020)
        # for i in output_dict:
        #     mlflow.log_metric(i, output_dict[i])
    except Exception as e:
        print (e) 
        output_dict = dict()

    return output_dict 

    
def str2percent(x):
    if type(x) is str:
        y = float(x.strip('%'))/100
    else:
        y = x
    return y
    
def data_manip_simul(data):
    try:
        comp_cols = ['desig_name', 'reporting_office_name','divisions', 'circle', 'area', 'branch_name','emp_lang_known1','emp_lang_known2','emp_lang_known3',
                     'emp_lang_known4','repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4',
                     'state','grade_name','emp_location','repo_location','emp_highest_qualification','repo_highest_qualification','repo_department']
        under_grad = dict.fromkeys(list(map(str.capitalize,['Diploma in business administration','<xth','XIIth Standard / PUC','Puc','Xth Standard','ITI','< Xth','Diploma','Diploma in Computer Application','Diploma in Mechanical Engineering','Diploma in Electrical Engineering','Advance Diploma'])), 'under_grad')
        grad = dict.fromkeys(list(map(str.capitalize,['Hotel management','Mathematics','Ca (inter)','Business management','Cpa','C.i.a. - internal audit','Ba,bl','B.m.s.','B.arch.','B.SC','B.com','B.Com','BA','BA, BL', 'BIT' ,'BL','B.Pharm','Others','Armed Forces Services', 'B.A.','B.B.M', 'LLB','B.B.A','B.C.A', 'B.Ed.', 'B.P.E.','BCS','B.Tech','B.Tech.','Bachelor of Journalism','Bachelor of Social Works', 'BE', 'Graduate','BL','B.Sc. (Tech)', 'BHM','BA, BL' 'B.Pharm','Bachelor of Social, Legal Sciences','BDS'])),'grad')
        post_grad = dict.fromkeys(list(map(str.capitalize,['M.a.m.','M.p.m.','Pgpm','Pgdpe','Pg programme in media management','Mhrm','M.phil.','M.tech.','MA','MMM','MBA','MSW','MFM','PGDBA','LLM','Post Diploma', 'Executive Diploma in HR Management', 'ME', 'MCA','PG Diploma', 'M.Sc.', 'M.Com.', 'MscIT','ML','Post Graduate', 'PGDM','PGDHRM', 'PGDBM', 'ML','M.Ed.'])),'post_grad')
        # data['employee_id'] = data['ï»¿"employee_id"']
        f_data = dict(data[['employee_id', 'state', 'branch', 'division','function_name', 'business_unit', 'department', 'sub_department',
       'cost_centre', 'band','emp_location','repo_location', 'work_contract', 'total_experience_year_in_mf','reporting_officer_total_experience_year_in_mf', 
       'age', 'reporting_office_age','emp_gender', 'emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3',
       'emp_lang_known4', 'repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4',
       'emp_marital_status', 'emp_highest_qualification','repo_highest_qualification', 'manager_net_mcares_score', 
        'gem_previous_quarter','gem_prev_to_prev_quarter', 'star_previous_quarter','star_prev_to_prev_quarter', 'training_previous_quarter',
       'training_prev_to_prev_quarter','dic_previous_quarter', 'monthly_compensation_gross_previous_quarter','fixed_component_previous_quarter',
       'annual_hike_percentage_previous_quarter','no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter',
        'pip_previous_quarter', 'promotion_count_year','employee_status']])
        
        
        f_data['emp_qualif_cat'] = f_data['emp_highest_qualification'].apply(lambda x: x.capitalize() if not pd.isna(x) else x)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(under_grad)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(grad)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(post_grad)
        f_data['repo_qualif_cat'] = f_data['repo_highest_qualification'].apply(lambda x: x.capitalize() if not pd.isna(x) else x)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(under_grad)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(grad)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(post_grad)
        cleaned_df = pd.DataFrame(f_data)
        binInterval = [0, 24, 48, 108]
        binLabels   = ['<=2years','<=4years','<=9years']
        cleaned_df['promotion_category'] = pd.cut(cleaned_df['promotion_count_year'],bins=binInterval,labels=binLabels)
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].cat.add_categories('NA')
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].fillna('NA')
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].astype(str)
#         cleaned_df['emp_qualif_cat']=cleaned_df.apply(
#         lambda row: 'under_grad' if pd.isna(row['emp_qualif_cat']) and row['grade_name'] =="L10C- Operational" else row['emp_qualif_cat'],axis=1 )
        cleaned_df['emp_qualif_cat'].fillna(cleaned_df['emp_qualif_cat'].value_counts().index[0],inplace=True)
        cleaned_df['repo_qualif_cat'].fillna(cleaned_df['repo_qualif_cat'].value_counts().index[0],inplace=True)
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Divorcee','Single')
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Widower','Single')
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Widow','Single')
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Separated','Single')
        cleaned_df.loc[:,'employee_status'] = cleaned_df['employee_status'].str.replace('Suspended','Active')
        cleaned_df.loc[:,'employee_status'] = cleaned_df['employee_status'].str.replace('Unpaid Leave','Active')
        cleaned_df['location_emp_rep'] = np.where(cleaned_df['emp_location'].str.lower() == cleaned_df['repo_location'].str.lower(), 'same', 'different') 
        
        cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']] = cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']].replace(r'^\s*$', np.nan, regex=True)
        cleaned_df['emp_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']].values]
        cleaned_df[['repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4']] = cleaned_df[['repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4']].replace(r'^\s*$', np.nan, regex=True)
        cleaned_df['emp_lang_count'] = cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']].count(axis=1)
        cleaned_df['repo_lang_count'] = cleaned_df[['repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4']].count(axis=1)
        cleaned_df['repo_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in cleaned_df[['repo_lang_known1','repo_lang_known2','repo_lang_known3','repo_lang_known4']].values]
        cleaned_df['emp_lang_count'] = cleaned_df['emp_lang_count'].replace(0,1)
        cleaned_df['repo_lang_count'] = cleaned_df['repo_lang_count'].replace(0,1)
        cleaned_df['training_previous_quarter'] = cleaned_df['training_previous_quarter'].apply(lambda x:1 if x >= 1 else 0)
        cleaned_df['training_previous_quarter'].fillna(cleaned_df['training_previous_quarter'].value_counts().index[0],inplace=True)
        cleaned_df['training_prev_to_prev_quarter'] = cleaned_df['training_prev_to_prev_quarter'].apply(lambda x:1 if x >= 1 else 0)
        cleaned_df['training_prev_to_prev_quarter'].fillna(cleaned_df['training_prev_to_prev_quarter'].value_counts().index[0],inplace=True)
        cleaned_df.update(cleaned_df[['gem_previous_quarter','gem_prev_to_prev_quarter', 'star_previous_quarter','star_prev_to_prev_quarter', 'training_previous_quarter',
       'training_prev_to_prev_quarter','dic_previous_quarter','annual_hike_percentage_previous_quarter',
       'no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter','pip_previous_quarter']].fillna(0))
        cleaned_df['gem_prev_to_prev_quarter'] = cleaned_df['gem_prev_to_prev_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['star_prev_to_prev_quarter'] = cleaned_df['star_prev_to_prev_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['gem_previous_quarter'] = cleaned_df['gem_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['star_previous_quarter'] = cleaned_df['star_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['dic_previous_quarter'] = cleaned_df['dic_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['pip_previous_quarter'] = cleaned_df['pip_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['reporting_officer_total_experience_year_in_mf'] = cleaned_df['reporting_officer_total_experience_year_in_mf'].fillna(cleaned_df['reporting_officer_total_experience_year_in_mf'].mean())
        if cleaned_df['manager_net_mcares_score'].isnull().all():
            cleaned_df['manager_net_mcares_score'] = cleaned_df['manager_net_mcares_score'].fillna(0)
        else:
            cleaned_df['manager_net_mcares_score'] = cleaned_df['manager_net_mcares_score'].fillna(cleaned_df['manager_net_mcares_score'].mean())
        cleaned_df['reporting_office_age'] = cleaned_df['reporting_office_age'].fillna(cleaned_df['reporting_office_age'].mean())
        cleaned_df['monthly_compensation_gross_previous_quarter'] = cleaned_df['monthly_compensation_gross_previous_quarter'].fillna(cleaned_df['monthly_compensation_gross_previous_quarter'].median())
        cleaned_df['annual_hike_percentage_previous_quarter'] = cleaned_df['annual_hike_percentage_previous_quarter'].apply(str2percent)
        cleaned_df['fixed_component_previous_quarter'] = cleaned_df['fixed_component_previous_quarter'].fillna(cleaned_df['fixed_component_previous_quarter'].median())
        cleaned_df['annual_hike_percentage_previous_quarter'] = cleaned_df['annual_hike_percentage_previous_quarter'].fillna(cleaned_df['annual_hike_percentage_previous_quarter'].median())
#         print (cleaned_df[['gem_previous_quarter','star_previous_quarter','pip_previous_quarter']])
        cat_cols = [ col for col in cleaned_df.columns if cleaned_df[col].dtypes == 'object' and col not in comp_cols ]
        cat_cols_df =  pd.get_dummies(cleaned_df[cat_cols])#,drop_first=True)
        cleaned_df.drop(labels=['emp_highest_qualification','repo_highest_qualification'],axis=1,inplace=True)
        final_data = pd.concat([cleaned_df,cat_cols_df],axis=1)
        cols_by_oprn = ['employee_id', 'band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL','band_OPERATIONAL', 'band_STRATEGIC', 
        'total_experience_year_in_mf','reporting_officer_total_experience_year_in_mf','manager_net_mcares_score', 
       'age', 'reporting_office_age','gem_prev_to_prev_quarter','gem_previous_quarter','star_prev_to_prev_quarter',\
        'star_previous_quarter','monthly_compensation_gross_previous_quarter','fixed_component_previous_quarter',\
        'annual_hike_percentage_previous_quarter','no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter',\
        'emp_lang_count', 'repo_lang_count','emp_marital_status_Married','emp_marital_status_Single', 'emp_gender_Female','emp_gender_Male',\
        'emp_qualif_cat_grad','emp_qualif_cat_post_grad','emp_qualif_cat_under_grad','repo_qualif_cat_grad', 'repo_qualif_cat_post_grad','repo_qualif_cat_under_grad',\
        'training_prev_to_prev_quarter','training_previous_quarter','dic_previous_quarter',\
        'pip_previous_quarter','location_emp_rep_same','location_emp_rep_different','promotion_category_<=2years', 'promotion_category_<=4years', 'promotion_category_<=9years',
       'promotion_category_NA','employee_status']
            
        final_data = final_data[cols_by_oprn]
        return final_data.drop(['employee_status','employee_id'],axis=1)
    except Exception as e:
        print (e)

def data_manip_simul_surv(data):
    try:
        comp_cols = ['desig_name', 'reporting_office_name','divisions', 'circle', 'area', 'branch_name','emp_lang_known1','emp_lang_known2','emp_lang_known3',
                     'emp_lang_known4','repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4',
                     'state','grade_name','emp_location','repo_location','emp_highest_qualification','repo_highest_qualification','repo_department']
        under_grad = dict.fromkeys(list(map(str.capitalize,['Diploma in business administration','<xth','XIIth Standard / PUC','Puc','Xth Standard','ITI','< Xth','Diploma','Diploma in Computer Application','Diploma in Mechanical Engineering','Diploma in Electrical Engineering','Advance Diploma'])), 'under_grad')
        grad = dict.fromkeys(list(map(str.capitalize,['Hotel management','Mathematics','Ca (inter)','Business management','Cpa','C.i.a. - internal audit','Ba,bl','B.m.s.','B.arch.','B.SC','B.com','B.Com','BA','BA, BL', 'BIT' ,'BL','B.Pharm','Others','Armed Forces Services', 'B.A.','B.B.M', 'LLB','B.B.A','B.C.A', 'B.Ed.', 'B.P.E.','BCS','B.Tech','B.Tech.','Bachelor of Journalism','Bachelor of Social Works', 'BE', 'Graduate','BL','B.Sc. (Tech)', 'BHM','BA, BL' 'B.Pharm','Bachelor of Social, Legal Sciences','BDS'])),'grad')
        post_grad = dict.fromkeys(list(map(str.capitalize,['M.a.m.','M.p.m.','Pgpm','Pgdpe','Pg programme in media management','Mhrm','M.phil.','M.tech.','MA','MMM','MBA','MSW','MFM','PGDBA','LLM','Post Diploma', 'Executive Diploma in HR Management', 'ME', 'MCA','PG Diploma', 'M.Sc.', 'M.Com.', 'MscIT','ML','Post Graduate', 'PGDM','PGDHRM', 'PGDBM', 'ML','M.Ed.'])),'post_grad')
        
        f_data = dict(data[['employee_id', 'state', 'branch', 'division','function_name', 'business_unit', 'department', 'sub_department',
       'cost_centre', 'band','emp_location','repo_location', 'work_contract', 'total_experience_year_in_mf','reporting_officer_total_experience_year_in_mf', 
       'age', 'reporting_office_age','emp_gender', 'emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3',
       'emp_lang_known4', 'repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4',
       'emp_marital_status', 'emp_highest_qualification','repo_highest_qualification', 'manager_net_mcares_score', 
        'gem_previous_quarter','gem_prev_to_prev_quarter', 'star_previous_quarter','star_prev_to_prev_quarter', 'training_previous_quarter',
       'training_prev_to_prev_quarter','dic_previous_quarter', 'monthly_compensation_gross_previous_quarter','fixed_component_previous_quarter',
       'annual_hike_percentage_previous_quarter','no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter',
        'pip_previous_quarter', 'promotion_count_year','employee_status']])
        
        
        f_data['emp_qualif_cat'] = f_data['emp_highest_qualification'].apply(lambda x: x.capitalize() if not pd.isna(x) else x)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(under_grad)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(grad)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(post_grad)
        f_data['repo_qualif_cat'] = f_data['repo_highest_qualification'].apply(lambda x: x.capitalize() if not pd.isna(x) else x)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(under_grad)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(grad)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(post_grad)
        cleaned_df = pd.DataFrame(f_data)
        binInterval = [0, 24, 48, 108]
        binLabels   = ['<=2years','<=4years','<=9years']
        cleaned_df['promotion_category'] = pd.cut(cleaned_df['promotion_count_year'],bins=binInterval,labels=binLabels)
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].cat.add_categories('NA')
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].fillna('NA')
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].astype(str)
#         cleaned_df['emp_qualif_cat']=cleaned_df.apply(
#         lambda row: 'under_grad' if pd.isna(row['emp_qualif_cat']) and row['grade_name'] =="L10C- Operational" else row['emp_qualif_cat'],axis=1 )
        cleaned_df['emp_qualif_cat'].fillna(cleaned_df['emp_qualif_cat'].value_counts().index[0],inplace=True)
        cleaned_df['repo_qualif_cat'].fillna(cleaned_df['repo_qualif_cat'].value_counts().index[0],inplace=True)
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Divorcee','Single')
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Widower','Single')
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Widow','Single')
        cleaned_df.loc[:,'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Separated','Single')
        cleaned_df.loc[:,'employee_status'] = cleaned_df['employee_status'].str.replace('Suspended','Active')
        cleaned_df.loc[:,'employee_status'] = cleaned_df['employee_status'].str.replace('Unpaid Leave','Active')
        cleaned_df['location_emp_rep'] = np.where(cleaned_df['emp_location'].str.lower() == cleaned_df['repo_location'].str.lower(), 'same', 'different') 
        
        cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']] = cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']].replace(r'^\s*$', np.nan, regex=True)
        cleaned_df['emp_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']].values]
        cleaned_df[['repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4']] = cleaned_df[['repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4']].replace(r'^\s*$', np.nan, regex=True)
        cleaned_df['emp_lang_count'] = cleaned_df[['emp_lang_known1','emp_lang_known2','emp_lang_known3','emp_lang_known4']].count(axis=1)
        cleaned_df['repo_lang_count'] = cleaned_df[['repo_lang_known1', 'repo_lang_known2','repo_lang_known3', 'repo_lang_known4']].count(axis=1)
        cleaned_df['repo_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in cleaned_df[['repo_lang_known1','repo_lang_known2','repo_lang_known3','repo_lang_known4']].values]
        cleaned_df['emp_lang_count'] = cleaned_df['emp_lang_count'].replace(0,1)
        cleaned_df['repo_lang_count'] = cleaned_df['repo_lang_count'].replace(0,1)
        cleaned_df['training_previous_quarter'] = cleaned_df['training_previous_quarter'].apply(lambda x:1 if x >= 1 else 0)
        cleaned_df['training_previous_quarter'].fillna(cleaned_df['training_previous_quarter'].value_counts().index[0],inplace=True)
        cleaned_df['training_prev_to_prev_quarter'] = cleaned_df['training_prev_to_prev_quarter'].apply(lambda x:1 if x >= 1 else 0)
        cleaned_df['training_prev_to_prev_quarter'].fillna(cleaned_df['training_prev_to_prev_quarter'].value_counts().index[0],inplace=True)
        
        cleaned_df.update(cleaned_df[['gem_previous_quarter','gem_prev_to_prev_quarter', 'star_previous_quarter','star_prev_to_prev_quarter', 'training_previous_quarter',
       'dic_previous_quarter','annual_hike_percentage_previous_quarter',
       'no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter','pip_previous_quarter']].fillna(0))
        cleaned_df['gem_prev_to_prev_quarter'] = cleaned_df['gem_prev_to_prev_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['star_prev_to_prev_quarter'] = cleaned_df['star_prev_to_prev_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['gem_previous_quarter'] = cleaned_df['gem_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['star_previous_quarter'] = cleaned_df['star_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['dic_previous_quarter'] = cleaned_df['dic_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['pip_previous_quarter'] = cleaned_df['pip_previous_quarter'].apply(lambda x:1 if x>=1 else 0)
        cleaned_df['reporting_officer_total_experience_year_in_mf'] = cleaned_df['reporting_officer_total_experience_year_in_mf'].fillna(cleaned_df['reporting_officer_total_experience_year_in_mf'].mean())
        if cleaned_df['manager_net_mcares_score'].isnull().all():
            cleaned_df['manager_net_mcares_score'] = cleaned_df['manager_net_mcares_score'].fillna(0)
        else:
            cleaned_df['manager_net_mcares_score'] = cleaned_df['manager_net_mcares_score'].fillna(cleaned_df['manager_net_mcares_score'].mean())
        cleaned_df['reporting_office_age'] = cleaned_df['reporting_office_age'].fillna(cleaned_df['reporting_office_age'].mean())
        cleaned_df['monthly_compensation_gross_previous_quarter'] = cleaned_df['monthly_compensation_gross_previous_quarter'].fillna(cleaned_df['monthly_compensation_gross_previous_quarter'].median())
        cleaned_df['annual_hike_percentage_previous_quarter'] = cleaned_df['annual_hike_percentage_previous_quarter'].apply(str2percent)
        cleaned_df['fixed_component_previous_quarter'] = cleaned_df['fixed_component_previous_quarter'].fillna(cleaned_df['fixed_component_previous_quarter'].median())
        cleaned_df['annual_hike_percentage_previous_quarter'] = cleaned_df['annual_hike_percentage_previous_quarter'].fillna(cleaned_df['annual_hike_percentage_previous_quarter'].median())
#         print (cleaned_df[['gem_previous_quarter','star_previous_quarter','pip_previous_quarter']])
        cat_cols = [ col for col in cleaned_df.columns if cleaned_df[col].dtypes == 'object' and col not in comp_cols ]
        cat_cols_df =  pd.get_dummies(cleaned_df[cat_cols])#,drop_first=True)
        cleaned_df.drop(labels=['emp_highest_qualification','repo_highest_qualification'],axis=1,inplace=True)
        final_data = pd.concat([cleaned_df,cat_cols_df],axis=1)
        cols_by_oprn = ['employee_id', 'band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL','band_OPERATIONAL', 'band_STRATEGIC', 
        'total_experience_year_in_mf','reporting_officer_total_experience_year_in_mf','manager_net_mcares_score', 
       'age', 'reporting_office_age','gem_prev_to_prev_quarter','gem_previous_quarter','star_prev_to_prev_quarter',\
        'star_previous_quarter','monthly_compensation_gross_previous_quarter','fixed_component_previous_quarter',\
        'annual_hike_percentage_previous_quarter','no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter',\
        'emp_lang_count', 'repo_lang_count','emp_marital_status_Married','emp_marital_status_Single', 'emp_gender_Female','emp_gender_Male',\
        'emp_qualif_cat_grad','emp_qualif_cat_post_grad','emp_qualif_cat_under_grad','repo_qualif_cat_grad', 'repo_qualif_cat_post_grad','repo_qualif_cat_under_grad',\
        'training_prev_to_prev_quarter','training_previous_quarter','dic_previous_quarter',\
        'pip_previous_quarter','location_emp_rep_same','location_emp_rep_different','promotion_category_<=2years', 'promotion_category_<=4years', 'promotion_category_<=9years',
       'promotion_category_NA','employee_status']
            
        clean_data = final_data[cols_by_oprn]
        
        return clean_data
    except Exception as e:
        print (e)

def strip_nums(d_frame):
    d_frame['state'] = d_frame['state'].apply(lambda x: x.split('-',1)[0] if not pd.isna(x) else x).fillna('N_A')
    split_cols = ['branch','division','function_name','business_unit','department','sub_department','cost_centre']
    d_frame[split_cols] = d_frame[split_cols].fillna('N_A')
    lam_func = lambda x: x.split('-',1)[1] if not pd.isna(x) and len(x.split('-',1))>1 else x
    d_frame[split_cols] = d_frame[split_cols].applymap(lam_func)
    return d_frame

def demograph_perf(data,model):
    """
    Function for tree map
    Output: a dataframe(can be saved in csv format or in SQL table)
    """
    statewise_perf = dict()
    perf_out = ['Active','Seperated']
    try:
        final_data = data_manip_simul(data)
        predicts = ['Seperated' if round(i[1],3)>0.6 else 'Active' for i in model.predict_proba(final_data)]
        final_data['preds'] = predicts
        final_data_pred = pd.concat([final_data['preds'],data],axis=1)
        final_data_pred = strip_nums(final_data_pred)
        df = pd.DataFrame(final_data_pred.groupby(['employee_id','state','branch','function_name','division','business_unit','department','sub_department','cost_centre','work_contract','band','grade_designation'])['preds'].value_counts())
        df.drop(labels='preds',inplace=True,axis=1)
        df.reset_index(inplace=True)
        df.dropna(inplace=True)
        # data_set = CSVDataSet(filepath="model_"+datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M')+"/treemap.csv")
        # data_set.save(df)
    
    except Exception as e:
        print ('demograph_perf',e)
    
    return df, model

def survival_model(data):
    # try:
        cph = CoxPHFitter(penalizer=0.01)
        data['employment_details_last_working_date'] = pd.to_datetime(data['employment_details_last_working_date'],format='%Y-%m-%d')
        # print(1111)
        data['employment_details_date_of_hire'] = pd.to_datetime(data['employment_details_date_of_hire'],format='%Y-%m-%d')
        data['employee_last_working_date_copy'] = data['employment_details_last_working_date'].apply(lambda x:pd.to_datetime(datetime.today()) if pd.isna(x) else x)
        data['years_in_org'] = data.apply(lambda x:((x['employee_last_working_date_copy']- x['employment_details_date_of_hire']).days/365.25),axis=1)
        # data['employee_id'] = data['ï»¿"employee_id"']
        surv_clean_data = data_manip_simul_surv(data)
        
        surv_clean_data['years_in_org'] = data['years_in_org']
        surv_clean_data['employee_status'] = pd.get_dummies(surv_clean_data['employee_status'],drop_first=False)['Active']
        
        dropped_data = surv_clean_data.drop(['employee_id','total_experience_year_in_mf','fixed_component_previous_quarter','band_DEPATMENT HEAD','band_EXECUTIVE','band_STRATEGIC','band_MANAGERIAL','star_prev_to_prev_quarter',
                                        'emp_marital_status_Married','emp_gender_Female','emp_qualif_cat_post_grad','emp_qualif_cat_under_grad',
                                        'repo_qualif_cat_post_grad','repo_qualif_cat_under_grad','location_emp_rep_same','promotion_category_<=2years',
                                        'promotion_category_<=4years','promotion_category_<=9years','emp_marital_status_Single','manager_net_mcares_score','gem_previous_quarter'],axis=1)
        
        cph.fit(dropped_data,'years_in_org','employee_status',step_size=0.0001)
             
        return cph
    # except Exception as e:
    #     print (e)

def attrition_predictions(data,model):
    stripped_df = data[(data['employee_status'] == 'Active')|(data['employee_status'] == 'Suspended')|(data['employee_status'] == 'Unpaid Leave')]
    pred_data = data_manip_simul(stripped_df)
    stripped_df = strip_nums(stripped_df)
    stripped_df['probability'] = [round(i[1],3) for i in model.predict_proba(pred_data)]
    stripped_df['Status'] = stripped_df['probability'].apply(lambda x:'High' if round(x,3)>0.6 else 'Low')
    return stripped_df
# def report_accuracy(predictions, test_y):
#     """Node for reporting the accuracy of the predictions performed by the
#     previous node. Notice that this function has no outputs, except logging.
#     """
#     # Get true class index
#     # Calculate accuracy of predictions
#     accuracy = accuracy_score(test_y,predictions)
#     recall = recall_score(test_y,predictions,average='macro')
#     # Log the accuracy of the model
#     log = logging.getLogger(__name__)
#     log.info("Model accuracy on test set: {0:.2f}%".format(accuracy * 100))
#     mlflow.log_metric("accuracy", accuracy)
#     mlflow.log_metric("recall", recall)
#     mlflow.log_param("time of prediction", str(datetime.now()))
#     mlflow.set_tag("Attrition", 2020)

#def _sigmoid(z):
#     """A helper sigmoid function used by the training and the scoring nodes."""
#     return 1 / (1 + np.exp(-z))
