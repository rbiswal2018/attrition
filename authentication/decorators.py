from django.core.exceptions import PermissionDenied
from authentication.models import UserProfile, CustomPermissions, Group, CrudPermissions
from attrition.settings import FLAG
import logging
from analytics.utils import execute_function
from analytics.query import state_query,business_unit_query

logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)

temp_dict = []
temp_dict.extend(execute_function(state_query))
temp_dict.extend(execute_function(business_unit_query))
def role_permission(func):
    """
    Decorator,which check whether the role have permission or not for performing specific action.
    If ole has no permission to perform then it will give error.
    :param func:
    :return:
    """
    logger.info("Control entered into role_permission")

    def wrapper(request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        logger.info("Control entered into wrapper method of role_permission class")
        if FLAG is True:
            try:
                func_name = str(func).split(' ')[1]
                role_name = UserProfile.objects.get(user_id=request.user.id).role
                group_id = Group.objects.get(name=role_name).id
                custom_perm_list = CustomPermissions.objects.get(group_id=group_id).permission_list
                permission_arr = list()
                for permission in custom_perm_list:
                    if permission in temp_dict or permission in ["OBand","MBand","OBand(HO)","DH(Excluding HO)"] :
                        pass
                    else:
                        temp = CrudPermissions.objects.get(name=permission).function_name
                        permission_arr.append(temp)
                for function_list in permission_arr:
                    flag = func_name in function_list
                    if flag is True:
                        break
                if flag is True:
                    return func(request, *args, **kwargs)
                else:
                    raise PermissionDenied
            except Exception as e:
                logger.error("Error from decorators::: %s" % e)
                raise PermissionDenied
        else:
            return func(request, *args, **kwargs)
    return wrapper
