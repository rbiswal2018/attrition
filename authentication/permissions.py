from rest_framework import permissions
from attrition import settings
import logging
import pdb

logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    logger.info("Control entered into IsOwnerOrReadOnly class")

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True
        return (request.user and request.user.is_superuser) or (obj.user == request.user)

class IsAdminUserOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    logger.info("Control entered into IsAdminUserOrReadOnly class")
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        return request.user and request.user.is_staff


class IsSameUserAllowEditionOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    logger.info("Control entered into IsSameUserAllowEditionOrReadOnly class")
    def has_permission(self, request, view):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # grant permission only if the method is the PUT method
        return request.user.is_staff or request.method == 'PUT'

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        return request.user.is_staff or (request.method == 'PUT' and obj.id == request.user.id)


class IsAuthenticatedUser(permissions.BasePermission):
    """
    Allows access only to authenticated users.


    """
    logger.info("Control entered into IsAuthenticatedUser class")

    def has_permission(self, request, view):
        if settings.FLAG:
            return request.user and request.user.is_authenticated
        else:
            return True

