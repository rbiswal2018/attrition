from django.contrib.auth.models import User, Group
from rest_framework import serializers
from authentication.models import UserProfile, CustomPermissions, CrudPermissions
from authentication.util import generate_token
from config import default_password


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'is_active', 'is_superuser', 'is_staff')
        extra_kwargs = {'password': {'write_only': True}}

class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)

    class Meta:
        model = UserProfile
        fields = ('user', 'role')

    def create(self, validated_data):
        """
        Overriding the default create method of the Model serializer.
        :param validated_data: data containing all the details of profile
        :return: returns a successfully created profile record
        """
        user_data = validated_data.pop('user')
        user = UserSerializer.create(UserSerializer(), validated_data=user_data)
        if validated_data['role'] == 'admin':
            user.is_staff = True
        else:
            user.is_staff = False
        user.set_password(default_password)
        user.save()
        profile, created = UserProfile.objects.update_or_create(user=user,
                                                                role=validated_data.pop('role'),
                                                                token_key=generate_token())
        return profile


class ChangePasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class GroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = ('id', 'name', )

    def create(self, validated_data):
        """
        To create role with permission.
        :param validated_data:
        :return:
        """
        name = validated_data["name"].title()
        group = Group(name=name, )
        group.save()
        perm_list = validated_data.get('permission_list', [])
        custom_obj = CustomPermissions(permission_list=perm_list, group=group)
        custom_obj.save()
        return group


class CustomPermissionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomPermissions
        fields = ('group_id', 'permission_list')

    def create(self, validated_data):
        """
        To get permission with specific role.
        :param validated_data:
        :return:
        """
        custom_permissions, created = CustomPermissions.objects.get_or_create(
            permission_list=validated_data.pop('permission_list'),
            group_id=validated_data('group_id'))

        return custom_permissions


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CrudPermissions
        fields = ('id', 'name',)
