"""
urls docstring
"""
from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework.routers import DefaultRouter
from authentication import views


Router = DefaultRouter()
Router.register("change_password", views.ChangePasswordView, basename='change')

urlpatterns = [
    path('', include(Router.urls)),
    path('jwt-auth/', obtain_jwt_token),
    path('fetch_users/', views.UserRecordsView.as_view()),
    path('signup/', views.UserRecordsView.as_view()),
    path('edit_user/', views.edit_user),
    path('delete_user/<int:pk>/', views.delete_user),
    path('activate_user/<int:pk>/', views.activate_user),  # for activate_user
    path('login/', views.login),
    path('get_role/', views.RoleView.as_view()),
    path('add_role/', views.RoleView.as_view()),
    path('edit_role/<int:pk>/', views.edit_role),
    path('delete_role/', views.delete_role),
    path('get_role_permission/', views.CustomPermissionsView.as_view()),
    path('reset_password/', views.reset_password),
    path('get_permission/', views.PermissionView.as_view()),
    path('set_default_password/<int:pk>/', views.set_default_password),  # to set default password
    path('logout/', views.logout),
]
