
"""
    Reusable functions
    generate_pass:for generating unique token key
            used in email with link


    send_email: for sending mail
            used in forgot password and signup

"""
import pdb
from random import randint, choice
from django.contrib.auth.models import User, Group
from authentication.models import UserProfile, CustomPermissions
from rest_framework.authtoken.models import Token
from django.db import connection
import logging

logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)


def get_cursor(query):
    """

    :param query:
    :return:
    """
    try:
        cursor = connection.cursor()
        cursor.execute(query)
        return cursor
    except Exception as e:
        logger.error("Errors: %s " % e)

    return None


def generate_token():
    """
        it will generate unique token key
    :return: token key
    """
    logger.info("Control entered into generate_pass method")
    charset = 'abcdefghijklmnopqrstuvwxyz1234567890'
    minlength = 100
    maxlength = 128
    length = randint(minlength, maxlength)
    return ''.join(map(lambda unused: choice(charset), range(length)))


def token_perm_details(user):
    """
    Return all values of a login user.
    :param user:
    :return:
    """
    logger.info("Control entered into token_perm_details method")
    flag = Token.objects.filter(user=user).exists()
    if flag is True:
        token_obj = Token.objects.get(user=user)
        token_obj.delete()
    else:
        token, _ = Token.objects.get_or_create(user=user)

    # fetching group_id from auth_group table
    flag = False
    group_id = ''
    permission_list = list()
    user_role = UserProfile.objects.get(user=User.objects.get(id=user.id).id).role
    if not user.is_superuser:
        # to fetch user_role from user_profile using auth_user id
        # user_role = UserProfile.objects.get(user=User.objects.get(id=user.id).id).role
        if Group.objects.filter(name=user_role).count() == 0:
            flag = True
        else:
            group_id = Group.objects.get(name=user_role).id
    else:
        # user_role = "admin"
        group_id = Group.objects.get(name=user_role).id

    if flag is False:
        # fetching permission_list from custom_permission table
        try:
            permission_list = CustomPermissions.objects.get(group_id=group_id).permission_list
        except Exception as e:
            logger.error("Error from permission list in utils: %s" % e)
    data = {
        "id": user.id,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "token": token.key, "permission_list": permission_list,
        "msg": "success", "role": user_role
    }

    return data


