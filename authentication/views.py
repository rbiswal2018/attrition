"""
authentication view -
"""
from django.db.models import Q
from django.http import JsonResponse
from django.contrib.auth import authenticate
from django.contrib.auth.models import User, Group
from rest_framework.permissions import IsAdminUser
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.authtoken.models import Token
from rest_framework.renderers import JSONRenderer
from authentication.decorators import role_permission
from authentication.models import UserProfile, CustomPermissions, CrudPermissions
from authentication.permissions import IsAuthenticatedUser
from authentication.serializer import UserProfileSerializer, GroupSerializer, \
    ChangePasswordSerializer, CustomPermissionsSerializer, PermissionSerializer
from authentication.util import token_perm_details, get_cursor, generate_token
import logging
import pdb
from config import default_password

logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)


class UserRecordsView(APIView):
    """
    A class based view for creating and fetching profile records
    """
    logger.info("Control entered into UserRecordsView")
    serializer_class = UserProfileSerializer
    model = User, UserProfile
    permission_classes = (IsAuthenticatedUser,)

    @staticmethod
    @role_permission
    def get(request):
        """
        :param : Format of the profile records to return to
        :return: Returns a list of profile records
        """

        logger.info("Control entered into get method of UserRecordsView")
        profiles = UserProfile.objects.all()
        serializer_context = {
            'request': request,
        }
        serializer = UserProfileSerializer(profiles, many=True, context=serializer_context)
        return Response({"message": "success", "data": serializer.data})

    @staticmethod
    @role_permission
    def post(request):
        """
        :User and User Profile Creation .
        :param request:
        :return:
       """
        logger.info("Control entered into post method of UserRecordsView")
        try:
            data = request.data
            if data['user']['username'] is not None \
                    and data['user']['username'] != "" and data['user']['username'] == data['user']['email']:
                serializer = UserProfileSerializer(data=data)
                if serializer.is_valid(raise_exception=ValueError):
                    email = serializer.data['user']['email']
                    flag = User.objects.filter(Q(email=email) | Q(username=email)).exists()
                    # it will check email is present or not
                    if flag is False:

                        serializer.create(validated_data=data)
                        user_id = User.objects.get(Q(email=email) | Q(username=email)).id
                        data = serializer.data
                        data["user"].update({"id": user_id})

                        return Response({"message": "success", "data": serializer.data}, status=status.HTTP_201_CREATED)
                    else:
                        return Response({"message": "email already exists"}, status=status.HTTP_409_CONFLICT)

                return Response(serializer.error_messages, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message": "failed",
                                 "error": "username can't be empty or null and email is always equal to username"})
        except Exception as e:
            logger.error("Error from signup: %s" % e)


@api_view(["PUT"])
@renderer_classes((JSONRenderer,))
@permission_classes((IsAuthenticatedUser,))
@role_permission
def edit_user(request):
    """
        To update the user instance.
    """
    logger.info("Control entered into edit_user method")
    try:
        user_data = request.data
        user_id = user_data['user']['user_id']
        user_obj = User.objects.get(id=user_id)
        user_profile_obj = UserProfile.objects.get(user=user_obj)
        user_obj.first_name = user_data['user']['first_name']
        user_obj.last_name = user_data['user']['last_name']
        user_profile_obj.role = user_data['role']
        user_obj.save()
        user_profile_obj.save()
        content = request.data
        return Response({"message": "success", "data": content}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("Error from edit_user: %s" % e)
        return Response({"message": "failed", "error": "user modification failed"}, status=status.HTTP_304_NOT_MODIFIED)


@api_view(["DELETE"])
@renderer_classes((JSONRenderer,))
@permission_classes((IsAuthenticatedUser,))
@role_permission
def delete_user(request,pk):
    """
    To delete the user instance
    :param request:
    :return:
    """
    # noinspection PyBroadException
    try:
        user_obj = User.objects.get(id=pk)
        user_profile_obj = UserProfile.objects.get(user=user_obj)
        user_obj.delete()
        user_profile_obj.delete()
        return Response({"message": "success"}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("Error from dummy_delete: %s" % e)
        return Response({"message": "failed", "error": "deletion failed"}, status=status.HTTP_304_NOT_MODIFIED)


@api_view(["PUT"])
@renderer_classes((JSONRenderer,))
@permission_classes((IsAuthenticatedUser,))
@role_permission
def activate_user(request, pk):
    """
        To change the is_active status of  user
    """
    logger.info("Control entered into activate_user method")
    return_dict = dict()
    try:
        user_data = request.data
        user_obj = User.objects.get(id=pk)
        user_obj.is_active = user_data['is_active']
        user_obj.save()
        return_dict['message'] = 'success'
        status_msg = status.HTTP_200_OK
    except Exception as e:
        logger.error("Error from activate_user: %s" % e)
        return_dict['message'] = 'failed'
        status_msg = status.HTTP_400_BAD_REQUEST

    return Response(return_dict, status=status_msg)


@api_view(["POST"])
def login(request):
    """
    :For login
    :param request:username ,password
    :return:token key
    :url auth/login/
    """
    logger.info("Control entered into login method")
    try:
        username = request.data.get("username").strip()
        password = request.data.get("password")
        username_flag = User.objects.filter(username=username).exists()
        if username_flag is True:
            user_obj = User.objects.get(Q(username=username) or Q(email=username))
            flag = user_obj.is_active

            if flag is True:
                if password != default_password:
                    user = authenticate(username=user_obj.username, password=password)
                    if Token.objects.filter(user=user).exists() is True:
                        token_obj = Token.objects.get(user=user)
                        token_obj.delete()

                    if not user:
                        return Response({"message": "failed", "error": "enter correct password"},
                                        status=status.HTTP_401_UNAUTHORIZED)

                    data = token_perm_details(user)
                    return Response({"message": data}, status=status.HTTP_200_OK)
                else:
                    token = UserProfile.objects.get(user=user_obj.id).token_key
                    return Response({"message": "reset password", "token": token}, status=status.HTTP_200_OK)
            else:
                return Response({"message": "Contact your admin"}, status=HTTP_401_UNAUTHORIZED)
        else:
            return Response({"message": "failed", "error": "username doesn't exist"},
                            status=HTTP_401_UNAUTHORIZED)

    except Exception as e:
        logger.error("Error from login: %s" % e)
        return Response({"message": "Login failed"}, status=HTTP_401_UNAUTHORIZED)


class RoleView(APIView):
    logger.info("Control entered into CustomPermissions APIView")
    permission_classes = (IsAuthenticatedUser,)

    @staticmethod
    # @role_permission
    def get(request):
        """
        Fetch all role.
        :rtype: object
        :param request:
        :return:
        """
        logger.info("Control entered into get method of CustomPermissions APIView")
        group_obj = Group.objects.all()
        serializer_context = {
            'request': request,
        }
        serializer = GroupSerializer(group_obj, many=True, context=serializer_context)
        return Response(serializer.data)

    @staticmethod
    @role_permission
    def post(request):
        """
        create a new role with its permission_list .
        :param request:
        :return:
        """
        logger.info("Control entered into post method of CustomPermissions APIView")
        try:
            new_dict = {}
            serializer = GroupSerializer(data=request.data)
            if serializer.is_valid(raise_exception=ValueError):
                serializer.create(validated_data=request.data)
                data = serializer.data
                new_dict["name"] = data['name'].capitalize()
                new_dict["id"] = Group.objects.get(name=new_dict["name"]).id
                new_dict["permission_list"] = CustomPermissions.objects.get\
                    (group=Group.objects.get(name=new_dict["name"]).id).permission_list
            return JsonResponse({"message": "success", "data": new_dict}, status=status.HTTP_200_OK)
        except Exception as e:
            logger.error("error", e)
            return Response({"message": "failed"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["PUT"])
@renderer_classes((JSONRenderer,))
@permission_classes((IsAuthenticatedUser, ))
@role_permission
def edit_role(request, pk):
    """
        To update the permission related to role and role name.
    """
    logger.info("Control entered into edit_user method")
    try:
        data = request.data
        role_obj = Group.objects.get(id=pk)
        role_obj.name = data['role']
        permission_list = data["permission_list"]
        custom_permission_obj = CustomPermissions.objects.get(group_id=pk)
        custom_permission_obj.permission_list = permission_list
        role_obj.save()
        custom_permission_obj.save()
        content = request.data
        return Response({"message": "success", "data": content}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("Error from edit_user: %s" % e)
        return Response({"message": "failed"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@renderer_classes((JSONRenderer,))
@permission_classes((IsAuthenticatedUser,))
@role_permission
def delete_role(request):
    """
    To delete the role instance
    :param request:
    :return:
    """
    # noinspection PyBroadException
    logger.info("Control entered into delete_role method")
    try:

        role_data = request.data
        role_id = role_data['role_id']
        role_obj = Group.objects.get(id=role_id)
        custom_permission_obj = CustomPermissions.objects.filter(group_id=role_id)
        if custom_permission_obj.exists():
            custom_permission_obj.delete()

        role_obj.delete()

        return Response({"message": "success"}, status=status.HTTP_200_OK)
    except Exception as e:

        logger.error("Error from delete_role: %s" % e)
        return Response({"message": "failed"}, status=status.HTTP_400_BAD_REQUEST)


class CustomPermissionsView(APIView):
    """
    An api view for fetching role with permission.
    """
    logger.info("Control entered into RoleViewSet")
    serializer_class = GroupSerializer
    model = Group
    permission_classes = (IsAuthenticatedUser,)

    @staticmethod
    @renderer_classes((JSONRenderer,))
    @role_permission
    def get(request, group_id=None):

        """
        :param :
        :return:
        """
        substr = "where t.id=role_id"
        query = """select array_agg(row_to_json(t)) from (
        select g.id, g.name, c.permission_list from auth_group g, custom_permissions c
        where c.group_id=g.id
        ) as t role_group;"""

        logger.info("Control entered into get method of RoleViewSet")
        if group_id is None:
            query = query.replace('role_group', "")
        else:
            query = query.replace('role_group', substr).replace('role_id', str(group_id))

        cursor = get_cursor(query)
        overall_result = cursor.fetchone()[0]

        serializer_context = {
            'result': overall_result,
        }

        return Response(serializer_context)


class ChangePasswordView(ModelViewSet):
    """
    when user change the password
    url: /auth/change_password/
    param: old_password, new_password
    An endpoint for changing password.
    """
    logger.info("Control entered into ChangePasswordView ModelViewSet")
    object = ...  # type: object
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticatedUser,)

    def get_object(self, queryset=None):
        """
        :rtype: object
        :param queryset:
        :return:
        """
        logger.info("Control entered into get_object method  of ChangePasswordView")
        obj = self.request.user
        return obj

    def patch(self, request):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        logger.info("Control entered into patch method  of ChangePasswordView")
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": "password is incorrect"},
                                status=status.HTTP_412_PRECONDITION_FAILED)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            return Response({"message": "Success."}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def reset_password(request):
    """
    :when user reset password
    :param
    :url /auth/reset_password/
    """
    logger.info("Control entered into reset_password method")
    return_dict = dict()
    try:
        new_password = request.data["new_password"]
        token = request.data["token"]
        prof_obj = UserProfile.objects.get(token_key=token)
        user_obj = User.objects.get(email=prof_obj.user)
        user_obj.set_password(new_password)
        user_obj.save()
        prof_obj.token_key = ''
        prof_obj.save()
        return_dict["status"] = "True"
        return_dict["message"] = "Password Updated"
        status_message = status.HTTP_200_OK

    except Exception as e:
        logger.error("Error from reset_password: %s" % e)
        return_dict["status"] = "False"
        return_dict["message"] = "Update Failed"
        status_message = status.HTTP_200_OK

    return Response(return_dict, status=status_message)


class PermissionView(APIView):
    """
    List all permission.
    """
    logger.info("Control entered into CustomPermissions APIView")
    permission_classes = (IsAuthenticatedUser,)

    @staticmethod
    # @role_permission
    def get(request):
        """
        :rtype: object
        :param request:
        :return:
        """
        logger.info("Control entered into get method of CustomPermissions APIView")
        permission_obj = CrudPermissions.objects.all()
        serializer_context = {
            'request': request,
        }
        serializer = PermissionSerializer(permission_obj, many=True, context=serializer_context)
        return Response(serializer.data)


@api_view(["PUT"])
@renderer_classes((JSONRenderer,))
@permission_classes((IsAuthenticatedUser, IsAdminUser))
def set_default_password(request, pk):
    """
    To set the default password of a user.
    :param request:
    :return: HttpResponse
    """
    try:
        user_obj = User.objects.get(pk=pk)
        user_obj.set_password(default_password)
        user_obj.save()
        if Token.objects.filter(user=user_obj).exists() is False:
            token = UserProfile.objects.get(user=user_obj.id)
            token.token_key = generate_token()
            token.save()
        else:
            token = UserProfile.objects.get(user=user_obj.id)
            token.delete()
            token.token_key = generate_token()
            token.save()
        context = {
            'message': 'success'
        }

        return Response(context, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("set_default_password Errors: %s" % e)

    return Response({"message": "failed", "error": "default password updation failed"},
                    status=status.HTTP_304_NOT_MODIFIED)


@api_view(['POST'])
@permission_classes((IsAuthenticatedUser,))
def logout(request):
    """
     For logout
    :param request:
    :return:
    """
    logger.info("Control entered into logout method")
    user_id = request.user.id
    token = Token.objects.get(user=user_id)
    token.delete()
    return Response({"message": "success"}, status=status.HTTP_200_OK)

