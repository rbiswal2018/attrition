import itertools
import os
import timeit
import json
import pandas as pd
import pickle
import pdb
from datetime import datetime
from dateutil import parser
import numpy as np
from authentication.util import get_cursor
from data_loader.new_loader_query import *
import csv
from django.db import connection
import shutil
from analytics.utils import execute_function
from datetime import datetime
from model_health.serializers import model_health_Serializer, model_health_put_Serializer
from model_health.models import kedro_modelhealth_output
from carrot.utilities import publish_message

from attrition import settings
import logging

# logger = logging.getLogger(__name__)
# previous_level = logger.getEffectiveLevel()
# logger.setLevel(logging.ERROR)
# logger.setLevel(previous_level)
logger = logging.getLogger('carrot')


def read_file(path, file_obj):
    """
    To read the file name and its extension, which is uploaded.
    :param path:
    :param file_obj:
    :return:
    """
    # Save the file in file system .
    # path = '/home/user/Downloads/Mahindra_Data_Template/Data_template/ gem_master.csv'
    with open(path + str(file_obj), 'wb+') as destination:
    # with open(path, 'wb+') as destination:
        for chunk in file_obj.chunks():
            destination.write(chunk)
    file_name, extension = file_obj.name.split('.')
    return file_name, extension


def get_table_unique_col(name):
    """
    To get the unique columns of the selected table type.
    :param name:
    :return:
    """

    promotion_and_rating = ["employee_id", 'year']
    recruitement = ["employee_id"]
    compensation = ["employee_id", "last_anuual_hike_date"]
    competency_report_master_year = ["employee_id", "year"]
    dis_and_pip = ["sap_code", "issue"]
    consolidated_demographics = ["employee_id"]
    gem = ["sap_code", "year", "quarter"]
    star_performer = ["sapcode", "year", "month"]
    leave = ["personnel_number", "start_date"]
    transaction = ["emp_id", "start_date"]
    training = ["candidate_sap_code", "start", "program"]
    mcares_score_department = ['department', 'year']
    mcares_score_manager = ['manager_id', 'year']
    business_service_functions = ['cost_centre']
    major_service_functions = ['cost_centre']

    table_uniqe_col = {"compensation": compensation, "dis_and_pip": dis_and_pip,
                       'promotion_and_rating': promotion_and_rating,
                       'competency_report_master_year': competency_report_master_year,
                       "recruitement": recruitement, "consolidated_demographics": consolidated_demographics,
                       "gem": gem, "star_performer": star_performer, "leave": leave,
                       "training": training, "transaction": transaction, 'mcares_score_manager': mcares_score_manager,
                       'mcares_score_department': mcares_score_department,
                       "business_service_functions": business_service_functions,
                       "major_service_functions": major_service_functions}
    return table_uniqe_col[name]


def convert_dataframe(extension, path, file_name, file_obj, table_name):
    """
    To convert the file into dataframe for future process.
    :param extension:
    :param table_name:
    :param path:
    :param file_name:
    :param file_obj:
    :return:
    """
    df = None
    try:
        if extension == 'csv' or extension == 'CSV':
            df = pd.read_csv(path + file_name + "." + extension,
                             dtype={'employee_id': 'str', 'empid': 'str', 'reporting_officer_sap_code': 'str',
                                    'total_experience_months_in_mf': 'str',
                                    'experience_in_current_role_in_months': 'str', 'Sr.No': 'str', 'no_of_cont': 'str'},
                             encoding='iso-8859-1')
        if df.empty:
            return ""
        else:
            df = df.replace(r'^\s*$', np.nan, regex=True)
            df.fillna('', inplace=True)
            if table_name =='consolidated_demographics':
                df['cost_centre'] = df['cost_centre'].str.replace('^\d+-', '', regex=True)
                df['division'] = df['division'].str.replace('^\d+-', '', regex=True)
                df['business_unit'] = df['business_unit'].str.replace('^\d+-', '', regex=True)
                df['function_name'] = df['function_name'].str.replace('^\d+-', '', regex=True)
                df['department'] = df['department'].str.replace('^\d+-', '', regex=True)
                df['state'] = df['state'].str.replace('^.+-', '', regex=True)
                df['sub_department'] = df['sub_department'].str.replace('^.+-', '', regex=True)

                df = df[df.function_name != 'MAHINDRA ASSET MANAGEMENT CO. PVT. LTD.']
            # Fill all null values with blank .
            # Drop duplicate from the data frame based on the unique columns .

            df.columns = map(str.lower, df.columns)
            df.columns = df.columns.str.strip()
            df.drop_duplicates(subset=get_table_unique_col(table_name), keep="last", inplace=True)
            df.to_csv(path + file_name + ".csv", index=False)
            return df
    except Exception as e:
        logger.error("Error from convert_dataframe :: %s" % e)


def rename_data_frame(df):
    """
        Convert all the columns to lower case , remove all the special char and convert all white space to
        underscore .
    :param :    dataframe
    :return:    dataframe
    """
    df.columns = df.columns.str.replace(' ', '_').str.replace('[^a-zA-Z0-9 _]', '').str.lower()
    return df


def match_length(actual_len, received_len):
    """
    To match the no of columns of our table to the uploaded file.
    :param actual_len:
    :param received_len:
    :return:
    """
    error_type = "invalid_column_size"
    message = "Column size does not match. {description}"
    description = "Actual columns needed is %s and the uploaded file has %s columns." % (
        actual_len, received_len)
    message = message.format(**{'description': description})

    return error_type, message


def check_date_type(table_name, data):
    error_type = "invalid_date_type"
    message = "Check your date format in {0} column and row {1}. " \
              "The required date format is DD MONTH YYYY. Eg:-(10 March 2017)".format(data["column"], data['row'])

    return error_type, message


def compare_columns(actual, incoming):
    """
    To check the column name of the table to the uploaded file.
    :param actual:
    :param incoming:
    :return:
    """

    mismatch_list = list()
    for index, obj in enumerate(actual):
        columns = {}
        if obj['name'] != incoming[index]:
            columns["actual"] = obj['name']
            columns["incoming"] = incoming[index]
            mismatch_list.append(columns)
    return mismatch_list


def mismatch_record_count(dataframe, actual_columns, names, table_name):
    """
    To check the datatype of columns of uploaded file to our template.
    :param dataframe:
    :param actual_columns:
    :param names:
    :return:
    """
    try:
        column_list = []
        for index, name in enumerate(names):
            actual = actual_columns[index]
            series_obj = dataframe[name]
            return_data = parse_series(series_obj, actual['datatype'], table_name)
            if len(return_data) > 0:
                temp = {"column": name, "row": return_data[0]}
                column_list.append(temp)
                return column_list
        return column_list
    except Exception as e:
        logger.error("Error from mismatch_record_count :: %s" % e)


def parse_series(series_obj, datatype, table_name):
    """
    To give the row number of uploaded file whose datatype is in wrong format.
    :param series_obj:
    :param datatype:
    :return:
    """
    mismatch_row_no = []
    for index, row in enumerate(series_obj):
        if not validate(row, datatype, table_name, series_obj.name):
            mismatch_row_no.append(index + 1)
            break
    return mismatch_row_no


def validate(data, datatype, table_name, col_name):
    """
    :param data:
    :param datatype:
    :return:
    """
    flag = True

    if data != "":
        try:
            # check_column_date_type(data, datatype, table_name, col_name)
            if datatype == "date":
                datetime.strptime(data, '%d %B %Y')
            # elif datatype == "float":
            #     float(data)
            # elif datatype == "int":
            #     int(data)
        except ValueError as e:
            flag = False
    return flag


def check_header_of_columns(mismatch_columns):
    """
    To show the error msg.
    :param mismatch_columns:
    :param success:
    :param error_type:
    :param message:
    :param data:
    :return:
    """

    error_type = "column_mismatch"
    message = "Some column(s) did not match."
    data = mismatch_columns

    return error_type, message, data


def check_mismatch_type(mismatch_data_type):
    """
    To show the error msg.
    :param mismatch_data_type:
    :param success:
    :param error_type:
    :param message:
    :param data:
    :return:
    """
    error_type = "invalid_datatype"
    message = "Mixed datatype(s) found."
    data = mismatch_data_type

    return error_type, message, data


def data_for_insert_query(table_column_names, table_name):
    """
    To get staging table name and columns.
    :param table_column_names:
    :param table_name:
    :return:
    """
    table_columns = ''
    for col in table_column_names:
        table_columns += col['name'] + "   " + col['datatype'] + ","
    table_columns = table_columns[:-1]
    staging_table_name = table_name + '_staging'
    return table_columns, staging_table_name


def data_for_master(config_obj):
    """
    To get master table name and columns.
    :param config_obj:
    :return:
    """
    master_table_name = config_obj['master_table_name']
    table_columns = config_obj['master']
    unique_column = config_obj['unique_column']
    table_columns_master = ''
    table_column = ''
    for col in table_columns:
        table_columns_master += col['name'] + "   " + col['datatype'] + ","
        table_column += col['name'] + ","
    table_columns_master = table_columns_master[:-1]
    table_column = table_column[:-1]
    return master_table_name, table_columns_master, unique_column, table_column


def get_query(name):
    """

    :param name:
    :return:star_performer
    """
    table_name = "SELECT initcap(table_name) FROM information_schema.tables where table_schema = 'buisness_suite';"
    create_schema = "CREATE SCHEMA IF NOT EXISTS buisness_suite;"
    create_schema_predictive = "CREATE SCHEMA IF NOT EXISTS predictive;"
    create_staging = "create table buisness_suite.{0}({1});"
    create_master = "CREATE TABLE IF NOT EXISTS buisness_suite.{0}({1});"
    alter_staging = "ALTER TABLE buisness_suite.{0} add COLUMN bu text;"
    update_staging = "update buisness_suite.{0} set bu = '{1}' where bu is null;"
    copy_to_staging = "COPY buisness_suite.{0} FROM '{1}' CSV HEADER; "
    # copy_to_staging = "COPY buisness_suite.{0} FROM '{1}' WITH DELIMITER ',' CSV HEADER; "
    bulk_update = """
                 WITH upd AS (
                                UPDATE buisness_suite.{0} t
                                SET {1}
                                FROM buisness_suite.{2} s WHERE {3}
                                RETURNING {4}
                            )
                            INSERT INTO buisness_suite.{0}({5})
                                SELECT {5}
                                FROM buisness_suite.{2} s LEFT JOIN upd t USING({6})
                                WHERE {7} 

            """
    master_count = """select count(*) from buisness_suite.{0}"""

    drop_staging = " drop table buisness_suite.{0};"
    index = 'create index {0}_index on buisness_suite.{0} ({1});'
    bu_index = 'create index {0}_opr_category_index on buisness_suite.{0} ({1});'
    year_index = 'create index {0}_year_index on buisness_suite.{0} ({1});'
    quarter_index = 'create index {0}_quarter_index on buisness_suite.{0} ({1});'
    empid_index = 'create index {0}_empid_index on buisness_suite.{0} ({1});'
    month_index = 'create index {0}_month_index on buisness_suite.{0} ({1});'
    refresh_view = 'REFRESH MATERIALIZED VIEW buisness_suite.{0};'

    query_dictionary = {"table_name": table_name, "create_schema": create_schema,
                        "create_schema_predictive": create_schema_predictive,
                        "create_staging": create_staging,
                        "create_master": create_master, "alter_staging": alter_staging,
                        "update_staging": update_staging,
                        "copy_to_staging": copy_to_staging, "bulk_update": bulk_update, "master_count": master_count,
                        "drop_staging": drop_staging, "index": index, "refresh_view": refresh_view,
                        "bu_index": bu_index, "year_index": year_index, "quarter_index": quarter_index,
                        "empid_index": empid_index, "month_index": month_index}

    return query_dictionary[name]


def get_column(columns):
    """
    To get column for bulk_update query set statement.
    :param columns: column of sample_master table.
    :return: column = s.column
    """
    # print("...............................................",columns)
    # col_1 = columns.replace('text', '')
    # col_2 = col_1.replace('float', '')
    # col_3 = col_2.replace('date', '')
    column = columns.split(',')
    param = ""
    for col in column:
        param += col + "= s." + col + ","
    return param[:-1]


def bulk_update_where_condition(columns):
    where_condition_data = ""
    where_condition_insert_data = ""
    columns = ' '.join(columns)
    master_unique_column = get_unique_column('t', columns).split(',')
    staging_unique_column_list = get_unique_column('s', columns).split(',')
    staging_unique_column = get_unique_column('s', columns)
    for col_t, col_s in zip(master_unique_column, staging_unique_column_list):
        where_condition_data += col_t + " = " + col_s + " and "
        where_condition_insert_data += col_t + " is null" + " and "
    return where_condition_data[:-4], staging_unique_column, columns, where_condition_insert_data[:-4]


def get_unique_column(table_alias, columns):
    """
    To get column for update query.
    :param columns: column of sample_master table.
    :return: column = s.column
    """
    column = columns.split(',')
    param = ""
    for col in column:
        if table_alias == "":
            param += col + " ,"
        else:
            param += table_alias + "." + col + " ,"
    return param[:-1]


def get_master_column(columns):
    """
    To get column for bulk_update query set statement.
    :param columns: column of sample_master table.
    :return: column = s.column
    """
    # print("######################..............................",columns)
    # col_1 = columns.replace('text', '')
    # col_2 = col_1.replace('float', '')
    # col_3 = col_2.replace('date', '')
    column = columns.split(',')
    master_column = ""
    for col in column:
        master_column += col + " ,"
        # master_column += col[:-3] + " ,"
    # print("###########################",master_column)
    return master_column[:-1]


def get_record_count(staging_table_name, df):
    successful_count = get_query('master_count').format(staging_table_name)
    cursor = get_cursor(successful_count)
    count = 0
    for c in cursor:
        count = c
        break

    get_cursor(get_query('drop_staging').format(staging_table_name))

    total_record_count = len(df.index)
    failed_data = total_record_count - int(count[0])
    return count[0], failed_data


def clear_data(extension, file_name):
    """
    :param extension:
    :param file_name:
    :return:
    """

    os.remove(os.path.join(settings.MEDIA_ROOT + "/data_sets/", file_name + "." + extension))

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
view_list = ['leave_data_master_view', 'star_performer_master_view', 'gem_master_view', 'compensation_master_view',
    'dis_pip_master_view', 'compensation_latest_year_view', 'consolidated_demoghaphic_master_view',
    'training_data_master_view', 'transaction_master_view', 'annual_range', 'total_experience_range',
    'promotion_rating_view', 'final_view',
    'supervisor', 'pre_new_all_emp_supervisor_yearly_view', 'predict_promotion_rating_view',
    'promotion_and_rating_yearly_view', 'promotion_and_rating_latest_view']

view_list_bu_index = ['all_bu_training', 'recovery_bu_master_view']

view_list_year_index = ['all_bu_training', 'gem_master_view', 'leave_data_master_view', 'recovery_bu_master_view',
                        'star_performer_master_view', 'compensation_master_view',
                        'dis_pip_master_view', 'training_data_master_view',
                        'training_with_category_data', 'transaction_master_view']

view_list_quarter_index = ['all_bu_training', 'gem_master_view', 'leave_data_master_view', 'star_performer_master_view',
                           'dis_pip_master_view', 'training_data_master_view',
                           'compensation_master_view', 'training_with_category_data', 'transaction_master_view',
                           'pre_new_leave_yearly_data',
                           ]

view_list_empid_index = ['all_bu_training', 'gem_master_view', 'leave_data_master_view', 'recovery_bu_master_view',
                         'star_performer_master_view',
                         'dis_pip_master_view', 'training_data_master_view', 'compensation_master_view',
                         'training_with_category_data', 'transaction_master_view', 'all_bu_training_new',
                         'new_training_quarter_based_view']

view_list_month_index = ['recovery_bu_master_view']


def create_views():
    """
        Create materialize views and corresponding index .
    :return:
    """
    exe_start = timeit.default_timer()
    logger.info("***************************** CREATE VIEW ***************************************")

    view_name = ""
    materialize_views = [
        leave_data_master_view, star_performer_master_view, gem_master_view,
        compensation_master_view, compensation_latest_year_view, annual_range, training_data_master_view,
        dis_and_pip_master_view,
        transaction_master_view, promotion_rating_view, consolidated_dempgraphic_view, total_experience_range,
        promotion_rating_view,
        final_view, promotion_and_rating_yearly_view,
        supervisor_view, predict_promotion_rating_view, predict_promotion_data_view,
        pre_new_all_emp_supervisor_yearly_view]

    try:
        for index, name in enumerate(materialize_views):
            start = timeit.default_timer()
            # view_name = view_list[index]
            # print(view_name)
            cursor = get_cursor(name)  # Create view
            # create_index(view_list[index])  # Create all index
            # create_bu_index(view_list[index])  # Create all indexes based on specific columns.
            taken = round(timeit.default_timer() - start, 2)
        time_taken = round(timeit.default_timer() - exe_start, 2)
        # print("({0}).Execution Time({1})".format(view_name, time_taken))
        logger.info("************************** END VIEW CREATION*****************************************")
        logger.info(" All View Created Successfully . Execution Time({0})".format(time_taken))

    except Exception as e:
        logger.error(e, "view..", view_name)
        logger.error("Error in create_views {0}: {1}".format(view_name, str(e)))


def create_index(view_name):
    """
        Create index for all materialize views .
    :return:
    """
    try:
        column_for_index = ''

        columns = get_view_index_column(view_name)
        for col in columns:
            column_for_index += '"' + col + '"' + ','
        query = get_query('index').format(view_name, column_for_index[:-1])
        get_cursor(query)
    except Exception as e:
        logger.error("Error in create_index {0}:".format(str(e)))


def create_bu_index(view_name):
    """
    creating indexes on specific columns of view.
    :param view_name:
    :return:
    """
    try:
        bu_columns = get_view_index_column_name(view_name)
        if 'oprn_category' in bu_columns:
            bu_index_query = get_query('bu_index').format(view_name, bu_columns['oprn_category'])
            get_cursor(bu_index_query)
        if 'year' in bu_columns:
            year_index_query = get_query('year_index').format(view_name, bu_columns['year'])
            get_cursor(year_index_query)
        if 'quarter' in bu_columns:
            quarter_index_query = get_query('quarter_index').format(view_name, bu_columns['quarter'])
            get_cursor(quarter_index_query)
        if 'month' in bu_columns:
            month_index_query = get_query('month_index').format(view_name, bu_columns['month'])
            get_cursor(month_index_query)
        if 'empid' in bu_columns:
            empid_index_query = get_query('empid_index').format(view_name, bu_columns['empid'])
            get_cursor(empid_index_query)
    except Exception as e:
        logger.error("Error in create_index {0}:".format(str(e)))


def get_view_index_column(name):
    """
    To get the unique columns of the selected table type.
    :param name:
    :return:
    """
    recovery_bu_master_view = ["empid", "yyyymm"]
    compensation_master_view = ["employee_id", "year", "quarter_name"]
    dis_pip_master_view = ["sap_code", "year", "quarter_name"]
    training_data_master_view = ["candidate_sap_code", "training_year", "quarter_name"]
    training_with_category_data = ["candidate_sap_code", "training_year", "quarter_name"]
    transaction_master_view = ["personnel_number", "transcation_year", "quarter_name"]
    gem_master_view = ["sap_code", "year", "quarter"]
    star_performer_master_view = ["sapcode", "year", "month"]
    leave_data_master_view = ["personnel_number", "start_date"]
    all_bu_training = ["empid", "bu_mon", "bu_year"]
    table_uniqe_col = {"all_bu_training": all_bu_training, "recovery_bu_master_view": recovery_bu_master_view,
                       "gem_master_view": gem_master_view, "leave_data_master_view": leave_data_master_view,
                       "star_performer_master_view": star_performer_master_view,
                       "compensation_master_view": compensation_master_view,
                       "dis_pip_master_view": dis_pip_master_view,
                       "training_data_master_view": training_data_master_view,
                       "training_with_category_data": training_with_category_data,
                       "transaction_master_view": transaction_master_view
                       }
    return table_uniqe_col[name]


def get_view_index_column_name(name):
    """
    To get the unique columns of the selected table type.
    :param name:
    :return:
    :return:
    """
    recovery_bu_master_view = {"year": 'year', "oprn_category": 'oprn_category', "empid": 'empid', "month": 'month'}
    gem_master_view = {"empid": "sap_code", "year": "year", "quarter": "quarter_name"}
    compensation_master_view = {"empid": "employee_id", "year": "year", "quarter": "quarter_name"}
    dis_pip_master_view = {"empid": "sap_code", "year": "year", "quarter": "quarter_name"}
    training_data_master_view = {"empid": "candidate_sap_code", "year": "training_year", "quarter": "quarter_name"}
    training_with_category_data = {"empid": "candidate_sap_code", "year": "training_year", "quarter": "quarter_name"}
    transaction_master_view = {"empid": "personnel_number", "year": "transcation_year", "quarter": "quarter_name"}
    gem_master_view = {"empid": "sap_code", "year": "year", "quarter": "quarter_name"}
    star_performer_master_view = {"empid": "sapcode", "year": "year", "quarter": "quarter_name"}
    leave_data_master_view = {"empid": "personnel_number", "year": "year", "quarter": "quarter_name"}
    all_bu_training = {"bu": "bu", "year": "bu_year", "quarter": "quarter_name"}
    table_unique_col = {"all_bu_training": all_bu_training, "recovery_bu_master_view": recovery_bu_master_view,
                        "gem_master_view": gem_master_view, "leave_data_master_view": leave_data_master_view,
                        "star_performer_master_view": star_performer_master_view,
                        "compensation_master_view": compensation_master_view,
                        "dis_pip_master_view": dis_pip_master_view,
                        "training_data_master_view": training_data_master_view,
                        "training_with_category_data": training_with_category_data,
                        "transaction_master_view": transaction_master_view
                        }
    return table_unique_col[name]


def refresh_views():
    """
        Refresh all materialize views .
    :return:
    """
    name = ""
    try:
        exe_start = timeit.default_timer()
        logger.info("***************************** REFRESH VIEW ***************************************")
        for view_name in view_list:
            name = view_name.upper()
            start = timeit.default_timer()
            query = get_query('refresh_view').format(view_name)
            get_cursor(query)
            time_taken = round(timeit.default_timer() - start, 2)
            # print("refreshed view::  ", view_name)
            # #print("' {0} ' V
            #
            # iew Refreshed Successfully . Execution Time({1})".format(name, time_taken))
        time_taken = round(timeit.default_timer() - exe_start, 2)
        logger.info("************************** END REFRESH VIEW *****************************************")
        logger.info(" All View Refreshed Successfully . Execution Time({0})".format(time_taken))

    except Exception as e:
        logger.error("Error from refresh_views while creating ' {0} ' Details: {1}".format(name, str(e)))


def drop_views():
    """
        Drop all materialize views .
    :return:
    """
    logger.info("***************************** DROP VIEW ***************************************")
    try:
        prediction_drop_views()
        for view_name in view_list:
            query = 'DROP MATERIALIZED VIEW IF EXISTS  buisness_suite.{0} CASCADE;'.format(view_name)
            get_cursor(query)  # Drop views
            # print("drop view::  ", view_name)
    except Exception as e:
        logger.error("Error in drop_views {0}:".format(str(e)))


master_table_list = ['training', 'transaction', 'consolidated_demographics', 'gem', 'leave', 'star_performer',
                     'compensation', 'competency_report_master_year',
                     'dis_and_pip', 'mcares_score_manager', 'mcares_score_department', 'recruitement',
                     'promotion_and_rating', 'business_service_functions', 'major_service_functions']


def create_master_table_initial():
    try:
        create_schema = get_query('create_schema')
        get_cursor(create_schema)
        for table in master_table_list:
            config_obj = json.loads(open("data_loader/config/" + table + ".json").read())
            master_table_name, table_columns_master, unique_column, table_column = data_for_master(config_obj)
            create_master_query = get_query('create_master').format(master_table_name, table_columns_master)
            get_cursor(create_master_query)
    except Exception as e:
        logger.error("Error from create_master_table_initial :: %s" % e)
        return "failed"


def create_all_predictive_table():
    try:
        create_schema = get_query('create_schema_predictive')
        get_cursor(create_schema)
        #get_cursor(model_health_table)
        get_cursor(predictive_output)
        get_cursor(predict_tree_map_data)

    except Exception as e:
        logger.error("Error from create_all_predictive_table :: %s" % e)
        return "failed"


def copy_csv_in_prediction_table():
    try:
        path_list = []

        file_list = ['predict_performance_data.csv', 'rbmi_prediction_all_data_v1.csv']

        for i in file_list:
            predictive_path = settings.MEDIA_ROOT + '/predictive/' + str(i)
            path_list.append(predictive_path)

        copy_command_query_predict_performance_data = """COPY predictive.predict_performance_data 
        FROM '{0}' HEADER CSV ;""".format(path_list[0])
        copy_command_query_final_data_with_predictions = """COPY predictive.final_data_with_predictions 
                FROM '{0}' HEADER CSV ;""".format(path_list[1])
        # copy_command_query_pre_contract_yearly_view = """COPY buisness_suite.pre_contract_yearly_view
        #         FROM '{0}' HEADER CSV ;""".format(path_list[2])
        # get_cursor(copy_command_query_predict_performance_data)
        # get_cursor(copy_command_query_final_data_with_predictions)
        # get_cursor(copy_command_query_pre_contract_yearly_view)
    except Exception as e:
        logger.error("Error from copy_csv_in_prediction_table :: %s" % e)
        return "failed"


def save_predict_data(temp_quarter_details):
    try:
        path = settings.kedro_ROOT + '/data_to_predict.csv'
        logger.info(path)
        # query_obj = get_cursor("select * from buisness_suite.prediction_data_view")
        # query_data = query_obj.fetchall()
        SQL_for_file_output = "COPY (select * from buisness_suite.prediction_data_view) TO STDOUT WITH CSV HEADER"
        # Set up a variable to store our file path and name.
        # path = "c:\data\users.csv"
        cursor = connection.cursor()
        with open(path, 'w', encoding="utf-8") as f_output:
            cursor.copy_expert(SQL_for_file_output, f_output)
        move_files_to_version_folder(temp_quarter_details)
        # fp = open(path, 'w')
        # myFile = csv.writer(fp)
        # myFile.writerows(query_data)
        # fp.close()
    except Exception as e:
        logger.error(e, "view..")


def create_prediction_views(**request):
    """
           Create materialize views and corresponding index .
       :return:
       """
    exe_start = timeit.default_timer()

    logger.info("***************************** CREATE VIEW ***************************************")
    prediction_drop_views()
    prediction_quarter = request["quarter"]
    prediction_year = request["year"]
    quarter_year = request["year"]
    previous_quarter = ""
    previous_to_previous_quarter = ""
    previous_to_previous_previous_quarter = ""
    previous_year = ""
    previous_to_previous_year = ""
    previous_to_previous_previous_year = ""
    compensation_year = ""
    compensation_previous_year = ""
    compensation_previous_previous_year = ""
    directory = str(prediction_year) + "-" + str("prediction_quarter") + "model_" + datetime.strftime(datetime.now(),
                                                                                                      '%Y-%m-%d-%H-%M')
    output_dict = {}
    output_dict["model_version"] = directory
    output_dict['task_status'] = 'Published'
    serializer = model_health_Serializer(data=output_dict)
    if serializer.is_valid():
        serializer.save()
    view_name = ""
    mat_view_dict = {"pre_gem_master_yearly_view": "gem_master_view",
                     "pre_star_performer_yearly_view": "star_performer_master_view",
                     "pre_training_master_yearly_view": "training_data_master_view",
                     "pre_final_dis_pip_view": "dis_pip_master_view",
                     "pre_compensation_yearly_data": "compensation_master_view",
                     "pre_new_leave_yearly_data": "leave_data_master_view"}
    materialize_views = [
        predict_promotion_data_view, pre_gem_master_yearly_view,
        pre_star_performer_yearly_view, pre_training_master_yearly_view,
        pre_transaction_master_yearly_view, pre_final_dis_pip_view, pre_compensation_yearly_data,
        pre_new_leave_yearly_data, prediction_data_view]
    materialize_views_names = [
        "predict_promotion_data_view", "pre_gem_master_yearly_view",
        "pre_star_performer_yearly_view", "pre_training_master_yearly_view",
        "pre_transaction_master_yearly_view", "pre_final_dis_pip_view", "pre_compensation_yearly_data",
        "pre_new_leave_yearly_data", "prediction_data_view"]
    try:
        for index, name in enumerate(materialize_views_names):
            if name == 'pre_compensation_yearly_data':
                comp_date = execute_function(compensation_last_two_years.format(prediction_year))
                compensation_previous_year = comp_date[0]
                compensation_previous_previous_year = comp_date[1]
            elif name in ('pre_transaction_master_yearly_view', 'predict_promotion_data_view', 'prediction_data_view'):
                pass
            else:
                quarter_data = execute_function(
                    last_three_quarters.format(mat_view_dict[name], str(prediction_year) + prediction_quarter[1]))
                previous_year, previous_quarter = str(quarter_data[0])[:4], 'Q' + str(quarter_data[0])[4:6]
                previous_to_previous_year, previous_to_previous_quarter = str(quarter_data[1])[:4], 'Q' + str(
                    quarter_data[1])[4:6]
                previous_to_previous_previous_year, previous_to_previous_previous_quarter = str(quarter_data[2])[
                                                                                            :4], 'Q' + str(
                    quarter_data[2])[4:6]
            temp_dict = {"previous_quarter": previous_quarter,
                         "previous_to_previous_quarter": previous_to_previous_quarter,
                         "previous_to_previous_previous_quarter": previous_to_previous_previous_quarter,
                         "previous_year": previous_year,
                         "previous_to_previous_year": previous_to_previous_year,
                         "previous_to_previous_previous_year": previous_to_previous_previous_year,
                         "compensation_year": compensation_year,
                         "compensation_previous_year": compensation_previous_year,
                         "compensation_previous_previous_year": compensation_previous_previous_year}
            start = timeit.default_timer()
            # view_name = view_list[index]
            # if name == 'pre_compensation_yearly_data':
            query = eval(name).format(**temp_dict)
            cursor = get_cursor(query)  # Create view
            # create_index(view_list[index])  # Create all index
            # create_bu_index(view_list[index])  # Create all indexes based on specific columns.
            taken = round(timeit.default_timer() - start, 2)
        time_taken = round(timeit.default_timer() - exe_start, 2)
        temp_quarter_details = {"prediction_quarter": prediction_quarter, "prediction_year": prediction_year,
                                "previous_quarter": previous_quarter, "previous_year": previous_year,
                                "directory": directory}
        save_predict_data(temp_quarter_details)
        # print("({0}).Execution Time({1})".format(view_name, time_taken))
        logger.info("************************** END VIEW CREATION*****************************************")
        logger.info(" All View Created Successfully . Execution Time({0})".format(time_taken))

    except Exception as e:
        logger.error(e, "view..", view_name)
        logger.error("Error in create_views {0}: {1}".format(view_name, str(e)))


def prediction_drop_views():
    """
        Drop all materialize views .
    :return:
    """
    logger.info("***************************** DROP VIEW ***************************************")
    try:
        view_list = [
            'pre_gem_master_yearly_view',
            'pre_star_performer_yearly_view', 'pre_training_master_yearly_view', 'pre_transaction_master_yearly_view',
            'pre_final_dis_pip_view', 'pre_compensation_yearly_data', 'pre_new_leave_yearly_data',
            'prediction_data_view']
        for view_name in view_list:
            query = 'DROP MATERIALIZED VIEW IF EXISTS  buisness_suite.{0} CASCADE;'.format(view_name)
            get_cursor(query)  # Drop views
            # print("drop view::  ", view_name)
    except Exception as e:
        logger.error("Error in predction drop_views {0}:".format(str(e)))


def move_files_to_version_folder(temp_quarter_details):
    try:
        parent_dir = settings.MEDIA_ROOT
        path = os.path.join(parent_dir, temp_quarter_details["directory"])
        os.mkdir(path)
        os.chdir(r"C:\Users\User\Desktop\attrition_latest\attrition\attrition_mf_kedro_v2")
        os.system("kedro run")
        file_list = ["attrition_mf_model.pkl", "attrition_predictions.csv", "churn_surv_model.pkl", "model_health.pkl",
                     "treemap.csv"]
        for file_name in file_list:
            shutil.copy(
                r"C:\Users\User\Desktop\attrition_latest\attrition\attrition_mf_kedro_v2\data\06_models" + "/" + file_name,
                path)
        load_model_health_date(temp_quarter_details["directory"], temp_quarter_details, path)
    except Exception as e:
        logger.error("Error in move_files_to_version_folder {0}:".format(str(e)))


def load_model_health_date(directory, temp_quarter_details, path):
    try:
        output_dict = dict()
        model_health_data = pickle.load(open(path + "\model_health.pkl", 'rb'))
        output_dict['training_rows'] = model_health_data["training_rows"]
        output_dict['testing_rows'] = model_health_data["testing_rows"]
        output_dict['train_accuracy'] = round(model_health_data["train_accuracy"], 3)
        output_dict['test_accuracy'] = round(model_health_data["test_accuracy"], 3)
        output_dict['train_precision'] = round(model_health_data["train_precision"], 3)
        output_dict['test_precision'] = round(model_health_data["test_precision"], 3)
        output_dict['train_recall'] = round(model_health_data["train_recall"], 3)
        output_dict['test_recall'] = round(model_health_data["test_recall"], 3)
        output_dict['train_f1'] = round(model_health_data["train_f1"], 3)
        output_dict['test_f1'] = round(model_health_data["test_f1"], 3)
        output_dict['auc_test'] = round(model_health_data["auc_test"], 3)
        output_dict['train_year'] = temp_quarter_details["previous_year"]
        output_dict['train_quarter'] = temp_quarter_details["previous_quarter"]
        output_dict['pred_quarter'] = temp_quarter_details["prediction_quarter"]
        output_dict['pred_year'] = temp_quarter_details["prediction_year"]
        output_dict['task_status'] = 'completed'
        output_dict['model_version'] = directory
        model_object = kedro_modelhealth_output.objects.get(model_version=directory)
        serializer = model_health_put_Serializer(model_object, data=output_dict)
        if serializer.is_valid():
            serializer.save()
    except Exception as e:
        logger.info("exception occurred5: ", e)
