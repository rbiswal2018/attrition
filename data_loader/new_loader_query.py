########################Attrition_querys#########################################################

update_cost_centre = "update buisness_suite.consolidated_demographics set cost_centre = regexp_replace(cost_centre, '^\d+-', '')"
update_division = "update buisness_suite.consolidated_demographics set division = regexp_replace(division, '^\d+-', '')"
update_business_unit = "update buisness_suite.consolidated_demographics set business_unit = regexp_replace(business_unit, '^\d+-', '')"
update_function_name = "update buisness_suite.consolidated_demographics set function_name = regexp_replace(function_name, '^\d+-', '')"
update_department = "update buisness_suite.consolidated_demographics set department = regexp_replace(department, '^\d+-', '')"
update_sub_department = "update buisness_suite.consolidated_demographics set sub_department = regexp_replace(sub_department, '^\d+-', '')"
update_state = "update buisness_suite.consolidated_demographics set state = regexp_replace(state, '^.+-', '')"

star_performer_master_view = """create MATERIALIZED VIEW buisness_suite.star_performer_master_view
                        TABLESPACE pg_default
                        as
                        select *,case when quarter_name ='Q4' then (given_year-1)::text
                        else given_year::text end  as "year" from (
                        SELECT star_performer_master.sapcode,
                            star_performer_master.name_of_the_employee,
                            star_performer_master.location,
                            star_performer_master.month,
                            star_performer_master.year::integer  as given_year,
                            star_performer_master.department,
                                CASE
                                    WHEN star_performer_master.month ~* '(January|February|March)'::text THEN 'Q4'::text
                                    WHEN star_performer_master.month ~* '(April|May|June)'::text THEN 'Q1'::text
                                    WHEN star_performer_master.month ~* '(July|August|September)'::text THEN 'Q2'::text
                                    WHEN star_performer_master.month ~* '(October|November|December)'::text THEN 'Q3'::text
                                    ELSE star_performer_master.month
                                END AS quarter_name
                           FROM buisness_suite.star_performer_master)q
                        WITH DATA;"""
#################################################################################
gem_master_view = """CREATE MATERIALIZED VIEW buisness_suite.gem_master_view
                TABLESPACE pg_default
                AS select *,case when quarter_name ='Q4' then (given_year-1)::text 
                else given_year::text end as year from (
                SELECT gem_master.sap_code,
                    gem_master.gem_name,
                    gem_master.quarter,
                    gem_master.year::integer as given_year,
                        CASE
                            WHEN gem_master.quarter ~* '(QUARTER-1)'::text THEN 'Q1'::text
                            WHEN gem_master.quarter ~* '(QUARTER-2)'::text THEN 'Q2'::text
                            WHEN gem_master.quarter ~* '(QUARTER-3)'::text THEN 'Q3'::text
                            ELSE 'Q4'::text
                        END AS quarter_name
                   FROM buisness_suite.gem_master)q
                WITH DATA;"""
#################################################################################

compensation_master_view = """CREATE MATERIALIZED VIEW buisness_suite.compensation_master_view as
 SELECT distinct q1.employee_id,
    q1.employee_name,
    q1.monthly_compensation_gross,
    q1.annual_compensation,
    q1.fixed_component,
    q1.variable_component,
    q1.last_annual_hike_percentage,
    q1.last_anuual_hike_date,
    q1.given_year,
    q1.month,
    q1.quarter_name,
        CASE
            WHEN q1.quarter_name = 'Q4'::text THEN (q1.given_year - 1)::text
            ELSE q1.given_year::text
        END AS year
   FROM ( SELECT distinct q.employee_id,
            q.employee_name,
            q.monthly_compensation_gross,
            q.annual_compensation,
            q.fixed_component,
            q.variable_component,
            q.last_annual_hike_percentage,
            q.last_anuual_hike_date,
            q.given_year,
            q.month,
                CASE
                    WHEN q.month = ANY (ARRAY[1, 2, 3]) THEN 'Q4'::text
                    WHEN q.month = ANY (ARRAY[4, 5, 6]) THEN 'Q1'::text
                    WHEN q.month = ANY (ARRAY[7, 8, 9]) THEN 'Q2'::text
                    WHEN q.month = ANY (ARRAY[0]) THEN ''::text
                    ELSE 'Q3'::text
                END AS quarter_name
           FROM ( SELECT compensation_master.employee_id,
                    compensation_master.employee_name,
                    compensation_master.monthly_compensation_gross,
                    compensation_master.annual_compensation,
                    compensation_master.fixed_component,
                    compensation_master.variable_component,
                    compensation_master.last_annual_hike_percentage,
                    compensation_master.last_anuual_hike_date,
                        CASE
                            WHEN 
                            compensation_master.last_anuual_hike_date IS NOT NULL 
                            THEN 
                            date_part('year'::text, last_anuual_hike_date)::integer
                            ELSE 0
                        END AS given_year,
                        CASE
                            WHEN last_anuual_hike_date IS NOT NULL        
                            THEN date_part('month'::text, last_anuual_hike_date)::integer
                            ELSE 0
                        END AS month
                    FROM buisness_suite.compensation_master) q) q1
WITH DATA;

"""
#################################################################################
compensation_latest_year_view = """create MATERIALIZED VIEW buisness_suite.compensation_latest_year_view 
TABLESPACE pg_default
                AS select distinct on(employee_id) employee_id,annual_compensation,"year" from buisness_suite.compensation_master_view  where (employee_id,year)
in(
select distinct employee_id,max(year)
from buisness_suite.compensation_master_view group by employee_id
)
with data; 
"""
#################################################################################
annual_range = """CREATE MATERIALIZED VIEW buisness_suite.annual_range
TABLESPACE pg_default
AS SELECT t1.employee_id,
    t1.annual_compensation,
    s.concat AS annual_compensation_range
   FROM buisness_suite.compensation_latest_year_view t1
     LEFT JOIN ( SELECT q.min,
            q.max,
            q.concat,
            q.bucket
           FROM ( WITH min_max AS (
                         SELECT round(min(compensation_latest_year_view_1.annual_compensation::numeric)) AS min_val,
                            round('3500000'::numeric) AS max_val
                           FROM buisness_suite.compensation_latest_year_view compensation_latest_year_view_1
                        )
                 SELECT round(min(compensation_latest_year_view.annual_compensation::numeric)) AS min,
                    round(max(compensation_latest_year_view.annual_compensation::numeric)) AS max,
                        CASE
                            WHEN round(max(compensation_latest_year_view.annual_compensation::numeric)) >= '3500000'::numeric THEN concat('3500000', '>', '')
                            ELSE concat(round(min(compensation_latest_year_view.annual_compensation::numeric)), '-', round(max(compensation_latest_year_view.annual_compensation::numeric)))
                        END AS concat,
                    width_bucket(round(compensation_latest_year_view.annual_compensation::numeric), min_max.min_val, min_max.max_val, 10) AS bucket
                   FROM buisness_suite.compensation_latest_year_view,
                    min_max
                  WHERE compensation_latest_year_view.annual_compensation IS NOT NULL
                  GROUP BY (width_bucket(round(compensation_latest_year_view.annual_compensation::numeric), min_max.min_val, min_max.max_val, 10))
                  ORDER BY (width_bucket(round(compensation_latest_year_view.annual_compensation::numeric), min_max.min_val, min_max.max_val, 10))) q) s ON round(t1.annual_compensation::numeric) >= s.min AND round(t1.annual_compensation::numeric) <= s.max
WITH DATA;
"""
#################################################################################

dis_and_pip_master_view = """CREATE MATERIALIZED VIEW buisness_suite.dis_pip_master_view
TABLESPACE pg_default
AS 
select distinct q1.sap_code,
	q1.type_of_issue,
    q1.completed_ir_received_on_case_registered_in_dap,
    q1.given_year,
    q1.month,
    q1.quarter_name,
        CASE
            WHEN q1.quarter_name = 'Q4'::text THEN (q1.given_year - 1)::int
            ELSE q1.given_year::int
        END AS year
   FROM ( SELECT DISTINCT q.sap_code,
   			q.type_of_issue,
            q.completed_ir_received_on_case_registered_in_dap,
            q.year AS given_year,
            q.month,
                CASE
                    WHEN q.month = ANY (ARRAY[1, 2, 3]) THEN 'Q4'::text
                    WHEN q.month = ANY (ARRAY[4, 5, 6]) THEN 'Q1'::text
                    WHEN q.month = ANY (ARRAY[7, 8, 9]) THEN 'Q2'::text
                    ELSE 'Q3'::text
                END AS quarter_name
           FROM ( SELECT DISTINCT sap_code,
           			type_of_issue,
           			regexp_replace(completed_ir_received_on_case_registered_in_dap, '-', ' ','gi') as completed_ir_received_on_case_registered_in_dap,
                    date_part('year'::text, completed_ir_received_on_case_registered_in_dap::date)::integer AS year,
                    date_part('month'::text, completed_ir_received_on_case_registered_in_dap::date)::integer AS month
                   FROM buisness_suite.dis_and_pip_master
                  WHERE (type_of_issue ~* ('Disciplinary'::text) or type_of_issue ~* ('pip'::text) )
                  )q) q1
WITH DATA;"""
#################################################################################
training_data_master_view = """CREATE MATERIALIZED VIEW buisness_suite.training_data_master_view as
SELECT distinct q.candidate_sap_code,
    q.training_zone,
    q.training_program,
    q.work_contract,
    q.training_start_date,
    q.training_given_year,
    q.training_month,
    q.quarter_name,
    q.training_year as year
   FROM ( SELECT distinct q1.candidate_sap_code,
            q1.training_zone,
            q1.training_program,
            q1.work_contract,
            q1.training_start_date,
            q1.training_given_year,
            q1.training_month,
            q1.quarter_name,
                CASE
                    WHEN q1.quarter_name = 'Q4'::text THEN (q1.training_given_year - 1)::text
                    ELSE q1.training_given_year::text
                END AS training_year
           FROM ( SELECT DISTINCT training_master.candidate_sap_code,
                    btrim(training_master.training_zone) AS training_zone,
                    training_master.program AS training_program,
                    btrim(training_master.work_contract) AS work_contract,
                    start training_start_date,
                    date_part('year'::text, start)::integer AS training_given_year,
                    date_part('month'::text,start)::integer AS training_month,
                        CASE
                            WHEN date_part('month'::text,start)::integer = ANY (ARRAY[1, 2, 3]) THEN 'Q4'::text
                            WHEN date_part('month'::text,start)::integer = ANY (ARRAY[4, 5, 6]) THEN 'Q1'::text
                            WHEN date_part('month'::text, start)::integer = ANY (ARRAY[7, 8, 9]) THEN 'Q2'::text
                            ELSE 'Q3'::text
                        END AS quarter_name
                   FROM buisness_suite.training_master
                  WHERE start >= '2017-01-02'::date) q1) q
WITH DATA;"""

############################################################################################
training_with_category_data = """CREATE MATERIALIZED VIEW buisness_suite.training_with_category_data as
 SELECT t.candidate_sap_code,
    t.training_zone,
    t.training_program,
    t.work_contract,
    t.training_start_date,
    t.training_given_year,
    t.training_month,
    t.quarter_name,
    t.training_year,
    c.program_category
   FROM buisness_suite.training_data_master_view t
     LEFT JOIN buisness_suite.training_category c ON upper(btrim(t.training_program)) = btrim(c.program_name)
WITH DATA;
"""
############################################################################################
consolidated_dempgraphic_view = """ CREATE MATERIALIZED VIEW buisness_suite.consolidated_demoghaphic_master_view
TABLESPACE pg_default
AS SELECT q.employee_id,
    q.band,
    q.employee_group,
    case
    when  q.employment_details_last_working_date is null then q.age::integer
    when employment_details_last_working_date is not null  then date_part('year'::text,q.employment_details_last_working_date::date)-date_part('year'::text,q.head_count_date_of_birth::date)
    END AS age,
    q.age_group,
    q.head_count_date_of_birth,
    q.joining_month,
    q.lsr,
    q.reviewing_officer_sap_code,
    q.reviewing_officer_name,
    q.languages_known1,
    q.languages_known2,
    q.languages_known3,
    q.languages_known4,
    q.previous_experience_in_years,
    q.previous_company,
    q.marital_status,
    q.highest_qualification,
    q.source_of_recruitment,
    case
    when  q.employment_details_last_working_date is null then q.total_experience_months_in_mf::numeric
    when q.employment_details_last_working_date is not null  THEN (date_part('year'::text,q.employment_details_last_working_date::date)-date_part('year'::text,q.employment_details_date_of_hire::date))*12::numeric
    END AS total_experience_months_in_mf,
    case
    when  q.employment_details_last_working_date is null then q.experience_in_current_role_in_months::numeric
    when employment_details_last_working_date is not null then (date_part('year'::text,q.employment_details_last_working_date::date)-date_part('year'::text,q.employment_details_date_of_hire::date))*12::numeric
    END AS experience_in_current_role_in_months,
    q.mother_tongue,
    q.first_name,
    q.middle_name,
    q.last_name,
    q.event_date,
    q.employee_status,
    q.country,
    q.event,
    q.no_of_dependents,
    q.national_id,
    q.current_address,
    q.gender,
    q.flexi_data_date_of_birth,
    q.transaction_sequence_number,
    q.event_reason,
    q.position_code,
    q.grade_designation,
    q.position_entry_date,
    q.type_of_action,
    q.company,
    q.function_name,
    q.division,
    q.business_unit,
    q.department,
    q.sub_department,
    q.sub_department_4,
    q.sub_department_5,
    q.cost_centre,
    q.cost_centre_cost_center_name,
    q.country_group,
    q.zone,
    q.zonal_office,
    q.ro_location_group,
    q.personnel_area,
    q.personnel_sub_area,
    q.branch,
    q.time_zone,
    q.transfer_request,
    q.supervisor,
    q.manager_user_sys_id,
    q.job_code,
    q.role_designation,
    q.working_days_per_week,
    q.employee_sub_group,
    q.employment_type,
    q.notice_period_in_days,
    q.transfer_previous_location,
    q.transfer_type,
    q.state,
    q.work_contract,
    q.organizational_key,
    q.probationary_period_number,
    q.holiday_calendar,
    q.work_schedule,
    q.time_profile,
    q.employment_details_date_of_hire,
    q.employment_details_last_working_date,
    q.employment_details_group_hire_date,
    q.employment_details_eligible_to_rehire,
    q.last_working_date_year,
    q.quarter_name,
    q.last_working_date_month,
    q.pay_scale_type,
    q.pay_scale_area,
    q.pay_scale_group,
    q.pay_scale_level,
    q.final_pay_grade,
    q.date_of_resignation,
    q.reason_of_leaving,
        CASE
            WHEN q.quarter_name = 'Q4'::text THEN (q.last_working_date_year - 1)::text
            ELSE q.last_working_date_year::text
        END AS quarter_year
   FROM ( SELECT DISTINCT consolidated_demographics.employee_id,
            consolidated_demographics.band,
            consolidated_demographics.employee_group,
            consolidated_demographics.age,
            consolidated_demographics.age_group,
            consolidated_demographics.date_of_birth AS head_count_date_of_birth,
            consolidated_demographics.joining_month,
            consolidated_demographics.lsr,
            consolidated_demographics.reviewing_officer_sap_code,
            consolidated_demographics.reviewing_officer_name,
            consolidated_demographics.languages_known1,
            consolidated_demographics.languages_known2,
            consolidated_demographics.languages_known3,
            consolidated_demographics.languages_known4,
            consolidated_demographics.previous_experience_in_years,
            consolidated_demographics.previous_company,
            consolidated_demographics.marital_status,
            consolidated_demographics.highest_qualification,
            consolidated_demographics.source_of_recruitment,
            consolidated_demographics.total_experience_months_in_mf,
            consolidated_demographics.experience_in_current_role_in_months,
            consolidated_demographics.mother_tongue,
            consolidated_demographics.first_name,
            consolidated_demographics.middle_name,
            consolidated_demographics.last_name,
            consolidated_demographics.event_date,
            consolidated_demographics.employee_status,
            consolidated_demographics.country,
            consolidated_demographics.event,
            consolidated_demographics.no_of_dependents,
            consolidated_demographics.national_id,
            consolidated_demographics.current_address,
            consolidated_demographics.gender,
            consolidated_demographics.date_of_birth AS flexi_data_date_of_birth,
            consolidated_demographics.transaction_sequence_number,
            consolidated_demographics.event_reason,
            consolidated_demographics.position_code,
            consolidated_demographics.grade_designation,
            consolidated_demographics.position_entry_date,
            consolidated_demographics.type_of_action,
            consolidated_demographics.company,
            consolidated_demographics.function_name,
            consolidated_demographics.division,
            consolidated_demographics.business_unit,
            consolidated_demographics.department,
            consolidated_demographics.sub_department,
            consolidated_demographics.sub_department_4,
            consolidated_demographics.sub_department_5,
            consolidated_demographics.cost_centre,
            consolidated_demographics.cost_centre_cost_center_name,
            consolidated_demographics.country_group,
            consolidated_demographics.zone,
            consolidated_demographics.zonal_office,
            consolidated_demographics.ro_location_group,
            consolidated_demographics.personnel_area,
            consolidated_demographics.personnel_sub_area,
            consolidated_demographics.branch,
            consolidated_demographics.time_zone,
            consolidated_demographics.transfer_request,
            consolidated_demographics.supervisor,
            consolidated_demographics.manager_user_sys_id,
            consolidated_demographics.job_code,
            consolidated_demographics.role_designation,
            consolidated_demographics.working_days_per_week,
            consolidated_demographics.employee_sub_group,
            consolidated_demographics.employment_type,
            consolidated_demographics.notice_period_in_days,
            consolidated_demographics.transfer_previous_location,
            consolidated_demographics.transfer_type,
            consolidated_demographics.state,
            consolidated_demographics.work_contract,
            consolidated_demographics.organizational_key,
            consolidated_demographics.probationary_period_number,
            consolidated_demographics.holiday_calendar,
            consolidated_demographics.work_schedule,
            consolidated_demographics.time_profile,
            consolidated_demographics.employment_details_date_of_hire,
            consolidated_demographics.employment_details_last_working_date,
            consolidated_demographics.employment_details_group_hire_date,
            consolidated_demographics.employment_details_eligible_to_rehire,
            date_part('year'::text, consolidated_demographics.employment_details_last_working_date::date)::integer AS last_working_date_year,
                CASE
                    WHEN date_part('month'::text, consolidated_demographics.employment_details_last_working_date::date)::integer = ANY (ARRAY[1, 2, 3]) THEN 'Q4'::text
                    WHEN date_part('month'::text, consolidated_demographics.employment_details_last_working_date::date)::integer = ANY (ARRAY[4, 5, 6]) THEN 'Q1'::text
                    WHEN date_part('month'::text, consolidated_demographics.employment_details_last_working_date::date)::integer = ANY (ARRAY[7, 8, 9]) THEN 'Q2'::text
                    WHEN date_part('month'::text, consolidated_demographics.employment_details_last_working_date::date)::integer = ANY (ARRAY[10, 11, 12]) THEN 'Q3'::text
                    ELSE NULL::text
                END AS quarter_name,
            date_part('month'::text, consolidated_demographics.employment_details_last_working_date::date)::integer AS last_working_date_month,
            consolidated_demographics.pay_scale_type,
            consolidated_demographics.pay_scale_area,
            consolidated_demographics.pay_scale_group,
            consolidated_demographics.pay_scale_level,
            consolidated_demographics.final_pay_grade,
            consolidated_demographics.date_of_resignation,
            consolidated_demographics.reason_of_leaving
           FROM buisness_suite.consolidated_demographics where age is not null) q
WITH DATA;"""

#############################################################################################


promotion_rating_view = """CREATE MATERIALIZED VIEW buisness_suite.promotion_rating_view
TABLESPACE pg_default
AS SELECT DISTINCT ON (promotion_and_rating_master.employee_id) promotion_and_rating_master.employee_id,
    promotion_and_rating_master.last_promotion_date
   FROM buisness_suite.promotion_and_rating_master
  ORDER BY promotion_and_rating_master.employee_id, promotion_and_rating_master.last_promotion_date DESC
WITH DATA;
"""
#############################################################################################


predict_promotion_rating_view = """CREATE MATERIALIZED VIEW buisness_suite.predict_promotion_rating_view
TABLESPACE pg_default
AS SELECT DISTINCT ON (promotion_and_rating_master.employee_id) promotion_and_rating_master.employee_id,
    promotion_and_rating_master.last_promotion_date,
    date_part('year'::text,promotion_and_rating_master.last_promotion_date) AS promotion_year
   FROM buisness_suite.promotion_and_rating_master
  ORDER BY promotion_and_rating_master.employee_id, promotion_and_rating_master.last_promotion_date DESC
WITH DATA;
"""

#####################################################################################################333


final_view = """
CREATE MATERIALIZED VIEW buisness_suite.final_view
TABLESPACE pg_default
AS SELECT q.employee_id,
    q.band,
    q.employee_group,
    q.age,
    q.age_group,
    q.joining_month,
    q.lsr,
    q.reviewing_officer_sap_code,
    q.reviewing_officer_name,
    q.languages_known1,
    q.languages_known2,
    q.languages_known3,
    q.languages_known4,
    q.previous_experience_in_years,
    q.previous_company,
    q.marital_status,
    q.highest_qualification,
    q.source_of_recruitment,
    q.total_experience_months_in_mf,
    q.total_experience_months_in_mf_range,
    q.experience_in_current_role_in_months,
    q.mother_tongue,
    q.first_name,
    q.middle_name,
    q.last_name,
    q.event_date,
    q.employee_status,
    q.country,
    q.event,
    q.no_of_dependents,
    q.national_id,
    q.current_address,
    q.gender,
    q.flexi_data_date_of_birth,
    q.transaction_sequence_number,
    q.event_reason,
    q.position_code,
    q.grade_designation,
    q.position_entry_date,
    q.type_of_action,
    q.company,
    q.function_name,
    q.division,
    q.business_unit,
    q.department,
    q.sub_department,
    q.sub_department_4,
    q.sub_department_5,
    q.cost_centre,
    q.cost_centre_cost_center_name,
    q.country_group,
    q.zone,
    q.zonal_office,
    q.ro_location_group,
    q.personnel_area,
    q.personnel_sub_area,
    q.branch,
    q.time_zone,
    q.transfer_request,
    q.supervisor,
    q.manager_user_sys_id,
    q.job_code,
    q.role_designation,
    q.working_days_per_week,
    q.employee_sub_group,
    q.employment_type,
    q.notice_period_in_days,
    q.transfer_previous_location,
    q.transfer_type,
    q.state,
    q.work_contract,
    q.organizational_key,
    q.probationary_period_number,
    q.holiday_calendar,
    q.work_schedule,
    q.time_profile,
    q.employment_details_date_of_hire,
    q.employment_details_last_working_date,
    q.employment_details_group_hire_date,
    q.employment_details_eligible_to_rehire,
    q.last_working_date_year,
    q.quarter_name,
    q.last_working_date_month,
    q.pay_scale_type,
    q.pay_scale_area,
    q.pay_scale_group,
    q.pay_scale_level,
    q.final_pay_grade,
    q.date_of_resignation,
    q.reason_of_leaving,
    q.quarter_year,
    q.gem_sap_code,
    q.star_sap_code,
    q.training_sap_code,
    q.annual_compensation,
    q.annual_compensation_range,
    q.disc_sap_code,
    q.pip_sap_code,
    q.pip_issue,
    q.disc_issue,
    q.pr_employee_id,
    q.rc_employee_id,
    q.source_data,
        CASE
            WHEN q.star_sap_code IS NOT NULL THEN 'YES_STAR'::text
            ELSE 'NO_STAR'::text
        END AS star_performer,
        CASE
            WHEN q.gem_sap_code IS NOT NULL THEN 'YES_GEM'::text
            ELSE 'NO_GEM'::text
        END AS gem_performe,
        CASE
            WHEN q.training_sap_code IS NULL THEN 'Not Trained'::text
            ELSE 'Trained'::text
        END AS training_data,
        CASE
            WHEN q.pip_sap_code IS NOT NULL AND q.pip_issue ~* '(PIP)'::text THEN 'YES_PIP'::text
            ELSE 'NO_PIP'::text
        END AS pip_data,
        CASE
            WHEN q.disc_sap_code IS NOT NULL AND q.disc_issue ~* '(Disciplinary)'::text THEN 'YES_DIC'::text
            ELSE 'NO_DIC'::text
        END AS dic_data,
        CASE
            WHEN q.rc_employee_id IS NULL THEN 'NA'::text
            ELSE q.source_data
        END AS recruitement_data,
        CASE
            WHEN q.highest_qualification = ANY (ARRAY['Diploma in business administration'::text, '<xth'::text, 'XIIth Standard / PUC'::text, 'Puc'::text, 'Xth Standard'::text, 'ITI'::text, '< Xth'::text, 'Diploma'::text, 'Diploma in Computer Application'::text, 'Diploma in Mechanical Engineering'::text, 'Diploma in Electrical Engineering'::text, 'Advance Diploma'::text]) THEN 'Under Graduate'::text
            WHEN q.highest_qualification = ANY (ARRAY['Hotel management'::text, 'Mathematics'::text, 'Ca (inter)'::text, 'Business management'::text, 'Cpa'::text, 'C.i.a. - internal audit'::text, 'Ba,bl'::text, 'B.m.s.'::text, 'B.arch.'::text, 'B.SC'::text, 'B.com'::text, 'B.Com'::text, 'BA'::text, 'BA, BL'::text, 'BIT'::text, 'BL'::text, 'B.Pharm'::text, 'Others'::text, 'Armed Forces Services'::text, 'B.A.'::text, 'B.B.M'::text, 'LLB'::text, 'B.B.A'::text, 'B.C.A'::text, 'B.Ed.'::text, 'B.P.E.'::text, 'BCS'::text, 'B.Tech'::text, 'B.Tech.'::text, 'Bachelor of Journalism'::text, 'Bachelor of Social Works'::text, 'BE'::text, 'Graduate'::text, 'BL'::text, 'B.Sc. (Tech)'::text, 'BHM'::text, 'BA, BL'::text, 'B.Pharm'::text, 'Bachelor of Social, Legal Sciences'::text, 'BDS'::text]) THEN 'Graduate'::text
            WHEN q.highest_qualification = ANY (ARRAY['M.a.m.'::text, 'M.p.m.'::text, 'Pgpm'::text, 'Pgdpe'::text, 'Pg programme in media management'::text, 'Mhrm'::text, 'M.phil.'::text, 'M.tech.'::text, 'MA'::text, 'MMM'::text, 'MBA'::text, 'MSW'::text, 'MFM'::text, 'PGDBA'::text, 'LLM'::text, 'Post Diploma'::text, 'Executive Diploma in HR Management'::text, 'ME'::text, 'MCA'::text, 'PG Diploma'::text, 'M.Sc.'::text, 'M.Com.'::text, 'MscIT'::text, 'ML'::text, 'Post Graduate'::text, 'PGDM'::text, 'PGDHRM'::text, 'PGDBM'::text, 'ML'::text, 'M.Ed.'::text]) THEN 'Post Graduate'::text
            ELSE 'N\A'::text
        END AS higher_qualification_category,
        CASE
            WHEN q.reason_of_leaving = ANY (ARRAY['INVOL-ABSC-BCK-Separation - Termination (Backdated) - Absconding'::text, 'INVOL-ABSC-Separation - Termination - Absconding'::text, ' Involuntary'::text, 'Involuntary'::text, ' Involuntary (Backdated)'::text, 'Involuntary (Backdated)'::text, 'INVOL-BCK-Separation - Involuntary (Backdated)'::text, 'INVOL-DISC-Separation - Termination due to Disciplinary action'::text, 'INVOL-INTEG-BCK-Separation - Termination (Backdated) due to integrity issues'::text, 'INVOL-INTEG-Separation - Termination due to integrity issues'::text, 'INVOL-Separation - Involuntary'::text, ' Termination due to Disciplinary action'::text, 'Termination due to Disciplinary action'::text, ' Termination due to performance issues'::text, 'Termination due to performance issues'::text, ' Termination '::text, 'Termination'::text, 'INVOL-ABSC-Separation - Termination - Absconding'::text, ' Termination due to integrity issues'::text, 'Termination due to integrity issues'::text, 'Termination (Backdated) - Absconding'::text, 'Termination (Backdated) due to integrity issues'::text, 'Termination - Absconding'::text]) THEN 'Involuntary'::text
            WHEN q.reason_of_leaving = 'N/A'::text THEN 'N/A'::text
            WHEN q.reason_of_leaving = ANY (ARRAY['Death'::text, 'Retirement'::text, ' Retirement'::text]) THEN 'Death or retirement'::text
            WHEN q.reason_of_leaving IS null and q.employment_details_last_working_date is not null THEN 'voluntary'::text
			WHEN q.reason_of_leaving IS NULL THEN NULL::text
            ELSE 'voluntary'::text
        END AS reason_of_leaving_group,
        CASE
            WHEN q.source_data = ANY (ARRAY['Walkin'::text, 'Consultant'::text, 'Dealer Preference Candidate'::text, 'Campus Connect'::text, 'Naukri'::text, 'Job Fair'::text, 'Dealer Source'::text, 'Teamlease absorption'::text, 'MMFSL Career Website'::text, 'Career Website'::text]) THEN 'External'::text
            WHEN q.source_data = ANY (ARRAY['Referral'::text, 'Job Portal'::text, 'Group transfer'::text, 'Employee Referral'::text, 'IJP'::text]) THEN 'Internal'::text
            WHEN q.source_data = ANY (ARRAY['Rejoining'::text, 'HTD'::text, 'Others'::text, 'Rehire'::text, 'Graduate Trainee'::text, 'GT*'::text]) THEN 'Others'::text
            WHEN q.source_data = ANY (ARRAY['Special Projects'::text, 'Special projects '::text, 'Project Ghost'::text, 'Project-Swayam'::text, 'Special Projects*'::text]) THEN 'Projects'::text
            ELSE 'NA'::text
        END AS recruitement_data_category,
    q.business_service_function,
    q.business_service_function_cat,
    q.major_function,
    q.major_service_function_cat
   FROM ( SELECT DISTINCT c.employee_id,
            c.band,
            c.employee_group,
            c.age,
            c.age_group,
            c.joining_month,
            c.lsr,
            c.reviewing_officer_sap_code,
            c.reviewing_officer_name,
            c.languages_known1,
            c.languages_known2,
            c.languages_known3,
            c.languages_known4,
            c.previous_experience_in_years,
            c.previous_company,
            c.marital_status,
            c.highest_qualification,
            c.source_of_recruitment,
            c.total_experience_months_in_mf,
            ter.total_experience_months_in_mf_range,
            c.experience_in_current_role_in_months,
            c.mother_tongue,
            c.first_name,
            c.middle_name,
            c.last_name,
            c.event_date,
            c.employee_status,
            c.country,
            c.event,
            c.no_of_dependents,
            c.national_id,
            c.current_address,
            c.gender,
            c.flexi_data_date_of_birth,
            c.transaction_sequence_number,
            c.event_reason,
            c.position_code,
            c.grade_designation,
            c.position_entry_date,
            c.type_of_action,
            c.company,
            c.function_name,
            c.division,
            c.business_unit,
            c.department,
            c.sub_department,
            c.sub_department_4,
            c.sub_department_5,
            c.cost_centre,
            c.cost_centre_cost_center_name,
            c.country_group,
            c.zone,
            c.zonal_office,
            c.ro_location_group,
            c.personnel_area,
            c.personnel_sub_area,
            c.branch,
            c.time_zone,
            c.transfer_request,
            c.supervisor,
            c.manager_user_sys_id,
            c.job_code,
            c.role_designation,
            c.working_days_per_week,
            c.employee_sub_group,
            c.employment_type,
            c.notice_period_in_days,
            c.transfer_previous_location,
            c.transfer_type,
            c.state,
            c.work_contract,
            c.organizational_key,
            c.probationary_period_number,
            c.holiday_calendar,
            c.work_schedule,
            c.time_profile,
            c.employment_details_date_of_hire,
            c.employment_details_last_working_date,
            c.employment_details_group_hire_date,
            c.employment_details_eligible_to_rehire,
            c.last_working_date_year,
            c.quarter_name,
            c.last_working_date_month,
            c.pay_scale_type,
            c.pay_scale_area,
            c.pay_scale_group,
            c.pay_scale_level,
            c.final_pay_grade,
            c.date_of_resignation,
            c.reason_of_leaving,
            c.quarter_year,
            g.sap_code AS gem_sap_code,
            s.sapcode AS star_sap_code,
            t.candidate_sap_code AS training_sap_code,
            comp.annual_compensation,
            comp.annual_compensation_range,
            dpd.sap_code AS disc_sap_code,
            dpp.sap_code AS pip_sap_code,
            dpd.type_of_issue AS disc_issue,
            dpp.type_of_issue AS pip_issue,
            pr.employee_id AS pr_employee_id,
            rc.employee_id AS rc_employee_id,
            rc.source AS source_data,
            bsf.business_service_function,
            bsf.function_category AS business_service_function_cat,
            msf.major_function_name AS major_function,
            msf.function_category AS major_service_function_cat
           FROM buisness_suite.consolidated_demoghaphic_master_view c
             LEFT JOIN buisness_suite.gem_master_view g ON g.sap_code = c.employee_id
             LEFT JOIN buisness_suite.star_performer_master_view s ON s.sapcode = c.employee_id
             LEFT JOIN buisness_suite.training_data_master_view t ON t.candidate_sap_code = c.employee_id
             LEFT JOIN buisness_suite.annual_range comp ON comp.employee_id = c.employee_id
             LEFT JOIN buisness_suite.dis_pip_master_view dpd ON dpd.sap_code = c.employee_id AND dpd.type_of_issue ~* 'Disciplinary'::text
             LEFT JOIN buisness_suite.dis_pip_master_view dpp ON dpp.sap_code = c.employee_id AND dpp.type_of_issue ~* 'PIP'::text
             LEFT JOIN buisness_suite.promotion_rating_view pr ON pr.employee_id = c.employee_id
             LEFT JOIN buisness_suite.recruitement_master rc ON rc.employee_id = c.employee_id
             LEFT JOIN buisness_suite.total_experience_range ter ON ter.employee_id = c.employee_id
             LEFT JOIN buisness_suite.business_service_functions bsf ON bsf.cost_centre::text = c.cost_centre
             LEFT JOIN buisness_suite.major_service_functions msf ON msf.cost_centre::text = c.cost_centre) q
WITH DATA;
"""
############################################################################3


#############################################################################################


#############################################################################################


pre_gem_master_yearly_view = """
CREATE MATERIALIZED VIEW buisness_suite.pre_gem_master_yearly_view
TABLESPACE pg_default
AS 
SELECT gem_master_view.sap_code,
    count(
        CASE
            WHEN gem_master_view.quarter_name = '{previous_to_previous_previous_quarter}'::text AND gem_master_view.year = '{previous_to_previous_previous_year}'::text THEN gem_master_view.quarter
            ELSE NULL::text
        END) AS gem_prev_to_prev_prev_quarter,
    count(
        CASE
            WHEN gem_master_view.quarter_name = '{previous_to_previous_quarter}'::text AND gem_master_view.year = '{previous_to_previous_year}'::text THEN gem_master_view.quarter
            ELSE NULL::text
        END) AS gem_prev_to_prev_quarter,
    count(
        CASE
            WHEN gem_master_view.quarter_name = '{previous_quarter}'::text AND gem_master_view.year = '{previous_year}'::text THEN gem_master_view.quarter
            ELSE NULL::text
        END) AS gem_previous_quarter
   FROM buisness_suite.gem_master_view
  GROUP BY gem_master_view.sap_code
WITH DATA;"""

#############################################################################################


pre_star_performer_yearly_view = """create  MATERIALIZED VIEW buisness_suite.pre_star_performer_yearly_view
TABLESPACE pg_default
AS SELECT star_performer_master_view.sapcode,
    count(
        CASE
            WHEN star_performer_master_view.quarter_name = '{previous_to_previous_previous_quarter}'::text AND star_performer_master_view.year = '{previous_to_previous_previous_year}'::text THEN star_performer_master_view.month
            ELSE NULL::text
        END) AS star_prev_to_prev_prev_quarter,
    count(CASE
        WHEN star_performer_master_view.quarter_name = '{previous_to_previous_quarter}'::text AND star_performer_master_view.year = '{previous_to_previous_year}'::text THEN star_performer_master_view.month
        ELSE NULL::text
    END) AS star_prev_to_prev_quarter,
    count(CASE
        WHEN star_performer_master_view.quarter_name = '{previous_quarter}'::text AND star_performer_master_view.year = '{previous_year}'::text THEN star_performer_master_view.month
        ELSE NULL::text
    END) AS star_previous_quarter
   FROM buisness_suite.star_performer_master_view
  GROUP BY star_performer_master_view.sapcode
WITH DATA;
"""
#############################################################################################


pre_training_master_yearly_view = """create MATERIALIZED VIEW buisness_suite.pre_training_master_yearly_view
TABLESPACE pg_default
AS 
SELECT candidate_sap_code ,
count(
        CASE
            WHEN quarter_name = '{previous_to_previous_previous_quarter}'::text AND year = '{previous_to_previous_previous_year}'::text THEN training_start_date
            ELSE NULL::date
        END) AS training_prev_to_prev_rev_quarter,
    count(
        CASE
            WHEN quarter_name = '{previous_to_previous_quarter}'::text AND  year = '{previous_to_previous_year}'::text THEN training_start_date
            ELSE NULL::date
        END) AS training_prev_to_prev_quarter,
    count(
        CASE
            WHEN quarter_name = '{previous_quarter}'::text AND year = '{previous_year}'::text THEN training_start_date
            ELSE NULL::date
        END) AS training_previous_quarter
   FROM buisness_suite.training_data_master_view 
  GROUP by candidate_sap_code 
WITH DATA
"""

#############################################################################################


transaction_master_view = """CREATE MATERIALIZED VIEW buisness_suite.transaction_master_view as
SELECT distinct q.personnel_number,
    q.start_date,
    q.transcation_given_year,
    q.transaction_month,
    q.quarter_name,
    q.action_type,
    q.reason_for_action,
        CASE
            WHEN q.quarter_name = 'Q4'::text THEN (q.transcation_given_year - 1)::text
            ELSE q.transcation_given_year::text
        END AS transcation_year
   FROM ( SELECT transaction_master.personnel_number,
            transaction_master.start_date,
            date_part('year'::text, start_date)::integer AS transcation_given_year,
            date_part('month'::text, start_date)::integer AS transaction_month,
                CASE
                    WHEN date_part('month'::text,start_date)::integer = ANY (ARRAY[1, 2, 3]) THEN 'Q4'::text
                    WHEN date_part('month'::text, start_date)::integer = ANY (ARRAY[4, 5, 6]) THEN 'Q1'::text
                    WHEN date_part('month'::text, start_date)::integer = ANY (ARRAY[7, 8, 9]) THEN 'Q2'::text
                    ELSE 'Q3'::text
                END AS quarter_name,
            action_type,
            reason_for_action
           FROM buisness_suite.transaction_master
          WHERE start_date >= '2017-02-01'::date) q
WITH DATA;"""

#############################################################################################


pre_transaction_master_yearly_view = """CREATE MATERIALIZED VIEW buisness_suite.pre_transaction_master_yearly_view
TABLESPACE pg_default
AS SELECT transaction_master_view.personnel_number,
	count(
        CASE
            WHEN transaction_master_view.action_type = 'Promotion'::text AND transaction_master_view.quarter_name = 'Q1'::text AND transaction_master_view.transcation_year = '2018'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS promotion_2018_q1,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Promotion'::text AND transaction_master_view.quarter_name = 'Q2'::text AND transaction_master_view.transcation_year = '2018'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS promotion_2018_q2,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Promotion'::text AND transaction_master_view.quarter_name = 'Q3'::text AND transaction_master_view.transcation_year = '2018'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS promotion_2018_q3,

    count(
        CASE
            WHEN transaction_master_view.action_type = 'Promotion'::text AND transaction_master_view.quarter_name = 'Q4'::text AND transaction_master_view.transcation_year = '2018'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS promotion_2018_q4,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Promotion'::text AND transaction_master_view.quarter_name = 'Q1'::text AND transaction_master_view.transcation_year = '2019'::text 
            THEN transaction_master_view.start_date
            ELSE NULL
        END) AS promotion_2019_q1,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Promotion'::text AND transaction_master_view.quarter_name = 'Q2'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS promotion_2019_q2,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Promotion'::text AND transaction_master_view.quarter_name = 'Q3'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS promotion_2019_q3,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Intra-company Transfer'::text AND transaction_master_view.quarter_name = 'Q1'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS intra_company_transfer_2019_q1,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Intra-company Transfer'::text AND transaction_master_view.quarter_name = 'Q2'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS intra_company_transfer_2019_q2,
     count(
        CASE
            WHEN transaction_master_view.action_type = 'Intra-company Transfer'::text AND transaction_master_view.quarter_name = 'Q3'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS intra_company_transfer_2019_q3,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Inter-company Transfer'::text AND transaction_master_view.quarter_name = 'Q1'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS inter_company_transfer_2019_q1,
       count(
        CASE
            WHEN transaction_master_view.action_type = 'Inter-company Transfer'::text AND transaction_master_view.quarter_name = 'Q2'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS inter_company_transfer_2019_q2,
     count(
        CASE
            WHEN transaction_master_view.action_type = 'Inter-company Transfer'::text AND transaction_master_view.quarter_name = 'Q3'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS inter_company_transfer_2019_q3,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Salary Revision'::text AND transaction_master_view.quarter_name = 'Q1'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS salary_revision_2019_q1,
     count(
        CASE
            WHEN transaction_master_view.action_type = 'Salary Revision'::text AND transaction_master_view.quarter_name = 'Q2'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS salary_revision_2019_q2,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Salary Revision'::text AND transaction_master_view.quarter_name = 'Q3'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS salary_revision_2019_q3,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Organistional Reassignment'::text AND transaction_master_view.quarter_name = 'Q1'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS organistional_reassignment_2019_q1,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Organistional Reassignment'::text AND transaction_master_view.quarter_name = 'Q2'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS organistional_reassignment_2019_q2,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Organistional Reassignment'::text AND transaction_master_view.quarter_name = 'Q3'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS organistional_reassignment_2019_q3,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Extension of Probation'::text AND transaction_master_view.quarter_name = 'Q1'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS extension_of_probation_2019_q1,
    count(
        CASE
            WHEN transaction_master_view.action_type = 'Extension of Probation'::text AND transaction_master_view.quarter_name = 'Q2'::text AND transaction_master_view.transcation_year = '2018'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS extension_of_probation_2018_q2,
      count(
        CASE
            WHEN transaction_master_view.action_type = 'Extension of Probation'::text AND transaction_master_view.quarter_name = 'Q2'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS extension_of_probation_2019_q2,

     count(
        CASE
            WHEN transaction_master_view.action_type = 'Extension of Probation'::text AND transaction_master_view.quarter_name = 'Q3'::text AND transaction_master_view.transcation_year = '2019'::text THEN transaction_master_view.start_date
            ELSE NULL
        END) AS extension_of_probation_2019_q3

   FROM buisness_suite.transaction_master_view
  WHERE transaction_master_view.action_type ~* '(Promotion|Inter-company Transfer|Intra-company Transfer|Salary Revision|Organistional Reassignment|Extension of Probation)'::text
  GROUP BY transaction_master_view.personnel_number
WITH DATA;"""

#############################################################################################

leave_data_master_view = """CREATE MATERIALIZED VIEW buisness_suite.leave_data_master_view
                TABLESPACE pg_default
                AS 
                select *,
                 CASE
                            WHEN q1.quarter_name = 'Q4'::text THEN (q1.given_year - 1)::text
                            ELSE q1.given_year::text
                        END AS year from (
                SELECT distinct q.personnel_number,
                    q.leave_type,
                    q.end_date,
                    q.start_date,
                    q.no_of_days,
                    q.year as given_year,
                    q.month,
                        CASE
                            WHEN q.month = ANY (ARRAY[1, 2, 3]) THEN 'Q4'::text
                            WHEN q.month = ANY (ARRAY[4, 5, 6]) THEN 'Q1'::text
                            WHEN q.month = ANY (ARRAY[7, 8, 9]) THEN 'Q2'::text
                            ELSE 'Q3'::text
                        END AS quarter_name
                   FROM ( SELECT distinct leave_master.personnel_number,
                            leave_master.leave_type,
                            leave_master.end_date,
                            leave_master.start_date,
                            leave_master.no_of_days,
                            date_part('year'::text, leave_master.start_date)::integer AS year,
                            date_part('month'::text, leave_master.start_date)::integer AS month
                           FROM buisness_suite.leave_master
                           where leave_type  ~* '(Exigency Leave|Privilege Leave - Special|PL|Optional Holiday|
                           Leave with out pay|Loss of Pay|EL|Privilege Leave|Optional|LOP|Casual Leave|Sick  Leave)'::text) q)q1
                WITH DATA;"""
#############################################################################################

pre_new_leave_yearly_data = """CREATE  MATERIALIZED VIEW buisness_suite.pre_new_leave_yearly_data
        TABLESPACE pg_default
        AS SELECT DISTINCT leave_data_master_view.personnel_number,
              sum(
                CASE
                    WHEN leave_data_master_view.year = '{previous_to_previous_previous_year}'::text AND leave_data_master_view.quarter_name = '{previous_to_previous_previous_quarter}'::text THEN leave_data_master_view.no_of_days::double precision
                    ELSE NULL::double precision
                END) AS no_of_days_prev_to_prev_prev_quarter,
            sum(
                CASE
                    WHEN leave_data_master_view.year = '{previous_to_previous_year}'::text AND leave_data_master_view.quarter_name = '{previous_to_previous_quarter}'::text THEN leave_data_master_view.no_of_days::double precision
                    ELSE NULL::double precision
                END) AS no_of_days_prev_to_prev_quarter,
                sum(
                CASE
                    WHEN leave_data_master_view.year = '{previous_year}'::text AND leave_data_master_view.quarter_name = '{previous_quarter}'::text THEN leave_data_master_view.no_of_days::double precision
                    ELSE NULL::double precision
                END) AS no_of_days_previous_quarter
           FROM buisness_suite.leave_data_master_view
          GROUP BY leave_data_master_view.personnel_number
        WITH DATA 
 """

#############################################################################################

pre_final_dis_pip_view = """create  MATERIALIZED VIEW buisness_suite.pre_final_dis_pip_view
TABLESPACE pg_default
AS 
SELECT DISTINCT sap_code, 
         sum(case
            WHEN year::text = '{previous_to_previous_year}'::text AND quarter_name = '{previous_to_previous_quarter}'::text and type_of_issue ~* 'Disciplinary' THEN 1
            ELSE null
        end) AS dic_prev_to_prev_quarter,
         sum(case
            WHEN year ::text= '{previous_year}'::text AND quarter_name = '{previous_quarter}'::text and type_of_issue ~* 'Disciplinary' THEN 1
            ELSE null
        end) AS dic_previous_quarter,      
        sum(case
            WHEN year ::text= '{previous_to_previous_year}'::text AND quarter_name = '{previous_to_previous_quarter}'::text and type_of_issue ~* 'PIP' THEN 1
            ELSE null
        end) AS pip_prev_to_prev_quarter,
        sum(case
            WHEN year::text = '{previous_year}'::text AND quarter_name = '{previous_quarter}'::text and type_of_issue ~* 'PIP' THEN 1
            ELSE null
        end) AS pip_previous_quarter
   FROM buisness_suite.dis_pip_master_view
   group by sap_code
WITH DATA;

"""
#############################################################################################

pre_compensation_yearly_data = """create materialized view buisness_suite.pre_compensation_yearly_data as
select distinct employee_id,
max(CASE
    WHEN quarter_name = 'Q1'::text and "year"='{compensation_previous_previous_year}'THEN monthly_compensation_gross::text
    ELSE NULL::text
end )AS monthly_compensation_gross_prev_to_prev_quarter,
max(CASE
    WHEN quarter_name = 'Q1'::text and "year"='{compensation_previous_year}'THEN monthly_compensation_gross::text
    ELSE NULL::text
end) AS monthly_compensation_gross_previous_quarter,
max(CASE
    WHEN quarter_name = 'Q1'::text and "year"='{compensation_previous_previous_year}'THEN fixed_component::text
    ELSE NULL::text
end) as fixed_component_prev_to_prev_quarter,
max(CASE
    WHEN quarter_name = 'Q1'::text and "year"='{compensation_previous_year}'THEN fixed_component::text
    ELSE NULL::text
end) AS fixed_component_previous_quarter,

max(CASE
    WHEN quarter_name = 'Q1'::text and "year"='{compensation_previous_previous_year}'THEN variable_component::text
    ELSE NULL::text
end )as variable_component_prev_to_prev_quarter,
max(CASE
    WHEN quarter_name = 'Q1'::text and "year"='{compensation_previous_year}'THEN variable_component::text
    ELSE NULL::text
end) AS variable_component_previous_quarter,
max(CASE
    WHEN quarter_name = 'Q1'::text and "year"='{compensation_previous_previous_year}'THEN last_annual_hike_percentage::text
    ELSE NULL::text
end) AS annual_hike_percentage_prev_to_prev_quarter,
max(CASE
    WHEN quarter_name = 'Q1'::text and "year"='{compensation_previous_year}'THEN last_annual_hike_percentage::text
    ELSE NULL::text
end) AS annual_hike_percentage_previous_quarter
from buisness_suite.compensation_master_view
group by employee_id
with data
"""

#############################################################################################


total_experience_range = """CREATE MATERIALIZED VIEW buisness_suite.total_experience_range
TABLESPACE pg_default
AS SELECT a.employee_id,
    a.total_experience_months_in_mf,
    a.total_experience_months_in_mf_range
   FROM ( SELECT t1.employee_id,
            t1.total_experience_months_in_mf,
            s.concat AS total_experience_months_in_mf_range
           FROM buisness_suite.consolidated_demoghaphic_master_view t1
             LEFT JOIN ( SELECT q.min_val,
                    q.max_val,
                    q.concat,
                    q.bucket
                   FROM ( WITH min_max AS (
                                 SELECT round(min(consolidated_demoghaphic_master_view_1.total_experience_months_in_mf::numeric)) AS min_val,
                                    round(max(consolidated_demoghaphic_master_view_1.total_experience_months_in_mf::numeric)) AS max_val
                                   FROM buisness_suite.consolidated_demoghaphic_master_view consolidated_demoghaphic_master_view_1
                                )
                         SELECT round(min(consolidated_demoghaphic_master_view.total_experience_months_in_mf::numeric)) AS min_val,
                            round(max(consolidated_demoghaphic_master_view.total_experience_months_in_mf::numeric)) AS max_val,
                            concat(round(min(consolidated_demoghaphic_master_view.total_experience_months_in_mf::numeric)), '-', round(max(consolidated_demoghaphic_master_view.total_experience_months_in_mf::numeric))) AS concat,
                            width_bucket(round(consolidated_demoghaphic_master_view.total_experience_months_in_mf::numeric), round(min_max.min_val), round(min_max.max_val), 10) AS bucket
                           FROM buisness_suite.consolidated_demoghaphic_master_view,
                            min_max
                          WHERE consolidated_demoghaphic_master_view.total_experience_months_in_mf IS NOT NULL
                          GROUP BY (width_bucket(round(consolidated_demoghaphic_master_view.total_experience_months_in_mf::numeric), round(min_max.min_val), round(min_max.max_val), 10))
                          ORDER BY (width_bucket(round(consolidated_demoghaphic_master_view.total_experience_months_in_mf::numeric), round(min_max.min_val), round(min_max.max_val), 10))) q) s ON round(t1.total_experience_months_in_mf::numeric) >= s.min_val AND round(t1.total_experience_months_in_mf::numeric) <= s.max_val) a
WITH DATA;

"""
#############################################################################################

supervisor_view = """CREATE MATERIALIZED VIEW buisness_suite.supervisor
TABLESPACE pg_default
AS SELECT q1.employee_id,
    q1.supervisor_id,
    q1.first_name,
    q1.last_name,
    q1.languages_known1,
    q1.languages_known4,
    q1.emp_age,
    q1.gender,
    q1.grade_designation,
    q1.state,
    q1.mother_tongue,
    q1.languages_known2,
    q1.languages_known3,
    q1.location_current,
    q1.previous_experience_in_years,
    q1.previous_company,
    q1.marital_status,
    q1.highest_qualification,
    q1.recruitement_data,
    q1.employment_details_date_of_hire,
    q1.total_experience_months_in_mf,
    q1.experience_in_current_role_in_months,
    q1.col_rnk
   FROM ( SELECT q.employee_id,
            q.supervisor_id,
            e.first_name,
            e.last_name,
            e.languages_known1,
            e.languages_known4,
            e.age AS emp_age,
            e.gender,
            e.employee_sub_group AS grade_designation,
            e.state,
            e.mother_tongue,
            e.languages_known2,
            e.languages_known3,
            e.branch AS location_current,
            e.previous_experience_in_years,
            e.previous_company,
            e.marital_status,
            e.highest_qualification,
            e.recruitement_data,
            e.employment_details_date_of_hire,
            e.total_experience_months_in_mf,
            e.experience_in_current_role_in_months,
            rank() OVER (PARTITION BY q.employee_id) AS col_rnk
           FROM ( SELECT DISTINCT a.employee_id,
                    a.manager_user_sys_id AS supervisor_id
                   FROM buisness_suite.final_view a
                  GROUP BY a.employee_id, a.manager_user_sys_id) q
             JOIN buisness_suite.final_view e ON q.supervisor_id = e.employee_id) q1
  WHERE q1.col_rnk = 1
WITH DATA;

"""
#############################################################################################

pre_new_all_emp_supervisor_yearly_view = """

CREATE MATERIALIZED VIEW buisness_suite.pre_new_all_emp_supervisor_yearly_view
TABLESPACE pg_default
AS SELECT DISTINCT a.employee_id,
    a.state,
    a.branch,
    a.first_name,
    a.last_name,
    a.function_name,
    a.division,
    a.business_unit,
    a.department,
    a.sub_department,
    a.cost_centre,
    a.band,
    a.employee_sub_group AS grade_designation,
    a.work_contract,
    a.mother_tongue,
    a.total_experience_months_in_mf,
    a.experience_in_current_role_in_months,
    a.gender,
    a.languages_known1,
    a.languages_known2,
    a.languages_known3,
    a.languages_known4,
    a.branch AS location_current,
    a.previous_company,
    a.previous_experience_in_years,
    a.marital_status,
    a.highest_qualification,
    a.recruitement_data,
    a.employee_status,
    a.employment_details_date_of_hire,
    a.age,
    a.manager_user_sys_id,
    a.supervisor,
    a.employment_details_last_working_date::date,
    a.quarter_year AS employee_left_quarter,
    a.quarter_name,
    a.last_working_date_year,
    b.emp_age AS reporting_office_age,
    b.first_name AS repo_officer_fist_name,
    b.last_name AS repo_officer_last_name,
    b.total_experience_months_in_mf AS reporting_officer_total_experience_months_in_mf,
    b.experience_in_current_role_in_months AS reporting_officer_experience_in_current_role_in_months,
    b.gender AS reporting_officer_gender,
    b.state AS reporting_office_state,
    b.mother_tongue AS reporting_office_mother_tongue,
    b.grade_designation AS reporting_office_grade_name,
    b.languages_known1 AS reporting_officer_languages_known1,
    b.languages_known2 AS reporting_officer_languages_known2,
    b.languages_known3 AS reporting_officer_languages_known3,
    b.languages_known4 AS reporting_officer_languages_known4,
    b.location_current AS reporting_officer_location_current,
    b.previous_company AS reporting_officer_previous_company,
    b.previous_experience_in_years AS reporting_officer_previous_experience_in_years,
    b.marital_status AS reporting_officer_marital_status,
    b.highest_qualification AS reporting_officer_highest_qualification,
    b.recruitement_data AS reporting_officer_source_of_recruitment,
    msr.net_mcares_score
   FROM buisness_suite.final_view a
     LEFT JOIN buisness_suite.supervisor b ON a.employee_id = b.employee_id
     LEFT JOIN ( SELECT mcares_score_manager.manager_id,
            mcares_score_manager.net_mcares_score
           FROM buisness_suite.mcares_score_manager
          WHERE mcares_score_manager.year::text = '2020'::text) msr ON msr.manager_id = a.manager_user_sys_id
WITH DATA;

"""

promotion_and_rating_yearly_view = """CREATE MATERIALIZED VIEW buisness_suite.promotion_and_rating_latest_view
TABLESPACE pg_default
AS SELECT DISTINCT ON (promotion_and_rating_master_new.employee_id) promotion_and_rating_master_new.employee_id,
    promotion_and_rating_master_new.rating,
    promotion_and_rating_master_new.year
   FROM buisness_suite.promotion_and_rating_master_new
  WHERE ((promotion_and_rating_master_new.employee_id::text, promotion_and_rating_master_new.year::text) IN ( SELECT DISTINCT promotion_and_rating_master_new_1.employee_id,
            max(promotion_and_rating_master_new_1.year::text) AS max
           FROM buisness_suite.promotion_and_rating_master_new promotion_and_rating_master_new_1
          GROUP BY promotion_and_rating_master_new_1.employee_id))
WITH DATA;"""

#############################################################################################

prediction_data_view = """
CREATE MATERIALIZED VIEW buisness_suite.prediction_data_view as
  select *, case
            when date_part('year'::text,employment_details_last_working_date)::integer = date_part('year'::text,last_promotion_date)::integer then date_part('month'::text,employment_details_last_working_date)::integer-date_part('month'::text,last_promotion_date)::integer
            when  employment_details_last_working_date is null and last_promotion_year is not null THEN (EXTRACT(year FROM age(now(),last_promotion_date::date))*12+EXTRACT(month FROM age(now(),last_promotion_date::date)))::integer
            when employment_details_last_working_date is not null and last_promotion_year is not null THEN ((date_part('year'::text,employment_details_last_working_date)::integer-(last_promotion_year::integer))*12+EXTRACT(month FROM age(employment_details_last_working_date::date,last_promotion_date::date)))::integer
            else null
        END AS promotion_count_year

from(SELECT a.employee_id,
    a.first_name,
    a.last_name,
    a.state,
    a.branch,
    a.division,
    a.function_name,
    a.business_unit,
    a.department,
    a.sub_department,
    a.cost_centre,
    a.band,
    a.work_contract,
    a.employee_status,
    a.employment_details_date_of_hire,
    a.employment_details_last_working_date,
    a.employee_left_quarter as left_quarter_year,
    a.quarter_name as left_quarter,
    a.last_working_date_year,
    a.total_experience_months_in_mf::double precision/12::double precision as total_experience_year_in_mf,
    a.experience_in_current_role_in_months,
    a.reporting_officer_total_experience_months_in_mf::double precision/12::double precision as reporting_officer_total_experience_year_in_mf,
    a.reporting_officer_experience_in_current_role_in_months,
    a.age,
    a.reporting_office_age,
    a.gender AS emp_gender,
    a.languages_known1 AS emp_lang_known1,
    a.languages_known2 AS emp_lang_known2,
    a.languages_known3 AS emp_lang_known3,
    a.languages_known4 AS emp_lang_known4,
    a.reporting_officer_languages_known1 AS repo_lang_known1,
    a.reporting_officer_languages_known2 AS repo_lang_known2,
    a.reporting_officer_languages_known3 AS repo_lang_known3,
    a.reporting_officer_languages_known4 AS repo_lang_known4,
    a.location_current AS emp_location,
    a.marital_status AS emp_marital_status,
    a.highest_qualification AS emp_highest_qualification,
    a.reporting_officer_highest_qualification AS repo_highest_qualification,
    a.reporting_officer_location_current AS repo_location,
    a.grade_designation,
    a.repo_officer_fist_name,
    a.repo_officer_last_name,
    a.reporting_officer_gender,
    a.reporting_officer_marital_status,
    a.net_mcares_score as manager_net_mcares_score,
    g.gem_previous_quarter,
    g.gem_prev_to_prev_quarter,
    g.gem_prev_to_prev_prev_quarter,
    s.star_previous_quarter,
    s.star_prev_to_prev_quarter,
    s.star_prev_to_prev_prev_quarter,
    t.training_prev_to_prev_quarter,
    t.training_previous_quarter,
    t.training_prev_to_prev_rev_quarter,
    z.promotion_2019 AS promotion_previous_quarter,
    z.promotion_2018 AS promotion_prev_to_prev_quarter,
    z.promotion_2017 AS promotion_prev_to_prev_to_prev_quarter,
    d.dic_prev_to_prev_quarter,
    d.dic_previous_quarter,
    m.monthly_compensation_gross_previous_quarter,
    m.monthly_compensation_gross_prev_to_prev_quarter,
    m.fixed_component_previous_quarter,
    m.fixed_component_prev_to_prev_quarter,
    m.variable_component_previous_quarter,
    m.variable_component_prev_to_prev_quarter,
    m.annual_hike_percentage_prev_to_prev_quarter,
    m.annual_hike_percentage_previous_quarter,
    l.no_of_days_prev_to_prev_prev_quarter,
    l.no_of_days_prev_to_prev_quarter,
    l.no_of_days_previous_quarter,
    d.pip_prev_to_prev_quarter,
    d.pip_previous_quarter,
        CASE
            WHEN (z.promotion_2019) >= 1 THEN 1
            ELSE 0
        END AS promotion_year,
    npr.last_promotion_date,
    case when npr.last_promotion_date::text is not null and npr.last_promotion_date::text!=''  Then date_part('year'::text,npr.last_promotion_date)::text
    else null
    end as last_promotion_year
   FROM buisness_suite.pre_new_all_emp_supervisor_yearly_view a
     LEFT JOIN buisness_suite.pre_gem_master_yearly_view g ON a.employee_id = g.sap_code
     LEFT JOIN buisness_suite.pre_star_performer_yearly_view s ON a.employee_id = s.sapcode
     LEFT JOIN buisness_suite.pre_training_master_yearly_view t ON a.employee_id = t.candidate_sap_code
     LEFT JOIN buisness_suite.predict_promotion_data_view z ON a.employee_id = z.employee_id
     LEFT JOIN buisness_suite.pre_new_leave_yearly_data l ON a.employee_id = l.personnel_number
     LEFT JOIN buisness_suite.pre_final_dis_pip_view d ON a.employee_id = d.sap_code
     LEFT JOIN buisness_suite.pre_compensation_yearly_data m ON a.employee_id = m.employee_id
     LEFT JOIN buisness_suite.predict_promotion_rating_view npr ON a.employee_id = npr.employee_id)s

WITH DATA;  """
#############################################################################################

predictive_output = """
CREATE TABLE predictive.predictive_output (

employee_id	  VARCHAR (50) ,
first_name  VARCHAR (50) ,
last_name	 VARCHAR (50) ,
state  VARCHAR (50) ,
branch  VARCHAR (50) ,
division  VARCHAR (50) ,
function_name  VARCHAR (50) ,
business_unit  VARCHAR (50) ,	
department	  VARCHAR (50) ,
sub_department  VARCHAR (50) ,
cost_centre	band  VARCHAR (50) ,
band  VARCHAR (50) ,
work_contract  VARCHAR (50) ,
probability	 VARCHAR (50) ,
Grade  VARCHAR (50) 

);
"""
#############################################################################################

predict_promotion_data_view = """create MATERIALIZED VIEW buisness_suite.predict_promotion_data_view
                        TABLESPACE pg_default
                        as
select employee_id,count(
        CASE
            WHEN  promotion_year= '2019' THEN promotion_year
            ELSE NULL
        END) AS promotion_2019,
    count(
        CASE
            WHEN promotion_year= '2018' THEN promotion_year
            ELSE NULL
        END) AS promotion_2018,
        count(
        CASE
            WHEN promotion_year= '2017' THEN promotion_year
            ELSE NULL
        END) AS promotion_2017

        from buisness_suite.predict_promotion_rating_view group by employee_id
 with data"""
#############################################################################################

predict_tree_map_data = """CREATE TABLE predictive.predictive_treemap_output (
	employee_id varchar(355),
	state varchar(355),
	branch varchar(355),
	function_name varchar(355),
	division varchar(355),
	business_unit varchar(355),
	department varchar(355),
	sub_department varchar(355),
	cost_centre varchar(355),
	work_contract varchar(355),
	band varchar(355),
	grade_designation varchar(355),
	preds varchar(355)
);"""
#############################################################################################

predictive_output = """CREATE TABLE predictive.predictive_output (
	employee_id varchar(50),
	first_name varchar(50),
	last_name varchar(50),
	state varchar(355),
	branch varchar(50),
	division varchar(50),
	function_name varchar(355),
	business_unit varchar(50),
	department varchar(50),
	sub_department varchar(355),
	cost_centre varchar(50),
	band varchar(50),
	work_contract varchar(355),
	probability varchar(50),
	grade varchar(50),
	grade_designation varchar(100)
);
"""  #############################################################################################
model_health_table = """CREATE TABLE predictive.modelhealth_output (id serial NOT NULL,
training_rows int4 NOT NULL, testing_rows int4 NOT NULL, test_accuracy float8 NOT NULL,
train_accuracy float8 NOT NULL, test_recall float8 NOT NULL, train_recall float8 NOT NULL,
train_precision float8 NOT NULL, test_precision float8 NOT NULL, train_f1 float8 NOT NULL, test_f1 float8 NOT NULL,
auc_test float8 NOT NULL, pred_quarter text NOT NULL, pred_year int4 NOT NULL,
train_year int4 NOT NULL, train_quarter text NOT NULL, CONSTRAINT modelhealth_output_pkey PRIMARY KEY (id)
);
"""
##########################################################333

last_three_quarters = """select distinct concat(year::text,substring(quarter_name, '[0-9]+')::text)::int as qq from 
buisness_suite.{} where concat(year::text,substring(quarter_name, '[0-9]+')::text)::int<'{}' 
order by qq desc limit 3"""

compensation_last_two_years = """select distinct year from buisness_suite.compensation_master_view where year<='{}' order by year desc limit 2"""