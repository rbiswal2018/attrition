from django.urls import path
from data_loader import views
from data_loader.views import FileView, drop_view, create_view, refresh_view, dropdown_data, create_master_table, \
    create_predictive_table_view, DownloadFileView,create_prediction_view

urlpatterns = [
    path('file/', FileView.as_view(), name='file-upload'),
    path('save_file_to_downloads/', DownloadFileView.as_view(), name='download-file-upload'),
    path('download/', views.get_files),
    path('create_view/', create_view),
    path('dropdown_data/', dropdown_data),
    path('refresh_view/', refresh_view),
    path('drop_view/', drop_view),
    path('create_master_table/', create_master_table),
    path('create_predictive_table/', create_predictive_table_view),
    path('create_prediction_view/', create_prediction_view),

]
