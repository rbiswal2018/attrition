import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from io import BytesIO
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from authentication.util import get_cursor
from data_loader.loader_functions import read_file, convert_dataframe, rename_data_frame, match_length, \
    compare_columns, mismatch_record_count, check_date_type, check_header_of_columns, check_mismatch_type, \
    data_for_insert_query, get_query, data_for_master, get_column, bulk_update_where_condition, get_master_column, \
    get_record_count, clear_data, create_views, drop_views, refresh_views, create_master_table_initial, \
    create_all_predictive_table, copy_csv_in_prediction_table,create_prediction_views
from attrition import settings
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
import zipfile
import logging
import os
from datetime import datetime
from carrot.utilities import publish_message
from django.db import connection

# Initialize the logger
logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)
import pdb

class DownloadFileView(APIView):
    pass
    """
    For uploading file in media from local system.

        To upload file in to the file system .
    """
    def post(self, request, *args, **kwargs):
        """
        To load the file.
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            up_file = request.FILES['file']
            path = settings.MEDIA_ROOT + '/downloads/' + str(up_file.name)

            with open(path, 'wb+') as destination:
                for chunk in up_file.chunks():
                    destination.write(chunk)
            return Response({"message": "successfully uploaded"}, status=status.HTTP_200_OK)
        except Exception as e:
            logger.error("Error in FileView : {0}".format(str(e)))
            return Response({"message": "upload failed"}, status=status.HTTP_400_BAD_REQUEST)

table_list = ['Recruitement','Training', 'Employee Transaction ',
              'Consolidated Employee Demographics','Competency Report','Compensation','Gem Rewards', 'Leave', 'Star Rewards',
              'Dis And Pip','Mcares Score Department','Mcares Score Manager','Promotion And Rating','Major Service Funtion'
              , 'Business Service Funtion']

@api_view(['GET'])
def dropdown_data(request):
    """
    For all table name for dropdown.
    :param request:
    :return:
    """
    logger.info("Control entered into dropdown_data APIView")
    return Response({"message": "success", "data": table_list}, status=status.HTTP_200_OK)


table_name_dict = {"Recruitement": "recruitement", "Promotion And Rating": "promotion_and_rating",
                   "Consolidated Employee Demographics": "consolidated_demographics",
                   "Gem Rewards": "gem",'Competency Report':'competency_report_master_year',
                   "Leave": "leave", "Star Rewards": "star_performer", "Employee Transaction ": "transaction",
                   "Training": "training" ,"Compensation": "compensation",
                   "Dis And Pip": "dis_and_pip",
                   'Mcares Score Department':'mcares_score_department','Mcares Score Manager':'mcares_score_manager',
                   'Business Service Funtion':'business_service_functions', 'Major Service Funtion':'major_service_functions'}


class FileView(APIView):
    """
    For uploading file in media from local system.

    """
    logger.info("Control entered into FileView APIView")
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            logger.info("Control entered into post method of FileView")
            obj = {}
            table_name_key = request.POST.get('table_name')
            table_name = ''
            for i in table_list:
                if i.strip() == table_name_key.strip():
                    table_name += table_name_dict.get(i)
                    break
            obj["response"] = []
            for file_obj in request.FILES.getlist('file'):
                return_response = handle_uploaded_file(file_obj, table_name.lower())
                return_response.update({"file_name": file_obj.name})
                obj["response"].append(return_response)
            return Response(obj)

        except Exception as e:
            logger.error("File View:", str(e))
            raise Exception("Upload Failed")


def handle_uploaded_file(file_obj, table_name):
    """
    :param file_obj:
    :param table_name:
    :return:
    """
    data = error_type = success = message = ''
    path = settings.MEDIA_ROOT + '/data_sets/'
    file_name, extension = read_file(path, file_obj)
    try:
        if extension == 'csv' or extension == 'CSV':
            # convert file to data frame
            df = convert_dataframe(extension, path, file_name, file_obj, table_name)
            if df is not None:
                # Rename DataFrame .
                columns_after_rename = rename_data_frame(df).columns
                # Based on the table name read the corresponding config file .
                config_obj = json.loads(open("data_loader/config/" + table_name + ".json").read())
                actual_column_names = config_obj['columns']
                table_column_names = config_obj['table_column']
                #  SANITY CHECK.
                actual_len = len(actual_column_names)
                received_len = len(columns_after_rename)
                # compare actual and received columns
                if actual_len != received_len:
                    error_type, message = match_length(actual_len, received_len)
                    raise Exception(error_type, message)

                mismatch_columns = compare_columns(actual_column_names, columns_after_rename)
                # mismatch_data_type = mismatch_record_count(df, actual_column_names, columns_after_rename)
                mismatch_data_type = mismatch_record_count(df, actual_column_names, columns_after_rename,table_name)
                # Exception Handling
                if len(mismatch_data_type)>0:
                    error_type, message = check_date_type(table_name,mismatch_data_type[0])
                    raise Exception(error_type, message)

                if len(mismatch_columns) > 0:
                    error_type, message, data = check_header_of_columns(mismatch_columns)
                    raise Exception(error_type, message, data)

                elif len(mismatch_data_type) > 0:
                    error_type, message, data = check_mismatch_type(mismatch_data_type)
                    raise Exception(error_type, message, data)
                else:
                    table_columns, staging_table_name = data_for_insert_query(table_column_names, table_name)
                    master_table_name, table_columns_master, unique_column, table_column = data_for_master(config_obj)
                    changed_column = get_column(table_column)
                    where_condition_data, returned_data, columns, where_condition_insert_data = \
                        bulk_update_where_condition(unique_column)
                    master_column = get_master_column(table_column)
                    # Query Execution
                    create_query = get_query('create_staging').format(staging_table_name, table_columns)
                    # print("...............create_query",create_query)
                    get_cursor(create_query)

                    create_master_query = get_query('create_master').format(master_table_name, table_columns_master)
                    get_cursor(create_master_query)
                    # below line of code will work for production deployment,but not for local
                    copy_query = get_query('copy_to_staging').format(staging_table_name, path + file_name + ".csv")
                    # print("............",copy_query)
                    get_cursor(copy_query)
                    # below commented line of code will work in local, for using loader u have to uncomment it.
                    # cursor = connection.cursor()
                    # copy_cmd = "COPY buisness_suite.{0} FROM STDIN DELIMITER ',' CSV HEADER".format(staging_table_name)
                    # file_path = path + file_name + ".csv"
                    # cursor.copy_expert(copy_cmd, open(file_path, "r"))

                    master_query = get_query('bulk_update').format(master_table_name, changed_column,
                                                                   staging_table_name, where_condition_data,
                                                                   returned_data, master_column, columns,
                                                                   where_condition_insert_data)
                    get_cursor(master_query)
                    upload_count, failed_data = get_record_count(staging_table_name, df)
                    success = "True"
                    message = "{0} records has been Uploaded Successfully, {1} records were discarded". \
                        format(upload_count, failed_data)
            else:
                message = "Wrong file Uploaded, please upload proper file into proper table"
                raise Exception(message)

        else:
            clear_data(extension, file_name)
            error_type = 'Unsupported Format'
            message = "This is an unsupported file format. Please Upload CSV File only. "
            raise Exception(error_type, message)

    except Exception as e:
        # print("error............",e)
        logger.error("handle_uploaded_file Error: %s" % e)

        if error_type == '':
            return {'success': "False", 'message': str(e), "error_type": "Unknown error"}

        return {'success': "False", 'message': message, "error_type": error_type, "data": data}

    clear_data(extension, file_name)
    obj = {"success": success, "error_type": error_type, "message": message, "data": data}
    return obj


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                                            DOWNLOAD UPLOADING FORMAT
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


@csrf_exempt
def get_files(request):
    logger.info("Control entered into get_files APIView")
    try:
        path1 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('business_service_function.csv')
        path2 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('compensation.csv')
        path3 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('competency_report_master.csv')
        path4 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('gem_master.csv')
        path6 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('leave_master.csv')
        path7 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('consolidated_demographic.csv')
        path8 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('star_rewards.csv')
        path9 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('training_master.csv')
        path10 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('transactions.csv')
        path11 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('macres_department_score.csv')
        path12 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('recruitement.csv')
        path13 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('macres_manager_score.csv')
        path14 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('dis_and_pip.csv')
        path15 = settings.MEDIA_ROOT + '/' + 'downloads/' + str('major_service_function.csv')

        file_names = [path1, path2, path3, path4, path6, path7, path8, path9, path10, path11, path12, path13,
                      path14, path15]

        zip_subdir = "Data_template"
        zip_filename = "%s.zip" % zip_subdir
        s = BytesIO()
        # The zip compressor
        zf = zipfile.ZipFile(s, "w")
        for fpath in file_names:
            fdir, fname = os.path.split(fpath)
            zip_path = os.path.join(zip_subdir, fname)
            zf.write(fpath, zip_path)
        zf.close()
        resp = HttpResponse(s.getvalue(), content_type="application/x-zip-compressed")
        resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename
    
        return resp
    except Exception as e:
        logger.error("Error from get_files: %s" % e)


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


@api_view(['GET'])
def create_view(request):
    try:
        create_views()
        return Response("All views and index successfully created .")
    except Exception as e:
        return Response("failed")


@api_view(['GET'])
def refresh_view(request):
    try:

        refresh_views()
        return Response("All views successfully refreshed.", status=status.HTTP_200_OK)
    except Exception as e:
        return Response("failed", status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def drop_view(request):
    try:
        drop_views()
        return Response("All views and index successfully droped")
    except Exception as e:
        return Response("failed", e)


@api_view(["GET"])
def create_master_table(request):
    try:
        create_master_table_initial()
        return Response("All master table is created")
    except Exception as e:
        return Response("failed", e)


@api_view(["GET"])
def create_predictive_table_view(request):
    try:
        create_all_predictive_table(request)
        return Response("All master table is created")
    except Exception as e:
        return Response("failed", e)
def f(text):
   def r(**kwargs):
     return create_prediction_views(**kwargs)
   r.__name__ = text 
   return r

@api_view(["POST"])
def create_prediction_view(request):
    try:
        # print(request.data)
        # pdb.set_traccreate_predictive_tablee()
        # name = f(directory)
        # create_prediction_views.__name__ =directory
        temp_data = request.data
        publish_message(create_prediction_views,priority=1, queue='model_run',**temp_data)
        # create_prediction_views(**request.data)
        return Response({"message": "success"}, status=status.HTTP_200_OK)
    except Exception as e:
        return Response("failed", e)






