import psycopg2
# import pymssql
# import sqlalchemy
import timeit
import logging
import pdb
import json
import subprocess
import shlex
import os
import sys
import django
sys.path.append("/home/arima/mywork/mahindra_project/attrition/attrition_project/attrition")
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'attrition.settings')
django.setup()

# Gets or creates a logger
logger = logging.getLogger(__name__)

# set log level
logger.setLevel(logging.WARNING)

# define file handler and set formatter
file_handler = logging.FileHandler('insights.log')
formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(name)s : %(message)s')
file_handler.setFormatter(formatter)

# add file handler to logger
logger.addHandler(file_handler)

# urllib3.disable_warnings()
import simulation.views as views


def fun1():
    print("000000")
    views.get_quarterly_simulation_data()

if __name__ == '__main__':
    fun1()


