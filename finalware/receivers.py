import logging

from django.conf import settings
from django.db.models import signals
from django.apps import apps
from django.db import DEFAULT_DB_ALIAS

from .utils import load_site_objects
from .utils import create_superuser
from .utils import add_role
from django.contrib.auth.models import Group

# Initialize the logger
logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)
# log = logging.getLogger(__name__)


def pre_migrate_receiver(app_config, verbosity=2, interactive=False, using=DEFAULT_DB_ALIAS, **kwargs):
    """
    Disable the superuser creation prompt.
    """
    from django.contrib.auth import management
    if hasattr(management, 'create_superuser'):
        signals.post_migrate.disconnect(
            management.create_superuser,
            sender=apps.get_app_config('auth'),
            dispatch_uid="django.contrib.auth.management.create_superuser"
        )
        if verbosity >= 2:
            logger.info("Disabling create_superuser prompt")


def post_migrate_receiver(app_config, verbosity=2, interactive=False, using=DEFAULT_DB_ALIAS, **kwargs):
    """
    Finalize the website loading.
    """
    load_site_objects(verbosity)
    # print("yes")
    create_superuser(verbosity)
    if len(Group.objects.all()) == 0:
        add_role()

