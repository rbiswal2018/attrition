import logging
from django.apps import apps
from django.contrib.sites.models import Site
from django.contrib.auth.models import Group, User
from django.db import connection

from authentication.models import UserProfile
from authentication.util import generate_token
from data_loader.loader_functions import create_master_table_initial, create_all_predictive_table, create_views, \
    copy_csv_in_prediction_table
from . import defaults as defs
import pdb
# Initialize the logger
logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)

crud_perm_query = """ INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(1, '{UserRecordsView.get}', 'Fetch User');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(2, '{UserRecordsView.post}', 'Add User');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(3, '{edit_user}', 'Edit User');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(4, '{delete_user}', 'Delete User');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(5, '{activate_user}', 'Change Status');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(6, '{RoleView.get}', 'Show Role');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(8, '{edit_role}', 'Edit Role');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(9, '{delete_role}', 'Delete Role');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(10, '{CustomPermissionsView.get}', 'Get Permission With Role');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(11, '{PermissionView.get}', 'See Permission_list');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(7, '{RoleView.post}', 'Add Role');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(12, '{get_tree_map_data,get_average_tenure_data, get_source_of_recruitement_data, get_gender_data, get_marital_status_data, get_employee_left_count,
                    get_attrition_percentage,get_pip_data,get_dic_data,get_training_data,get_star_performer_data,get_gem_performer_data,
                    get_annual_compensation_data,get_drop_down_data,get_rating_data,get_voluntary_involuntary_data,get_higher_qualification_data,
                    get_work_contract_data,get_reporting_manager_data,get_manager_mcares_data,get_department_mcares_data,get_functional_competency_attrition_ratio,
                    get_average_tenure, get_attrition_trend, get_cohert_analytics_data, get_age_group_data,get_grade_data,get_role_drop_down_data,
                    get_chart_csv_data,get_chart_csv_consolidate_data,get_business_service_function,get_major_service_function,get_chart_csv_contribution_data,
                    get_toggle_contribution_data}', 'Dashboard');
                    INSERT INTO public.crud_permissions
                    (id, function_name, "name")
                    VALUES(13, '{get_roc_curve_mf,get_model_health_output,insert_modelhealth_output,get_tree_map_data,get_pdb_data,get_pdb_dropdown,class_group_importance,get_emp_predict_data,get_predictive_drop_down_data,
                    lime_explainer_view,employee_wise_simulation,get_survival_data,get_simulation_data}', 'Predictive');
                     """

cust_query = """INSERT INTO public.custom_permissions (permission_list,group_id) VALUES 
                ('{Fetch User,Add User,Edit User,Delete User,Change Status,Show Role,Add Role,Edit Role,
                Delete Role,Get Permission With Role,See Permission_list,Dashboard,Predictive}',
                (select id from auth_group where name='admin'));"""


# Setup the available sites for this project
def load_site_objects(verbosity):
    """
    Load the available Sites for this project.
    `SITE_ID` in the settings file will decide the `current` site object.
    """
    if not apps.is_installed('django.contrib.sites'):
        return
    site_info = getattr(defs, 'SITE_OBJECTS_INFO_DICT')
    if site_info:
        for pk in sorted(site_info.keys()):
            site, created = Site.objects.get_or_create(pk=pk)
            if site:
                site.name = site_info[pk]['name']
                site.domain = site_info[pk]['domain']
                site.save()
                if verbosity >= 2:
                    if created:
                        logger.info('Creating {} Site'.format(site.domain))
                        # print('Creating {} Site'.format(site.domain))
                    else:
                        # print('Updated {} Site'.format(site.domain))
                        logger.info('Updated {} Site'.format(site.domain))


def create_superuser(verbosity):
    """
    Create or update a superuser.
    """
    username = getattr(defs, 'SITE_SUPERUSER_USERNAME')
    email = getattr(defs, 'SITE_SUPERUSER_EMAIL')
    password = getattr(defs, 'SITE_SUPERUSER_PASSWORD')
    if not password:
        logger.info("Skipped superuser create/update. No password supplied.")
        # print('Skipped superuser create/update. No password supplied.')
        return

    from django.contrib.auth import get_user_model

    User = get_user_model()
    if User.USERNAME_FIELD == 'email' and email:
        user, created = User.objects.get_or_create(email=email)
    elif User.USERNAME_FIELD == 'username' and username:
        user, created = User.objects.get_or_create(username=username)
    else:
        # print('Skipped superuser create/update. No username or email supplied.')
        logger.info("Skipped superuser create/update. No username or email supplied.")
        return

    if not created and not user.is_superuser:
        # print('Unable to promote normal user to superuser. Use createsuperuser command.')
        logger.info("Unable to promote normal user to superuser. Use createsuperuser command.")
        return

    if email and hasattr(user, 'email'):
        user.email = email
    if username and hasattr(user, 'username'):
        user.username = username
    user.set_password(password)
    user.is_staff = True
    user.is_active = True
    user.is_superuser = True
    user.save()
    # UserProfile(user=User(user), role=getattr(defs, 'SITE_ROLE')).save()
    logger.info('Superuser created or updated')
    if verbosity >= 2:
        if created:
            logger.info('Creating superuser')
        else:
            logger.info('Updated superuser')


def add_role():
    """
    It save role data according to given data in seting.
    :param :
    :return:
    """
  
    role_name = getattr(defs, 'SITE_ROLE')
    my_group = Group(name=role_name)
    my_group.save()
    add_profile(role_name)
    create_crud_perm(crud_perm_query)
    create_crud_perm(cust_query)
    create_master_table()
    create_predictive_table()
    create_views()
    # copy_csv_in_prediction_table()


def add_profile(role_name):
    """
    It save user data according to given data in setting.
    :return:
    """
    user_prof = UserProfile(user=User(User.objects.get(username=getattr(defs, 'SITE_SUPERUSER_USERNAME')).id),
                            role=role_name, token_key=generate_token())
    user_prof.save()


def create_crud_perm(query):
    """
    It will give connection_obj of django for connecting database.
    :param query:
    :return:
    """
    try:
        cursor = connection.cursor()
        cursor.execute(query)
    except Exception as e:
        logger.error("Errors: %s " % e)


def create_master_table():
    """
    It will create all master table.
    :return:
    """
    try:
        create_master_table_initial()
        create_views()
    except Exception as e:
        logger.error("Error: %s create_view" % e)

def create_predictive_table():
    """It will create all predictive table.
    """
    try:
        create_all_predictive_table()
    except Exception as e:
        logger.error("Error: %s " % e)
