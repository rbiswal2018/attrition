from django.apps import AppConfig


class LoadDefaultConfig(AppConfig):
    name = 'load_default'
