from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from load_default import views


urlpatterns = [
    path('select_model/', views.load_model_files),
    # path('load_file_from_kedro/', views.load_file_from_kedro),

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

