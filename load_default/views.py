import pandas as pd
import pickle
import pandas
import logging
from attrition import settings
import pdb
from rest_framework.response import Response
from rest_framework import status

from rest_framework.decorators import api_view, permission_classes
import os

logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)


class load_default_files:
    @classmethod
    def run(cls, model_version):
        try:
            logger.info("Control entered into load_default_files")
            cls.final_data = None
            cls.model = None
            cls.path = settings.MEDIA_ROOT + '/data_sets/'
            cls.original = settings.MEDIA_ROOT + '/' + model_version + '/'
            cls.simulation_path = settings.MEDIA_ROOT + '/simulation/'
            cls.predictive_path = settings.MEDIA_ROOT + '/predictive/'
            cls.model_health_path = settings.MEDIA_ROOT + '/model_health/'
            cls.pred_data = pd.read_csv(cls.original + '/attrition_pred_data_17112021_q32022_v1_11-01-23-1.csv')
            cls.model_data = pickle.load(open(cls.original + '/attrition_model_17112021_q32022_v1_11-01-23.pkl', 'rb'))
            cls.model_health_data = pd.read_csv(cls.original + '/attrition_data_model_health_17112021_q32022_v1_11-01-23.csv')
            cls.simulatio_data = pd.read_csv(cls.original + '/prediction_data_view_202301111755.csv')
            cls.surv_model = pickle.load(open(cls.original + '/churn_rate_17112021_q32022_v1_11-01-23.pkl', 'rb'))
            logger.info("Control ended after load_default_files")
        except Exception as e:
            print("error...............................................",e)
            logger.error("error in load_default_files %s" % e)


load_default_files.run("original")


@api_view(['POST'])
# @permission_classes((IsAuthenticatedUser,))
def load_model_files(request):
    try:
        result = {}
        print(request.data)
        load_default_files.run(request.data[0])
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['GET'])
# # @permission_classes((IsAuthenticatedUser,))
# def load_file_from_kedro(request):
#     try:
#         # module_dir = '/home/user/project/mahindra_attrition/data/06_models/rfr.pickle/2022-07-14T09.34.00.656Z'
#         # module_dir = '/home/user/project/mahindra_attrition/data/01_raw'
#         # file_path = os.path.join(module_dir, 'rfr.pickle')
#         print("11111111111111")
#         # file_path = os.path.join(module_dir, 'data_to_predict.csv')
#         print("22222222222222")
#         # file = pd.read_csv(file_path, encoding= 'unicode_escape')
#         # print("..............file",file)
#         # print(file.head())
#         from kedro.framework.context import load_context
#         context = load_context('./')
#         return output
#     except Exception as e:
#         print("error",e)
#         logger.error("error in load_file_from_kedro api %s" % e)
#         return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)