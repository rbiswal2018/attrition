import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../app/components/authentications/login/login.component';
import { DashboardComponent } from '../app/components/dashboard/dashboard.component';
import { DescriptiveComponent } from '../app/components/tab-components/descriptive/descriptive.component';
import { PredictiveComponent } from '../app/components/tab-components/predictive/predictive.component';
import { ComparisonComponent } from '../app/components/tab-components/comparison/comparison.component';
import { UserComponent } from '../app/components/admin-panel/user/user.component';
import { RoleComponent } from '../app/components/admin-panel/role/role.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ObserveModelComponent } from './components/admin-panel/observe-model/observe-model.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'descriptive',
    component: DescriptiveComponent
  },
  {
    path: 'predictive',
    component: PredictiveComponent
  },
  {
    path: 'comparison',
    component: ComparisonComponent
  },
  {
    path: 'user',
    component: UserComponent
  },
  {
    path: 'role',
    component: RoleComponent
  },
  {
    path: 'observemodel',
    component: ObserveModelComponent
  },
  {
    path: 'settings',
    component: SettingsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
