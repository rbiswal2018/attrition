import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnChanges {
  loginStatus: any = localStorage.getItem('user_data');

  ngOnInit() {
  }
  ngOnChanges() {
  }

}
