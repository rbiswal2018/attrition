import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import {MatTabsModule} from '@angular/material/tabs';
import {MatMenuModule} from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatIconModule} from '@angular/material/icon';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatTreeModule} from '@angular/material/tree';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/authentications/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DescriptiveComponent } from './components/tab-components/descriptive/descriptive.component';
import { PredictiveComponent } from './components/tab-components/predictive/predictive.component';
import { ComparisonComponent } from './components/tab-components/comparison/comparison.component';
import { ComparisonFilterComponent } from './components/tab-components/comparison/comparison-filter/comparison-filter.component';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { LinechartComponent } from './ngx-charts/linechart/linechart.component';
import { BasicLineChartComponent } from './ngx-charts/basic-line-chart/basic-line-chart.component';
import { TreemapComponent } from './ngx-charts/treemap/treemap.component';
import { UserComponent } from './components/admin-panel/user/user.component';
import { UserFormComponent } from './components/admin-panel/user/user.component';
import { ObserveModelComponent } from './components/admin-panel/observe-model/observe-model.component';
import { DataTablesModule } from "angular-datatables";
import {MatTooltipModule} from '@angular/material/tooltip';
import { UserEditFormComponent } from './components/admin-panel/user/user.component';
import { RoleFormComponent } from './components/admin-panel/role/role.component';
import { RoleEditFormComponent } from './components/admin-panel/role/role.component';
import { RoleComponent } from './components/admin-panel/role/role.component';
import { TokenInterceptorService } from './common/services/intereptor.service';
import { BarChartComponent } from './ngx-charts/bar-chart/bar-chart.component';
import { PieChartComponent } from './ngx-charts/pie-chart/pie-chart.component';
import { SelectAutocompleteModule } from 'mat-select-autocomplete';
import { ModelhealthComponent } from './predictive/modelhealth/modelhealth.component';
import { PredictvePartComponent } from './predictive/predictve-part/predictve-part.component';
import { LineChartComponent } from './ngx-charts/line-chart/line-chart.component';
import { PredictiveNavComponent } from './components/predictive-nav/predictive-nav.component';
import { SimulationComponent } from './predictive/simulation/simulation.component';
import { PredictiveTreeMapComponent } from './ngx-charts/predictive-tree-map/predictive-tree-map.component';
import { ParametersBarChartComponent } from './ngx-charts/parameters-bar-chart/parameters-bar-chart.component';
import { PdbCurveComponent } from './ngx-charts/pdb-curve/pdb-curve.component';
import { PredictEmpTableComponent } from './predictive/predict-emp-table/predict-emp-table.component';
import { GroupedBarChartComponent } from './ngx-charts/grouped-bar-chart/grouped-bar-chart.component';
import { RoundPipe, CapitalizePipe, NullToDashPipe, ReversePipe, Division, PaddZero } from "./common/shared/utility";
import { EmpSearchComponent } from './predictive/simulation/emp-search/emp-search.component';
import { SettingsComponent } from './components/settings/settings.component';
import { LoaderComponent } from './components/loader/loader.component';
import { HorizontalStackedComponent } from './ngx-charts/horizontal-stacked/horizontal-stacked.component';
import { PredictiveNgxTreemapComponent } from './ngx-charts/predictive-ngx-treemap/predictive-ngx-treemap.component';
declare var $: any;

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    DashboardComponent,
    DescriptiveComponent,
    PredictiveComponent,
    ComparisonComponent,
    LinechartComponent,
    BasicLineChartComponent,
    TreemapComponent,
    UserComponent,
    UserFormComponent,
    UserEditFormComponent,
    RoleComponent,
    RoleFormComponent,
    RoleEditFormComponent,
    BarChartComponent,
    PieChartComponent,
    ModelhealthComponent,
    PredictvePartComponent,
    ComparisonFilterComponent,
    GroupedBarChartComponent,
    LineChartComponent,
    PredictiveNavComponent,
    SimulationComponent,
    PaddZero,
    PredictiveTreeMapComponent,
    ParametersBarChartComponent,
    PdbCurveComponent,
    PredictEmpTableComponent,
    EmpSearchComponent,
    RoundPipe,
    CapitalizePipe,
    NullToDashPipe,
    ReversePipe,
    Division,    
    SettingsComponent, LoaderComponent, HorizontalStackedComponent, PredictiveNgxTreemapComponent,
    ObserveModelComponent
  ],
  imports: [
    NgbModule,
    NgbModalModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatIconModule,
    MatMenuModule,
    MatSelectModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatButtonModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatTreeModule,
    NgxChartsModule,
    SelectAutocompleteModule,
    DataTablesModule,
    MatIconModule,
    MatTooltipModule

  ],
  entryComponents: [UserFormComponent, UserEditFormComponent, RoleFormComponent, RoleEditFormComponent],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
