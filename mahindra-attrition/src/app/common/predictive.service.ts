import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictiveService {

  constructor(public httpClient: HttpClient, private router: Router) { }
  
  get_model_health_data() {
    return this.httpClient.get(environment.apiUrl + 'api-modelhealth/insights/');
  }
  insert_model_health_data() {
    return this.httpClient.get(environment.apiUrl + 'api-modelhealth/insertmodelhealth/');
  }
  get_pdb_data(data){
    return this.httpClient.post(environment.apiUrl + "api-predictive/pdb_curve/",data);
  }

  get_pdb_dropdown(){
    return this.httpClient.get(environment.apiUrl + "api-predictive/pdb_dropdown/");
  }
  predictive_tree_map_list(data){
    return this.httpClient.post(environment.apiUrl + "api-predictive/state_predictive/",data)
  }
  lime_explainer_graph_data(data){
    return this.httpClient.post(environment.apiUrl + "api/simulation/lime_explainer/",data)
  }

  survival_curve_data(data){
    return this.httpClient.post(environment.apiUrl + "api/simulation/get_surv_curve_data/",data)
  }

  employee_data(emp_id){
    return this.httpClient.post(environment.apiUrl + "api/simulation/emp_detail/",emp_id)
  }
  
  

  getpredicitveFilterDropdownData() {
    return this.httpClient.get(environment.apiUrl + 'api-predictive/predicitve_drop_down_data/').pipe(map(data => {
         return data;
    }));
 }


  simulation_data(data){
    return this.httpClient.post(environment.apiUrl + "api/simulation/get_simulation_data/",data);
  }
  predictive_emp_list(data){
    return this.httpClient.post(environment.apiUrl + "api-predictive/get_emp_predict/",data)
  }
get_line_chart_data(){
  return this.httpClient.get(environment.apiUrl + 'api-modelhealth/roc_chart/');
}

get_predictive_parameter_service(data){
  return this.httpClient.post(environment.apiUrl + "api-predictive/parameter_predict_data/",data)
}
}
