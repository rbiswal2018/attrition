import { OnInit, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class AdminpanelServices {

    private userTableGlobalData = new BehaviorSubject<string>('');
    userTableGlobalData$ = this.userTableGlobalData.asObservable();
    private userTableRowData = new BehaviorSubject<string>('');
    userTableRowData$ = this.userTableRowData.asObservable();

    private roleTableGlobalData = new BehaviorSubject<string>('');
    roleTableGlobalData$ = this.roleTableGlobalData.asObservable();
    private roleTableRowData = new BehaviorSubject<string>('');
    roleTableRowData$ = this.roleTableRowData.asObservable();

    constructor(public httpClient: HttpClient) {}

    fetchUser() {
        return this.httpClient.get(environment.apiUrl + 'api/auth/fetch_users/');
    }

    addNewUser(data) {
       return this.httpClient.post(environment.apiUrl + 'api/auth/signup/', data).pipe(map(user => {
            return user;
       }));
    }

    get_model_observe_data() {
        return this.httpClient.get(environment.apiUrl + 'api-modelhealth/get_model_observation/')
        };

    select_model(data) {
        console.log(data[0])
        return this.httpClient.post(environment.apiUrl + 'api/load_default/select_model/',data).pipe(map(user => {
            return user;
       }));
        };
    run_model(data) {
        console.log(data[0])
        return this.httpClient.post(environment.apiUrl + 'api/loader/create_prediction_view/',data).pipe(map(user => {
            return user;
        }));
        };
             
         
     

    editUser(data) {
        return this.httpClient.put(environment.apiUrl + 'api/auth/edit_user/', data).pipe(map(user => {
            return user;
       }));
    }

    deleteUser(data, id) {
        return this.httpClient.delete(environment.apiUrl + 'api/auth/delete_user/'+ id +'/', data).pipe(map(user => {
            return user;
       }));
    }

    setUserGlobalData(data) {
        this.userTableGlobalData.next(data);
    }

    editUserRowData(data) {
        this.userTableRowData.next(data);
    }

    setRoleGlobalData(data) {
        this.roleTableGlobalData.next(data);
    }

    editRoleRowData(data) {
        this.roleTableRowData.next(data);
    }

    getRole() {
        return this.httpClient.get(environment.apiUrl + 'api/auth/get_role/');
    }

    getPermission() {
        return this.httpClient.get(environment.apiUrl + 'api/auth/get_role_permission/');
    }

    getPermissionList() {
        return this.httpClient.get(environment.apiUrl + 'api/auth/get_permission/');
    }
    
    getStateBusinessUnit(){
        return this.httpClient.get(environment.apiUrl + 'api-analytics/get_role_dropdown_data/');
    }


    addNewRole(data) {
        return this.httpClient.post(environment.apiUrl + 'api/auth/add_role/', data).pipe(map(user => {
            return user;
       }));
    }

    editRole(data, id) {
        return this.httpClient.put(environment.apiUrl + 'api/auth/edit_role/'+ id +'/', data).pipe(map(user => {
            return user;
       }));
    }

    deleteRole(data) {
        return this.httpClient.post(environment.apiUrl + 'api/auth/delete_role/', data).pipe(map(res => {
            return res;
       }));
    }

    setDefaultPwd(id) {
        return this.httpClient.put(environment.apiUrl + 'api/auth/set_default_password/'+ id +'/', '');
    }

    changePwd(data) {
        return this.httpClient.patch(environment.apiUrl + 'api/auth/change_password/', data);
    }

}
