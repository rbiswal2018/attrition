import { OnInit, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(public httpClient: HttpClient) {}

    loginUser(data) {
       return this.httpClient.post(environment.apiUrl + 'api/auth/login/', data).pipe(map(user => {
            return user;
       }));
    }

    logOutUser(data) {
        return this.httpClient.post(environment.apiUrl + 'api/auth/logout/', data).pipe(map(res => {
       }));
    }

    resetPassword(data) {
       return this.httpClient.post(environment.apiUrl + 'api/auth/reset_password/', data).pipe(map(res => {
            return res;
       }));
    } 

}
