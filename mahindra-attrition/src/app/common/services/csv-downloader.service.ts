import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CsvDownloaderService {

  constructor() { }

  convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
      var line = '';
      for (var index in array[i]) {
        if (line != '') line += ','

        line += array[i][index];
      }

      str += line + '\r\n';
    }

    return str;
  }
  exportCSVFile(headers, items, fileTitle) {

    if (headers) {
      items.unshift(headers);
    }
    // Convert Object to JSON
    var jsonObject = JSON.stringify(items);

    var csv = this.convertToCSV(jsonObject);

    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
      var link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
        // Browsers that support HTML5 download attribute
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", exportedFilenmae);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

  exportCSVFile_absolute_percent(headers, items, fileTitle,data,value_name) {

    if (headers) {
      items.unshift(headers);
    }
    // Convert Object to JSON
    var jsonObject = JSON.stringify(items);

    var csv = this.convertToCSV(jsonObject);

    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
      var link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
        // Browsers that support HTML5 download attribute
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", exportedFilenmae);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }

if (value_name == "performer_by_gem_performer") {
      data[value_name].forEach(element => {
        element["total_count"] = 0 
      });
    } 
  }

  download_csv($event, value_name, range_value, per_value, file_name, data) {
    var emp_count

    if (range_value != "true") {
      var key_value = function () {
        if (value_name == "gender_based_performer") {
          return "gender"
        }
        else if (value_name == "grade_based_performer") {

          return "grade_name"
        }
        else if (value_name == "performer_by_language_count") {
          return "languages_known_count"
        }
        else if (value_name == "oprn_category_based_performer") {
          return "oprn_category"
        }
        else if (value_name == "performer_by_star_performer") {
          return "star_performer"
        }
        else if (value_name == "performer_by_gem_performer") {

          data[value_name].forEach(element => {
            element["total_count"] = element["total"]
          });

          return "performer"
        } else if (value_name == "performer_by_training") {
          return "training_data"
        } else if (value_name == "tier_data") {
          return "tier"
        }
        else if (value_name == "performer_by_pip") {
          return "pip_data"
        }
        else if (value_name == "performer_by_dic") {
          return "dic_data"
        }
      }()
    } else if (range_value == "true") {
      var key_value = function () {
        if (value_name == "performer_by_age") {
          return "age_range"
        }
        else if (value_name == "performer_by_leave_count") {
          return "leave_range"
        }
        else if (value_name == "performer_by_experience_in_current_role") {
          return "exp_range"
        }
        else if (value_name == "performer_by_experience_in_mf") {
          return "exp_range"
        }
        else if (value_name == "performer_by_Compensation") {

          return "percentile"
        }
      }()
    }

    var itemsNotFormatted;
    var headers = {
      key: key_value,
      total_count: "Total_Count",
      a_count: 'A_count', // remove commas to avoid errors
      b_count: "B_count",
      c_count: "C_count",
      d_count: "D_count"
    };

    if (per_value == 'true') {
      var headers = {
        key: key_value,
        total_count: "Total_Count_Percentage",
        a_count: 'A_Count_Percentage', // remove commas to avoid errors
        b_count: "B_Count_Percentage",
        c_count: "C_Count_Percentage",
        d_count: "D_Count_Percentage"
      }
    };
    if (per_value == 'true') { itemsNotFormatted = data[value_name] } else if (per_value != 'true') {
      itemsNotFormatted = data[value_name]
    }
    if (per_value == 'true') { emp_count = "total" } else if (per_value != 'true') {
      emp_count = "total_count"
    }
    if (per_value == 'true') { key_value = "key" } else if (per_value != 'true') {
    }
    var itemsFormatted = [];
    // format the data
    itemsNotFormatted.forEach((item) => {


      if (per_value == "true") {
        item[emp_count] = "100%"
      }
      itemsFormatted.push({
        key: "'" + item[key_value] + "'",
        total_count: item[emp_count],
        a_count: item.a_count,
        b_count: item.b_count,
        c_count: item.c_count,
        d_count: item.d_count,
      });
    });

    var fileTitle = file_name; // or 'my-unique-title'

    this.exportCSVFile_absolute_percent(headers, itemsFormatted, fileTitle,data,value_name); // call the exportCSVFile() function to process the JSON and trigger the download
  }

  download_top_managers($event, data,oprn_cat) {

  
    var itemsNotFormatted = data

    if (["ELCV","ECVR","EREF","ELMV","EOAS","EDMT","EFREF","SBH-FES","EFES","EFNM","EREF"].includes(oprn_cat)){

      var headers = {
        Emp_Id: "Emp_Id",
        EmpName: "EmpName",
        Operational_Category: "Oper Category",
        RBMI_Percentage: "Business Count",
        Performence: "Performence ",
      }
        var itemsFormatted = []

        itemsNotFormatted.forEach((item) => {
          itemsFormatted.push({
            Emp_ID: item.empid,
            EmpName: item.empname,
            Operational_Category: item.oprn_category,
            RBMI_Percentage: item.rbmi,
            Performence: item.performance_category
          });
        })
  
      }
      else if(["CVCL","AREC"].includes(oprn_cat)){

      var headers = {
        Emp_Id: "Emp_Id",
        EmpName: "EmpName",
        Operational_Category: "Oper Category",
        RBMI_Percentage: "CE%",
        Performence: "Performence ",
      }
        var itemsFormatted = []

        itemsNotFormatted.forEach((item) => {
          itemsFormatted.push({
            Emp_ID: item.empid,
            EmpName: item.empname,
            Operational_Category: item.oprn_category,
            RBMI_Percentage: item.rbmi,
            Performence: item.performance_category
          });
        })
      }
      else if(["EOCV","EOCE","NLMV"].includes(oprn_cat)){

        var headers = {
          Emp_Id: "Emp_Id",
          EmpName: "EmpName",
          Operational_Category: "Oper Category",
          RBMI_Percentage: "Finance Amount",
          Performence: "Performence ",
        }
          var itemsFormatted = []
  
          itemsNotFormatted.forEach((item) => {
            itemsFormatted.push({
              Emp_ID: item.empid,
              EmpName: item.empname,
              Operational_Category: item.oprn_category,
              RBMI_Percentage: item.rbmi,
              Performence: item.performance_category
            });
          })
        }

        else if(["RSPM"].includes(oprn_cat)){

          var headers = {
            Emp_Id: "Emp_Id",
            EmpName: "EmpName",
            Operational_Category: "Business Count",
            RBMI_Percentage: "CE%",
            Performence: "Performence ",
          }
            var itemsFormatted = []
    
            itemsNotFormatted.forEach((item) => {
              itemsFormatted.push({
                Emp_ID: item.empid,
                EmpName: item.empname,
                Operational_Category: item.cd_per,
                RBMI_Percentage: item.rbmi,
                Performence: item.performance_category
              });
            })
          }
          else if(["RREC","FREC"].includes(oprn_cat)){

            var headers = {
              Emp_Id: "Emp_Id",
              EmpName: "EmpName",
              Operational_Category: "CD%",
              RBMI_Percentage: "CE%",
              Performence: "Performence ",
            }
              var itemsFormatted = []
      
              itemsNotFormatted.forEach((item) => {
                itemsFormatted.push({
                  Emp_ID: item.empid,
                  EmpName: item.empname,
                  Operational_Category: item.cd_per,
                  RBMI_Percentage: item.rbmi,
                  Performence: item.performance_category
                });
              })
            }

            else if(["ESBC","EHBC","ENPC","LREC"].includes(oprn_cat)){

              var headers = {
                Emp_Id: "Emp_Id",
                EmpName: "EmpName",
                Operational_Category: "Oprn Category",
                RBMI_Percentage: "RBMI Achivements",
                Performence: "Performence ",
              }
                var itemsFormatted = []
        
                itemsNotFormatted.forEach((item) => {
                  itemsFormatted.push({
                    Emp_ID: item.empid,
                    EmpName: item.empname,
                    Operational_Category: item.oprn_category,
                    RBMI_Percentage: item.rbmi,
                    Performence: item.performance_category
                  });
                })
              }
  

  
    var fileTitle = "Employee Data";
    this.exportCSVFile(headers, itemsFormatted, fileTitle);
  }


  download_predictive_user_data($event, data) {
    var itemsNotFormatted = data
    let headers:any = {};
    
 
      headers = {
        Emp_Id: "Emp_Id",
        EmpName: "EmpName",
        PredictedAttrition: "PredictedAttrition",
        Status: "Status ",
      }
        var itemsFormatted = []

        itemsNotFormatted.forEach((item) => {
      
          itemsFormatted.push({
            Emp_ID: item.employee_id,
            EmpName: item.empname,
            PredictedAttrition: item.probability,
            Status: item.grade
          });
        })
  
    var fileTitle = "Employee Data";
    this.exportCSVFile(headers, itemsFormatted, fileTitle);
  }
  download_pdb_csv(Category, data) {
    var itemsNotFormatted = data
    var headers = {
      Category: "Category",
      Dependency: "Dependency",
      Count: "Count",
      PartialDependency: "PartialDependency",
    };
    var itemsFormatted = []
    itemsNotFormatted.forEach((item) => {
      itemsFormatted.push({
        Category: Category,
        Dependency: item.Name,
        Count: item.count,
        PartialDependency: item.Average,

      });
    })
    var fileTitle = "Pdb_chart_data";
    this.exportCSVFile(headers, itemsFormatted, fileTitle);
  }
  download_parameter_csv(data) {
    var itemsNotFormatted = data
    var headers = {
      positive_parameters: "Positive parameters",
      negative_parameters: "Negative Parameters",
      Value: "Value"
    };
    var itemsFormatted = []
    itemsNotFormatted.forEach((item) => {
      
      itemsFormatted.push({
       
        positive_parameters: item.positive_parameter,
        negative_parameters: item.negative_parameter,
        Value: item.value,

      });
    }
    
    )
    
    var fileTitle = "Pdb_chart_data";
    this.exportCSVFile(headers, itemsFormatted, fileTitle);
  }
}
