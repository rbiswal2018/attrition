import { OnInit, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class FilterService {
    constructor(public httpClient: HttpClient) {}

    getFilterDropdownData() {
       return this.httpClient.get(environment.apiUrl + 'api-analytics/drop_down_data/').pipe(map(data => {
            return data;
       }));
    }

    getEmpLeftCount(data) {
       return this.httpClient.post(environment.apiUrl + 'api-analytics/left_count', data).pipe(map(count => {
            return count;
       }));
    }

    getAttritionPercentage(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/attrition_percentage', data).pipe(map(percentage => {
            return percentage;
        }));
    }

    getTenureData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_tenure_data/', data).pipe(map(tenure => {
            return tenure;
        }));
    }

    getAttritionTrend(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/attrition_trend/', data).pipe(map(trend => {
            return trend;
        }));
    }

    getTreemapData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/tree_map/', data).pipe(map(treemap => {
            return treemap;
        }));
    }

    getCompensationData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/compensation_data/', data).pipe(map(compensation => {
            return compensation;
        }));
    }

    getCohertData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_cohert_analytics_data/', data).pipe(map(compensation => {
            return compensation;
        }));
    }

    getHiringData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_recruitement_data/', data).pipe(map(hiring => {
            return hiring;
        }));
    }

    getRatingData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_rating_data/', data).pipe(map(rating => {
            return rating;
        }));
    }

    getAgeGroup(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/age_graph/', data).pipe(map(agegroup => {
            return agegroup;
        }));
    }

    getGemData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/gem_performer_graph/', data).pipe(map(gem => {
            return gem;
        }));
    }

    getStarData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/star_performer_graph/', data).pipe(map(star => {
            return star;
        }));
    }

    getGenderData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_gender_data/', data).pipe(map(gender => {
            return gender;
        }));
    }

    getGradeData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/grade_graph/', data).pipe(map(grade => {
            return grade;
        }));
    }

    getPipData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/pip_data_graph/', data).pipe(map(pip => {
            return pip;
        }));
    }

    getDisData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/dic_data_graph/', data).pipe(map(dis => {
            return dis;
        }));
    }

    getMaritalData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_marital_data', data).pipe(map(status => {
            return status;
        }));
    }

    getTrainingData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/training_data_graph/', data).pipe(map(training => {
            return training;
        }));
    }

    getvoluntaryData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/vol_invol_data/', data).pipe(map(vol => {
            return vol;
        }));
    }

    getBusinssServiceFunction(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_business_service_function/', data).pipe(map(bsf => {
            return bsf ;
        }));
    }
    
    getMajorServiceFunction(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_major_service_function/', data).pipe(map(bsf => {
            return bsf ;
        }));
    }


    getQualificationData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/higher_qualification_data/', data).pipe(map(qualification => {
            return qualification;
        }));
    }

    getManagerDeptData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/mcares_department/', data).pipe(map(mdept => {
            return mdept;
        }));
    }

    getWorkContractData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_work_contract_data/', data).pipe(map(work => {
            return work;
        }));
    }

    getReportManagerData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/reporting_manager_data/', data).pipe(map(report => {
            return report;
        }));
    }

    getManagerMcaresData(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/mcares_manager/', data).pipe(map(mcaredata => {
            return mcaredata;
        }));
    }
    getFunctionalCompetency(data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_functional_competency_score/', data).pipe(map(functionalCompetency => {
            return functionalCompetency;
        }));
    }


     getEmployeeCsvChartData(chart_data) {
        let url = environment.apiUrl + 'api-analytics/download_csv_chart_Data/'
        let options = {}
        options['responseType'] = 'blob' as 'json';
        return this.httpClient.post<Blob>(url,chart_data,options)  
    }

    getEmployeeContributionChartData(chart_data) {
        let url = environment.apiUrl + 'api-analytics/get_contribution_data/'
        let options = {}
        options['responseType'] = 'blob' as 'json';
        return this.httpClient.post<Blob>(url,chart_data,options)  
    }
    getEmployeeContributionToggleData(chart_data) {
        return this.httpClient.post(environment.apiUrl + 'api-analytics/get_toggle_contribution_data/', chart_data).pipe(map(functionalCompetency => {
            return functionalCompetency;
        }));
    }
     getEmployeeConsolidateCsvChartData(chart_data) {
        let url = environment.apiUrl + 'api-analytics/download_csv_consolidate_chart_Data/'
        let options = {}
        options['responseType'] = 'blob' as 'json';
        return this.httpClient.post<Blob>(url,chart_data,options)
       
    }
      


}
