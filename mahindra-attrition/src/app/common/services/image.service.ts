import { OnInit, Injectable } from '@angular/core';
import domtoimage from 'dom-to-image-more';
declare var $: any;

@Injectable({
    providedIn: 'root'
})
export class ImageService {
    
    constructor() {}

    makeTable(node, data, obj) {
        let table = $('<table class="table table-bordered image-table"><thead><tr></tr></thead><tbody></tbody></table>');
        for (let val of obj) {
            $(table).find("thead tr").append('<th>' + val + '</th>');
        }
        for (let obj of data) {
            $(table).find("tbody").append('<tr><td>' + obj.name + '</td><td>' + obj.value + '</td></tr>');
        }
        $("#"+node).append(table);
        this.downloadImage(node, table);
    }

    makeComparisonTable(node, data, obj) {
        if (node === "hidden-grade-graph") {
            $("#"+node).show();
        }
        let table = $('<table class="table table-bordered image-table"><thead><tr></tr></thead><tbody></tbody></table>');
        for (let val of obj) {
            $(table).find("thead tr").append('<th>' + val + '</th>');
        }
        let tempObj = {};
        for (let obj of data) {
            const name = obj.name;
            for (let val of obj.series) {
                if (val.name === 'left') {
                    tempObj[name+' LEFT'] = val.value;
                } else {
                    tempObj[name+' RIGHT'] = val.value;
                }
            }
        }
        const final_obj_length = Object.keys(tempObj);
        for (let i = 0; i < final_obj_length.length; i++) {
            $(table).find("tbody").append('<tr><td>' + final_obj_length[i] + '</td><td>' + tempObj[final_obj_length[i]] + '</td></tr>');
        }
        $("#"+node).append(table);
        this.downloadImage(node, table);
    }

    downloadImage(node, table) {
        const filename = $(document.getElementById(node)).find("h2").text();
        domtoimage.toJpeg(document.getElementById(node), { quality: 0.95 })
        .then(function (dataUrl) {
            var link = document.createElement('a');
            link.download = filename;
            link.href = dataUrl;
            link.click();
            if (node === "hidden-grade-graph") {
                $("#"+node).hide();
            }
            $(table).remove();
        });
    }

}
