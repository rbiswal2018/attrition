import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
   constructor() {}
   intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.indexOf('/reset_password') > 1 || req.url.indexOf('/login') > 1) {
            return next.handle(req);
        }
        const idToken = JSON.parse(localStorage.getItem('user_data')).message.token;
        if (idToken) {
            const cloned = req.clone({
                headers: req.headers.set('Authorization', 'Token ' + idToken)
            });
            return next.handle(cloned);
        } else {
            return next.handle(req);
        }
    }
}
