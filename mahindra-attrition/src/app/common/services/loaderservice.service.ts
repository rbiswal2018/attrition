import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class LoaderserviceService {
  constructor(public httpClient: HttpClient, private router: Router) { }
  private getOptions() {
    return  {
      headers:  new HttpHeaders().set('Authorization', 'Token ' + localStorage.getItem('token'))
    };
  }

  dropdown_data() {
    return this.httpClient.get(environment.apiUrl + 'api/loader/dropdown_data/')
  }
  

  save_file(files, table_name) {
    const formData: FormData = new FormData();
    for (let index = 0; index < files.length; index++) {
      const element = files[index];
      formData.append('file', element,element.name); 
    }
    formData.append('table_name', table_name);

    return this.httpClient.post(environment.apiUrl + 'api/loader/file/', formData,{
      reportProgress:true,
      observe:"events",
    })
  }

  // Download Loading Templates
 
  public async downloadResource(): Promise<Blob> {
    let url = environment.apiUrl +'api/loader/download/',
        options = this.getOptions();
    options['responseType'] = 'blob' as 'json';
    const file =  await this.httpClient.get<Blob>(url,options).toPromise();
    return file;
  }

  refresh_data(){
    return this.httpClient.get(environment.apiUrl + "api/loader/refresh_view/")
  }
}


