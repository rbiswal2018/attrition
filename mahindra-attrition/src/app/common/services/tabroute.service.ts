import { Component, OnInit, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TabRoute {


    private tabRouteValue = new BehaviorSubject<number>(0);
    tabRouteValue$ = this.tabRouteValue.asObservable();

    private dloader = new BehaviorSubject<number>(0);
    dloader$ = this.dloader.asObservable();

    private cloader = new BehaviorSubject<number>(0);
    cloader$ = this.cloader.asObservable();

    private clearLeftTreemapGraph = new BehaviorSubject<number>(0);
    clearLeftTreemapGraph$ = this.clearLeftTreemapGraph.asObservable();

    //comparison
    private dataRouteValue = new BehaviorSubject<string>('');
    dataRouteValue$ = this.dataRouteValue.asObservable();
    private rightQualRouteValue = new BehaviorSubject<string>('');
    rightQualRouteValue$ = this.rightQualRouteValue.asObservable();
    private leftmdept = new BehaviorSubject<string>('');
    leftmdept$ = this.leftmdept.asObservable();
    private rightmdept = new BehaviorSubject<string>('');
    rightmdept$ = this.rightmdept.asObservable();
    private leftreport = new BehaviorSubject<string>('');
    leftreport$ = this.leftreport.asObservable();
    private rightreport = new BehaviorSubject<string>('');
    rightreport$ = this.rightreport.asObservable();    
    private leftmmcares = new BehaviorSubject<string>('');
    leftmmcares$ = this.leftmmcares.asObservable();
    private rightmmcares = new BehaviorSubject<string>('');
    rightmmcares$ = this.rightmmcares.asObservable();    
    private leftwork = new BehaviorSubject<string>('');
    leftwork$ = this.leftwork.asObservable();
    private rightwork = new BehaviorSubject<string>('');
    rightwork$ = this.rightwork.asObservable();  
    private leftFunctionalCompetency = new BehaviorSubject<string>('');
    leftFunctionalCompetency$ = this.leftFunctionalCompetency.asObservable();
    private rightFunctionalCompetency = new BehaviorSubject<string>('');
    rightFunctionalCompetency$ = this.rightFunctionalCompetency.asObservable(); 
    private leftBusinessService = new BehaviorSubject<string>('');
    leftBusinessService$ = this.leftBusinessService.asObservable();
    private rightBusinessService = new BehaviorSubject<string>('');
    rightBusinessService$ = this.rightBusinessService.asObservable();
    private leftMajorService = new BehaviorSubject<string>('');
    leftMajorService$ = this.leftMajorService.asObservable();
    private rightMajorService = new BehaviorSubject<string>('');
    rightMajorService$ = this.rightMajorService.asObservable();    

    private gradeData = new BehaviorSubject<string>('');
    gradeData$ = this.gradeData.asObservable();



    //descriptive
    private descQual = new BehaviorSubject<string>('');
    descQual$ = this.descQual.asObservable();
    private descMdept = new BehaviorSubject<string>('');
    descMdept$ = this.descMdept.asObservable();
    private descReport = new BehaviorSubject<string>('');
    descReport$ = this.descReport.asObservable();
    private descWork = new BehaviorSubject<string>('');
    descWork$ = this.descWork.asObservable();
    private descMmcares = new BehaviorSubject<string>('');
    descMmcares$ = this.descMmcares.asObservable();
    private descFunctionalCompetency = new BehaviorSubject<string>('');
    descFunctionalCompetency$ = this.descFunctionalCompetency.asObservable();


    
    constructor() {}

    redirectToTab(link: number) {
        this.tabRouteValue.next(link);
    }

    setData(val) {
        this.dataRouteValue.next(val);
    }

    setRightQualRouteValue(val) {
        this.rightQualRouteValue.next(val);
    }

    setLeftMdept(val) {
        this.leftmdept.next(val);
    }

    setRightMdept(val) {
        this.rightmdept.next(val);
    }

    setLeftReport(val) {
        this.leftreport.next(val);
    }

    setRightReport(val) {
        this.rightreport.next(val);
    }

    setLeftMmcares(val) {
        this.leftmmcares.next(val);
    }

    setRightMmcares(val) {
        this.rightmmcares.next(val);
    }

    setLeftWork(val) {
        this.leftwork.next(val);
    }

    setRightWork(val) {
        this.rightwork.next(val);
    }
    setLeftFunctionalCompetency(val) {
        this.leftFunctionalCompetency.next(val);
    }
    setRightFunctionalCompetency(val) {
        this.rightFunctionalCompetency.next(val);
    }
    setLeftBusinessService(val) {
        this.leftBusinessService.next(val);
    }
    setRightBusinessService(val) {
        this.rightBusinessService.next(val);
    }
    setLeftMajorService(val) {
        this.leftMajorService.next(val);
    }
    setRightMajorService(val) {
        this.rightMajorService.next(val);
    }  

    setDescQual(val) {
        this.descQual.next(val);
    }

    setDescMdept(val) {
        this.descMdept.next(val);
    }

    setDescReport(val) {
        this.descReport.next(val);
    }

    setDescWork(val) {
        this.descWork.next(val);
    }

    setDeskMmcares(val) {
        this.descMmcares.next(val);
    }

    setGradeData(val) {
        this.gradeData.next(val);
    }
    setDescFunctionalCompetency(val) {
        this.descFunctionalCompetency.next(val);
    }

    setLoader(val) {
        this.dloader.next(val);
    }

    setComparisonLoader(val) {
        this.cloader.next(val);
    }

    setLeftGraphArrEmpty(val) {
        this.clearLeftTreemapGraph.next(val);
    }

}
