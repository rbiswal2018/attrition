import { Pipe, PipeTransform } from '@angular/core';
import { Injectable } from '@angular/core';
import domtoimage from 'dom-to-image-more';


@Injectable({
  providedIn: 'root'
})

@Pipe({ name: 'round' })
export class RoundPipe implements PipeTransform {
  transform(value) {
    return isNaN(value) ? value : parseFloat(parseFloat(value).toFixed(2));
  }
}

@Pipe({ name: 'capitalize' })
export class CapitalizePipe implements PipeTransform {
  transform(str) {
    str = str.toLowerCase().trim().replace(/_/g, " ").split(/[\s]+/);
    str = str.map(function (d) {
      d = d.charAt(0).toUpperCase() + d.slice(1);
      return d;
    })
    return str.join(" ");
  }
}

@Pipe({ name: 'nulltodash' })
export class NullToDashPipe implements PipeTransform {
  transform(val) {
    return (val == null) ? '-' : val;
  }
}

@Pipe({ name: 'reverse' })

export class ReversePipe implements PipeTransform {
  transform(value) {
    return value.slice().reverse();
  }
}

@Pipe({ name: 'division' })
export class Division implements PipeTransform {
  transform(value) {
    return value / 12;
  }
}

@Pipe({ name: 'leftzero' })
export class PaddZero implements PipeTransform {
  transform(value) {
    if (value.toString().length == 1) {
      return "0" + value;
    } else {
      return value;
    }
  }
}

export class Utils {
  public static setContainerState(el, status) {
    el = el.querySelector('.loading');
    if (status === 'nodata') {
      el.hidden = false;
      el.querySelector('.loading-img').hidden = true;
      // $('.fa-download').hide();
      // $('.fa-info-circle').hide();
      el.querySelector('.no-data').hidden = false;
    } else if (status === 'loading') {
      el.hidden = false;
      el.querySelector('.loading-img').hidden = false;
      el.querySelector('.no-data').hidden = true;
    } else if (status === 'done-loading') {
      el.hidden = true;
    }
  }
  public static form_temp_table(recv_data, recv_key, per_value) {
    // console.log(recv_key)
    var temp_recv_key = ''

    if (recv_key == 'languages_known_count') {

      temp_recv_key = "No.of</br>LanguagesKnown"
    }
    else if(recv_key == "tier"){
      temp_recv_key = "Tier"

    }
    else if(recv_key == "age_range"){
      temp_recv_key = "Age Group"
      
    }else if(recv_key == "oprn_category"){
      temp_recv_key = "Operational</br>Category"
      
    }else if(recv_key == "leave_range"){
      temp_recv_key = "Leave Range</br>(No. of days)"
      
    }else if(recv_key == "grade_name"){
      temp_recv_key = "Grade"
      
    }else if(recv_key == "training_data"){
      temp_recv_key = "Training"
      
    }else if(recv_key == "performer"){
      temp_recv_key = "Reward"
      
    }else if(recv_key == "percentile"){
      temp_recv_key = `Annual Compensation</br>in Percentile`
      
    }else if(recv_key == "exp_range"){
      temp_recv_key = "Experience in Current Role</br>(in Months)"
      
    }else if(recv_key == "total_exp_range_years"){
      temp_recv_key = "Total Experience</br>(in Years)"
      
    }else if(recv_key == "dic_data"){
      temp_recv_key = "Disciplinary"
      
    }else if(recv_key == "pip_data"){
      temp_recv_key = "PIP"
      
    }
    else if(recv_key == "key"){
      temp_recv_key = "Tier"
      
    }
    else {
      temp_recv_key = recv_key

    }

    let data = []
    var keys = [];
    if (temp_recv_key == 'Reward' || recv_key == 'key') {
      var get_key = 'total'
    }
    else {
      var get_key = 'total_count'
    }
    if (per_value == 'true') {
      recv_data.forEach(element => {
        if (temp_recv_key == 'Grade') {

          let temp_obj = {}
          temp_obj[temp_recv_key] = element['key']
          temp_obj["A(%)"] = element["a_count"].toFixed(1)
          temp_obj["B(%)"] = element["b_count"].toFixed(1)
          temp_obj["C(%)"] = element["c_count"].toFixed(1)
          temp_obj["D(%)"] = element["d_count"].toFixed(1)
          temp_obj["Total"+"</br>"+"(%)"] = 100
          data.push(temp_obj)
        }
        else {
          let temp_obj = {}          
          if(temp_recv_key == 'Total Experience</br>(in Years)'){
            recv_key = "exp_range"
          }
          if(element['key'] == "0-10"){
            element['key'] = element['key'].replace("0-10", "00-10")
          }      
          if(recv_key == 'languages_known_count'||recv_key == 'exp_range'||recv_key =='percentile'||recv_key =='leave_range'||recv_key =='oprn_category'){
            temp_obj[temp_recv_key] = element['key']+"</br>"+" "+"</br>"
            temp_obj["A(%)"] = element["a_count"].toFixed(1)
            temp_obj["B(%)"] = element["b_count"].toFixed(1)
            temp_obj["C(%)"] = element["c_count"].toFixed(1)
            temp_obj["D(%)"] = element["d_count"].toFixed(1)
            if(temp_recv_key == 'Experience in Current Role</br>(in Months)'||temp_recv_key == `Annual Compensation</br>in Percentile`){
              temp_obj["Total"+"</br>"+"(%)"] = 100
            }else{
              temp_obj["Total(%)"] = 100
            }
          }else {           
            temp_obj[temp_recv_key] = element['key'].replace("_", "").replace(" ", "")
            temp_obj["A(%)"] = element["a_count"].toFixed(1)
            temp_obj["B(%)"] = element["b_count"].toFixed(1)
            temp_obj["C(%)"] = element["c_count"].toFixed(1)
            temp_obj["D(%)"] = element["d_count"].toFixed(1)
            temp_obj["Total(%)"] = 100
          }
          data.push(temp_obj)

        }
      }
      )
    }
    else {
      // console.log(recv_key)
      recv_data.forEach(element => {
        let temp_obj = {}        
        if (temp_recv_key == 'Grade') {
          temp_obj[temp_recv_key] = element[recv_key]
          temp_obj["A"] = element["a_count"]
          temp_obj["B"] = element["b_count"]
          temp_obj["C"] = element["c_count"]
          temp_obj["D"] = element["d_count"]
          temp_obj["Total"+"</br>"+"`"] = element[get_key]
          data.push(temp_obj)     
          
        }
        else {
          if(temp_recv_key == 'Total Experience</br>(in Years)'){
            recv_key = "exp_range"
          }
          if(element[recv_key] == "0-10"){
            element[recv_key] = element[recv_key].replace("0-10", "00-10")
          }
          
          if(recv_key == 'languages_known_count'||recv_key == 'exp_range'||recv_key =='percentile'||recv_key =='leave_range'||recv_key =='oprn_category'){
            temp_obj[temp_recv_key] = element[recv_key]+"</br>"+" "+"</br>"
            temp_obj["A"] = element["a_count"]
            temp_obj["B"] = element["b_count"]
            temp_obj["C"] = element["c_count"]
            temp_obj["D"] = element["d_count"]
            if(temp_recv_key == 'Experience in Current Role</br>(in Months)'||temp_recv_key == `Annual Compensation</br>in Percentile`){
              temp_obj["Total"+"</br>"+"`"] = element[get_key]
            }else{
              temp_obj["Total"] = element[get_key]
            }
          }else {
            temp_obj[temp_recv_key] = element[recv_key].replace("_", "").replace(" ", "")
            temp_obj["A"] = element["a_count"]
            temp_obj["B"] = element["b_count"]
            temp_obj["C"] = element["c_count"]
            temp_obj["D"] = element["d_count"]
            temp_obj["Total"] = element[get_key]
          }
          data.push(temp_obj)          
        }
      })
    }
    var temp_str = ""
    temp_str = temp_str + '<table class="table table-responsive-xl image-table-css" style="color: white;,"><tr>'
    for (let key in data[0]) {
      temp_str = temp_str + '<td>' + key + '</td>'
    }
    temp_str = temp_str + "</tr>"
    for (var i = 0; i < data.length; i++) {
      temp_str = temp_str + '<tr>'
      for (let key in data[i]) {
        temp_str = temp_str + '<td>' + data[i][key] + '</td>'
      }
      temp_str = temp_str + '</tr>'
    }
    temp_str = temp_str + "</table>"
    return temp_str
  }
  public static dom_to_img(elm, file_name, value_name, key, data, per_value) {

    // let chart: any = elm.closest(".download-wrap").querySelector('.chart');
    try {
      var chart: any = elm.closest(".download-wrap");
      let data_Array = data[value_name]
      let table_html = this.form_temp_table(data_Array, key, per_value)

      chart.querySelector(".image-table").innerHTML = table_html

      domtoimage.toJpeg(chart)
        .then(function (dataUrl) {
          var link = document.createElement('a');
          link.download = file_name + ".jpeg";
          link.href = dataUrl;
          link.click();
          chart.querySelector(".image-table").innerHTML = ""
        }
    );
    }
    catch (Error) {
      
      chart.querySelector(".image-table").innerHTML = ""
    
    }
    // finally{
    //   chart.querySelector(".image-table").innerHTML = ""
    // }

  }
  public static dom_to_img_predict(elm, file_name) {

    let chart: any = elm.closest(".download-wrap").querySelector('.chart');
    try {
      let chart: any = elm.closest(".download-wrap").querySelector('.chart');
      
    domtoimage.toJpeg(chart,  { width: chart.scrollWidth, height: chart.scrollHeight })
      .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = file_name + ".jpeg";
        link.href = dataUrl;
        link.click();
      }
      
      );

    }
    catch (Error) {
     
     
    }
    // finally{
    //   chart.querySelector(".image-table").innerHTML = ""
    // }

  }

  public static calculateImrValues(dataset, type) {
    let sum = dataset.reduce(function (a, b) { return a + b; }),
      avg = (sum / dataset.length),
      mr = calculateMr(dataset),
      value = 0;

    function calculateMr(dataset: any) {
      let mr = 0,
        count = 0;
      for (let i = 0; i < dataset.length - 1; i++) {
        mr += Math.abs(dataset[i] - dataset[i + 1]);
        count++;
      }
      return (mr / count);
    }

    if (type == "UCL") {
      value = avg + (2.66 * mr);
    } else if (type == "LCL") {
      value = avg - (2.66 * mr);
    } else {
      value = avg;
    }

    return RoundPipe.prototype.transform(value);
  }

}
