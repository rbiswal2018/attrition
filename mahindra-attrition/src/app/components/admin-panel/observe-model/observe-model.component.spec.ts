import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObserveModelComponent } from './observe-model.component';

describe('ObserveModelComponent', () => {
  let component: ObserveModelComponent;
  let fixture: ComponentFixture<ObserveModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObserveModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObserveModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
