import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminpanelServices } from 'src/app/common/services/admin-panel.service';

@Component({
  selector: 'app-observe-model',
  templateUrl: './observe-model.component.html',
  styleUrls: ['./observe-model.component.scss']
})
export class ObserveModelComponent implements OnInit {
  @ViewChild('dataTable', {static: true}) table;
  dataTable: any;
  public tableDataArr: any = [];
  rolePermissions: any[] = [];
  prediction_details :any = {};
  openAddUserDialog: any;


  constructor(private admin: AdminpanelServices) { }

  ngOnInit() {
    this.fetchUserData()
  }
  filterSubmit(){
    this.admin.run_model(this.prediction_details).subscribe((res) => {
      console.log("res")
      });
  }
  
  fetchUserData() {
    this.tableDataArr = [];
    this.admin.get_model_observe_data().subscribe((res: any) => {
      console.log(res.data)
      
      if (res.data.data.length > 0) {
        for (const data of res.data.data) {
          const tempTableArr = [];
          tempTableArr.push(data.ModelVersion);
          tempTableArr.push(data.TrainingRows);
          tempTableArr.push(data.TrainAccuracy);          
          tempTableArr.push(data.TestingRows);
          tempTableArr.push(data.TestRecall);
          tempTableArr.push(data.TestPrecision);
          tempTableArr.push(data.PredQuarter);
          tempTableArr.push(data.TestF1);
          tempTableArr.push(data.TrainYear);
          tempTableArr.push(data.TrainQuarter);
          tempTableArr.push(data.permission_list);
          this.tableDataArr.push(tempTableArr);
        }
        this.dataTable = $(this.table.nativeElement);
        this.plotTable();
      }
    });
  }

  containsPermissionOrNot(name) {
    for (let val of this.rolePermissions) {
      if (val.toLowerCase() === name.toLowerCase()) {
        return true;
      }
    }
  }


  plotTable() {
    // const result = this.containsPermissionOrNot('Get Permission With Role');
    // if (!result) {
    //   this.tableDataArr = [];
    // }
    console.log(this.tableDataArr)
    const customThis = this;
    this.dataTable.DataTable({
      data: this.tableDataArr,
      bLengthChange: false,
      scrollY: '300px',
      scrollCollapse: true,
      order: [[ 0, 'desc' ]],
      paging: false,
      columns: [
        { title: 'ModelVersion' },
        { title: 'TrainingRows' },
        { title: 'TrainAccuracy' },
        { title: 'TestingRows' },
        { title: 'TestRecall'},
        { title: 'TestPrecision' },
        { title: 'PredQuarter'},
        { title: 'TestF1' },
        { title: 'TrainYear'},
        { title: 'Actions'} 
      ],
      columnDefs: [ {
          targets: -1,
          width:'100px',
          bSortable: false,
          render(d, t, r) {
            let btn;
              btn = `<button class="table-custom-btn run-btn" style="cursor:pointer" >
                    <i class="fa fa-play" aria-hidden="true"></i></i></button>`;
            return btn;
          }
        }],
        drawCallback: () => {
          $('.run-btn').on('click', (e) => {
            console.log("ss")
            const rowData = this.dataTable.DataTable().row($(e.target).parents('tr')).data();
            console.log(rowData )
            this.set_model(rowData);
          });
            
        }
    });
  }

  set_model(data) {
    console.log("ss")
    // this.admin.select_model(data);

    this.admin.select_model(data).subscribe((res) => {
    console.log("res")
    });
    
  }


}
