import { Component, OnInit, ViewChildren, QueryList, ViewChild, AfterViewInit, ChangeDetectorRef, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AdminpanelServices } from '../../../common/services/admin-panel.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { Router } from '@angular/router';
import { concat } from 'lodash';
declare var $: any;
import * as _ from "lodash";
import { forkJoin } from 'rxjs';


@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {

  @ViewChild('dataTable', {static: true}) table;
  dataTable: any;
  public tableDataArr: any = [];
  roleList: any;
  userData: any;
  statedropdowndata:any = [] 
  budropdowndata:any = [] 
  loginStatus: any = localStorage.getItem('user_data');
  dropdowndata: any[] = [];
  rolePermissions: any[] = [];

  constructor(public dialog: MatDialog, private admin: AdminpanelServices,
              private cd: ChangeDetectorRef,  private snackBar: MatSnackBar,  private router: Router) { }

  ngOnInit() {
    this.admin.getStateBusinessUnit().subscribe((res) => {
      this.statedropdowndata = this.makeStateBusinessUnitData(res["data"]["state_list"]);
      this.budropdowndata = this.makeStateBusinessUnitData(res["data"]["business_unit"]);
    });
    if (this.loginStatus === null) {
      this.router.navigate(['/']);
    }
    this.rolePermissions = JSON.parse(localStorage.getItem('user_data')).message.permission_list;
    this.userData = JSON.parse(localStorage.getItem('user_data')).message.role;
   
    this.fetchUserData();
    this.admin.roleTableGlobalData$.subscribe((res: any) => {
      if (res) {
        this.dataTable = $(this.table.nativeElement);
        
        if( $.fn.dataTable.isDataTable( this.table.nativeElement )){
          // console.log("inside")
          this.dataTable.DataTable().destroy();
          this.fetchUserData();
        }
      
       
      }
    });
  }

  ngAfterViewInit() {
  }

  containsPermissionOrNot(name) {
    for (let val of this.rolePermissions) {
      if (val.toLowerCase() === name.toLowerCase()) {
        return true;
      }
    }
  }


  openAddUserDialog() {
    const dialogRef = this.dialog.open(RoleFormComponent, {
      width: '400px'
    });
  }

  plotTable() {
    const result = this.containsPermissionOrNot('Get Permission With Role');
    if (!result) {
      this.tableDataArr.length = 0;
    }
    const customThis = this;
    this.dataTable.DataTable({
      data: this.tableDataArr,
      bLengthChange: false,
      scrollY: '350px',
      scrollCollapse: true,
      order: [[ 0, 'desc' ]],
      paging: false,
      columns: [
        { title: 'ID' },
        { title: 'Role' },
        { title: 'Permission' },
        { title: 'Actions' }
      ],
      columnDefs: [ {
          targets: -1,
          width:'100px',
          bSortable: false,
          render(d, t, r) {
            let btn;
            if (r[1] === 'admin' && customThis.userData === 'admin') {
              btn = `<button class="table-custom-btn table-edit-btn disabled" disabled style="cursor:not-allowed;">
                    <i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button class="table-custom-btn table-dlt-btn disabled" disabled style="cursor:not-allowed;">
                    <i class="fa fa-trash" aria-hidden="true"></i></button>`;
            } else if (r[1] !== 'admin' && customThis.userData === 'admin') {
              let editBtn;
              let dltBtn;
              if (customThis.containsPermissionOrNot('edit role')) {
                editBtn = `<button class="table-custom-btn table-edit-btn">
                <i class="fa fa-pencil" aria-hidden="true"></i></button>`;
              } else {
                editBtn = `<button class="table-custom-btn table-edit-btn disabled" disabled style="cursor:not-allowed;">
                <i class="fa fa-pencil" aria-hidden="true"></i></button>`;
              }
              if (customThis.containsPermissionOrNot('delete role')) {
                dltBtn = `<button class="table-custom-btn table-dlt-btn">
                <i class="fa fa-trash" aria-hidden="true"></i></button>`;
              } else {
                dltBtn = `<button class="table-custom-btn table-dlt-btn disabled" disabled style="cursor:not-allowed;">
                <i class="fa fa-trash" aria-hidden="true"></i></button>`;
              }
              btn = editBtn + dltBtn;
            } else if (r[1] !== 'admin' && customThis.userData !== 'admin') {
              let editBtn;
              let dltBtn;
              if (customThis.containsPermissionOrNot('edit role')) {
                editBtn = `<button class="table-custom-btn table-edit-btn">
                <i class="fa fa-pencil" aria-hidden="true"></i></button>`;
              } else {
                editBtn = `<button class="table-custom-btn table-edit-btn disabled" disabled style="cursor:not-allowed;">
                <i class="fa fa-pencil" aria-hidden="true"></i></button>`;
              }
              if (customThis.containsPermissionOrNot('delete role')) {
                dltBtn = `<button class="table-custom-btn table-dlt-btn">
                <i class="fa fa-trash" aria-hidden="true"></i></button>`;
              } else {
                dltBtn = `<button class="table-custom-btn table-dlt-btn disabled" disabled style="cursor:not-allowed;">
                <i class="fa fa-trash" aria-hidden="true"></i></button>`;
              }
              btn = editBtn + dltBtn;
            } else {
              btn = `<button class="table-custom-btn table-edit-btn disabled" disabled style="cursor:not-allowed;">
                    <i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button class="table-custom-btn table-dlt-btn disabled" disabled style="cursor:not-allowed;">
                    <i class="fa fa-trash" aria-hidden="true"></i></button>`;
            }
            return btn;
          }
        }],
        drawCallback: () => {
          $('.table-edit-btn').on('click', (e) => {
            const rowData = this.dataTable.DataTable().row($(e.target).parents('tr')).data();
            this.editUser(rowData);
          });
          $('.table-dlt-btn').on('click', (e) => {
            let r = confirm("Are you sure ?");
            if (r == true) {
              const rowData = this.dataTable.DataTable().row($(e.target).parents('tr')).data();
              this.admin.deleteRole({'role_id':rowData[0]}).subscribe((res) => {
                if (res['message'] === 'success') {
                  this.admin.setRoleGlobalData(true);
                  this.snackBar.open('Role deleted successfully.', '', {duration: 3000});
                }
              })
            }
          });
        }
    });
  }

  fetchUserData() {
    this.tableDataArr.length = 0;
    this.admin.getPermission().subscribe((res: any) => {
      if (res.result.length > 0) {
        for (const data of res.result) {
          const tempTableArr = [];
          tempTableArr.push(data.id);
          tempTableArr.push(data.name);
          tempTableArr.push(data.permission_list);
          this.tableDataArr.push(tempTableArr);
        }
        this.dataTable = $(this.table.nativeElement);
        this.plotTable();
      }
    });
  }
  makeStateBusinessUnitData(arr) {
    const tempArr = [];
    for (let val of arr) {
      if (val !== '') {
        const tempObj = {};
        tempObj['display'] = val;
        tempObj['value'] = val;
        tempArr.push(tempObj);
      }
    }
    return tempArr;
  }


  editUser(data) {
    this.admin.editRoleRowData(data);
    const dialogRef = this.dialog.open(RoleEditFormComponent, {
      width: '400px'
    });
  }

}


/* User create form */
@Component({
  selector: 'app-role-form',
  templateUrl: 'role-form.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleFormComponent implements OnInit {

  
  @ViewChildren(SelectAutocompleteComponent) multiSelect: 
  QueryList<SelectAutocompleteComponent>;
  public role_name: any;
  public roleList: any[] = [];
  dropdowndata: any[] = [];
  statedropdowndata:any[] = []
  budropdowndata:any[] = []
  banddropdown = [{"display":"OBand","value":"OBand"},{"display":"MBand","value":"MBand"},{"display":"DH(Excluding HO)","value":"DH(Excluding HO)"}]

  selectedOptions = [];
  stateselectedOptions = [];
  buselectedOptions = [];
  selected: any[] = [];
  stateselected: any[] = [];
  buselected: any[] = [];
  optionArr: any[] = [];
  stateoptionArr: any[] = [];
  buoptionArr: any[] = [];
  bandselected:any[] = [];
  bandoptionArr: any[] = [];



  constructor(private admin: AdminpanelServices,
              public dialogRef: MatDialogRef<RoleComponent>, private snackBar: MatSnackBar) {}

  ngOnInit() {
    this.admin.getPermissionList().subscribe((res) => {
      this.dropdowndata = this.makeDropdownData(res);
    });
    this.admin.getStateBusinessUnit().subscribe((res) => {
      this.statedropdowndata = this.makeStateBusinessUnitData(res["data"]["state_list"]);
      this.budropdowndata = this.makeStateBusinessUnitData(res["data"]["business_unit"]);
    });
  }

  makeDropdownData(arr) {
    const tempArr = [];
    for (let val of arr) {
      if (val !== '') {
        const tempObj = {};
        tempObj['display'] = val.name;
        tempObj['value'] = val.name;
        tempArr.push(tempObj);
      }
    }
    return tempArr;
  }

  makeStateBusinessUnitData(arr) {
    const tempArr = [];
    for (let val of arr) {
      if (val !== '') {
        const tempObj = {};
        tempObj['display'] = val;
        tempObj['value'] = val;
        tempArr.push(tempObj);
      }
    }
    return tempArr;
  }



  dropdownChangeFn(selected) {
    this.selected = selected;
    if (this.selected.length >= 0) {
      this.optionArr = [];
      for (let option of this.selected) {
        this.optionArr.push(option);
      }
    }
  }
  statedropdownChangeFn(selected) {
    this.stateselected = selected;
    if (this.stateselected.length >= 0) {
      this.stateoptionArr = [];
      for (let option of this.stateselected) {
        this.stateoptionArr.push(option);
      }
    }
  }
  budropdownChangeFn(selected) {
    this.buselected = selected;
    if (this.buselected.length >= 0) {
      this.buoptionArr = [];
      for (let option of this.buselected) {
        this.buoptionArr.push(option);
      }
    }
  }

  banddropdownChangeFn(selected) {
    this.bandselected = selected;
    if (this.bandselected.length >= 0) {
      this.bandoptionArr = [];
      for (let option of this.bandselected) {
        this.bandoptionArr.push(option);
      }
    }
  }
  createRole() {
    console.log("clicked")
  
    if (
      this.role_name !== undefined &&
      this.optionArr.length > 0&&this.buoptionArr.length>0&&this.stateoptionArr.length==0&&this.bandoptionArr.length>0
    ) {
      $('.global-loader').show();
      const mainDataObj: any = {};
      mainDataObj.name = this.role_name;
      this.optionArr =this.optionArr.concat(this.stateoptionArr)
      this.optionArr =this.optionArr.concat(this.buoptionArr)
      this.optionArr =this.optionArr.concat(this.bandoptionArr)

      mainDataObj.permission_list = this.optionArr;
      this.admin.addNewRole(mainDataObj).subscribe((res: any) => {
        $('.global-loader').hide();
        this.dialogRef.close();
        this.admin.setRoleGlobalData(true);
        this.snackBar.open('Role Added Successfully.', '', {duration: 3000});
      });
    }
    else if(this.role_name !== undefined &&
      this.optionArr.length > 0&&this.buoptionArr.length==0&&this.stateoptionArr.length>0){
        $('.global-loader').show();
        const mainDataObj: any = {};
        mainDataObj.name = this.role_name;
        this.optionArr =this.optionArr.concat(this.stateoptionArr)
        this.optionArr =this.optionArr.concat(this.buoptionArr)
        this.optionArr =this.optionArr.concat(this.bandoptionArr)
        mainDataObj.permission_list = this.optionArr;

        this.admin.addNewRole(mainDataObj).subscribe((res: any) => {
          console.log(this.optionArr)

          $('.global-loader').hide();
          this.dialogRef.close();
          this.admin.setRoleGlobalData(true);
          this.snackBar.open('Role Added Successfully.', '', {duration: 3000});
        });
    }
    else if(this.role_name==undefined){
      let updateSuccess = "*Please Fill The RoleName"
       $('.msg_display .text-edit').text(updateSuccess).show()
        }
    else if( this.optionArr.length == 0){
          let updateSuccess = "*Please Select Atleast One Permission"
           $('.msg_display .text-edit').text(updateSuccess).show()
            }
    else if( this.buoptionArr.length==0&&this.stateoptionArr.length==0){
              let updateSuccess =  "*Select either State or Business Unit"
               $('.msg_display .text-edit').text(updateSuccess).show()
                }
    else if(this.buoptionArr.length>0&&this.stateoptionArr.length>0){
         let updateSuccess = "*Select either State or Business Unit"
          $('.msg_display .text-edit').text(updateSuccess).show()
           }
    else if(this.bandoptionArr.length>0){
            let updateSuccess = "*Please select atleast one band "
             $('.msg_display .text-edit').text(updateSuccess).show()
              }
  }

}


/* User edit form */
@Component({
  selector: 'app-role-edit-form',
  templateUrl: 'role-edit-form.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleEditFormComponent implements OnInit {

  @ViewChildren(SelectAutocompleteComponent) multiSelect: 
  QueryList<SelectAutocompleteComponent>;
  public role_name: any;
  public roleList: any[] = [];
  dropdowndata: any[] = [];
  statedropdowndata:any[] = []
  budropdowndata:any[] = []
  selectedOptions: any = [''];
  stateselectedOptions:any = [''];
  buselectedOptions = [];
  selected: any[] = [];
  stateselected: any[] = [];
  buselected: any[] = [];
  optionArr: any[] = [];
  stateoptionArr: any[] = [];
  buoptionArr: any[] = [];
  bandoptionArr: any[] = [];
  bandselected:any[] = [];
  
  banddropdown = [{"display":"OBand","value":"OBand"},{"display":"MBand","value":"MBand"},{"display":"DH(Excluding HO)","value":"DH(Excluding HO)"}]

  

  id: any;

  constructor(private admin: AdminpanelServices,
              public dialogRef: MatDialogRef<RoleComponent>, private snackBar: MatSnackBar) {}

  ngOnInit() {
   
     
    let resource2 = this.admin.getPermissionList()
    let resource3 = this.admin.getStateBusinessUnit()
    let self = this
    forkJoin([resource2,resource3]).subscribe(results => {
      var temp_permisiion_list = []
     this.admin.roleTableRowData$.subscribe((res: any) => {
      this.id =res[0];
      this.role_name =res[1];
      temp_permisiion_list =res[2]
     })
        let temp:any = []
        for (let index = 0; index < results[0]["length"]; index++) {
          temp.push(results[0][index]["name"])
                  }
        self.selectedOptions = _.filter(temp_permisiion_list, function (d) {
          return  _.includes(temp, d)
       });
       this.optionArr = this.selectedOptions;
        this.dropdowndata = this.makeDropdownData(results[0]);
        this.statedropdowndata = this.makeStateBusinessUnitData(results[1]["data"]["state_list"]);
        this.budropdowndata = this.makeStateBusinessUnitData(results[1]["data"]["business_unit"]);
        this.stateselected = _.filter(results[1]["data"]["state_list"], function (d) {
          return  _.includes(temp_permisiion_list, d)
       })
       this.stateoptionArr =  this.stateselected
        this.buselected = _.filter(results[1]["data"]["business_unit"], function (d) {
         return  _.includes(temp_permisiion_list, d)
       }) 
       console.log(this.buselected)
       this.buoptionArr =  this.buselected

       this.bandselected = _.filter(["MBand","OBand","DH(Excluding HO)"], function (d) {
        return  _.includes(temp_permisiion_list, d)
      }) 
      console.log(this.bandselected)

      this.bandoptionArr =  this.bandselected 
    })
}

  makeDropdownData(arr) {
    const tempArr = [];
    for (let val of arr) {
      if (val !== '') {
        const tempObj = {};
        tempObj['display'] = val.name;
        tempObj['value'] = val.name;
        tempArr.push(tempObj);
      }
    }
    return tempArr;
  }
  makeStateBusinessUnitData(arr) {
    const tempArr = [];
    for (let val of arr) {
      if (val !== '') {
        const tempObj = {};
        tempObj['display'] = val;
        tempObj['value'] = val;
        tempArr.push(tempObj);
      }
    }
    return tempArr;
  }
  dropdownChangeFn(selected) {
    this.selected = selected;
    if (this.selected.length >= 0) {
      this.optionArr = [];
      for (let option of this.selected) {
        this.optionArr.push(option);

      }
    }
  }
  statedropdownChangeFn(selected) {
    this.stateselected = selected;
    if (this.stateselected.length >= 0) {
      this.stateoptionArr = [];
      for (let option of this.stateselected) {
        this.stateoptionArr.push(option);
      }
    }
  }
  budropdownChangeFn(selected) {
    this.buselected = selected;
    if (this.buselected.length >= 0) {
      this.buoptionArr = [];
      for (let option of this.buselected) {
        this.buoptionArr.push(option);
      }
    }

  }
  banddropdownChangeFn(selected) {
    this.bandselected = selected;
    if (this.bandselected.length >= 0) {
      this.bandoptionArr = [];
      for (let option of this.bandselected) {
        this.bandoptionArr.push(option);
      }
    }
  }
  editRole() {
    if (
      this.role_name !== undefined &&
      this.optionArr.length > 0&&this.buoptionArr.length>0&&this.stateoptionArr.length==0
    ) {
      console.log("in 1st if")
      $('.global-loader').show();
      const mainDataObj: any = {};
      mainDataObj.role = this.role_name;
      // mainDataObj.permission_list = this.optionArr;
      this.optionArr =this.optionArr.concat(this.stateoptionArr)
      this.optionArr =this.optionArr.concat(this.buoptionArr)
      this.optionArr =this.optionArr.concat(this.bandoptionArr)

      mainDataObj.permission_list = this.optionArr;
      this.admin.editRole(mainDataObj, this.id).subscribe((res: any) => {
        $('.global-loader').hide();
        this.dialogRef.close();
        this.admin.setRoleGlobalData(true);
        this.snackBar.open('Role Updated Successfully.', '', {duration: 3000});
      });
    }
    else if( this.role_name !== undefined &&
      this.optionArr.length > 0&&this.buoptionArr.length==0&&this.stateoptionArr.length>0&&this.bandoptionArr){
        console.log("in 2st if")

        $('.global-loader').show();
        const mainDataObj: any = {};
        mainDataObj.role = this.role_name;
        // mainDataObj.permission_list = this.optionArr;
        this.optionArr =this.optionArr.concat(this.stateoptionArr)
        this.optionArr =this.optionArr.concat(this.buoptionArr)
        this.optionArr =this.optionArr.concat(this.bandoptionArr)

        mainDataObj.permission_list = this.optionArr;
        this.admin.editRole(mainDataObj, this.id).subscribe((res: any) => {
          $('.global-loader').hide();
          this.dialogRef.close();
          this.admin.setRoleGlobalData(true);
          this.snackBar.open('Role Updated Successfully.', '', {duration: 3000});
        });
    }
    else if(this.role_name==""){
      let updateSuccess = "*Please Fill The RoleName"
       $('.msg_display .text-edit').text(updateSuccess).show()
        }
    else if(this.optionArr.length==0){
          let updateSuccess = "*Please Select Atleast One Permission"
           $('.msg_display .text-edit').text(updateSuccess).show()
            }
    else if(this.buoptionArr.length==0&&this.stateoptionArr.length==0){
              let updateSuccess =  "*Select either State or Business Unit"
               $('.msg_display .text-edit').text(updateSuccess).show()
                }
  }

}






