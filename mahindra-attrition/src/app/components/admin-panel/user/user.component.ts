import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AdminpanelServices } from '../../../common/services/admin-panel.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, AfterViewInit {

  @ViewChild('dataTable', {static: false}) table;
  dataTable: any;
  public tableDataArr: any = [];
  roleList: any;
  userData: any;
  userPermissions: any[] = [];
  loginStatus: any = localStorage.getItem('user_data');

  constructor(public dialog: MatDialog, private admin: AdminpanelServices,
              private cd: ChangeDetectorRef,  private snackBar: MatSnackBar,  private router: Router) { }

  ngOnInit() {
    if (this.loginStatus === null) {
      this.router.navigate(['/']);
    }
    this.userPermissions = JSON.parse(localStorage.getItem('user_data')).message.permission_list;
    this.userData = JSON.parse(localStorage.getItem('user_data')).message.role;
    this.fetchUserData();
    this.admin.userTableGlobalData$.subscribe((res: any) => {
      if (res) {
        this.dataTable = $(this.table.nativeElement);
        this.dataTable.DataTable().destroy();
        this.fetchUserData();
      }
    });
  }

  ngAfterViewInit() {
  }

  containsPermissionOrNot(name) {
    for (let val of this.userPermissions) {
      if (val.toLowerCase() === name.toLowerCase()) {
        return true;
      }
    }
  }

  openAddUserDialog() {
    const dialogRef = this.dialog.open(UserFormComponent, {
      width: '400px'
    });
  }

  plotTable() {
    const result = this.containsPermissionOrNot('fetch user');
    if (!result) {
      this.tableDataArr = [];
    }
    const customThis = this;
    this.dataTable.DataTable({
      data: this.tableDataArr,
      bLengthChange: false,
      scrollY: '350px',
      scrollCollapse: true,
      paging: false,
      order: [[ 0, 'desc' ]],
      columns: [
        { title: 'ID' },
        { title: 'Firstname' },
        { title: 'Lastname' },
        { title: 'Email' },
        { title: 'active' },
        { title: 'Role' },
        { title: 'Status' },
        { title: 'Actions' }
      ],
      columnDefs: [
        {
          targets: [ 4 ],
          visible: false,
          searchable: false
        },
        {
          targets: -2,
          bSortable: false,
          render(d, t, r) {
            let switchBtn;
            if (r[4] === true && r[5] === 'admin' && customThis.userData === 'admin' && customThis.containsPermissionOrNot('change status')) {
              switchBtn = `
              <fieldset class="checkbox-switch" disabled style="cursor:not-allowed;">
                <input type="checkbox" id="checkbox-` + r[0] + `" checked disabled>
                <label for="checkbox-` + r[0] + `" title="Change status" class="checkbox-right"></label>
              </fieldset>
              `;
            } else if (r[4] === false && r[5] === 'admin' && customThis.userData === 'admin' && customThis.containsPermissionOrNot('change status')) {
              switchBtn = `
              <fieldset class="checkbox-switch" disabled style="cursor:not-allowed;">
                <input type="checkbox" id="checkbox-` + r[0] + `">
                <label for="checkbox-` + r[0] + `" title="Change status" class="checkbox-right"></label>
              </fieldset>
              `;
            }  else if (r[4] === true && r[5] !== 'admin' && customThis.userData === 'admin' && customThis.containsPermissionOrNot('change status')) {
              switchBtn = `
              <fieldset class="checkbox-switch">
                <input type="checkbox" id="checkbox-` + r[0] + `" checked>
                <label for="checkbox-` + r[0] + `" title="Change status" class="checkbox-right"></label>
              </fieldset>
              `;
            }  else if (r[4] === false && r[5] !== 'admin' && customThis.userData === 'admin' && customThis.containsPermissionOrNot('change status')) {
              switchBtn = `
              <fieldset class="checkbox-switch">
                <input type="checkbox" id="checkbox-` + r[0] + `">
                <label for="checkbox-` + r[0] + `" title="Change status" class="checkbox-right"></label>
              </fieldset>
              `;
            } else if (r[4] === true && r[5] === 'admin' && customThis.userData !== 'admin' && customThis.containsPermissionOrNot('change status')) {
              switchBtn = `
              <fieldset class="checkbox-switch" disabled style="cursor:not-allowed;">
                <input type="checkbox" id="checkbox-` + r[0] + `" chcekced>
                <label for="checkbox-` + r[0] + `" title="Change status" class="checkbox-right"></label>
              </fieldset>
              `;
            } else if (r[4] === false && r[5] === 'admin' && customThis.userData !== 'admin' && customThis.containsPermissionOrNot('change status')) {
              switchBtn = `
              <fieldset class="checkbox-switch" disabled style="cursor:not-allowed;">
                <input type="checkbox" id="checkbox-` + r[0] + `">
                <label for="checkbox-` + r[0] + `" title="Change status" class="checkbox-right"></label>
              </fieldset>
              `;
            } else if (r[4] === true && r[5] !== 'admin' && customThis.userData !== 'admin' && customThis.containsPermissionOrNot('change status')) {
              switchBtn = `
              <fieldset class="checkbox-switch">
                <input type="checkbox" id="checkbox-` + r[0] + `" chcekced>
                <label for="checkbox-` + r[0] + `" title="Change status" class="checkbox-right"></label>
              </fieldset>
              `;
            }  else if (r[4] === false && r[5] !== 'admin' && customThis.userData !== 'admin' && customThis.containsPermissionOrNot('change status')) {
              switchBtn = `
              <fieldset class="checkbox-switch">
                <input type="checkbox" id="checkbox-` + r[0] + `">
                <label for="checkbox-` + r[0] + `" title="Change status" class="checkbox-right"></label>
              </fieldset>
              `;
            }
            return switchBtn;
          }
        }, {
          targets: -1,
          bSortable: false,
          render(d, t, r) {
            let btn;
            if (r[5] === 'admin' && customThis.userData === 'admin') {
              btn = `<button class="table-custom-btn table-edit-btn disabled" disabled style="cursor:not-allowed;">
                    <i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button class="table-custom-btn table-dlt-btn disabled" disabled style="cursor:not-allowed;">
                    <i class="fa fa-trash" aria-hidden="true"></i></button>
                    <button class="table-custom-btn table-pwd-btn disabled" disabled style="cursor:not-allowed;">
                    <i class="fa fa-unlock" aria-hidden="true"></i></button>`;
            } else if (r[5] !== 'admin' && customThis.userData === 'admin') {
                let editBtn;
                let dltBtn;
                let pwdBtn;
                if (customThis.containsPermissionOrNot('edit user')) {
                  editBtn = `<button class="table-custom-btn table-edit-btn">
                  <i class="fa fa-pencil" aria-hidden="true"></i></button>`;
                } else {
                  editBtn = `<button class="table-custom-btn table-edit-btn disabled" disabled style="cursor:not-allowed;">
                  <i class="fa fa-pencil" aria-hidden="true"></i></button>`;
                }
                if (customThis.containsPermissionOrNot('delete user')) {
                  dltBtn = `<button class="table-custom-btn table-dlt-btn">
                  <i class="fa fa-trash" aria-hidden="true"></i></button>`;
                } else {
                  dltBtn = `<button class="table-custom-btn table-dlt-btn disabled" disabled style="cursor:not-allowed;">
                  <i class="fa fa-trash" aria-hidden="true"></i></button>`;
                }
                pwdBtn = `<button class="table-custom-btn table-pwd-btn">
                <i class="fa fa-unlock" aria-hidden="true"></i></button>`;
                btn = editBtn + dltBtn + pwdBtn;
            } else if (r[5] !== 'admin' && customThis.userData !== 'admin') {
                let editBtn;
                let dltBtn;
                let pwdBtn;
                if (customThis.containsPermissionOrNot('edit user')) {
                  editBtn = `<button class="table-custom-btn table-edit-btn">
                  <i class="fa fa-pencil" aria-hidden="true"></i></button>`;
                } else {
                  editBtn = `<button class="table-custom-btn table-edit-btn disabled" disabled style="cursor:not-allowed;">
                  <i class="fa fa-pencil" aria-hidden="true"></i></button>`;
                }
                if (customThis.containsPermissionOrNot('delete user')) {
                  dltBtn = `<button class="table-custom-btn table-dlt-btn">
                  <i class="fa fa-trash" aria-hidden="true"></i></button>`;
                } else {
                  dltBtn = `<button class="table-custom-btn table-dlt-btn disabled" disabled style="cursor:not-allowed;">
                  <i class="fa fa-trash" aria-hidden="true"></i></button>`;
                }
                pwdBtn = `<button class="table-custom-btn table-pwd-btn">
                <i class="fa fa-unlock" aria-hidden="true"></i></button>`;
                btn = editBtn + dltBtn + pwdBtn;
            } else {
              btn = `<button class="table-custom-btn table-edit-btn disabled" disabled style="cursor:not-allowed;">
                    <i class="fa fa-pencil" aria-hidden="true"></i></button>
                    <button class="table-custom-btn table-dlt-btn disabled" disabled style="cursor:not-allowed;">
                    <i class="fa fa-trash" aria-hidden="true"></i></button>
                    <button class="table-custom-btn table-pwd-btn disabled" disabled style="cursor:not-allowed;">
                    <i class="fa fa-unlock" aria-hidden="true"></i></button>`;
            }
            return btn;
          }
        }],
        drawCallback: () => {
          $('.table-edit-btn').on('click', (e) => {
            const rowData = this.dataTable.DataTable().row($(e.target).parents('tr')).data();
            this.editUser(rowData);
          });
          $('.table-dlt-btn').on('click', (e) => {
            let r = confirm("Are you sure ?");
            if (r == true) {
              const rowData = this.dataTable.DataTable().row($(e.target).parents('tr')).data();
              this.admin.deleteUser({'user_id':rowData[0]}, rowData[0]).subscribe((res) => {
                if (res['message'] === 'success') {
                  this.admin.setUserGlobalData(true);
                  this.snackBar.open('User deleted successfully.', '', {duration: 3000});
                }
              })
            }
          });
          $('.table-pwd-btn').on('click', (e) => {
            const rowData = this.dataTable.DataTable().row($(e.target).parents('tr')).data();
            let r = confirm("Are you sure ?");
            if (r == true) {
              this.admin.setDefaultPwd(rowData[0]).subscribe((res) => {
                if (res['message'] === "success") {
                  this.snackBar.open('Password set to default.', '', {duration: 3000});
                }
              })
            }
          });
        }
    });
  }

  fetchUserData() {
    this.tableDataArr = [];
    this.admin.fetchUser().subscribe((res: any) => {
      if (res.message === 'success') {
        for (const data of res.data) {
          const tempTableArr = [];
          tempTableArr.push(data.user.id);
          tempTableArr.push(data.user.first_name);
          tempTableArr.push(data.user.last_name);
          tempTableArr.push(data.user.email);
          tempTableArr.push(data.user.is_active);
          tempTableArr.push(data.role);
          this.tableDataArr.push(tempTableArr);
        }
        this.dataTable = $(this.table.nativeElement);
        this.plotTable();
      }
    });
  }

  editUser(data) {
    this.admin.editUserRowData(data);
    const dialogRef = this.dialog.open(UserEditFormComponent, {
      width: '400px'
    });
  }

}

/* User create form */
@Component({
  selector: 'app-user-form',
  templateUrl: 'user-form.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserFormComponent implements OnInit {

  public fname: any;
  public lname: any;
  public email: any;
  public role: any = -1;
  public roleList: any[] = [];

  constructor(private admin: AdminpanelServices,
              public dialogRef: MatDialogRef<UserComponent>, private snackBar: MatSnackBar) {}

  ngOnInit() {
    this.admin.getRole().subscribe((res: any) => {
      this.roleList = res;
    });
  }

  createUser() {
    if (
      this.fname !== undefined &&
      this.lname !== undefined &&
      this.email !== undefined &&
      this.role !== -1
    ) {
      $('.global-loader').show();
      const userDetailsObj: any = {};
      const mainDataObj: any = {};
      userDetailsObj.first_name = this.fname;
      userDetailsObj.last_name = this.lname;
      userDetailsObj.username = this.email;
      userDetailsObj.email = this.email;
      mainDataObj.user = userDetailsObj;
      mainDataObj.role = this.role;
      this.admin.addNewUser(mainDataObj).subscribe((res: any) => {
        $('.global-loader').hide();
        this.dialogRef.close();
        this.admin.setUserGlobalData(true);
        this.snackBar.open('User Added Successfully.', '', {duration: 3000});
      });
    }
  }

}


/* User edit form */
@Component({
  selector: 'app-user-edit-form',
  templateUrl: 'user-edit-form.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserEditFormComponent implements OnInit {

  public fname: any;
  public lname: any;
  public email: any;
  public role: any;
  public id: any;
  public roleList: any[] = [];

  constructor(private admin: AdminpanelServices,
              public dialogRef: MatDialogRef<UserComponent>, private snackBar: MatSnackBar) {}

  ngOnInit() {
    this.admin.getRole().subscribe((res: any) => {
      this.roleList = res;
    });
    this.admin.userTableRowData$.subscribe((res) => {
      this.id = res[0];
      this.fname = res[1];
      this.lname = res[2];
      this.email = res[3];
      this.role = res[5];
    });
  }

  editUser() {
    if (
      this.fname !== undefined &&
      this.lname !== undefined &&
      this.email !== undefined &&
      this.role !== -1
    ) {
      $('.global-loader').show();
      const userDetailsObj: any = {};
      const mainDataObj: any = {};
      userDetailsObj.user_id = this.id;
      userDetailsObj.first_name = this.fname;
      userDetailsObj.last_name = this.lname;
      mainDataObj.user = userDetailsObj;
      mainDataObj.role = this.role;
      this.admin.editUser(mainDataObj).subscribe((res: any) => {
        $('.global-loader').hide();
        this.dialogRef.close();
        this.admin.setUserGlobalData(true);
        this.snackBar.open('User Updated Successfully.', '', {duration: 3000});
      });
    }
  }

}
