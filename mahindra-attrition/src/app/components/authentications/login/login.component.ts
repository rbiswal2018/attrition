import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { AuthService } from '../../../common/services/auth.service';
import { TabRoute } from '../../../common/services/tabroute.service';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    trigger('leftOpenClose', [
      state('open', style({
        width: '40%'
      })),
      state('closed', style({
        width: '0px'
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ])
    ]),
    trigger('rightOpenClose', [
      state('open', style({
        transform: 'translate(-50%, -50%) scale(1)'
      })),
      state('closed', style({
        transform: 'translate(-50%, -50%) scale(0)'
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ])
    ]),
    trigger('resetOpenClose', [
      state('open', style({
        transform: 'translate(-50%, -50%) scale(1)'
      })),
      state('closed', style({
        transform: 'translate(-50%, -50%) scale(0)'
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ])
    ])
  ]
})
export class LoginComponent implements OnInit {

  leftSide = 'closed';
  rightSide = 'closed';
  resetBox = 'closed';
  public emailID: any;
  public password: any;
  public resetPassword: any;
  public token: any;
  loginStatus: any = localStorage.getItem('user_data');

  constructor(private auth: AuthService, private router: Router, private _snackBar: MatSnackBar, public tabRoute: TabRoute) { }

  ngOnInit() {
    if (this.loginStatus !== null) {
      this.router.navigate(['/dashboard']);
    }
    if (this.leftSide === 'closed') {
      setTimeout(() => this.leftSide = 'open');
    }
    if (this.rightSide === 'closed') {
      setTimeout(() => this.rightSide = 'open');
    }
  }

  openResetPwd() {
    this.rightSide = 'closed';
    this.resetBox = 'open';
  }

  closeResetPwd() {
    this.resetBox = 'closed';
    this.rightSide = 'open';
  }

  login() {
    if (
      this.emailID !== '' &&
      this.password !== ''
    ) {
      const formData: FormData = new FormData();
      formData.append('username',  this.emailID);
      formData.append('password', this.password);
      this.auth.loginUser(formData).subscribe((res: any) => {
        if (res.message.msg === 'success') {
          this.tabRoute.redirectToTab(0);
          localStorage.setItem('user_data', JSON.stringify(res));
          this.router.navigate(['/dashboard']);
        } else if (res.message === 'reset password') {
          this.token = res.token;
          this.openResetPwd();
        }
      }, (error) => {
        this._snackBar.open(error.error.error, '', {
          duration: 4000,
        });
      });
    } else {
      this._snackBar.open('Email and password field must not be empty.', '', {
        duration: 4000,
      });
    }
  }

  resetPwd() {
    if (
      this.resetPassword !== undefined
    ) {
      const tempObj = {};
      tempObj['token'] = this.token;
      tempObj['new_password'] = this.resetPassword;
      this.auth.resetPassword(tempObj).subscribe((res: any) => {
        if (res['status'] === 'True') {
          this.closeResetPwd();
          this._snackBar.open('Password reset successful.', '', {
            duration: 4000,
          });
        } else {
          this._snackBar.open( res.message , '', {
            duration: 4000,
          });
        }
      }, (error) => {
        this._snackBar.open(error.error.error, '', {
          duration: 4000,
        });
      });
    } else {
      this._snackBar.open('Password field can not be empty.', '', {
        duration: 4000,
      });
    }
  }

}
