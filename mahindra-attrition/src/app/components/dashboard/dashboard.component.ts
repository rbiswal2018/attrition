import { Component, OnInit, ViewChildren, QueryList, AfterViewInit, OnChanges, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { TabRoute } from '../../common/services/tabroute.service';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { FilterService } from '../../common/services/filter.service';
declare var $: any;
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    trigger('leftOpenClose', [
      state('open', style({
        width: '250px',
        opacity: 1
      })),
      state('closed', style({
        width: '0px',
        opacity: 0
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ])
    ])
  ]
})
export class DashboardComponent implements OnInit, AfterViewInit, OnChanges {

  @ViewChildren(SelectAutocompleteComponent) multiSelect:
    QueryList<any>;
  leftSide = 'closed';
  minDate: any = '2017-01-01';
  maxDate: any = this.getTodayDate();
  csvFilterData ={};

  @Output() emitCsvFilterData = new EventEmitter<Object>();

  dropdownTypeArr = ['state', 'branch', 'function_name', 'division', 'business_unit', 'department',
    'sub_department', 'cost_centre', 'work_contract', 'band', 'employee_sub_group'];
  filterDataKeyNames = ['state_list', 'branch_list', 'function_name', 'division_list', 'business_unit', 'department',
    'sub_department', 'cost_centre', 'work_contract', 'band', 'employee_sub_group'];
  filterSelectDisplayName = ['Select State', 'Select Branch', 'Select Function', 'Select Division', 'Select Business', 'Select Department',
    'Select Sub Department', 'Select Cost Center', 'Select Work Contract', 'Select Band', 'Select Sub Group'];
  mainDropdownData: any;
  public sidebarDropdownSelectList: any[];
  public activeTab: any;
  loaderactive: any = true;
  mainDashboardData: any = {};
  tempTreeMapData: any[] = [];
  selectedOptions = [];
  selected = this.selectedOptions;
  treemapActiveVal: any = 'state';
  selectionHierarchyArr: any = ['state', 'branch'];
  startDate: any;
  endDate: any;
  dropdownActiveIndex: any;
  sidebarMainData: any;
  sidebarTreemapData: any;
  dropdownObj: any = {};
  optionArr: any[] = [];
  dropdownActiveHandle: any[] = [];
  comparisonData: any[] = [];
  pieswitchData: any[] = [];
  loginStatus: any = localStorage.getItem('user_data');
  dropdown_sequence: any[] = [];
  hide_bar = true;
  loaderhidden = true;
  getClikedTreemapVal: any;
  getBreadcrumbData: any;


  constructor(private tabroute: TabRoute, private filterS: FilterService, private router: Router, private cd: ChangeDetectorRef, private _snackBar: MatSnackBar) {

  }

  ngOnInit() {
    if (this.loginStatus === null) {
      this.router.navigate(['/']);
    }
    this.startDate = '2019-04-01';
    this.endDate = this.getTodayDate();
    this.csvFilterData['start_date'] = this.startDate
    this.csvFilterData['start_date'] = this.endDate

    if (this.leftSide === 'closed') {
      setTimeout(() => this.leftSide = 'open');
    }
    this.tabroute.tabRouteValue$.subscribe((res) => {
      // console.log(res)
      if (res == 3) {
        this.loaderhidden = false;
      }
      else {

        this.loaderhidden = true;
      }
      this.activeTab = res;
      if (this.multiSelect !== undefined) {
        for (let i = 0; i < this.multiSelect['_results'].length; i++) {
          this.multiSelect['_results'][i].selectedValue = [];
        }
      }
    });
    /* Getting filter data */
    this.filterS.getFilterDropdownData().subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDropdownData = res.data;
        let count = 0;
        this.sidebarDropdownSelectList = [];
        for (let i of this.filterDataKeyNames) {
          const tempObj = {};
          tempObj['main_display_name'] = this.filterSelectDisplayName[count];
          tempObj['main_data'] = this.makeDropdownData(res.data[this.filterDataKeyNames[count]]);
          this.sidebarDropdownSelectList.push(tempObj);
          count++;
        }
        this.csvFilterData = {
          "start_date":this.startDate,
          "end_date":this.endDate,
          "state":this.makeSingleQuotesObj(res.data.state_list) 
        }
        this.loaderactive = false;
        this.pieswitchData.push({ "state": this.makeSingleQuotesObj(res.data.state_list) });
        this.filterS.getTreemapData({ "start_date": this.startDate, "end_date": this.endDate, "state": this.makeSingleQuotesObj(res.data.state_list) }).subscribe((res: any) => {
          if (res.message === 'success') {
            this.mainDashboardData['treemap_data'] = res.data.performer_by_state;
          }
        });
        this.filterS.getRatingData({ "start_date": this.startDate, "end_date": this.endDate, "state": this.makeSingleQuotesObj(res.data.state_list), 'type_rating': 'average_rating' }).subscribe((res: any) => {
          if (res.message === 'success') {
            this.mainDashboardData['rating_data'] =  { name: 'rating',"value":res.data.data}
          }
        });
        this.getAllDashboardData({ "start_date": this.startDate, "end_date": this.endDate, "state": this.makeSingleQuotesObj(res.data.state_list) });
      }
    });

  }

  ngAfterViewInit() {

  }
  ngOnChanges() {
  }

  makeSingleQuotesObj(arr) {
    const newArr = JSON.stringify(arr).replace(/"/g, "'");
    return '' + newArr + '';
  }

  getTodayDate() {
    let today: any = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    return today;
  }
  getDateByObj(val) {
    let today: any = val;
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    return today;
  }

  makeDropdownData(arr) {
    const tempArr = [];
    for (let val of arr) {
      if (val !== '') {
        const tempObj = {};
        tempObj['display'] = val;
        tempObj['value'] = val;
        tempArr.push(tempObj);
      }
    }
    return tempArr;
  }

  makeDashboardAPIData(data, cur_date, last_yr, map_key, filter_key) {
    const tempObj = {};
    tempObj['start_date'] = last_yr;
    tempObj['end_date'] = cur_date;
    tempObj['state'] = data;
    return tempObj;
  }

  treemapData() {
    const tempObj = {};
    tempObj['start_date'] = this.startDate;
    tempObj['end_date'] = this.endDate;
    return tempObj;
  }

  getRatingData(data) {
    const tempVal = data;
    tempVal['type_rating'] = 'average_rating';
    return tempVal;
  }

  
  getPieChartSwitchValue(val: number) {
    const tempObj = {};
    for (let obj of this.pieswitchData) {
      for (let val in obj) {
        tempObj[val] = obj[val];
      }
    }
    if (val === 1) {
      tempObj['type_rating'] = 'average_rating';
      tempObj['start_date'] = this.startDate;
      tempObj['end_date'] = this.endDate;
    } else if (val === 2) {
      tempObj['type_rating'] = 'previous_rating';
      tempObj['start_date'] =  this.startDate;
      tempObj['end_date'] = this.endDate;
    }
    this.filterS.getRatingData(tempObj).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData.rating_data = {"name":"rating","value":res.data.data}
      }
    });
  }


  getAllDashboardData(data) {
    this.filterS.getEmpLeftCount(data).subscribe((res: any) => {
      if (res.data.data[0][0][0] !== null) {
        this.mainDashboardData['total_emp_left'] = res.data.data[0][0][0];
      } else {
        this.mainDashboardData['total_emp_left'] = '';
      }
    });
    this.filterS.getAttritionPercentage(data).subscribe((res: any) => {
      if (res.data[0][0][0] !== null) {
        this.mainDashboardData['attrition_percentage'] = res.data[0][0][0].toFixed(2);
      } else {
        this.mainDashboardData['attrition_percentage'] = '';
      }
    });
    this.filterS.getTenureData(data).subscribe((res: any) => {
      if (res.data.data[0][0][0] !== null) {
        this.mainDashboardData['tenure_data'] = parseFloat(res.data.data[0][0]).toFixed(2);
      } else {
        this.mainDashboardData['tenure_data'] = '';
      }
    });
    this.filterS.getAttritionTrend(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['attrition_trend'] = res;
      }
    });
    this.filterS.getCompensationData(data).subscribe((res: any) => {
      if (res.messyage === 'success') {
        this.mainDashboardData['compensate_data'] = { name: 'compensate', value: res.data.data };
      }
    });
    this.filterS.getCohertData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['cohert_data'] = { name: 'cohert', value: res.data.data };
      }
    });
    this.filterS.getHiringData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['hiring_data'] = { name: 'hiring', value: res.data.data };
      }
    });
    this.filterS.getAgeGroup(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['age_group_data'] = { name: 'agegroup', value: res.data.data };
      }
    });
    this.filterS.getGemData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['rewards_gem_data'] = { name: 'gem', value: res.data.data };
      }
    });
    this.filterS.getStarData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['rewards_star_data'] = { name: 'star', value: res.data.data };
      }
    });
    this.filterS.getGenderData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['gender_data'] = { name: 'gender', value: res.data.data };
      }
    });
    this.filterS.getGradeData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['grade_data'] = { name: 'grade', value: res.data.data };
      }
    });
    this.filterS.getPipData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['pip_data'] = { name: 'pip', value: res.data.data };
      }
    });
    this.filterS.getDisData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['dis_data'] = { name: 'dis', value: res.data.data };
      }
    });
    this.filterS.getMaritalData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['marital_data'] = { name: 'marital', value: res.data.data };
      }
    });
    this.filterS.getTrainingData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['training_data'] = { name: 'training', value: res.data.data };
      }
    });
    this.filterS.getvoluntaryData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['voluntary'] = { 'name': 'voluntary', 'value': res.data.data };
      }
    });
    this.filterS.getBusinssServiceFunction(data).subscribe((res: any) => {

      if (res.message === 'success') {
        this.mainDashboardData['businsess_service_function'] = { 'name': 'businessservice', 'value': res.data.data };
      }
    });

    this.filterS.getMajorServiceFunction(data).subscribe((res: any) => {

      if (res.message === 'success') {
        this.mainDashboardData['major_service_function'] = { 'name': 'majorservice', 'value': res.data.data };
      }
    });

    this.filterS.getWorkContractData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['work_contract'] = { 'name': 'work', 'value': res.data.data };
        this.tabroute.setDescWork(this.mainDashboardData['work_contract']);
      }
    });
    this.filterS.getQualificationData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['qualification'] = { 'name': 'qualification', 'value': res.data.data };
        // this.tabroute.setDescQual(this.mainDashboardData['qualification']);
      }
    });
    this.filterS.getManagerDeptData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['mdept'] = { 'name': 'mdept', 'value': res.data };
        this.mainDashboardData["deptCorrelation"] = res.cor_factor


        this.tabroute.setDescMdept(this.mainDashboardData['mdept']);
      }
    });
    this.filterS.getReportManagerData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['report'] = { 'name': 'left_report', 'value': res.data.data };
        this.tabroute.setDescReport(this.mainDashboardData['report']);
      }
    });
    this.filterS.getFunctionalCompetency(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['functionalCompetency'] = { 'name': 'functionalCompetency', 'value': res.data };
        this.tabroute.setDescFunctionalCompetency(this.mainDashboardData['functionalCompetency']);
      }
    });
    this.filterS.getManagerMcaresData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['mmcares'] = { 'name': 'mmcares', 'value': res.data };
        this.mainDashboardData["managerCorrelation"] = res.cor_factor
        // console.log(this.mainDashboardData["managerCorrelation"])
        this.tabroute.setDeskMmcares(this.mainDashboardData['mmcares']);
        if (this.activeTab === 0) {
          this.tabroute.setLoader(false);
          this._snackBar.open('Data Updated Successfully', 'Close', {
            duration: 3000
          });
        }
      }
    });
    this.filterS.getBusinssServiceFunction(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['businsess_service_function'] = { 'name': 'businessservice', 'value': res.data.data };
      }
    });

    this.filterS.getMajorServiceFunction(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['major_service_function'] = { 'name': 'majorservice', 'value': res.data.data };
      }
    });

  }

  



  /* Dropdown change function */
  getCustomStartDate(val) {
    const date = this.getDateByObj(val);
    this.startDate = date;
  }




  getCustomEndDate(val) {
    const date = this.getDateByObj(val);
    this.endDate = date;
  }

  getTypeOfDropdown(index) {
    this.dropdownActiveIndex = index;
  }

  getSelectedOptions(val) {
    this.selected = val;
    if (this.selected.length > 0) {
      if (this.dropdown_sequence.includes(this.dropdownActiveIndex) === false) {
        this.dropdown_sequence.push(this.dropdownActiveIndex);
      } else {
        const index = this.dropdown_sequence.indexOf(this.dropdownActiveIndex);
        this.dropdown_sequence.splice(index, 1);
        this.dropdown_sequence.push(this.dropdownActiveIndex);
      }
    } else if (this.selected.length < 1) {
      this.cd.detectChanges();
      if (this.dropdown_sequence.includes(this.dropdownActiveIndex)) {
        const index = this.dropdown_sequence.indexOf(this.dropdownActiveIndex);
        this.dropdown_sequence.splice(index, 1);
      }
    }
  }

  resetDropdown() {
    for (let i = 0; i < this.multiSelect['_results'].length; i++) {
      this.multiSelect['_results'][i].selectedValue = [];
    }
    this.dropdown_sequence = [];
    if (this.activeTab === 2) {
      this.tabroute.setLeftGraphArrEmpty(true);
    }
  }


  sidebarSubmit() {
    const tempObj = {};
    const ratingObj = {};
    this.pieswitchData = [];
    if (this.activeTab === 0) {
      this.tabroute.setLoader(true);
    } else {
      this.tabroute.setComparisonLoader(true);
    }
    for (let i = 0; i < this.dropdown_sequence.length; i++) {
      const tempVal = this.multiSelect['_results'][this.dropdown_sequence[i]].selectedValue;
      // console.log(tempVal)
      if (tempVal.length > 0) {
        const key: any = this.dropdownTypeArr[this.dropdown_sequence[i]];
        tempObj[key] = JSON.stringify(tempVal).replace(/"/g, "'");
        ratingObj[key] = JSON.stringify(tempVal).replace(/"/g, "'");
        this.pieswitchData.push(tempObj);
      }
    }
    const tempSeqVal = JSON.stringify(this.dropdown_sequence);
    const tempSeqArr = JSON.parse(tempSeqVal);
    this.comparisonData = [];
    let comparison_left_treemap_data_string = JSON.stringify(this.dropdown_sequence);
    let comparison_left_treemap_data = JSON.parse(comparison_left_treemap_data_string);
    if (Object.keys(tempObj).length > 0) {
      tempObj['start_date'] = this.startDate;
      tempObj['end_date'] = this.endDate;
      ratingObj['start_date'] = this.startDate;
      ratingObj['end_date'] = this.endDate;
      ratingObj['type_rating'] = 'average_rating';
      this.filterS.getTreemapData(tempObj).subscribe((res: any) => {
        if (res.data.performer_by_state.length > 0) {
          if (res.message === 'success') {
            if (res.data.performer_by_state.length === 1 && res.data.performer_by_state[0].percentage === 0) {
              this.mainDashboardData['treemap_data'] = [];
              this.mainDashboardData['error_msg'] = { key: true, value: 'No data available for the selected sequence.' }
            } else {
              this.mainDashboardData['error_msg'] = undefined;
              const tempObj2 = {};
              tempObj2['index'] = this.dropdownActiveIndex;
              tempObj2['value'] = res.data.performer_by_state;
              if (this.activeTab === 0) {
                this.mainDashboardData['treemap_data'] = tempObj2;
              }
            }
          }
        } else if (res.data.performer_by_state.length < 1) {
          this.mainDashboardData['treemap_data'] = [];
          this.mainDashboardData['error_msg'] = { key: true, value: 'Invalid sequence selection!!' }
        }
      });
      this.filterS.getRatingData(ratingObj).subscribe((res: any) => {
        if (res.message === 'success') {
          this.mainDashboardData['rating_data'] =  { name: 'rating',"value":res.data.data}
        }
      });
      if (this.activeTab === 0) {
        this.mainDashboardData['breadcrumb'] = { 'key': true, 'val': tempSeqArr };
        this.getAllDashboardData(tempObj);
      } else if (this.activeTab === 2) {
        this.comparisonData.push({ 'mainData': tempObj }, { 'treemapData': tempObj }, { 'index': this.dropdownActiveIndex }, { 'breadcrumb': comparison_left_treemap_data });
      }
    } else {
      tempObj['start_date'] = this.startDate;
      tempObj['end_date'] = this.endDate;
      ratingObj['start_date'] = this.startDate;
      ratingObj['end_date'] = this.endDate;
      ratingObj['type_rating'] = 'average_rating';
      tempObj['state'] = this.mainDropdownData.state_list;
      ratingObj['state'] = this.mainDropdownData.state_list;
      this.pieswitchData.push(tempObj);
      this.filterS.getRatingData(ratingObj).subscribe((res: any) => {
        if (res.message === 'success') {
          this.mainDashboardData['rating_data'] =  { name: 'rating',"value":res.data.data}
        }
      });
      this.mainDashboardData['breadcrumb'] = { 'key': false, 'val': '' };
      this.mainDashboardData['error_msg'] = undefined;
      if (this.activeTab === 0) {
        this.getAllDashboardData(tempObj);
      } else if (this.activeTab === 2) {
        this.comparisonData.push({ 'mainData': tempObj }, { 'treemapData': tempObj }, { 'index': null }, { 'breadcrumb': comparison_left_treemap_data });
      }
      this.filterS.getTreemapData(tempObj).subscribe((res: any) => {
        if (res.message === 'success') {
          const tempObj2 = {};
          tempObj2['index'] = null;
          tempObj2['value'] = res.data.performer_by_state;
          if (this.activeTab === 0) {
            this.mainDashboardData['treemap_data'] = tempObj2;
          }
        }
      });
    }
    this.csvFilterData = {...tempObj}
      // this.emitCsvFilterData.emit(this.csvFilterData)

  }


}
