import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoaderserviceService } from 'src/app/common/services/loaderservice.service';
import { HttpEventType } from '@angular/common/http';
declare var $: any;

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  @ViewChild('downloadZipLink',{static:false}) private downloadZipLink: ElementRef;
  file_names: any = []
  file_name: any;
  file_count = 0
  refresh_buttonhtml = "Refresh"
  data_loader = {}
  loader_data = []
  loaderactive:boolean;
  waiting = false;
  files = [];
  refresh_button_status = false
  result_text = ""
  refresh_button_bind:any
  fileName: any = []
  default_value = 'Select table name';
  selected_table_name: any = "";
  table_name: any;
  percentage: any;
  dropdown_data_value: any;
 
  loader_service: any;
  tableHasError: boolean = true;
  unique_col_data = [
    { "header": "Recruitement", "data": "empid" },
    { "header": "Consolidated Employee Demographics", "data": "employee_id" },
    { "header": "Gem Rewards", "data": "sap_code, year, quarter" },
    { "header": "Star Rewards", "data": "emp_id, year, month" },
    { "header": "Leave", "data": "personnel_number, start_date" },
    { "header": "Employee Transaction", "data": "personnel_number, start_date" },
    { "header": "Training", "data":"candidate_sap_code,start,program"},
    { "header": "Competency Report", "data": "employee_id, year" },
    { "header": "Mcares Score Department", "data": "department,year" },
    { "header": "Mcares Score Manager", "data": "manager_id,year" },
    { "header": "Promotion And Rating", "data": "employee_id,year" },
    { "header": "Compensation", "data": "employee_id, last_anuual_hike_date" },
    { "header": "Dis And Pip", "data": "sap_code, issue" },
    { "header": "Major Service Funtion", "data": "cost_centre" },
    { "header": "Business Service Funtion", "data": "cost_centre" }
  ]


  constructor(private route: ActivatedRoute,
    private service: LoaderserviceService) { }

    ngOnDestroy(): void {
      this.refresh_button_bind.unbind()
    }
  ngOnInit() {
    let self = this
    
    if (localStorage.getItem("file_names") != null) {
      self.file_names = localStorage.getItem("file_names").split(",");
      self.file_names.pop()
      self.file_count = self.file_names.length
    }
    $(".instructionModal").modal();
    $('.more-details').click(function () {
      $(this).hide();
      $(".dataInfo").toggle();
    });
    this.dropdownData()

    self.refresh_button_bind = $('#load').on('click', function() {
     
      self.refresh_buttonhtml = '<i class="fa fa-circle-o-notch fa-spin"></i> Refreshing Data'
  });
  }

  onFileChanged(event: any) {
    this.files = event.target.files
  }

  dropdownChange(data, value) {
    this.selected_table_name = data.target.value;
    if (value === "default") {
      this.tableHasError = true;
    } else {
      this.tableHasError = false;
    }
  }

  fileSubmit() {
    this.loader_data = []
    let self = this
    let flag = true
    $(".loading-container").show();
    $(".loading-success").hide();
    $(".loading-failed").hide();
    $(".btn_logger").prop("disabled", true)
    $(".choose-file").prop("disabled", true).css({ "cursor": "not-allowed" }).click(function () {
      $(".loading-success").hide();
      $(".loading-failed").hide();
    });
    $(".upload").css({ "background-color": "#9c9c9c" });
    for (let index = 0; index < this.fileName.length; index++) {
      const element = this.fileName[index];
      var splitted_array = element.split(".")
      var file_extension = splitted_array[1]
      if (file_extension != "csv") {
        flag = false
      }
    }

    if (flag != false) {
      this.service.save_file(this.files, this.selected_table_name).subscribe((response: any) => {
        if (response.type === HttpEventType.UploadProgress) {
          this.percentage = Math.round(response.loaded / response.total * 100) - 1 + '%'
        }

        if (response.body !== undefined) {
          this.percentage = 100 + "%"
          this.loader_data = response.body["response"]
          response.body['response'].forEach(element => {

            if (localStorage.getItem("file_names")) {
              self.file_names = localStorage.getItem("file_names").split(",");

            }
            if (element.success === "True") {
              $(".loading-container").hide();
              $(".loading-failed").hide();
              $(".loading-success").show();
              $(".choose-file").prop("disabled", false).css({ "cursor": "default" });
              $(".btn_logger").prop("disabled", false).css({ "cursor": "pointer" });
              $(".upload").css({ "background-color": "#686868" });

              if (self.file_names.length == 0) {

                self.file_names = element.file_name
                localStorage.setItem("file_names", self.file_names + ",")
              }
              else {
                self.file_names.pop()

                self.file_names = self.file_names + "," + element.file_name
                localStorage.setItem("file_names", self.file_names + ",")

              }

              if (localStorage.getItem("file_names") != null) {
                self.file_names = localStorage.getItem("file_names").split(",");
                self.file_names.pop()

                self.file_count = self.file_names.length
              }
            } else {
              $(".loading-container").hide();
              $(".loading-failed").show();
              $(".loading-success").hide();
              $(".choose-file").prop("disabled", false).css({ "cursor": "default" });
              $(".btn_logger").prop("disabled", false).css({ "cursor": "pointer" });
              $(".upload").css({ "background-color": "#686868" });
            }
          })
        };
      });
    }
    else {
      this.loader_data.push({ "file_name": "", "success": "False", "message": "Wrong Files Uploaded, Please upload a csv files." })
      $(".choose-file").prop("disabled", false).css({ "cursor": "default" });
    };
  }

  public async downloadZip(): Promise<void> {
    const blob: any = await this.service.downloadResource();
    const url = window.URL.createObjectURL(blob);

    const link = this.downloadZipLink.nativeElement;
    link.href = url;
    link.download = 'Mahindra_Data_Template.zip';
    link.click();

    window.URL.revokeObjectURL(url);
  }

  closeAlert() {
    $('.upload-alert').hide();
  }

  dropdownData() {
    this.service.dropdown_data().subscribe((response: any) => {
      
      this.dropdown_data_value = response.data
    });
  }

  refresh_view(){
  let self = this
  this.refresh_button_status = true
  $("#danger_view_modal").modal('show');
    this.service.refresh_data().subscribe(
      (res: any) => {

        self.refresh_button_status = false
      
       if ($('#danger_view_modal').hasClass('show')) {
        $("#danger_view_modal").modal('hide');
    }

     
      self.refresh_buttonhtml = "Refresh"
      $("#view_modal").modal('show');
      self.result_text = "Data Has Been Refreshed Successfully"
    
      },
      err => {
        self.refresh_button_status = false

        if ($('#danger_view_modal').hasClass('show')) {
          $("#danger_view_modal").modal('hide');
      }
        $("#view_modal").modal('show');
        self.refresh_buttonhtml = "Refresh"

        self.result_text  = "Failed "

      }
    )
 
}
}


