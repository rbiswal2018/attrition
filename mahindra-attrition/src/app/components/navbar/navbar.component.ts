import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { TabRoute } from '../../common/services/tabroute.service';
import { AuthService } from '../../common/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public showDashboardMenu: any;
  public userEmail: any;
  public userEmailFirstLetter: any;
  selectedIndex= 0;
  constructor(private tabroute: TabRoute, private router: Router, private auth: AuthService) { }
  ngOnInit() {
    this.tabroute.tabRouteValue$.subscribe((res) => {

      this.selectedIndex = res
    })  
    this.userEmail = JSON.parse(localStorage.getItem('user_data')).message.email;
    this.userEmailFirstLetter = this.userEmail.charAt(0);
    const curURL = (this.router.url).substr(1);
    if (curURL === 'dashboard') {
      this.showDashboardMenu = false;
    } else {
      this.showDashboardMenu = true;
    }
  }

  logout() {
    localStorage.removeItem('user_data');
    this.router.navigate(['/']);
  }

  tabChange(tab: MatTabChangeEvent) {
    this.tabroute.redirectToTab(tab.index);
  }

}
