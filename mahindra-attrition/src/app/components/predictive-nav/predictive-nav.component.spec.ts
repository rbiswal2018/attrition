import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictiveNavComponent } from './predictive-nav.component';

describe('PredictiveNavComponent', () => {
  let component: PredictiveNavComponent;
  let fixture: ComponentFixture<PredictiveNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictiveNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictiveNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
