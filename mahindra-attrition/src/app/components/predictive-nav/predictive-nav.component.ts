import { Component, OnInit, ViewChildren, QueryList,ChangeDetectorRef  } from '@angular/core';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { FilterService } from 'src/app/common/services/filter.service';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { PredictiveService } from 'src/app/common/predictive.service';
@Component({
  selector: 'app-predictive-nav',
  templateUrl: './predictive-nav.component.html',
  styleUrls: ['./predictive-nav.component.scss'],
  animations: [
    trigger('leftOpenClose', [
      state('open', style({
        width: '250px',
        opacity: 1
      })),
      state('closed', style({
        width: '0px',
        opacity: 0
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ])
    ])
  ]
})
export class PredictiveNavComponent implements OnInit {
  public sidebarDropdownSelectList: any[];
  @ViewChildren(SelectAutocompleteComponent) multiSelect: 
  QueryList<SelectAutocompleteComponent>;
  hide_bar = true;
  mainDropdownData: any;
  pieswitchData: any[] = [];
  FilterData:any
  selectedOptions = [];
  selected = this.selectedOptions;
  dropdownActiveIndex: any;
  dropdown_sequence: any[] = [];
  loaderactive: any = true;
  PredictiveNavComponent: any;
  leftSide: any;
  filterDataKeyNames = ['state_list', 'branch_list', 'function_name', 'division_list', 'business_unit', 'department', 
  'sub_department', 'cost_centre', 'work_contract', 'band', 'grade_designation'];
  dropdownTypeArr = ['state', 'branch', 'function_name', 'division', 'business_unit', 'department', 
  'sub_department', 'cost_centre', 'work_contract', 'band', 'grade_designation'];
  filterSelectDisplayName = ['Select State', 'Select Branch', 'Select Function', 'Select Division', 'Select Business', 'Select Department',
  'Select Sub Department', 'Select Cost Center', 'Select Work Contract', 'Select Band', 'Select Sub Group'];

  constructor( private filterS: FilterService,private api: PredictiveService, private cd: ChangeDetectorRef) { }


  ngOnInit() {
    this.hide_bar = true
    this.api.getpredicitveFilterDropdownData().subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDropdownData = res.data;
        let count = 0;
        this.FilterData  = {"state":this.makeSingleQuotesObj(res.data.state_list)}
        this.sidebarDropdownSelectList = [];
        for (let i of this.filterDataKeyNames) {
          const tempObj = {};
          tempObj['main_display_name'] = this.filterSelectDisplayName[count];
        //  console.log(this.filterSelectDisplayName[count],res.data[this.filterDataKeyNames[count]])
          tempObj['main_data'] = this.makeDropdownData(res.data[this.filterDataKeyNames[count]]);
          this.sidebarDropdownSelectList.push(tempObj);
          count++;
        }
        this.loaderactive = false;
      }
    });
  }
  makeDropdownData(arr) {
    // console.log("sss",arr)
    const tempArr = [];
    for (let val of arr) {
      if (val !== '') {
        const tempObj = {};
        tempObj['display'] = val;
        tempObj['value'] = val;
        tempArr.push(tempObj);
      }
    }
    return tempArr;
  }
  
  makeSingleQuotesObj(arr) {
    const newArr = JSON.stringify(arr).replace(/"/g, "'");
    return '' + newArr + '';
  }
  resetDropdown() {
    for (let i = 0; i < this.multiSelect['_results'].length; i++) {
      this.multiSelect['_results'][i].selectedValue = [];
    }
    this.FilterData  = {"state":this.makeSingleQuotesObj(this.mainDropdownData.state_list)}


  }
  getSelectedOptions(val) {
    this.selected = val;
    if (this.selected.length > 0) {
      if (this.dropdown_sequence.includes(this.dropdownActiveIndex) === false) {
        this.dropdown_sequence.push(this.dropdownActiveIndex);
      } else {
        const index = this.dropdown_sequence.indexOf(this.dropdownActiveIndex);
        this.dropdown_sequence.splice(index, 1);
        this.dropdown_sequence.push(this.dropdownActiveIndex);
      }
    } else if (this.selected.length < 1) {
      this.cd.detectChanges();
      if (this.dropdown_sequence.includes(this.dropdownActiveIndex)) {
        const index = this.dropdown_sequence.indexOf(this.dropdownActiveIndex);
        this.dropdown_sequence.splice(index, 1);
      }
    }
  }
  getTypeOfDropdown(index) {
    this.dropdownActiveIndex = index;
  }


  sidebarSubmit() {
    const tempObj= {};
    const ratingObj = {};
    this.pieswitchData = [];
    for (let i = 0; i < this.dropdown_sequence.length; i++) {
      const tempVal = this.multiSelect['_results'][this.dropdown_sequence[i]].selectedValue;

      if (tempVal.length > 0) {
        const key: any = this.dropdownTypeArr[this.dropdown_sequence[i]];
        tempObj[key] = JSON.stringify(tempVal).replace(/"/g, "'");
      }
    }

    if (Object.keys(tempObj).length > 0) {


    }
    else{
      tempObj['state'] = this.mainDropdownData.state_list;
    }
    this.FilterData = tempObj
    // console.log(this.FilterData)
  
  
  }
}
