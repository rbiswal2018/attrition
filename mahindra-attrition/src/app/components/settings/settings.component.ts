import { Component, OnInit } from '@angular/core';
import { AdminpanelServices } from '../../common/services/admin-panel.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  public user_obj: any;
  public user_id: any;
  public firstname: any;
  public lastname: any;
  public email: any;
  public role: any;
  public old_pwd: any;
  public new_pwd: any;

  constructor(public admin: AdminpanelServices,  private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.user_obj = JSON.parse(localStorage.getItem('user_data'));
    this.user_id = this.user_obj.message.id;
    this.firstname = this.user_obj.message.first_name;
    this.lastname = this.user_obj.message.last_name;
    this.email = this.user_obj.message.email;
    this.role = this.user_obj.message.role;
  }

  update() {
    const user = {};
    user['user'] = {
      'user_id': this.user_id,
      'first_name': this.firstname,
      'last_name': this.lastname
    };
    user['role'] = this.role;
    this.admin.editUser(user).subscribe((res: any) => {
      if (res['message'] === 'success') {
        this.user_obj.message['first_name'] = res.data.user['first_name'];
        this.user_obj.message['last_name'] = res.data.user['last_name'];
        localStorage.setItem('user_data', JSON.stringify(this.user_obj));
        this.snackBar.open('User updated successfully.', '', {duration: 3000});
      }
    })
  }

  changePwd() {
    if (
      this.old_pwd !== undefined &&
      this.new_pwd !== undefined
    ) {
      const obj = {};
      obj['old_password'] = this.old_pwd;
      obj['new_password'] = this.new_pwd;
      this.admin.changePwd(obj).subscribe((res) => {
        this.old_pwd = undefined;
        this.new_pwd = undefined;
        this.snackBar.open('Password changed successfully.', '', {duration: 3000});
      });
    } else {
      this.snackBar.open('Both password fields needs to be filled.', '', {duration: 3000});
    }
  }

}
