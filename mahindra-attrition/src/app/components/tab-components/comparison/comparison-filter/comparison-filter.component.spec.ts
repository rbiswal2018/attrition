import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparisonFilterComponent } from './comparison-filter.component';

describe('ComparisonFilterComponent', () => {
  let component: ComparisonFilterComponent;
  let fixture: ComponentFixture<ComparisonFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComparisonFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparisonFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
