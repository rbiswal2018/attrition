import { Component, OnInit, ViewChildren, QueryList, Input, OnChanges, ViewChild, ChangeDetectorRef, SimpleChange, SimpleChanges, OnDestroy } from '@angular/core';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import domtoimage from 'dom-to-image-more';
import { TabRoute } from '../../../../common/services/tabroute.service';
import { ImageService } from '../../../../common/services/image.service';
import { FilterService } from 'src/app/common/services/filter.service';
declare var $: any;

@Component({
  selector: 'app-comparison-filter',
  templateUrl: './comparison-filter.component.html',
  styleUrls: ['./comparison-filter.component.scss']
})
export class ComparisonFilterComponent implements OnInit, OnChanges, OnDestroy {

  @ViewChildren(SelectAutocompleteComponent) multiSelect: 
  QueryList<SelectAutocompleteComponent>;
  @ViewChild('leftqual', {static: false}) left_qual_table;
  @ViewChild('rightqual', {static: false}) right_qual_table;
  @ViewChild('leftmdept', {static: false}) left_mdept;
  @ViewChild('rightmdept', {static: false}) right_mdept;
  @ViewChild('leftreport', {static: false}) left_report;
  @ViewChild('rightreport', {static: false}) right_report;
  @ViewChild('leftmmcare', {static: false}) left_mmcare;
  @ViewChild('rightmmcare', {static: false}) right_mmcare;
  @ViewChild('leftwork', {static: false}) left_work;
  @ViewChild('rightwork', {static: false}) right_work;
  @ViewChild('leftFunctionalCompetency', {static: false}) leftFunctionalCompetency;
  @ViewChild('rightFunctionalCompetency', {static: false}) rightFunctionalCompetency;
  @ViewChild('leftBusinessService', {static: false}) leftBusinessService;
  @ViewChild('rightBusinessService', {static: false}) rightBusinesService;
  @ViewChild('majorService', {static: false}) leftMajorService;
  @ViewChild('majorService', {static: false}) rightMajorService;
  dataTable: any;
  tableDataArr: any[] = [];
  @Input() mainData: any;
  @Input() leftSideFilters: any;
  @Input() rightsideFilters: any;

  selectedOptions = [1, 2, 3];
  selected = this.selectedOptions; 
  showAgeGraph: any = false;
  showRewardGraph: any = true;
  showGenderGraph: any = true;
  showGradeGraph: any = true;
  showPipGraph: any = false;
  showMaritalGraph: any = false;
  showTrainingGraph: any = false;
  showVoluntaryGraph: any = false;
  showQualificationData: any = false;
  showMcaresDept: any = false;
  showWorkContract: any = false;
  showReport: any = false;
  showMmcare: any= false;
  showFunctionalCompetency: any = false;
  showBusinessService: any = false;
  showMajorService: any = false;
  ratingFilterVal: any = 1;
  curTreemapStatus: any;
  makeMainGroupBarChartData: any[] = [];
  public showpip = true;
  public showdis = false;
  public tempGradeData: any[] = [];
  public gradeData: any;
  public startPoint: number = 5;
  public hideFilterLoader = true;
  left_table_no_data_available: any = false;
  right_table_no_data_available: any = false;
  showVolInvolLeft = true
  showvolInvolCategoryLeft =false
  showVolInvolRight = true
  showvolInvolCategoryRight =false
  showBusinessServiceNonCatLeft = true
  showBusinessServiceCategoryLeft =false
  showBusinessServiceNonCatRight = true
  showBusinessServiceCategoryRight =false

  showMajorFunctionServiceLeft = true
  showMajorFunctionServiceCategoryLeft =false
  showMajorFunctionServiceRight = true
  showMajorFunctionServiceCategoryRight =false

  factorOptions = [
    {
      display: 'Age group',
      value: 0
    }, {
      display: 'Rewards',
      value: 1
    }, {
      display: 'Gender',
      value: 2
    }, {
      display: 'Grade',
      value: 3
    }, {
      display: 'PIP/DIS',
      value: 4
    }, {
      display: 'Maritial status',
      value: 5
    }, {
      display: 'Training',
      value: 6
    }, {
      display: 'Voluntary / Involuntary',
      value: 7
    }, {
      display: 'Work contract',
      value: 10
    },  {
      display: 'Highest qualification',
      value: 8
    }, {
      display: 'Mcares department',
      value: 9
    },{
      display: 'Reporting manager',
      value: 11
    },{
      display: 'Mcare manager',
      value: 12
    }, {
      display: 'Functional Competency',
      value: 13
    },{
      display: 'Business/Service Function',
      value: 14
    },{
      display: 'Major Function',
      value: 15
    }

  ];

  getSelectedOptions(selected) {
    this.showAgeGraph = false;
    this.showRewardGraph = false;
    this.showGenderGraph = false;
    this.showGradeGraph = false;
    this.showPipGraph = false;
    this.showMaritalGraph = false;
    this.showTrainingGraph = false;
    this.showVoluntaryGraph = false;
    this.showQualificationData = false;
    this.showMcaresDept = false;
    this.showWorkContract = false;
    this.showReport = false;
    this.showMmcare = false;
    this.showFunctionalCompetency = false;
    this.showBusinessService = false;
    this.showMajorService = false;
    this.selected = selected;
    for (let option of this.selected) {
      this.displaySelectedFilterGraph(option);
    }
  }

  displaySelectedFilterGraph(val) {
    switch(val) {
      case 0:
      this.showAgeGraph = true;
      break;
      case 1:
      this.showRewardGraph = true;
      break;
      case 2:
      this.showGenderGraph = true;
      break;
      case 3:
      this.showGradeGraph = true;
      break;
      case 4:
      this.showPipGraph = true;
      break;
      case 5:
      this.showMaritalGraph = true;
      break;
      case 6:
      this.showTrainingGraph = true;
      break;
      case 7:
      this.showVoluntaryGraph = true;
      break;
      case 8:
      this.showQualificationData = true;
      break;
      case 9:
      this.showMcaresDept = true;
      this.changeDetector.detectChanges();
      this.fetchUserData(this.mainData.left_mdept.value, this.left_mdept);
      this.fetchUserData(this.mainData.right_mdept.value, this.right_mdept);
      break;
      case 10:
      this.showWorkContract = true;
      this.changeDetector.detectChanges();
      this.fetchUserData(this.mainData.left_work.value, this.left_work);
      this.fetchUserData(this.mainData.right_work.value, this.right_work);
      break;
      case 11:
      this.showReport = true;
      this.changeDetector.detectChanges();
      this.fetchUserData(this.mainData.left_report.value, this.left_report);
      this.fetchUserData(this.mainData.right_report.value, this.right_report);
      break;
      case 12:
      this.showMmcare = true;
      this.changeDetector.detectChanges();
      this.fetchUserData(this.mainData.left_mmcares.value, this.left_mmcare);
      this.fetchUserData(this.mainData.right_mmcares.value, this.right_mmcare);
      break;
      case 13:
      this.showFunctionalCompetency = true;
      this.changeDetector.detectChanges();
      this.fetchUserData(this.mainData.leftFunctionalCompetency.value, this.leftFunctionalCompetency);
      this.fetchUserData(this.mainData.rightFunctionalCompetency.value, this.rightFunctionalCompetency);
      break;
      case 14:
      this.showBusinessService = true;
      break;
      case 15:
      this.showMajorService = true;
      break;
    }
  }

  makeDataFormat(data) {
  }

  constructor(private changeDetector : ChangeDetectorRef, private filterS: FilterService, public tabroute: TabRoute, public imageService: ImageService) { }

  downloadAsImage(node, data, obj) {
    this.imageService.makeComparisonTable(node, data, obj);
  }

  downloadSingleGraphAsImage(node, data, obj) {
    this.imageService.makeTable(node, data, obj);
  }

  makeCsvConvertableObj(arr, obj) {
    let tempArr = [];
    for (let val of arr) {
      const tempName = val.name;
      for (let val2 of val.series) {
        const tempObj = {};
        tempObj[obj[0]] = tempName+ '('+val2.name+')';
        tempObj[obj[1]] = val2.value;
        tempArr.push(tempObj);
      }
    }
    return tempArr;
  }

  convertToCSV(arr) {
    const array = [Object.keys(arr[0])].concat(arr);
    return array.map(it => {
      return Object.values(it).toString()
    }).join('\n')
  }

  exportToCsv(filename, arr, val, obj) {
    let processRow;
    if (val) {
      processRow = this.convertToCSV(this.makeCsvConvertableObj(arr.value, obj));
    } else {
      if (obj !== '') {
        let tempArr = [];
        for (let i = 0; i < arr.value.length; i++) {
          let tempObj = {};
          tempObj[obj[0]] = arr.value[i].name;
          tempObj[obj[1]] = arr.value[i].value;
          tempArr.push(tempObj);
        }
        processRow = this.convertToCSV(tempArr);
      } else {
        processRow = this.convertToCSV(arr.value);
      }
    }
    var blob = new Blob([processRow], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) {
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) {
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename+".csv");
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
  }

  pipordis(val) {
    if (val) {
      this.showdis = true;
      this.showpip = false;
    } else {
      this.showpip = true;
      this.showdis = false;
    }
  }

  ngOnInit() {
    this.tabroute.leftmdept$.subscribe((res) => {
      if (this.left_mdept !== undefined) {
        this.fetchUserData(res['value'], this.left_mdept);
      }
    });
    this.tabroute.rightmdept$.subscribe((res) => {
      if (this.right_mdept !== undefined) {
        this.fetchUserData(res['value'], this.right_mdept);
      }
    });
    this.tabroute.leftreport$.subscribe((res) => {
      if (this.left_report !== undefined) {
        this.fetchUserData(res['value'], this.left_report);
      }
    });
    this.tabroute.rightreport$.subscribe((res) => {
      if (this.right_report !== undefined) {
        this.fetchUserData(res['value'], this.right_report);
      }
    });
    this.tabroute.leftmmcares$.subscribe((res) => {
      if (this.left_mmcare !== undefined) {
        this.fetchUserData(res['value'], this.left_mmcare);
      }
    });
    this.tabroute.rightmmcares$.subscribe((res) => {
      if (this.right_mmcare !== undefined) {
        this.fetchUserData(res['value'], this.right_mmcare);
      }
    });
    this.tabroute.leftwork$.subscribe((res) => {
      if (this.left_work !== undefined) {
        this.fetchUserData(res['value'], this.left_work);
      }
    });
    this.tabroute.leftFunctionalCompetency$.subscribe((res: any) => {
      if (res.value !== undefined) {
        if (res.value.length < 1) {
          this.left_table_no_data_available = true;
        } else {
          this.left_table_no_data_available = false;
        }
      }
      if (this.leftFunctionalCompetency !== undefined) {
        this.fetchUserData(res['value'], this.leftFunctionalCompetency);
      }
    });
    this.tabroute.rightFunctionalCompetency$.subscribe((res: any) => {
      if (res.value !== undefined) {
        if (res.value.length < 1) {
          this.right_table_no_data_available = true;
        } else {
          this.right_table_no_data_available = false;
        }
      }
      if (this.rightFunctionalCompetency !== undefined) {
        this.fetchUserData(res['value'], this.rightFunctionalCompetency);
      }
    });
    this.tabroute.leftBusinessService$.subscribe((res: any) => {
      if (res.value !== undefined) {
        if (res.value.length < 1) {
          this.left_table_no_data_available = true;
        } else {
          this.left_table_no_data_available = false;
        }
      }
      if (this.leftBusinessService !== undefined) {
        this.fetchUserData(res['value'], this.leftBusinessService);
      }
    });
    this.tabroute.rightwork$.subscribe((res) => {
      if (this.right_work !== undefined) {
        this.fetchUserData(res['value'], this.right_work);
      }
    });
    this.tabroute.gradeData$.subscribe((res: any) => {
      if (res.value !== undefined) {
        this.startPoint = 5;
        this.gradeData = this.gradeLoadFirstTime(res.value);
        this.tempGradeData = res.value;
      }
    });
    this.tabroute.cloader$.subscribe((res: any) => {
      if (res === false) {
        this.hideFilterLoader = res;
      } else {
        this.hideFilterLoader = true;
      }
    });
  }

  plotTable(data, dataTable, cols) {
    if ( $.fn.DataTable.isDataTable(dataTable) ) {
      $(dataTable).DataTable().destroy();
    }
    dataTable.DataTable({
      data: data,
      retrieve: true,
      "scrollY": "300px",
      columns: cols,
      "scrollX": true,
    });
  }

  fetchUserData(val, table) {
    const tableDataArr = [];
    const cols = [];
    const colsData = [];
    if (val.length > 0) {
      for (const data of Object.keys(val[0])) {
          colsData.push({'title': data});
          cols.push(data);
      }
      for (const data of val) {
        const tempTableArr = [];
        for (const col of cols) {
          tempTableArr.push(data[col]);
        }
        tableDataArr.push(tempTableArr);
      }
      const dataTable = $(table.nativeElement);
      this.plotTable(tableDataArr, dataTable, colsData);
    } else  {
      const dataTable = $(table.nativeElement);
      if ( $.fn.DataTable.isDataTable(dataTable) ) {
        $(dataTable).DataTable().destroy();
        $(dataTable).DataTable().clear().draw();
      }
    }
  }

  gradeLoadFirstTime(data) {
    const tempArr = [];
    if (data.length > 5) {
      for (let i = 0; i < 5; i++){
        tempArr.push(data[i]);
      }
      return {'name':'grade', 'value': tempArr};
    } else {
      for (let i = 0;i < data.length; i++){
        tempArr.push(data[i]);
      }
      return {'name':'grade', 'value': tempArr};
    }
  }

  changeGradeDataNext() {
    if ((this.startPoint + 5) < this.tempGradeData.length) {
      let tempArr = [];
      for (let i = this.startPoint; i < (this.startPoint + 5); i++) {
        tempArr.push(this.tempGradeData[i]);
      }
      this.gradeData = {'name':'grade', 'value': tempArr};
      this.startPoint = this.startPoint + 5;
    } else if ((this.startPoint + 5) > this.tempGradeData.length && this.startPoint !== this.tempGradeData.length) {
      let tempArr = [];
      for (let i = this.startPoint; i < this.tempGradeData.length; i++) {
        tempArr.push(this.tempGradeData[i]);
      }
      this.gradeData = {'name':'grade', 'value': tempArr};
      this.startPoint = this.tempGradeData.length;
    }
  }

  changeGradeDataPrev() {
    if ((this.startPoint - 5) > 0 && this.gradeData.value.length === 5) {
      let tempArr = [];
      for (let i = (this.startPoint - 6); i >= (this.startPoint - 10); i--) {
        tempArr.unshift(this.tempGradeData[i]);
      }
      this.gradeData = {'name':'grade', 'value': tempArr};
      this.startPoint = this.startPoint - 5;
    } else if ((this.startPoint - 5) > 0 && this.gradeData.value.length < 5) {
      let tempArr = [];
      for (let i = (this.startPoint - (this.gradeData.value.length + 1)); i >= (this.startPoint - (this.gradeData.value.length + 5)); i--) {
        tempArr.unshift(this.tempGradeData[i]);
      }
      this.gradeData = {'name':'grade', 'value': tempArr};
      this.startPoint = this.startPoint - (this.gradeData.value.length - 1);
    } else if ((this.startPoint - 5) <= 0 && this.startPoint !== 5) {
      let tempArr = [];
      for (let i = this.startPoint; i >= 0; i--) {
        tempArr.unshift(this.tempGradeData[i]);
      }
      this.gradeData = {'name':'grade', 'value': tempArr};
      this.startPoint = 5;
    }
  }

  ngOnChanges(changes: SimpleChanges) {

    if (this.leftSideFilters !== undefined) {
      this.showBusinessServiceNonCatLeft = true
      this. showBusinessServiceCategoryLeft =false
      this.showBusinessServiceNonCatRight = true
      this.showBusinessServiceCategoryRight =false
      this.showMajorFunctionServiceLeft = true
      this.showMajorFunctionServiceCategoryLeft =false
      this.showMajorFunctionServiceRight = true
      this.showMajorFunctionServiceCategoryRight =false
    }
    if (this.rightsideFilters !== undefined) {
      this.showBusinessServiceNonCatLeft = true
      this. showBusinessServiceCategoryLeft =false
      this.showBusinessServiceNonCatRight = true
      this.showBusinessServiceCategoryRight =false
      this.showMajorFunctionServiceLeft = true
      this.showMajorFunctionServiceCategoryLeft =false
      this.showMajorFunctionServiceRight = true
      this.showMajorFunctionServiceCategoryRight =false

    }
  }

  ngOnDestroy() {
    this.mainData = [];
  }

  graphBarClick(event,type){

    if(event["name"]=='businessservice'){
      if(type=='leftside'){
        this.showBusinessServiceNonCatLeft = false
        this.showBusinessServiceCategoryLeft =true
        this.mainData['left_businessservice_category'] = {name:"",value:[]}
        const filter = {...this.leftSideFilters}
        filter["business_service_function_cat"] =[event["data"]["name"]]
        this.filterS.getBusinssServiceFunction(filter).subscribe((res: any) => {
          if (res.message === 'success') {
            this.mainData['left_businessservice_category'] = { name: 'businessservice_category', value: res.data.data };
          }
        });

      }
      else{
        this.showBusinessServiceNonCatRight = false
        this.showBusinessServiceCategoryRight =true
        this.mainData['right_business_service_data_category'] = {name:"",value:[]}
        const filter = {...this.rightsideFilters}
        delete filter['chart_name']
        filter["business_service_function_cat"] =[event["data"]["name"]]
        this.filterS.getBusinssServiceFunction(filter).subscribe((res: any) => {
          if (res.message === 'success') {
            this.mainData['right_business_service_data_category'] = { name: 'businessservice_category', value: res.data.data };
          }
    });
    }
  }

if(event["name"]=='majorservice'){
  if(type=='leftside'){
    this.showMajorFunctionServiceLeft = false
    this.showMajorFunctionServiceCategoryLeft =true
    this.mainData['left_majorfunction_category'] = {name:"",value:[]}
    const filter = {...this.leftSideFilters}
    delete filter['chart_name']
    filter["major_service_function_cat"] =[event["data"]["name"]]
    this.filterS.getMajorServiceFunction(filter).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainData['left_majorfunction_category'] = { name: 'majorfunction_category', value: res.data.data };
      }
});


  }
  else{
    this.showMajorFunctionServiceRight = false
    this.showMajorFunctionServiceCategoryRight =true    
    this.mainData['right_majorfunction_category'] = {name:"",value:[]}
    const filter = {...this.rightsideFilters}
    delete filter['chart_name']
    filter["major_service_function_cat"] =[event["data"]["name"]]
    this.filterS.getMajorServiceFunction(filter).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainData['right_majorfunction_category'] = { name: 'majorfunction_category', value: res.data.data };
      }
    });
    
    }
    

  }

}


backBusinessServiceFunction(type){
  if (type=='left'){
    this.showBusinessServiceCategoryLeft =false
    this.showBusinessServiceNonCatLeft = true

  }
  else{
    this.showBusinessServiceCategoryRight =false
    this.showBusinessServiceNonCatRight = true

  }

}
backMajorFunctionService(type){
  if(type=='left'){
    this.showMajorFunctionServiceLeft = true
    this.showMajorFunctionServiceCategoryLeft =false
  }
  else{
    this.showMajorFunctionServiceRight = true
    this.showMajorFunctionServiceCategoryRight =false
  }
}

}
