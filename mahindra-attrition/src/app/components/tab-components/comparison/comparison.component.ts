import { Component, OnInit, ViewChildren, QueryList, AfterViewInit, OnChanges, Input, Output, EventEmitter, ChangeDetectorRef, OnDestroy } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
import { TabRoute } from '../../../common/services/tabroute.service';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { FilterService } from '../../../common/services/filter.service';
import * as d3 from 'd3';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
declare var $: any;
import {MatSnackBar} from '@angular/material/snack-bar';
import { ImageService } from '../../../common/services/image.service';

@Component({
  selector: 'app-comparison',
  templateUrl: './comparison.component.html',
  styleUrls: ['./comparison.component.scss'],
  animations: [
    trigger('rightOpenClose', [
      state('open', style({
        width: '250px',
        opacity: 1
      })),
      state('closed', style({
        width: '0px',
        opacity: 0
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ])
    ])
  ]
})
export class ComparisonComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {

  @ViewChildren(SelectAutocompleteComponent) multiSelect: 
  QueryList<any>;
  @Input() leftDropdownClick: any[];
  @Output() treemapDataComp = new EventEmitter<object>();
  @Output() breadcrumb = new EventEmitter<object>();
  dropdownTypeArr = ['state', 'branch', 'function_name', 'division', 'business_unit', 'department', 
  'sub_department', 'cost_centre', 'work_contract', 'band', 'employee_sub_group'];
  filterDataKeyNames = ['state_list', 'branch_list', 'function_name', 'division_list', 'business_unit', 'department', 
  'sub_department', 'cost_centre', 'work_contract', 'band', 'employee_sub_group'];
  filterSelectDisplayName = ['Select State', 'Select Branch', 'Select Function', 'Select Division', 'Select Business', 'Select Department',
  'Select Sub Department', 'Select Cost Center', 'Select Work Contract', 'Select Band', 'Select Sub Group'];
  minDate: any = '2017-01-01';
  maxDate: any = this.getTodayDate();
  startDate: any;
  endDate: any;mainDropdownData: any;
  rightSide: any = 'closed';
  public sidebarDropdownSelectList: any[];
  public activeTab: any;
  loaderactive: any = true;
  mainDashboardData: any = {};
  tempTreeMapData: any[] = [];
  left_side_emp_left:any
  right_side_emp_left:any;
  rigt_side_attrition_percentage:any;
  left_side_attrition_percentage:any;
  left_filters:any;
  right_filters:any;

  selectedOptions = [];
  selected: any[] = [];
  dropdownActiveIndex: any;
  sidebarMainData: any;
  sidebarTreemapData: any;
  dropdownObj: any = {};
  optionArr: any[] = [];
  dropdownActiveHandle: any[] = [];
  leftTreemapData: any;
  rightTreemapData: any;
  leftTreemapActive: any = false;
  rightTreemapActive: any = false;
  groupedChartFormat: any = {};
  leftBarDataActive: any = true;
  rightBarDataActive: any = true;
  leftBarData: any[] = [];
  rightBarData: any[] = [];
  passingDataArr: any[] = [];
  factorArr: any[] = [];
  loadFilter: boolean = false;
  public left_qualification_temp_data: any;
  public right_qualification_temp_data: any;
  public left_work_temp_data: any;
  public right_work_temp_data: any;
  public left_functionalCompetency_temp_data: any;
  public right_functionalCompetency_temp_data: any;
  public left_businesService_temp_data: any;
  public right_businesService_temp_data: any;
  public left_MajorService_temp_data: any;
  public right_MajorService_temp_data: any;
  public left_mdept_temp_data: any;
  public right_mdept_temp_data: any;
  public left_report_temp_data: any;
  public right_report_temp_data: any;
  public left_mmcares_temp_data: any;
  public right_mmcares_temp_data: any;
  loginStatus: any = localStorage.getItem('user_data');
  dropdown_sequence: any[] = [];
  right_breadcrumb: any;
  left_breadcrumb: any;
  left_treemap_error_msg: any = {};
  right_treemap_error_msg: any = {};
  left_business_data:any;
  right_business_data:any;
  left_funciton_data:any;
  right_funciton_data:any;

  constructor(private tabroute: TabRoute, private filterS: FilterService, private router: Router, private cd: ChangeDetectorRef,  private _snackBar: MatSnackBar, public imageService: ImageService) { }

  ngOnInit() {
    if (this.loginStatus === null) {
      this.router.navigate(['/']);
    }
    this.startDate = '2019-04-01';
    this.leftBarDataActive = true;
    this.rightBarDataActive = true;
    this.left_breadcrumb = false;
    this.right_breadcrumb = false;
    this.endDate = this.getTodayDate();
    if (this.rightSide === 'closed') {
      setTimeout(() => this.rightSide = 'open');
    }
    this.tabroute.clearLeftTreemapGraph$.subscribe((res: any) => {
      if (res === true) {
        this.left_breadcrumb = [];
      }
    });
    /* Getting filter data */
    this.filterS.getFilterDropdownData().subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDropdownData = res.data;
        let count = 0;
        this.sidebarDropdownSelectList = [];
        for (let i of this.filterDataKeyNames) {
          const tempObj = {};
          tempObj['main_display_name'] = this.filterSelectDisplayName[count];
          tempObj['main_data'] = this.makeDropdownData(res.data[this.filterDataKeyNames[count]]);
          this.sidebarDropdownSelectList.push(tempObj);
          count++;
        }
        this.loaderactive = false;
        this.filterS.getTreemapData({"start_date":this.startDate, "end_date":this.endDate, "state":this.makeSingleQuotesObj(res.data.state_list)}).subscribe((res: any) => {
          if (res.message === 'success') {
            if (this.leftTreemapActive === false && this.rightTreemapActive === false) {
              this.leftTreemapData = res.data.performer_by_state;
              this.rightTreemapData = res.data.performer_by_state;
            }
          }
        });
        
        this.getAllDashboardData({"start_date":this.startDate, "end_date":this.endDate, "state":this.makeSingleQuotesObj(res.data.state_list)});
      }




    });
  }

  makeIndGroupBarChartData(data) {
    if (this.leftBarDataActive) {
      this.leftBarData.push(data);
    }
    if (this.rightBarDataActive) {
      this.rightBarData.push(data);
    }
    const valueArr = [];
    const tempArr = [];
    for (let val of data) {
      valueArr.push(val.value);
      const tempObj = {};
      tempObj['name'] = val.name;
      tempObj['series'] = [
        {
          'name': 'left',
          'value': ''
        },
        {
          'name': 'right',
          'value': ''
        }
      ];
      tempArr.push(tempObj);
    }
    if (this.leftBarDataActive) {
      let count = 0;
      for (let val of tempArr) {
        for (let val2 of val.series) {
          if (val2.name === 'left') {
            val2.value = valueArr[count];
            count++;
          }
        }
      }
      if (this.rightBarDataActive === false) {
        for (let val of this.rightBarData) {
          for (let val1 of val) {
            const tempName = val1.name;
            for (let val2 of tempArr) {
              if (val2.name === tempName) {
                for (let val3 of val2.series) {
                  if (val3.name === 'right') {
                    val3.value = val1.value;
                  }
                }
              }
            }
          }
        }
      }
    }
    if (this.rightBarDataActive) {
      let count = 0;
      for (let val of tempArr) {
        for (let val2 of val.series) {
          if (val2.name === 'right') {
            val2.value = valueArr[count];
            count++;
          }
        }
      }
      if (this.leftBarDataActive === false) {
        for (let val of this.leftBarData) {
          for (let val1 of val) {
            const tempName = val1.name;
            for (let val2 of tempArr) {
              if (val2.name === tempName) {
                for (let val3 of val2.series) {
                  if (val3.name === 'left') {
                    val3.value = val1.value;
                  }
                }
              }
            }
          }
        }
      }
    }
    return tempArr;
  }

  ngOnDestroy() {
    this.mainDashboardData = undefined;
    this.tabroute.setComparisonLoader(true);
  }

  getAllDashboardData(data) {
  if(this.leftBarDataActive){

    this.left_filters = {...data}
  }
  if(this.rightBarDataActive){
    this.right_filters = {...data}
  }

    
    this.mainDashboardData = [];
     
    this.filterS.getEmpLeftCount(data).subscribe((res: any) => {
      if (res.message === 'success') {
        if (this.leftBarDataActive) {
          this.left_side_emp_left =   res.data.data[0][0][0];
          this.mainDashboardData['left_side_emp_left'] =res.data.data[0][0][0];
          if (!this.rightBarDataActive) {
            this.mainDashboardData['right_side_emp_left'] = this.right_side_emp_left;
          }
        }
        if (this.rightBarDataActive) {
          this.right_side_emp_left = res.data.data[0][0][0];
          this.mainDashboardData['right_side_emp_left'] = res.data.data[0][0][0];
          if (!this.leftBarDataActive) {
            this.mainDashboardData['left_side_emp_left'] = this.left_side_emp_left;
          }
        } 
      }
    });
    this.filterS.getAttritionPercentage(data).subscribe((res: any) => {
      if (res.message === 'success') {
        if (this.leftBarDataActive) {
          this.left_side_attrition_percentage = res.data[0][0][0].toFixed(2);
          this.mainDashboardData['left_side_attrition_percentage'] = res.data[0][0][0].toFixed(2);
          if (!this.rightBarDataActive) {
            this.mainDashboardData['right_side_attrition_percentage'] = this.rigt_side_attrition_percentage;
          }
        }
        if (this.rightBarDataActive) {
          this.rigt_side_attrition_percentage =  res.data[0][0][0].toFixed(2);
          this.mainDashboardData['right_side_attrition_percentage'] =  res.data[0][0][0].toFixed(2);
          if (!this.leftBarDataActive) {
            this.mainDashboardData['left_side_attrition_percentage'] = this.left_side_attrition_percentage;
          }
        } 
      }
    })

    
    this.filterS.getAgeGroup(data).subscribe((res: any) => {
      if (res.message === 'success') {
        if (this.leftBarDataActive) {
          this.mainDashboardData['left_age_group'] = {'name': 'left_age_group', 'value': res.data.data};
        }
        if (this.rightBarDataActive) {
          this.mainDashboardData['right_age_group'] = {'name': 'right_age_group', 'value': res.data.data};
        }
      }
    });
    this.filterS.getQualificationData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['qualification'] = {'name':'qualification', 'value': this.makeIndGroupBarChartData(res.data.data)};
      }
    });
    this.filterS.getGemData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['gem'] = {'name':'gem', 'value': this.makeIndGroupBarChartData(res.data.data)};
        this.loadFilter = true;
      }
    });
    this.filterS.getStarData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['star'] = {'name':'star', 'value': this.makeIndGroupBarChartData(res.data.data)};
      }
    });
    this.filterS.getGenderData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['gender'] = {'name':'gender', 'value': this.makeIndGroupBarChartData(res.data.data)};
      }
    });
    this.filterS.getGradeData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['grade'] = {'name':'grade', 'value': this.makeIndGroupBarChartData(res.data.data)};
        this.tabroute.setGradeData(this.mainDashboardData['grade']);
        this.mainDashboardData['imgGrade'] = {'name':'imgGrade', 'value': this.makeIndGroupBarChartData(res.data.data)};
        this.tabroute.setGradeData(this.mainDashboardData['imgGrade']);
      }
    });
    this.filterS.getPipData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['pip'] = {'name':'pip', 'value': this.makeIndGroupBarChartData(res.data.data)};
      }
    });
    this.filterS.getDisData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['dis']  = {'name': 'dis', 'value': this.makeIndGroupBarChartData(res.data.data)};
      }
    });
    this.filterS.getBusinssServiceFunction(data).subscribe((res: any) => {
      if (this.leftBarDataActive) {
        this.mainDashboardData['left_business_service_data'] = {'name': 'businessservice', 'value': res.data.data};
        this.left_business_data =  {'name': 'businessservice', 'value': res.data.data};
      }
      if (!this.rightBarDataActive) {
        this.mainDashboardData['right_business_service_data']= this.right_business_data
      }

      if (this.rightBarDataActive) {
        this.mainDashboardData['right_business_service_data'] = {'name': 'businessservice', 'value': res.data.data};
        this.right_business_data = {'name': 'businessservice', 'value': res.data.data};
      }
      if (!this.leftBarDataActive) {
        this.mainDashboardData['left_business_service_data'] = this.left_business_data
      }
    });

    this.filterS.getMajorServiceFunction(data).subscribe((res: any) => {
      if (this.leftBarDataActive) {
        this.mainDashboardData['left_major_service_data'] = {'name': 'majorservice', 'value': res.data.data};
      this.left_funciton_data ={'name': 'majorservice', 'value': res.data.data}; 
      }
      if (!this.rightBarDataActive) {
        this.mainDashboardData['right_major_service_data']=this.right_funciton_data
      }
      if (this.rightBarDataActive) {
        this.mainDashboardData['right_major_service_data'] = {'name': 'majorservice', 'value': res.data.data};
      this.right_funciton_data =  {'name': 'majorservice', 'value': res.data.data};
      }
      if (!this.leftBarDataActive) {
        this.mainDashboardData['left_major_service_data'] =  this.left_funciton_data
      }

    });
    this.filterS.getTrainingData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['training'] = {'name':'training', 'value': this.makeIndGroupBarChartData(res.data.data)};
      }
    });
    this.filterS.getvoluntaryData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainDashboardData['voluntary'] = {'name':'voluntary', 'value': this.makeIndGroupBarChartData(res.data.data)};
      }
    });
    this.filterS.getManagerDeptData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        if (this.leftBarDataActive) {
          this.left_mdept_temp_data = {'name': 'left_mdept', 'value': res.data};
          this.mainDashboardData['left_mdept'] = {'name': 'left_mdept', 'value': res.data};
          if (!this.rightBarDataActive) {
            this.mainDashboardData['right_mdept'] = this.right_mdept_temp_data;
          }
        }
        if (this.rightBarDataActive) {
          this.right_mdept_temp_data = {'name': 'right_mdept', 'value': res.data};
          this.mainDashboardData['right_mdept'] = {'name': 'right_mdept', 'value': res.data};
          if (!this.leftBarDataActive) {
            this.mainDashboardData['left_mdept'] = this.left_mdept_temp_data;
          }
        }
        this.tabroute.setLeftMdept(this.mainDashboardData['left_mdept']);
        this.tabroute.setRightMdept(this.mainDashboardData['right_mdept']);
      }
    });
    this.filterS.getReportManagerData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        if (this.leftBarDataActive) {
          this.left_report_temp_data = {'name': 'left_report', 'value': res.data.data};
          this.mainDashboardData['left_report'] = {'name': 'left_report', 'value': res.data.data};
          if (!this.rightBarDataActive) {
            this.mainDashboardData['right_report'] = this.right_report_temp_data;
          }
        }
        if (this.rightBarDataActive) {
          this.right_report_temp_data = {'name': 'right_report', 'value': res.data.data};
          this.mainDashboardData['right_report'] = {'name': 'right_report', 'value': res.data.data};
          if (!this.leftBarDataActive) {
            this.mainDashboardData['left_report'] = this.left_report_temp_data;
          }
        }
        this.tabroute.setLeftReport(this.mainDashboardData['left_report']);
        this.tabroute.setRightReport(this.mainDashboardData['right_report']);
      }
    });
    this.filterS.getManagerMcaresData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        if (this.leftBarDataActive) {
          this.left_mmcares_temp_data = {'name': 'left_mmcares', 'value': res.data};
          this.mainDashboardData['left_mmcares'] = {'name': 'left_mmcares', 'value': res.data};
          if (!this.rightBarDataActive) {
            this.mainDashboardData['right_mmcares'] = this.right_mmcares_temp_data;
          }
        }
        if (this.rightBarDataActive) {
          this.right_mmcares_temp_data = {'name': 'right_mmcares', 'value': res.data};
          this.mainDashboardData['right_mmcares'] = {'name': 'right_mmcares', 'value': res.data};
          if (!this.leftBarDataActive) {
            this.mainDashboardData['left_mmcares'] = this.left_mmcares_temp_data;
          }
        }
        this.tabroute.setLeftMmcares(this.mainDashboardData['left_mmcares']);
        this.tabroute.setRightMmcares(this.mainDashboardData['right_mmcares']);
        this.tabroute.setComparisonLoader(false);
        this._snackBar.open('Data Updated Successfully', 'Close', {
          duration: 3000
        });
      }
    });
    this.filterS.getWorkContractData(data).subscribe((res: any) => {
      if (res.message === 'success') {
        if (this.leftBarDataActive) {
          this.left_work_temp_data = {'name': 'left_work', 'value': res.data.data};
          this.mainDashboardData['left_work'] = {'name': 'left_work', 'value': res.data.data};
          if (!this.rightBarDataActive) {
            this.mainDashboardData['right_work'] = this.right_work_temp_data;
          }
        }
        if (this.rightBarDataActive) {
          this.right_work_temp_data = {'name': 'right_work', 'value': res.data.data};
          this.mainDashboardData['right_work'] = {'name': 'right_work', 'value': res.data.data};
          if (!this.leftBarDataActive) {
            this.mainDashboardData['left_work'] = this.left_work_temp_data;
          }
        }
        this.tabroute.setLeftWork(this.mainDashboardData['left_work']);
        this.tabroute.setRightWork(this.mainDashboardData['right_work']);
      }
    });
    this.filterS.getFunctionalCompetency(data).subscribe((res: any) => {
      if (res.message === 'success') {
        if (this.leftBarDataActive) {
          this.left_functionalCompetency_temp_data = {'name': 'leftFunctionalCompetency', 'value': res.data};
          this.mainDashboardData['leftFunctionalCompetency'] = {'name': 'leftFunctionalCompetency', 'value': res.data};
          if (!this.rightBarDataActive) {
            this.mainDashboardData['rightFunctionalCompetency'] = this.right_functionalCompetency_temp_data;
          }
        }
        if (this.rightBarDataActive) {
          this.right_functionalCompetency_temp_data = {'name': 'rightFunctionalCompetency', 'value': res.data};
          this.mainDashboardData['rightFunctionalCompetency'] = {'name': 'rightFunctionalCompetency', 'value': res.data};
          if (!this.leftBarDataActive) {
            this.mainDashboardData['leftFunctionalCompetency'] = this.left_functionalCompetency_temp_data;
          }
        }
        this.tabroute.setLeftFunctionalCompetency(this.mainDashboardData['leftFunctionalCompetency']);
        this.tabroute.setRightFunctionalCompetency(this.mainDashboardData['rightFunctionalCompetency']);
      }
    });

    this.filterS.getBusinssServiceFunction(data).subscribe((res: any) => {
      if (this.leftBarDataActive) {
        this.mainDashboardData['left_business_service_data'] = {'name': 'businessservice', 'value': res.data.data};
        this.left_business_data =  {'name': 'businessservice', 'value': res.data.data};
      }
      if (!this.rightBarDataActive) {
        this.mainDashboardData['right_business_service_data']= this.right_business_data
      }

      if (this.rightBarDataActive) {
        this.mainDashboardData['right_business_service_data'] = {'name': 'businessservice', 'value': res.data.data};
        this.right_business_data = {'name': 'businessservice', 'value': res.data.data};
      }
      if (!this.leftBarDataActive) {
        this.mainDashboardData['left_business_service_data'] = this.left_business_data
      }
    });

    this.filterS.getMajorServiceFunction(data).subscribe((res: any) => {
      if (this.leftBarDataActive) {
        this.mainDashboardData['left_major_service_data'] = {'name': 'majorservice', 'value': res.data.data};
      this.left_funciton_data ={'name': 'majorservice', 'value': res.data.data}; 
      }
      if (!this.rightBarDataActive) {
        this.mainDashboardData['right_major_service_data']=this.right_funciton_data
      }
      if (this.rightBarDataActive) {
        this.mainDashboardData['right_major_service_data'] = {'name': 'majorservice', 'value': res.data.data};
      this.right_funciton_data =  {'name': 'majorservice', 'value': res.data.data};
      }
      if (!this.leftBarDataActive) {
        this.mainDashboardData['left_major_service_data'] =  this.left_funciton_data
      }

    });

  }

  makeDashboardAPIData(data, cur_date, last_yr, map_key, filter_key) {
    const tempObj = {};
    tempObj['start_date'] = last_yr;
    tempObj['end_date'] = cur_date;
    tempObj['state'] = data;
    tempObj['tree_map_key'] = map_key;
    tempObj['filter_key'] = filter_key;
    return tempObj;
  }

  ngOnChanges() {
    if (this.leftDropdownClick.length > 0) {
      this.listenToLeftSidebarSubmit(this.leftDropdownClick);
    }
  }

  resetDropdown() {
    for (let i = 0; i < this.multiSelect['_results'].length; i++) {
      this.multiSelect['_results'][i].selectedValue = [];
    }
    this.right_breadcrumb = [];
  }

  ngAfterViewInit() {
  }

  listenToLeftSidebarSubmit(data) {
    this.leftBarDataActive = true;
    this.leftBarData = [];
    this.rightBarDataActive = false;
    this.getAllDashboardData(data[0].mainData);
    this.left_breadcrumb = data[3].breadcrumb;
    this.filterS.getTreemapData(data[1].treemapData).subscribe((res: any) => {
      if ( res.data.performer_by_state.length > 0) {
        if (res.message === 'success') {
          if (res.data.performer_by_state.length === 1 && res.data.performer_by_state[0].percentage === 0) {
            this.leftTreemapData = [];
            this.left_treemap_error_msg['error_msg'] = {key: true, value: 'No data available for the selected sequence.'};
          } else {
            this.left_treemap_error_msg['error_msg'] = undefined;
            const tempObj2 = {};
            tempObj2['index'] = data[2].index;
            tempObj2['value'] = res.data.performer_by_state;
            this.leftTreemapData = tempObj2;
          }
        }
      } else if ( res.data.performer_by_state.length < 1) {
        this.leftTreemapData = [];
        this.left_treemap_error_msg['error_msg'] = {key: true, value: 'Invalid sequence selection!!'}
      }
    });
  }

  makeSingleQuotesObj(arr) {
    const newArr = JSON.stringify(arr).replace(/"/g, "'");
    return '' + newArr + '';
  }

  getTodayDate() {
    let today: any = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    return today;
  }

  getPrevYear() {
    let today: any = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear() - 1;
    today = yyyy + '-' + mm + '-' + dd;
    return today;
  }

  getDateByObj(val) {
    let today: any = val;
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    today = yyyy + '-' + mm + '-' + dd;
    return today;
  }

  makeDropdownData(arr) {
    const tempArr = [];
    for (let val of arr) {
      if (val !== '') {
        const tempObj = {};
        tempObj['display'] = val;
        tempObj['value'] = val;
        tempArr.push(tempObj);
      }
    }
    return tempArr;
  }

  treemapData() {
    const tempObj = {};
    tempObj['start_date'] = this.startDate;
    tempObj['end_date'] = this.endDate;
    return tempObj;
  }

  returnDiv(val) {
    if (val === 1) {
      this.rightTreemapActive = false;
      this.leftTreemapActive = true;
      this.leftBarDataActive = true;
      this.leftBarData = [];
      this.rightBarDataActive = false;
    } else if (val === 2) {
      this.leftTreemapActive = false;
      this.rightTreemapActive = true;
      this.rightBarDataActive = true;
      this.rightBarData = [];
      this.leftBarDataActive = false;
    }
  }

   /* Dropdown change function */
   getCustomStartDate(val) {
    const date = this.getDateByObj(val);
    this.startDate = date;
  }

  getCustomEndDate(val) {
    const date = this.getDateByObj(val);
    this.endDate = date;
  }

  getTypeOfDropdown(index) {
    this.dropdownActiveIndex = index;
  }

  getSelectedOptions(val) {
    this.selected = val;
    if (this.selected.length > 0) {
      if (this.dropdown_sequence.includes(this.dropdownActiveIndex) === false) {
        this.dropdown_sequence.push(this.dropdownActiveIndex);
      } else {
        const index = this.dropdown_sequence.indexOf(this.dropdownActiveIndex);
        this.dropdown_sequence.splice(index, 1);
        this.dropdown_sequence.push(this.dropdownActiveIndex);
      }
    } else if (this.selected.length < 1) {
      this.cd.detectChanges();
      if (this.dropdown_sequence.includes(this.dropdownActiveIndex)) {
        const index = this.dropdown_sequence.indexOf(this.dropdownActiveIndex);
        this.dropdown_sequence.splice(index, 1);
      }
    }
  }

  rightSidebarSubmit() {
    this.rightBarDataActive = true;
    this.rightBarData = [];
    this.leftBarDataActive = false;
    const tempObj= {};
    const mainDataObj = {};
    this.dropdownObj = {};
    this.tabroute.setComparisonLoader(true);
    for (let i = 0; i < this.dropdown_sequence.length; i++) {
      const tempVal = this.multiSelect['_results'][this.dropdown_sequence[i]].selectedValue;
      if (tempVal.length > 0) {
        const key: any = this.dropdownTypeArr[this.dropdown_sequence[i]];
        tempObj[key] = JSON.stringify(tempVal).replace(/"/g, "'");
      }
    }
    const tempSeqVal= JSON.stringify(this.dropdown_sequence);
    const tempSeqArr = JSON.parse(tempSeqVal);
    this.right_breadcrumb = {'key': true, 'val':tempSeqArr};
    if (Object.keys(tempObj).length > 0) {
      tempObj['start_date'] = this.startDate;
      tempObj['end_date'] = this.endDate;
      this.getAllDashboardData(tempObj);
      this.filterS.getTreemapData(tempObj).subscribe((res: any) => {
        if ( res.data.performer_by_state.length > 0) {
          if (res.message === 'success') {
            if (res.data.performer_by_state.length === 1 && res.data.performer_by_state[0].percentage === 0) {
              this.rightTreemapData = [];
              this.right_treemap_error_msg['error_msg'] = {key: true, value: 'No data available for the selected sequence.'};
            } else {
              this.right_treemap_error_msg['error_msg'] = undefined;
              const tempObj2 = {};
              tempObj2['index'] = this.dropdownActiveIndex;
              tempObj2['value'] = res.data.performer_by_state;
              this.rightTreemapData = tempObj2;
            }
          }
        } else if ( res.data.performer_by_state.length < 1) {
          this.rightTreemapData = [];
          this.right_treemap_error_msg['error_msg'] = {key: true, value: 'Invalid sequence selection!!'}
        }
      });
    } else {
      tempObj['start_date'] = this.startDate;
      tempObj['end_date'] = this.endDate;
      tempObj['state'] = this.mainDropdownData.state_list;
      this.right_breadcrumb = {'key': false, 'val':''};
      this.right_treemap_error_msg['error_msg'] = undefined;
      this.getAllDashboardData(tempObj);
      this.filterS.getTreemapData(tempObj).subscribe((res: any) => {
        if (res.message === 'success') {
          const tempObj2 = {};
          tempObj2['index'] = null;
          tempObj2['value'] = res.data.performer_by_state;
          this.rightTreemapData = tempObj2;
        }
      });
    }
  }

}
