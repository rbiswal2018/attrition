import { Component, OnInit, Input, AfterViewInit, OnChanges, ViewChildren, QueryList, Output, EventEmitter, SimpleChanges, ViewChild, ChangeDetectorRef } from '@angular/core';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { JsonPipe } from '@angular/common';
import { TabRoute } from '../../../common/services/tabroute.service';
import domtoimage from 'dom-to-image-more';
import { ImageService } from '.././../../common/services/image.service';
declare var $: any;
import { FilterService } from '.././../../common/services/filter.service';
import * as _ from "lodash";





@Component({
  selector: 'app-descriptive',
  templateUrl: './descriptive.component.html',
  styleUrls: ['./descriptive.component.scss']
})
export class DescriptiveComponent implements OnInit, OnChanges, AfterViewInit {

  @ViewChildren(SelectAutocompleteComponent) multiSelect: 
  QueryList<SelectAutocompleteComponent>;
  @Input() mainData: any;
  @Input() csvFilter: any;
  @Output() pieRadioSwitch = new EventEmitter<number>();
  @Output() treemapData = new EventEmitter<object>();
  @Output() breadcrumb = new EventEmitter<object>();
  @ViewChild('qual', {static: false}) qual_table;
  @ViewChild('mdept', {static: false}) mdept;
  @ViewChild('report', {static: false}) report;
  @ViewChild('work', {static: false}) work;
  @ViewChild('mmcares', {static: false}) mmcares;
  @ViewChild('functionalCompetency', {static: false}) functionalCompetency;
  selectedOptions = [0, 1, 2];
  mcaresManagerCofactor:any;
  mcaresDepartmentCofactor:any;
  selected = this.selectedOptions;
  showHiring = true;
  mainTempData:any;
  showHiringCategory =false
  showVolInvol = true
  
  showvolInvolCategory =false
  showBusinessService = true
  showBusinessServiceCategory =false
  showMajorFunctionService = true
  showMajorFunctionServiceCategory =false
  showAgeGraph: any = true;
  showRewardGraph: any = true;
  showGenderGraph: any = true;
  showGradeGraph: any = false;
  showPipGraph: any = false;
  rating_toggle : any =  false;
  compensation_toggle:any= false;
  experience_toggle:any = false;
  recruitement_toggle:any=false;
  age_group_toggle:any = false;
  gem_performe_toggle:any = false;
  star_performer_toggle:any=false;
  gender_toggle:any= false;
  employee_sub_group_toggle:any =false
  pip_toggle:any =false
  dis_toggle:any=false
  marital_status_toggle:any=false
  training_data_toggle:any=false
  higher_qualification_category_toggle:any=false
  business_service_function_cat_toggle:any=false
  major_service_function_cat_toggle:any=false
  recruitement_toggle_category:any=false
  business_service_function_toggle:any=false
  majorfunction_service_name:any
  business_service_name:any
  toggle_unsubscibe :any
  major_service_function_toggle:any = false
  recruitement_chart_name:any
  showMaritalGraph: any = false;
  showTrainingGraph: any = false;
  showVoluntaryGraph: any = false;
  showWorkContractGraph: any = false;
  showQualificationData: any = false;
  showMcaresDept: any = false;
  showReport: any = false;
  showMcaresmanager: any = false;
  showFunctionalCompetency: any = false;
  showBusinessServiceFunction: any = false;
  showMajorFunction: any = false;
  table_no_data_available: any = false;
  ratingFilterVal: any = 1;
  curTreemapStatus: any;
  toggleDashboardData:any = {}
  showCohertToggle = false
  hideCohertToggle = true
  public showpip = true;
  public showdis = false;
  public hideFilterLoader: boolean = true;
  selected_toggle:any ;
  factorOptions = [
    {
      display: 'Age group',
      value: 0
    }, {
      display: 'Rewards',
      value: 1
    }, {
      display: 'Gender',
      value: 2
    }, {
      display: 'Grade',
      value: 3
    }, {
      display: 'PIP/DIS',
      value: 4
    }, {
      display: 'Maritial status',
      value: 5
    }, {
      display: 'Training',
      value: 6
    }, {
      display: 'Voluntary / Involuntary',
      value: 7
    },{
      display: 'Business / Service Function',
      value: 8
    }, {
      display: 'Major Function',
      value: 9
    },{
      display: 'Work contract',
      value: 10
    }, {
      display: 'Mcares department',
      value: 12
    }, {
      display: 'Reporting manager',
      value: 13
    }, {
      display: 'Highest qualification',
      value: 11
    }, {
      display: 'Mcares manager',
      value: 14
    }, {
      display: 'Functional Competency',
      value: 15
    }
  ];

  dropdownTypeArr = ['state', 'branch', 'function_name', 'division', 'business_unit', 'department', 
  'sub_department', 'cost_centre', 'work_contract', 'band', 'employee_sub_group'];
  
  constructor(public tabroute: TabRoute,private filterS: FilterService, private changeDetector : ChangeDetectorRef, public imageService: ImageService) {
  }

  downloadAsImage(node, val, obj) {
    this.imageService.makeTable(node, val, obj);
  }

  convertToCSV(arr) {
    const array = [Object.keys(arr[0])].concat(arr);
    return array.map(it => {
      return Object.values(it).toString()
    }).join('\n')
  }


  
  exportToCsv(filename, arr, obj) {
    if (obj !== '') {
      let tempArr = [];
      for (let i = 0; i < arr.length; i++) {
        let tempObj = {};
        tempObj[obj[0]] = arr[i].name;
        tempObj[obj[1]] = arr[i].value;
        tempArr.push(tempObj);
      }
      arr = tempArr;
    }
    let processRow = this.convertToCSV(arr);
    var blob = new Blob([processRow], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename+".csv");
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
  }


  getSelectedOptions(selected) {
    this.showAgeGraph = false;
    this.showRewardGraph = false;
    this.showGenderGraph = false;
    this.showGradeGraph = false;
    this.showPipGraph = false;
    this.showMaritalGraph = false;
    this.showTrainingGraph = false;
    this.showVoluntaryGraph = false;
    this.showWorkContractGraph= false;
    this.showQualificationData = false;
    this.showMcaresDept = false;
    this.showReport = false;
    this.showMcaresmanager = false;
    this.showBusinessServiceFunction = false;
    this.showMajorFunction = false;
    this.showFunctionalCompetency = false;
    this.selected = selected;
    for (let option of this.selected) {
      this.displaySelectedFilterGraph(option);
    }
  }

  displaySelectedFilterGraph(val) {
    switch(val) {
      case 0:
      this.showAgeGraph = true;
      break;
      case 1:
      this.showRewardGraph = true;
      break;
      case 2:
      this.showGenderGraph = true;
      break;
      case 3:
      this.showGradeGraph = true;
      break;
      case 4:
      this.showPipGraph = true;
      break;
      case 5:
      this.showMaritalGraph = true;
      break;
      case 6:
      this.showTrainingGraph = true;
      break;
      case 7:
      this.showVoluntaryGraph = true;
      break;
      case 8:
      this.showBusinessServiceFunction = true;
      break;
      case 9:
      this.showMajorFunction = true;
      break;
      case 10:
      this.showWorkContractGraph = true;
      this.changeDetector.detectChanges();
      this.fetchUserData(this.mainData.work_contract.value, this.work);
      break;
      case 11:
      this.showQualificationData = true;
      break;
      case 12:
      this.showMcaresDept = true;
      this.changeDetector.detectChanges();
      this.fetchUserData(this.mainData.mdept.value, this.mdept);
      break;
      case 13:
      this.showReport = true;
      this.changeDetector.detectChanges();
      this.fetchUserData(this.mainData.report.value, this.report);
      break;
      case 14:
      this.showMcaresmanager = true;
      this.changeDetector.detectChanges();
      this.fetchUserData(this.mainData.mmcares.value, this.mmcares);
      break;
      case 15:
      this.showFunctionalCompetency = true;
      this.changeDetector.detectChanges();
      this.fetchUserData(this.mainData.functionalCompetency.value, this.functionalCompetency);
      break;
    }
  
    
  }

  plotTable(data, dataTable, cols) {
    if ( $.fn.DataTable.isDataTable(dataTable) ) {
      $(dataTable).DataTable().destroy();
    }
    dataTable.DataTable({
      data: data,
      retrieve: true,
      "scrollY": "300px",
      columns: cols,
      "scrollX": true,
    });
  }

  fetchUserData(val, table) {
    const tableDataArr = [];
    const cols = [];
    const colsData = [];
    if (val.length > 0) {
      for (const data of Object.keys(val[0])) {
          colsData.push({'title': data});
          cols.push(data);
      }
      for (const data of val) {
        const tempTableArr = [];
        for (const col of cols) {
          tempTableArr.push(data[col]);
        }
        tableDataArr.push(tempTableArr);
      }
      const dataTable = $(table.nativeElement);
      this.plotTable(tableDataArr, dataTable, colsData);
    } else  {
      const dataTable = $(table.nativeElement);
      if ( $.fn.DataTable.isDataTable(dataTable) ) {
        $(dataTable).DataTable().destroy();
        $(dataTable).DataTable().clear().draw();
      }
    }
  }

  
  ratingFilter() {
    this.rating_toggle= 0;
    this.sendRadioSwitch(this.ratingFilterVal);
  }

  sendRadioSwitch(val) {
    this.pieRadioSwitch.emit(val);
  }

  pipordis(val) {
    if (val) {
      this.showdis = true;
      this.showpip = false;
    } else {
      this.showpip = true;
      this.showdis = false;      
    }
    this.pip_toggle=0
  this.dis_toggle=0
    this.mainData["pip_data"]={"name":"pip","value":this.mainTempData['pip_data'].value}
    this.mainData["dis_data"]={"name":"dis","value":this.mainTempData['dis_data'].value}


  }

toggle_contribution(val,chart_name){

if(!val&&chart_name=="total_experience_months_in_mf_range"){
this.mainData["cohert_data"]={"name":"cohert","value":this.mainTempData["cohert_data"].value}

}
else if(!val&&chart_name=="annual_compensation_range"){
  this.mainData["compensate_data"]={"name":"compensate","value":this.mainTempData["compensate_data"].value}
  }
  else if(!val&&chart_name=="recruitement_data_category"){
    this.mainData["hiring_data"]={"name":"hiring","value":this.mainTempData["hiring_data"].value}
    }
    else if(!val&&chart_name=="age_group"){
  this.mainData["age_group_data"]={"name":"agegroup","value":this.mainTempData["age_group_data"].value}
  }
  else if(!val&&chart_name=="gem_performe"){
    this.mainData["rewards_gem_data"]={"name":"gem","value":this.mainTempData["rewards_gem_data"].value}
    }
    else if(!val&&chart_name=="star_performer"){
      this.mainData["rewards_star_data"]={"name":"star","value":this.mainTempData["rewards_star_data"].value}
      }
      if(!val&&chart_name=="gender"){
        this.mainData["gender_data"]={"name":"gender","value":this.mainTempData["gender_data"].value}
        }
        else if(!val&&chart_name=="employee_sub_group"){
          this.mainData["grade_data"]={"name":"grade","value":this.mainTempData["grade_data"].value}
          }
          else if(!val&&chart_name=="marital_status"){
            this.mainData["marital_data"]={"name":"marital","value":this.mainTempData["marital_data"].value}
            }
            else if(!val&&chart_name=="training_data"){
              this.mainData["training_data"]={"name":"training","value":this.mainTempData["training_data"].value}
              }
              else if(!val&&chart_name=="higher_qualification_category"){
                this.mainData["qualification"]={"name":"qualification","value":this.mainTempData["qualification"].value}
                }

                else if(!val&&chart_name=='business_service_function_cat'){
                  this.mainData["businsess_service_function"]={"name":"businessservice","value":this.mainTempData['businsess_service_function'].value}
                }
                else if(!val&&chart_name=='major_service_function_cat'){
                  // console.log(this.mainTempData)
                  this.mainData["major_service_function"]={"name":"majorservice","value":this.mainTempData['major_service_function'].value}
                }
                else if(!val&&chart_name=='pip_data'){
                  this.mainData["pip_data"]={"name":"pip","value":this.mainTempData['pip_data'].value}
                }
                else if(!val&&chart_name=='dic_data'){
                  this.mainData["dis_data"]={"name":"dis","value":this.mainTempData['dis_data'].value}
                }
                else if(!val&&chart_name=='rating'){
                  this.mainData["rating_data"]={"name":"rating","value":this.mainTempData['rating_data'].value}
                }
                else if(!val&&chart_name=='recruitement_data'){
                  this.recruitement_toggle_category=0
                  this.mainData["hiring_data_category"]={"name":"hiring_category","value":this.mainTempData['hiring_data_category'].value}
                }
                else if(!val&&chart_name=='business_service_function'){
                  this.business_service_function_cat_toggle =0

                  this.mainData["businessservice_category"]= {"name":"businessservice_category","value":this.mainTempData['businessservice_category'].value}
          
                  
                }
                else if(!val&&chart_name=='major_function'){
                  this.major_service_function_cat_toggle = 0

                  this.mainData['majorfunction_category'] = { name: 'majorfunction_category',"value":this.mainTempData['majorfunction_category'].value}
          
                }

if(val){ 
 if(chart_name =='rating'){
  if (this.ratingFilterVal=='1'){
    this.csvFilter['chart_name'] = "average_rating"
                                }
  else{
    this.csvFilter['chart_name'] = "previous_year_rating"
     }
                         }   
else{

  this.csvFilter['chart_name'] = chart_name

}
const temp_filter = {...this.csvFilter}
// console.log(chart_name)
if(chart_name=='recruitement_data'){

  temp_filter['recruitement_data_category'] = [this.recruitement_chart_name]

}

if(chart_name=='business_service_function'){

  temp_filter["business_service_function_cat"]= [this.business_service_name]

}
if(chart_name=='major_function'){

  temp_filter["major_service_function_cat"]= [this.majorfunction_service_name]

}



if(chart_name == 'major_service_function_cat'){
       temp_filter['major_service_function_cat'] = ['Major Function']
}
if(chart_name == 'business_service_function_cat'){
  temp_filter['business_service_function_cat'] = ['Service Function','Business Function']
}
if(_.isEmpty(this.mainTempData)){

this.mainTempData ={...this.mainData}
}
this.toggle_unsubscibe  = this.filterS.getEmployeeContributionToggleData(temp_filter).subscribe(x => {
if(chart_name=='total_experience_months_in_mf_range'&&val){
 this.mainData["cohert_data"] = {"name":"cohert","value":x["data"] }
  }
  else if(chart_name=='annual_compensation_range'&&val){
    this.mainData['compensate_data'] = { name: 'compensate', value: x["data"]};
    }
    else if(chart_name=='recruitement_data_category'&&val){
      this.mainData['hiring_data'] = { name:'hiring_con', value: x["data"]};
      }
      else if(chart_name=='age_group'&&val){
        this.mainData["age_group_data"]={"name":"agegroup","value":x['data']}
      }
      else if(chart_name=='gem_performe'&&val){
        this.mainData["rewards_gem_data"]={"name":"gem","value":x['data']}
      }
      else if(chart_name=='star_performer'&&val){
        this.mainData["rewards_star_data"]={"name":"star","value":x['data']}
      }
      else if(chart_name=='gender'&&val){
        this.mainData["gender_data"]={"name":"gender","value":x['data']}
      }
      else if(chart_name=='employee_sub_group'&&val){
        this.mainData["grade_data"]={"name":"grade","value":x['data']}
      }
      else if(chart_name=='marital_status'&&val){
        this.mainData["marital_data"]={"name":"marital","value":x['data']}
      }
      else if(chart_name=='training_data'&&val){
        this.mainData["training_data"]={"name":"training","value":x['data']}
      }
      else if(chart_name=='higher_qualification_category'&&val){
        this.mainData["qualification"]={"name":"qualification","value":x['data']}
      }
      else if(chart_name=='business_service_function_cat'&&val){
        this.mainData["businsess_service_function"]={"name":"businessservice_toggle","value":x['data']}
      }
      else if(chart_name=='major_service_function_cat'&&val){
        this.mainData["major_service_function"]={"name":"majorservice_toggle","value":x['data']}
      }
      else if(chart_name=='pip_data'){
        this.mainData["pip_data"]={"name":"pip","value":x['data']}
      }
      else if(chart_name=='dic_data'){
        this.mainData["dis_data"]={"name":"dis","value":x['data']}
      }
      else if(chart_name=='rating'){
        this.mainData["rating_data"]={"name":"rating","value":x['data']}
      }
      else if(chart_name=='recruitement_data'){
        this.mainData["hiring_data_category"]={"name":"hiring_category","value":x['data']}
      }
      
      else if(chart_name=='business_service_function'){

        this.mainData["businessservice_category"]= {"name":"businessservice_category","value":x['data']}

      }

      else if(chart_name=='major_function'){

        this.mainData['majorfunction_category'] = { name: 'majorfunction_category',"value":x['data']}

      }
})
}
  }


graphBarClick(event){
  if(event["name"]=='hiring'){
        this.showHiringCategory = true
        this.showHiring = false
        this.recruitement_toggle = 0

        this.mainData['hiring_data_category'] = {}
        const filter = {...this.csvFilter}
        delete filter['chart_name']
        this.recruitement_chart_name = event["data"]["name"]
        filter["recruitement_data_category"] =[event["data"]["name"]]
        if(_.isEmpty(this.mainTempData)){

          this.mainTempData ={...this.mainData}
          }
        this.filterS.getHiringData(filter).subscribe((res: any) => {
          if (res.message === 'success') {
            this.mainData['hiring_data_category'] = { name: 'hiring_category', value: res.data.data };
            this.mainTempData.hiring_data_category = { name: 'hiring_category', value: res.data.data };
          }
        });
  }
  if(event["name"]=='voluntary'){
    this.showvolInvolCategory = true
    this.showVolInvol = false

    this.mainData['vol_invol_category'] = {name:"",value:[]}
    const filter = {...this.csvFilter}
    delete filter['chart_name']
    filter["reason_of_leaving_group"] =[event["data"]["name"]]
    if(_.isEmpty(this.mainTempData)){

      this.mainTempData ={...this.mainData}
      }
    this.filterS.getvoluntaryData(filter).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainData['vol_invol_category'] = { name: 'voluntary_category', value: res.data.data };
      }
    });
  }
  if(event["name"]=='businessservice'){
    this.business_service_function_cat_toggle=0

    this.business_service_function_toggle = 0
     this.showBusinessService = false
    this.showBusinessServiceCategory =true

    this.mainData['businessservice_category'] = {name:"",value:[]}
    const filter = {...this.csvFilter}
    delete filter['chart_name']
    filter["business_service_function_cat"] =[event["data"]["name"]]
    this.business_service_name=event["data"]["name"]
    if(_.isEmpty(this.mainTempData)){

      this.mainTempData ={...this.mainData}
      }

    this.filterS.getBusinssServiceFunction(filter).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainData['businessservice_category'] = { name: 'businessservice_category', value: res.data.data };
        this.mainTempData.businessservice_category = { name: 'businessservice_category', value: res.data.data };
    //  console.log(this.mainTempData)
      }
    });
  }

  if(event["name"]=='majorservice'){
    this.major_service_function_cat_toggle=0   
    this.major_service_function_toggle=0
    this.showMajorFunctionService = false
    this.showMajorFunctionServiceCategory =true

    this.mainData['majorfunction_category'] = {name:"",value:[]}
    const filter = {...this.csvFilter}
    delete filter['chart_name']
    filter["major_service_function_cat"] =[event["data"]["name"]]
    this.majorfunction_service_name=event["data"]["name"]
    if(_.isEmpty(this.mainTempData)){

      this.mainTempData ={...this.mainData}
      }
    this.filterS.getMajorServiceFunction(filter).subscribe((res: any) => {
      if (res.message === 'success') {
        this.mainData['majorfunction_category'] = { name: 'majorfunction_category', value: res.data.data };
        this.mainTempData['majorfunction_category'] = { name: 'majorfunction_category', value: res.data.data };

      }
    });

  }
  
}
backVolInvoluntary(){
  this.showVolInvol = true
  this.showvolInvolCategory = false

}
  backHiringChannel(){
    this.showHiringCategory = false
    this.showHiring = true
  }
  backBusinessServiceFunction(){
    this.showBusinessServiceCategory =false
    this.showBusinessService = true
  }
  backMajorFunctionService(){
    this.showMajorFunctionService = true
    this.showMajorFunctionServiceCategory =false
  }
  


    


  ngOnInit() {
    // console.log("ssss")
    // $(function () {
    //   $('[data-toggle="tooltip"]').tooltip()
    // })
    $('[data-toggle="tooltip"]').tooltip();

    this.tabroute.descMdept$.subscribe((res) => {
      if (this.mdept !== undefined) {
        this.fetchUserData(res['value'], this.mdept);
      }
    });
    this.tabroute.descReport$.subscribe((res) => {
      if (this.report !== undefined) {
        this.fetchUserData(res['value'], this.report);
      }
    });
    this.tabroute.descWork$.subscribe((res) => {
      if (this.work !== undefined) {
        this.fetchUserData(res['value'], this.work);
      }
    });
    this.tabroute.descMmcares$.subscribe((res) => {
      if (this.mmcares !== undefined) {
        this.fetchUserData(res['value'], this.mmcares);
      }
    });
    this.tabroute.descFunctionalCompetency$.subscribe((res: any) => {
      if (res.value !== undefined) {
        if (res.value.length < 1) {
          this.table_no_data_available = true;
        } else {
          this.table_no_data_available = false;
        }
      }
      if (this.functionalCompetency !== undefined) {
        this.fetchUserData(res['value'], this.functionalCompetency);
      }
    });
    this.tabroute.dloader$.subscribe((res: any) => {
      if (res === false) {
        this.hideFilterLoader = res;
      } else {
        this.hideFilterLoader = true;
      }
    });
  }

 get_employee_csv(chart_name){

  if(chart_name =='rating'){
    if (this.ratingFilterVal=='1'){
      this.csvFilter['chart_name'] = "average_rating"
                                  }
    else{
      this.csvFilter['chart_name'] = "previous_year_rating"
       }
                           }   
  else{
    this.csvFilter['chart_name'] = chart_name
  }
const temp_filter = {...this.csvFilter}
  if(chart_name == 'major_service_function_cat'){
    temp_filter['major_service_function_cat'] = ['Major Function']
}
if(chart_name == 'business_service_function_cat'){
temp_filter['business_service_function_cat'] = ['Service Function','Business Function']
}
    this.filterS.getEmployeeCsvChartData(temp_filter).subscribe(x => {
      var blob = new Blob([x], { type: "application/xlsx" });
      const url = window.URL.createObjectURL(blob);
      let a = document.createElement('a');
      a.href = url;
      if(chart_name == 'reason_of_leaving_group'){
        chart_name = "vol_involuntary"
      }
      a.download = "Empdetails_"+chart_name+'.xlsx';// you can take a custom name as well as provide by server
  
      // start download
      a.click();
  // after certain amount of time remove this object!!!
    
  })
}


get_contribution_csv(chart_name){

  if(chart_name =='rating'){
    if (this.ratingFilterVal=='1'){
      this.csvFilter['chart_name'] = "average_rating"
                                  }
    else{
      this.csvFilter['chart_name'] = "previous_year_rating"
       }
                           }   
  else{
    this.csvFilter['chart_name'] = chart_name
  }
  const temp_filter = {...this.csvFilter}

  if(chart_name == 'major_service_function_cat'){
         temp_filter['major_service_function_cat'] = ['Major Function']
  }
  if(chart_name == 'business_service_function_cat'){
    temp_filter['business_service_function_cat'] = ['Service Function','Business Function']
  }
  this.filterS.getEmployeeContributionChartData(temp_filter).subscribe(x => {
      var blob = new Blob([x], { type: "application/xlsx" });
      const url = window.URL.createObjectURL(blob);
      let a = document.createElement('a');
      a.href = url;
    
      a.download = "Contribution_"+chart_name+'.xlsx';// you can take a custom name as well as provide by server
  
      // start download
      a.click();
  // after certain amount of time remove this object!!!
    
  })
}

get_consolidate_csv(chart_name){

  if(chart_name =='rating'){
    if (this.ratingFilterVal=='1'){
      this.csvFilter['chart_name'] = "average_rating"
                                  }
    else{
      this.csvFilter['chart_name'] = "previous_year_rating"
       }
                           }   
  else{
    this.csvFilter['chart_name'] = chart_name
  }
  const temp_filter = {...this.csvFilter}

  if(chart_name == 'major_service_function_cat'){
         temp_filter['major_service_function_cat'] = ['Major Function']
  }
  if(chart_name == 'business_service_function_cat'){
    temp_filter['business_service_function_cat'] = ['Service Function','Business Function']
  }
  this.filterS.getEmployeeConsolidateCsvChartData(temp_filter).subscribe(x => {
      var blob = new Blob([x], { type: "application/xlsx" });
      const url = window.URL.createObjectURL(blob);
      let a = document.createElement('a');
      a.href = url;
      if(chart_name == 'reason_of_leaving_group'){
        chart_name = "vol_involuntary"
      }
      a.download = "Consolidated_"+chart_name+'.xlsx';// you can take a custom name as well as provide by server
  
      // start download
      a.click();
  // after certain amount of time remove this object!!!
    
  })
}


  ngAfterViewInit() {
   
  }

  ngOnChanges(changes:SimpleChanges) {
    this.changeDetector.detectChanges();
    this.showHiringCategory =false
    this.showHiring = true
    this.showVolInvol = true
    this.showvolInvolCategory = false
    this.showBusinessServiceCategory =false
    this.showBusinessService = true
    this.showMajorFunctionService = true
    this.showMajorFunctionServiceCategory =false
  this.compensation_toggle=0
  this.experience_toggle=0
  this.recruitement_toggle=0
  this.rating_toggle=0
  this.age_group_toggle=0
  this.gem_performe_toggle=0
  this. star_performer_toggle=0
  this.gender_toggle=0
  this.employee_sub_group_toggle=0
  this.pip_toggle=0
  this.dis_toggle=0
  this.marital_status_toggle=0
  this.training_data_toggle=0
  this.higher_qualification_category_toggle=0
  this.business_service_function_cat_toggle=0
  this.major_service_function_cat_toggle=0
  this.recruitement_toggle_category=0
  this.business_service_function_toggle =0
this.major_service_function_toggle = 0

  this.mainTempData ={}


//  if(changes.mainData){
//   if (changes.mainData.currentValue != undefined) {
//    console.log("sss")
//    console.log(changes.mainData.currentValue)
//    this.mainTempData = _.cloneDeep(changes.mainData.currentValue)
//   }
//  }
    if (this.mainData !== undefined) {
      this.ratingFilterVal=1
    }
  }
  
}
