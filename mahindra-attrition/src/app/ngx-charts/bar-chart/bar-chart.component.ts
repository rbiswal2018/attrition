import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
declare var $: any;
import * as d3 from 'd3';
import * as _ from 'lodash';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit, OnChanges {

  @Output() emitGraphData = new EventEmitter<Object>();
  @Input() mainGraphData: any;
  xaxislabel: any;
  yaxislabel: any;
  view: any;
  plotGraph: any = false;
  loaderactive: any = true;
  bin_data:any;
  temp_obj = {};
  data: any[] = [];
  mycolorfn  = [];

  onResize() {
    const width = $('.bar-chart-parent').width();
    this.view = [width, 400];
  }

  provideColor(arr) {
    for (let obj of arr) {
      const temObj: any = {};
      temObj['name'] = obj.name;
      if (obj.value <= 10) {
        temObj['value'] = "#216353";
      } else if (obj.value > 10 && obj.value <= 20) {
        temObj['value'] = "#ffe75e";
      } else if (obj.value > 20 && obj.value <= 30) {
        temObj['value'] = "#de7119";
      } else if (obj.value > 30){
        temObj['value'] = "#e31837";
      }
      this.mycolorfn.push(temObj);
    }
  }

  makeShortNumber(number) {
    if (number.includes("-")){
    let splitNumber = number.split("-");

    let first_number = parseFloat((splitNumber[0] / 100000).toFixed(1));
    let second_number = parseFloat((splitNumber[1] / 100000).toFixed(1));
    let first_output;
    let second_output;
    if (first_number >= 1) {
      first_output = first_number + "L";
    } else { 
      first_output = splitNumber[0];
    }
    if (second_number >= 1) {
      second_output = second_number + "L";
    } else {
      second_output = splitNumber[1];
    }
    return first_output + " - " + second_output;
  }
  else{

    return number

  }
}

  clickBar(event){
  

    if(this.mainGraphData.name==="hiring"){
      const val ={"data":event,"name":this.mainGraphData.name}
    this.emitGraphData.emit(val)
    }

    if( this.mainGraphData.name === 'voluntary' ){
      const val ={"data":event,"name":this.mainGraphData.name}
    this.emitGraphData.emit(val)
    }
    if( this.mainGraphData.name === 'businessservice' ){
      const val ={"data":event,"name":this.mainGraphData.name}
    this.emitGraphData.emit(val)
    }
    if( this.mainGraphData.name === 'majorservice' ){
      const val ={"data":event,"name":this.mainGraphData.name}
    this.emitGraphData.emit(val)
    }


  }
  validateBinData(data) {
    let tempArr: any = [];
    for (let val of data) {
      if (val.name !== 'None') {
        const tempObj: any = {};
        tempObj['name'] = this.makeShortNumber(val.name).toString();
        tempObj['value'] = val.value;
        tempArr.push(tempObj);
      }
    }
    return tempArr;
  }

  constructor() { }

  ngOnInit() {
    const width = $('.bar-chart-parent').width();
    this.view = [width, 400];
  }

  ngOnChanges() {
    if (this.mainGraphData !== undefined) {
      if (this.mainGraphData.name === 'compensate') {
        this.mycolorfn = [];
        this.xaxislabel = 'Annual compensation(fixed)';
        this.yaxislabel = 'Attrition percentage';
        let compensateData = this.validateBinData(this.mainGraphData.value);
        this.provideColor(compensateData);
        this.data = compensateData;
      }

      if (this.mainGraphData.name === 'cohert' ||this.mainGraphData.name === "total_experience_months_in_mf_range" ) {
        this.mycolorfn = [];
        this.xaxislabel = 'Total experience in months';
        this.yaxislabel = 'Attrition percentage';
        const cohertData = this.validateBinData(this.mainGraphData.value);
        this.provideColor(cohertData);
        this.data = cohertData;
      }
      if (this.mainGraphData.name === 'hiring' || this.mainGraphData.name === 'hiring_con' ) {
        this.mycolorfn = [];
        this.xaxislabel = 'Hiring channel';
        this.yaxislabel = 'Attrition percentage';
        const hiringData = this.mainGraphData.value;
        hiringData.forEach(element => {
          element["category"] = "hiring"
        });
        this.provideColor(hiringData);
        this.data = hiringData;
      }
      if (this.mainGraphData.name === 'hiring_category' ) {
        this.mycolorfn = [];
        this.xaxislabel = 'Hiring channel';
        this.yaxislabel = 'Attrition percentage';
        const hiringData = this.mainGraphData.value;
        hiringData.forEach(element => {
          element["category"] = "hiring"
        });
        this.provideColor(hiringData);
        this.data = hiringData;
      }


      if (this.mainGraphData.name === 'agegroup') {
        this.mycolorfn = [];
        this.xaxislabel = 'Age group';
        this.yaxislabel = 'Attrition percentage';
        const ageGroupData = this.mainGraphData.value;
        this.provideColor(ageGroupData);
        this.data = ageGroupData;
      }
      if (this.mainGraphData.name === 'gem') {
        this.mycolorfn = [];
        this.xaxislabel = 'Rewards';
        this.yaxislabel = 'Attrition percentage';
        const rewardsGemData = this.mainGraphData.value;
        this.provideColor(rewardsGemData);
        this.data = rewardsGemData;
      }
      if (this.mainGraphData.name === 'star') {
        this.mycolorfn = [];
        this.xaxislabel = 'Rewards';
        this.yaxislabel = 'Attrition percentage';
        const rewardsStarData = this.mainGraphData.value;
        this.provideColor(rewardsStarData);
        this.data = rewardsStarData;
      }
      if (this.mainGraphData.name === 'gender') {
        this.mycolorfn = [];
        this.xaxislabel = 'Gender';
        this.yaxislabel = 'Attrition percentage';
        const genderData = this.mainGraphData.value;
        this.provideColor(genderData);
        this.data = genderData;
      }
      if (this.mainGraphData.name === 'grade') {
        this.mycolorfn = [];
        this.xaxislabel = 'Grade';
        this.yaxislabel = 'Attrition percentage';
        const gradeData = this.mainGraphData.value;
        this.provideColor(gradeData);
        this.data = gradeData;
      }
      if (this.mainGraphData.name === 'pip') {
        this.mycolorfn = [];
        this.xaxislabel = 'PIP';
        this.yaxislabel = 'Attrition percentage';
        const pipData = this.mainGraphData.value;
        this.provideColor(pipData);
        this.data = pipData;
      }
      if (this.mainGraphData.name === 'dis') {
        this.mycolorfn = [];
        this.xaxislabel = 'DIS';
        this.yaxislabel = 'Attrition percentage';
        const disData = this.mainGraphData.value;
        this.provideColor(disData);
        this.data = disData;
      }
      if (this.mainGraphData.name === 'marital') {
        this.mycolorfn = [];
        this.xaxislabel = 'Maritals status';
        this.yaxislabel = 'Attrition percentage';
        const maritalData = this.mainGraphData.value;
        this.provideColor(maritalData);
        this.data = maritalData;
      }
      if (this.mainGraphData.name === 'training') {
        this.mycolorfn = [];
        this.xaxislabel = 'Training';
        this.yaxislabel = 'Attrition percentage';
        const trainingData = this.mainGraphData.value;
        this.provideColor(trainingData);
        this.data = trainingData;
      }
      if (this.mainGraphData.name === 'businessservice'||this.mainGraphData.name === 'businessservice_category'||this.mainGraphData.name === 'businessservice_toggle') {
        this.mycolorfn = [];
        this.xaxislabel = 'Business/Service Function ';
        this.yaxislabel = 'Attrition percentage';
        const business_service_data = this.mainGraphData.value;
        this.provideColor(business_service_data);

        this.data = business_service_data;
      }
      if (this.mainGraphData.name === 'majorservice'||this.mainGraphData.name === 'majorfunction_category'||this.mainGraphData.name === 'majorservice_toggle') {
        this.mycolorfn = [];
        this.xaxislabel = 'Major Function ';
        this.yaxislabel = 'Attrition percentage';
        const major_service_data = this.mainGraphData.value;
        this.provideColor(major_service_data);
        this.data = major_service_data;
      }


      if (this.mainGraphData.name === 'voluntary') {
        this.mycolorfn = [];
        this.xaxislabel = 'Voluntary / Involuntary';
        this.yaxislabel = 'Attrition percentage';
        const voluntaryData = this.mainGraphData.value;
        this.provideColor(voluntaryData);
        this.data = voluntaryData;
      }
      if (this.mainGraphData.name === 'qualification') {
        this.mycolorfn = [];
        this.xaxislabel = 'Qualification';
        this.yaxislabel = 'Attrition percentage';
        const voluntaryData = this.mainGraphData.value;
        this.provideColor(voluntaryData);
        this.data = voluntaryData;
      }
      if (this.mainGraphData.name === 'left_age_group') {
        this.mycolorfn = [];
        this.xaxislabel = 'Age group';
        this.yaxislabel = 'Attrition percentage';
        const ageGroupData = this.mainGraphData.value;
        this.provideColor(ageGroupData);
        this.data = ageGroupData;
      }
      if (this.mainGraphData.name === 'right_age_group') {
        this.mycolorfn = [];
        this.xaxislabel = 'Age group';
        this.yaxislabel = 'Attrition percentage';
        const ageGroupData = this.mainGraphData.value;
        this.provideColor(ageGroupData);
        this.data = ageGroupData;
      }
      this.plotGraph = true;
      this.loaderactive = false;
    }
  }

}
