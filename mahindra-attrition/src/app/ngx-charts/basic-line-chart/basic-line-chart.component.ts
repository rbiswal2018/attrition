import { Component, OnInit, Input, AfterViewInit, OnChanges } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-basic-line-chart',
  templateUrl: './basic-line-chart.component.html',
  styleUrls: ['./basic-line-chart.component.scss']
})
export class BasicLineChartComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() mainGraphData: any;
  xaxislabel: any;
  yaxislabel: any = 'Attrition percentage';
  view: any;
  plotGraph: any = false;
  loaderactive: any = true;
  data: any = [
    {
      name: 'Attrition trend',
      series: ''
    }
  ];

  onResize() {
    const width = $('#basic-line-chart-parent').width();
    this.view = [width, 300];
  }

  plotBasicLineChart(val) {
    this.data = [];
    const tempObj: any = {};
    // console.log(this.mainGraphData)
    for (let val of this.mainGraphData.data) {
      val.name = val.name.toString();
    }
    if (this.mainGraphData.axis_key.toLowerCase() === 'year') {
      tempObj['name'] = 'Year';
      this.xaxislabel = 'Year';
    } else if (this.mainGraphData.axis_key.toLowerCase() === 'month') {
      tempObj['name'] = 'Month';
      this.xaxislabel = 'Month';
    } else if (this.mainGraphData.axis_key.toLowerCase() === 'day') {
      tempObj['name'] = 'Day';
      this.xaxislabel = 'Day';
    }
    tempObj['series'] = this.mainGraphData.data;
    this.data.push(tempObj);
    // console.log(this.data)
  }

  constructor() { }

  ngOnInit() {
    const width = $('#basic-line-chart-parent').width();
    this.view = [width, 300];
  }

  ngAfterViewInit() {
  }

  ngOnChanges() {
    if (this.mainGraphData !== undefined) {
      this.plotBasicLineChart(this.mainGraphData);
      this.plotGraph = true;
      this.loaderactive = false;
    }
  }

}
