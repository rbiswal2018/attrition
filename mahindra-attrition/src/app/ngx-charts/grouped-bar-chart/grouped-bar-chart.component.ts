import { Component, OnInit, OnChanges, Input } from '@angular/core';
declare var $: any;
import * as d3 from 'd3';
import * as _ from 'lodash';

@Component({
  selector: 'app-grouped-bar-chart',
  templateUrl: './grouped-bar-chart.component.html',
  styleUrls: ['./grouped-bar-chart.component.scss']
})
export class GroupedBarChartComponent implements OnInit, OnChanges {
  @Input() mainGraphData: any;
  xaxislabel: any;
  yaxislabel: any;
  view: any;
  plotGraph: any = false;
  view2: any;
  plotGraph2: any = false;
  loaderactive: any = true;
  bin_data:any;
  temp_obj = {};
  data: any[] = [];
  data2: any[] = [];
  mycolorfn  = [];
  scheme: any =  {
    domain: ['#5AA454', '#C7B42C', '#AAAAAA']
  };

  onResize() {
    const width = $('.grouped-bar-chart-parent').width();
    this.view = [width, 400];
  }

  constructor() { }

  ngOnInit() {
    const width = $('.grouped-bar-chart-parent').width();
    this.view = [width, 400];
  }

  provideColor(arr) {
    for (let val of arr) {
      for (let obj of val.series) {
        const temObj: any = {};
        temObj['name'] = obj.name;
        if (obj.value <= 10) {
          temObj['value'] = "#216353";
        } else if (obj.value > 10 && obj.value <= 20) {
          temObj['value'] = "#ffe75e";
        } else if (obj.value > 20 && obj.value <= 30) {
          temObj['value'] = "#de7119";
        }else {
          temObj['value'] = "#e31837";
        }
        this.mycolorfn.push(temObj);
      }
    }
  }

  ngOnChanges() {
    if (this.mainGraphData !== undefined) {
      if (this.mainGraphData.name === 'hiring') {
        this.xaxislabel = 'Hiring channel';
        this.yaxislabel = 'Attrition percentage';
        const hiringData = this.mainGraphData.value;
        this.data = hiringData;
      }
      if (this.mainGraphData.name === 'agegroup') {
        this.xaxislabel = 'Age group';
        this.yaxislabel = 'Attrition percentage';
        const ageGroupData = this.mainGraphData.value;
        this.data = ageGroupData;
      }
      if (this.mainGraphData.name === 'gem') {
        this.xaxislabel = 'Rewards';
        this.yaxislabel = 'Attrition percentage';
        const rewardsGemData = this.mainGraphData.value;
        this.data = rewardsGemData;
      }
      if (this.mainGraphData.name === 'star') {
        this.xaxislabel = 'Rewards';
        this.yaxislabel = 'Attrition percentage';
        const rewardsStarData = this.mainGraphData.value;
        this.data = rewardsStarData;
      }
      if (this.mainGraphData.name === 'gender') {
        this.xaxislabel = 'Gender';
        this.yaxislabel = 'Attrition percentage';
        const genderData = this.mainGraphData.value;
        this.data = genderData;
      }
      if (this.mainGraphData.name === 'imgGrade') {
        this.xaxislabel = 'Grade';
        this.yaxislabel = 'Attrition percentage';
        const gradeData = this.mainGraphData.value;
        this.view2 = [800, 600];
        this.data2 = gradeData;
      }
      if (this.mainGraphData.name === 'grade') {
        this.xaxislabel = 'Grade';
        this.yaxislabel = 'Attrition percentage';
        const gradeData = this.mainGraphData.value;
        this.data = gradeData;
      }
      if (this.mainGraphData.name === 'pip') {
        this.xaxislabel = 'PIP';
        this.yaxislabel = 'Attrition percentage';
        const pipData = this.mainGraphData.value;
        this.data = pipData;
      }
      if (this.mainGraphData.name === 'dis') {
        this.xaxislabel = 'DIS';
        this.yaxislabel = 'Attrition percentage';
        const disData = this.mainGraphData.value;
        this.data = disData;
      }
      if (this.mainGraphData.name === 'marital') {
        this.xaxislabel = 'Maritals status';
        this.yaxislabel = 'Attrition percentage';
        const maritalData = this.mainGraphData.value;
        this.data = maritalData;
      }
      if (this.mainGraphData.name === 'training') {
        this.xaxislabel = 'Training';
        this.yaxislabel = 'Attrition percentage';
        const trainingData = this.mainGraphData.value;
        this.data = trainingData;
      }
      if (this.mainGraphData.name === 'qualification') {
        this.xaxislabel = 'Qualification';
        this.yaxislabel = 'Attrition percentage';
        const trainingData = this.mainGraphData.value;
        this.data = trainingData;
      }
      if (this.mainGraphData.name === 'voluntary') {
        this.xaxislabel = 'Voluntary / Involuntary';
        this.yaxislabel = 'Attrition percentage';
        const voluntaryData = this.mainGraphData.value;
        this.data = voluntaryData;
      }
      if (this.mainGraphData.name === 'work') {
        this.xaxislabel = 'Work contract';
        this.yaxislabel = 'Attrition percentage';
        const workData = this.mainGraphData.value;
        this.data = workData;
      }
      this.plotGraph = true;
      this.loaderactive = false;
    }
  }

}
