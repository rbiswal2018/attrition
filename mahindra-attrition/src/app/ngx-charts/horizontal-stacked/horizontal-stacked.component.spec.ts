import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorizontalStackedComponent } from './horizontal-stacked.component';

describe('HorizontalStackedComponent', () => {
  let component: HorizontalStackedComponent;
  let fixture: ComponentFixture<HorizontalStackedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorizontalStackedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorizontalStackedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
