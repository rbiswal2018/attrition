import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { PredictiveService } from 'src/app/common/predictive.service';
import { Utils } from 'src/app/common/shared/utility';
import * as _ from "lodash";
@Component({
  selector: 'app-horizontal-stacked',
  templateUrl: './horizontal-stacked.component.html',
  styleUrls: ['./horizontal-stacked.component.scss']
})
export class HorizontalStackedComponent implements OnInit {
  line_chart_data = []

  single: any[];
  multi = [];
  xAxisTicks:any;
  onSelect:any;

 mycolorfn = [{"name":"location_emp_rep","value":"#007bff"},{"name":"emp_lang_count","value":"#007bff"},{"name":"training_previous","value":"#007bff"},{"name":"no_of_days_previous","value":"#007bff"},{"name":"total_experience_year_in_mf","value":"#007bff"},{"name":"promotion_category","value":"#ccc"}]
  // multi = [{name: "location_emp_rep",series:[{name:"location_emp_rep", value: 0.04265576793500666}]},
  // {name: "emp_lang_count", series:[{name:"emp_lang_count",value: 0.05949753981688032}]},
  // {name: "training_previous", series:[{name:"training_previous",value: 0.06294147673399028}]},
  // {name: "no_of_days_previous", series:[{name:"no_of_days_previous",value: 0.17274848932664064}]},
  // {name: "total_experience_year_in_mf", series:[{name:"total_experience_year_in_mf",value: 0.2019636157287561}]},
  // {name: "promotion_category", series:[{name:"promotion_category",value: -0.08}]}]
  


  private elmWidth;
  @Input() data: any;
  @ViewChild('chartContainer',{static:true}) element: ElementRef;

  private currElement: HTMLElement;
  private htmlElement: HTMLElement;
//  ??xAxisTicks: any[] = [0.04,0.05,0.06,0.17,0.20, -0.08];
  view= [];
  // options
 showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  // xAxisLabel = 'Percent';
  showYAxisLabel = true;

  constructor(private api: PredictiveService,private elRef: ElementRef,private service: PredictiveService) {
    this.currElement = elRef.nativeElement
  }

  ngOnInit() {
  }

  ngOnChanges(changes): void {
    let self = this

    this.elmWidth = this.element.nativeElement.offsetWidth;
    this.htmlElement = this.element.nativeElement;
    this.elmWidth = this.element.nativeElement.offsetWidth;

    if (changes.data.currentValue != undefined) {
      console.log(self.data)

      if (Object.keys(self.data).includes("type"))
      {
        let self = this
        Utils.setContainerState(self.currElement,'loading');
        this.service.lime_explainer_graph_data(self.data["parameters"]).subscribe((res: any) => {
          
          self.data = res.data
          console.log("ss",self.data)
          self.fetch_parameters_data(self.data);
                
        })
      }
      else{
      
      Utils.setContainerState(self.currElement,'loading');
      self = this
      setTimeout(function () {
        self.fetch_parameters_data(self.data);
      }, 1000);
    }
    
      
    }
  }

  fetch_parameters_data(data) {
    
    this.line_chart_data = data
    this.build_chart()
  }

  build_chart() {

    let temp_data = []
    let temp_color = []

    this.line_chart_data.forEach(element => {
      if (element["value"]<0){
        temp_color.push({"name":element["name"],"value":"#ccc"})
      }
      else{
        temp_color.push({"name":element["name"],"value":"#007bff"})
      }
      temp_data.push({name: element["name"],series:[{name:element["name"], value: element["value"]}]})
});
this.multi = temp_data
this.mycolorfn = temp_color

    let self = this;
    // $(".parameters").html("");
    
    if (_.isEmpty(self.data)) {
      Utils.setContainerState(self.currElement, "nodata");
      return;
    };
     Utils.setContainerState(self.currElement, "done-loading");

    //create margins and dimensions
    var margin = { top: 30, right: 30, bottom: 60, left: 30 };
    var width = this.elmWidth - margin.left - margin.right-100
    var height = (440 / 1) - margin.top - margin.bottom;
    this.view = [this.elmWidth,height]

    Utils.setContainerState(this.currElement, "done-loading");

  }

}
