import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { PredictiveService } from 'src/app/common/predictive.service';
import * as d3 from 'd3';
import * as _ from "lodash";
import * as d3Tip from 'd3-tip';
import { Utils } from 'src/app/common/shared/utility';
declare var $: any
@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {

  x: any;
  auc_a_value: any
  auc_hide =true
 
  @Input() line_chart_data :any;

  @ViewChild('chartContainer',{static:true}) element: ElementRef;
  private elmWidth;
  y: any;
  private currElement: HTMLElement;
  private htmlElement: HTMLElement;
  line_chart_data_a = [];
 
  tipObject;
  constructor(private api: PredictiveService, private elRef: ElementRef,private service: PredictiveService) {
    this.currElement = this.elRef.nativeElement;
  }
  ngOnChanges(changes): void {
    
  
    let self = this
    console.log(changes.line_chart_data.currentValue)
    if (changes.line_chart_data.currentValue != undefined) {
      console.log(this.line_chart_data)
      console.log("sssssssssssssssss")

      if(this.line_chart_data.key =="model_health"){
      self.line_chart_data_a = this.line_chart_data.line_data
      self.auc_a_value = this.line_chart_data.auc_value
      this.auc_hide = true
      Utils.setContainerState(self.currElement,'loading');

      this.build_chart()

      }
      console.log(this.line_chart_data.key =="simulation")
      if(this.line_chart_data.key =="simulation"){
        Utils.setContainerState(self.currElement,'loading')
        this.service.survival_curve_data(this.line_chart_data["line_data"]).subscribe((res: any) => {
          let temp_object = {}
      
        
          self.line_chart_data_a =  res.data
          console.log(self.line_chart_data_a)
          this.auc_hide = false
          console.log("ss")
          if (_.isEmpty(self.line_chart_data_a )) {
            Utils.setContainerState(self.currElement, "nodata");
            return;
          };
          
          this.build_chart()
                
        })
      
      }

      
      // self.auc_a_value =  this.line_chart_data.Auc_A
      
    }
  
  }

  ngOnInit() {

    this.elmWidth = this.element.nativeElement.offsetWidth;
    this.htmlElement = this.element.nativeElement;

    this.tipObject = (<any>d3Tip)()
      .attr('class', 'd3-tip d3-tip-treemap')
      .offset([-10, 0])
      .html(function (d) {
        return '<p><span style = "color:red;"> ' + '</span>' + "TRUE_POS :" + d["true_pos"] + '</p>' +
          '<p><span style = "color:red;"> ' + '</span>' + "FALSE_POS :" + d["false_pos"] + '</p>';
      }
      );

    // this.fetch_line_chart_data()
  }
  // fetch_line_chart_data() {
  //   let self = this
  //   this.api.get_line_chart_data().subscribe((res: any) => {
  //     self.line_chart_data_a = res.data_A
  //     if (_.isEmpty(self.line_chart_data_a )) {
  //       // Utils.setContainerState(self.currElement, "nodata");
  //       return;
  //     };
  //     // Utils.setContainerState(self.currElement, "done-loading");
  //     self.auc_a_value = res.Auc_A
  
  //     self.build_chart()
  //   });

  // }

  build_chart() {
    console.log("ssss" )
    d3.select(".line_chart").html("")
    

    var margin = { top: 50, right: 50, bottom: 50, left: 50 },
      width = this.elmWidth - margin.left - margin.right,
      height = 520 - margin.top - margin.bottom;

      if(this.line_chart_data.key =="simulation"){
        this.x = d3.scaleLinear().range([0, width]).domain([0,  d3.max(this.line_chart_data_a, function(d) { return +d.false_pos_A; })]);
        this.y = d3.scaleLinear().range([height, 0]).domain([d3.min(this.line_chart_data_a, function(d) { return d.true_pos_A; }), 1]);
      }
      if(this.line_chart_data.key =="model_health"){

    this.x = d3.scaleLinear().range([0, width]).domain([0, 1]);
    this.y = d3.scaleLinear().range([height, 0]).domain([0, 1]);
      }


    var self = this

    var parseTime = d3.timeParse("%d-%b-%y");

    // define the line
    var valueline_a = d3.line()
      .x(function (d) { return self.x(d["false_pos_A"]); })
      .y(function (d) { return self.y(d["true_pos_A"]); });

    // append the svg obgect to the body of the page
    // appends a 'group' element to 'svg'
    // moves the 'group' element to the top left margin
    var sw = width + margin.left + margin.right
    var sh = height + margin.top + margin.bottom + 10
    var svg = d3.select(".line_chart").append("svg")
      // .attr("width", width + margin.left + margin.right)
      // .attr("height", height + margin.top + margin.bottom + 10)
      .attr("viewBox",`0 0  ${sw} ${sh}`)
      .append("g")
      .attr("transform",
        "translate(" + 60 + "," + margin.top + ")");


    // Get the data
    // format the data
 
    this.line_chart_data_a.forEach(function (d) {

      d.true_pos_A = d["true_pos_A"];
      d.false_pos_A = d["false_pos_A"];
    }
    );
   

    svg.append("path")
      .data([this.line_chart_data_a])
      .attr("class", "line")
      .attr("d", valueline_a)
      .attr("fill", "none")
      .attr("stroke", "#4ed54e")
  
    // svg.call(this.tipObject);

    // Add the X Axis
    svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(this.x));

    // Add the Y Axis
    svg.append("g")
      .call(d3.axisLeft(this.y));



      if(this.line_chart_data.key == "simulation"){
        svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - margin.left)
        .attr("x", 0 - (height / 2))
        .attr("dy", "1.5em")
        .style("font-size", "13px")
        .style("font-family", "openSans_regular")
        .style("text-anchor", "middle")
        .text("Probability");
  
      svg.append("text")
        .attr("transform",
          "translate(" + (width / 2) + ", " + (height + margin.top + margin.bottom - 20) + ")")
        .style("text-anchor", "middle")
        .style("font-family", "openSans_regular")
        .attr("dy", "-3em")
        .style("font-size", "13px")
        .text("Tenure(In Years)");
         Utils.setContainerState(self.currElement, "done-loading");

  
        }
        

    if(this.line_chart_data.key == "model_health"){
      
    svg.append('line')
    .attr('id', 'baseline')
    .attr("x1", 0)
    .attr("x2", width)
    .attr("y1", height)
    .attr("y", 0)
    .attr("stroke", "black")
    .attr("opacity", "0.8")
    .attr("stroke-dasharray", "5,5");
      svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 0 - margin.left)
      .attr("x", 0 - (height / 2))
      .attr("dy", "1.5em")
      .style("font-size", "13px")
      .style("font-family", "openSans_regular")
      .style("text-anchor", "middle")
      .text("False positive");

    svg.append("text")
      .attr("transform",
        "translate(" + (width / 2) + ", " + (height + margin.top + margin.bottom - 20) + ")")
      .style("text-anchor", "middle")
      .style("font-family", "openSans_regular")
      .attr("dy", "-3em")
      .style("font-size", "13px")
      .text("True positive");
  }
        Utils.setContainerState(self.currElement, "done-loading");

}
}
