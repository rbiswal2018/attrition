import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
declare var $: any;
import * as _ from "lodash";
import { PredictiveService } from 'src/app/common/predictive.service';
import { Utils } from 'src/app/common/shared/utility';


@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.scss']
})
export class LinechartComponent implements OnInit {
  x: any;
  auc_a_value: any
  auc_hide =true
  xaxislabel: any;
  yaxislabel: any = 'Attrition percentage';
  view: any;
  plotGraph: any = false;
  loaderactive: any = true;
  onResize:any;
  data: any = [
    {
      name: 'Attrition trend',
      series: ''
     
    }
  ];
  @Input() line_chart_data :any;

  @ViewChild('chartContainer',{static:true}) element: ElementRef;
  private elmWidth;
  y: any;
  private currElement: HTMLElement;
  private htmlElement: HTMLElement;
  line_chart_data_a = [];
 
  tipObject;
  constructor(private api: PredictiveService, private elRef: ElementRef) {
    this.currElement = this.elRef.nativeElement;
  }
  ngOnChanges(changes): void {
    
  
    let self = this
    if (changes.line_chart_data.currentValue != undefined) {
      let tempObj = {}
      // console.log(this.line_chart_data)

      if(this.line_chart_data.key =="model_health"){
        this.yaxislabel = 'False positive';
        this.xaxislabel = 'True positive';
        tempObj['name'] = 'True positive';
      self.line_chart_data_a = this.line_chart_data.line_data
      // this.data = self.line_chart_data_a 

      tempObj['series'] = self.line_chart_data_a 
    

      this.data.push(tempObj);
      // console.log(this.data)

      self.auc_a_value = this.line_chart_data.auc_value
      this.auc_hide = true
      Utils.setContainerState(self.currElement,'loading');

      if (_.isEmpty(self.line_chart_data_a )) {
        Utils.setContainerState(self.currElement, "nodata");
        return;
      }
      else{
        this.build_chart()
      }
      }
      if(this.line_chart_data.key =="simulation"){
        this.yaxislabel = 'Probability';
        this.xaxislabel = 'Tenure(In Years)';
        tempObj['name'] = 'Probability';

        self.line_chart_data_a = this.line_chart_data.line_data
        tempObj['series'] = self.line_chart_data_a 
        // console.log(tempObj)

        this.data.push(tempObj);
      // console.log(this.data)

      this.auc_hide = false
      Utils.setContainerState(self.currElement,'loading');
      if (_.isEmpty(self.line_chart_data_a )) {
        Utils.setContainerState(self.currElement, "nodata");
        return;
      }
      else{
        this.build_chart()
      }

       
      }

      
      // self.auc_a_value =  this.line_chart_data.Auc_A 
    }
  }

  ngOnInit() {

    this.elmWidth = this.element.nativeElement.offsetWidth;
    this.htmlElement = this.element.nativeElement;   

    // this.fetch_line_chart_data()
  }
  // fetch_line_chart_data() {
  //   let self = this
  //   this.api.get_line_chart_data().subscribe((res: any) => {
  //     self.line_chart_data_a = res.data_A
  //     if (_.isEmpty(self.line_chart_data_a )) {
  //       // Utils.setContainerState(self.currElement, "nodata");
  //       return;
  //     };
  //     // Utils.setContainerState(self.currElement, "done-loading");
  //     self.auc_a_value = res.Auc_A
  
  //     self.build_chart()
  //   });

  // }

  build_chart() {
    var margin = { top: 50, right: 50, bottom: 50, left: 50 },
      width = this.elmWidth - margin.left - margin.right,
      height = 520 - margin.top - margin.bottom;
      this.plotGraph = true
      this.view = [width,height]
      Utils.setContainerState(this.currElement, "done-loading");


  }
   

}
