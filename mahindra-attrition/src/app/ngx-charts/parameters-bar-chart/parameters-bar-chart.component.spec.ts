import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametersBarChartComponent } from './parameters-bar-chart.component';

describe('ParametersBarChartComponent', () => {
  let component: ParametersBarChartComponent;
  let fixture: ComponentFixture<ParametersBarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametersBarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametersBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
