import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import * as d3 from 'd3';
import * as _ from "lodash";
import * as d3Tip from 'd3-tip';
import { PredictiveService } from 'src/app/common/predictive.service';
import { Utils } from 'src/app/common/shared/utility';
declare var $: any

@Component({
  selector: 'app-parameters-bar-chart',
  templateUrl: './parameters-bar-chart.component.html',
  styleUrls: ['./parameters-bar-chart.component.scss']
})
export class ParametersBarChartComponent implements OnInit {

  line_chart_data = []
  low_performer_percentage: Number;
  private elmWidth;
  y: any;
  private currElement: HTMLElement;
  private htmlElement: HTMLElement;
  @ViewChild('chartContainer',{static:true}) element: ElementRef;
  @Input() data: any;
  constructor(private api: PredictiveService,private elRef: ElementRef,private service: PredictiveService) {
    this.currElement = elRef.nativeElement
  }

  ngOnInit() {
  }

  ngOnChanges(changes): void {
    let self = this

    this.elmWidth = this.element.nativeElement.offsetWidth;
    this.htmlElement = this.element.nativeElement;
    this.elmWidth = this.element.nativeElement.offsetWidth;

    if (changes.data.currentValue != undefined) {

      if (Object.keys(self.data).includes("type"))
      {
        let self = this
        Utils.setContainerState(self.currElement,'loading');
        this.service.lime_explainer_graph_data(self.data["parameters"]).subscribe((res: any) => {
          
          self.data = res.data
          console.log("ss",self.data)
          self.fetch_parameters_data(self.data);
                
        })
      }
      else{
      
      Utils.setContainerState(self.currElement,'loading');
      self = this
      setTimeout(function () {
        self.fetch_parameters_data(self.data);
      }, 1000);
    }
    
      
    }
  }

  fetch_parameters_data(data) {
    
    this.line_chart_data = data
    this.build_chart()
  }

  build_chart() {
    let self = this;
    $(".parameters").html("");
    
    if (_.isEmpty(self.data)) {
      Utils.setContainerState(self.currElement, "nodata");
      return;
    };
    // Utils.setContainerState(self.currElement, "done-loading");

    //create margins and dimensions
    var margin = { top: 30, right: 30, bottom: 60, left: 30 };
    var width = this.elmWidth - margin.left - margin.right-100
    var height = (440 / 1) - margin.top - margin.bottom;


    var svg = d3.select(".parameters")
      .append("svg")
      .attr("width", this.elmWidth+307)
      .attr("height", 500)
      // .style("margin-top", -55)

    var x = d3.scaleLinear()
      .range([0, width]);

    var y = d3.scaleBand()
      .rangeRound([0, height])
      .padding(0.1);

    var g = svg.append("g")
      .attr("transform", "translate("+280+"," + margin.top + ")");

    this.line_chart_data = this.line_chart_data.sort(d=>d.value).reverse()
    this.line_chart_data.forEach(function (d) {

      d.name = d.name;
      d.value = d.value

    })
    var min_value  = d3.min(this.line_chart_data, function(d) { return d.value; })
    if(min_value<0){
      var x_min_value  = min_value
    }
    else{
      x_min_value  = 0
    }

    x.domain([x_min_value,d3.max(this.line_chart_data, function(d) { return d.value; })]).nice();
    y.domain(this.line_chart_data.map(function (d) { return d.name; }));

    g.append("g")
      .attr("class", "axis axis-x")
      .attr("transform", "translate(+35," + height + ")")
      .call(d3.axisBottom(x)
        .tickFormat(d3.format("")));

    g.selectAll(".bar")
      .data(this.line_chart_data)
      .enter().append("rect")
      .attr("class", function (d) { return "bar bar--" + (d.value < 0 ? "negative" : "positive"); })
      .attr("x", function (d) { 
       return x(Math.min(0, d.value))+35;
       })
      .attr("y", function (d) {

        return y(d.name);
      })
      .attr("width", function (d) { return Math.abs(x(d.value) - x(0)); })
      .attr("height", y.bandwidth())
      .attr("fill", function (d) { return d.value < 0 ? "#ccc" : "#007bff"; });


    g.selectAll(".value")
      .data(this.line_chart_data)
      .enter().append("text")

    g.selectAll(".name")
      .data(this.line_chart_data)
      .enter().append("text")
      .attr("class", "name")
      .attr("x", function (d) { return x(0)+35; })
      .attr("y", function (d) { return y(d.name) + (y.bandwidth()/2) })
      .attr("dx", function (d) { return d.value < 0 ? 5 : -5; })
      .attr("dy", 4)
      .attr("text-anchor", function (d) { return d.value < 0 ? "start" : "end"; })
      .text(function (d) { return d.name; })
      .attr("font-size", "13px")


    svg.append("text")
      .attr("transform",
        "translate(" + (this.elmWidth/2) + ", " + (height + margin.top + margin.bottom + 45) + ")")
      .style("text-anchor", "middle")
      .style("font-family", "openSans_regular")
      .style("margin-top", "4px")
      .attr("dy", "-3em")
      .style("font-size", "13px")
      .text("");

    Utils.setContainerState(this.currElement, "done-loading");

  }
}
