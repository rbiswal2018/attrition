import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdbCurveComponent } from './pdb-curve.component';

describe('PdbCurveComponent', () => {
  let component: PdbCurveComponent;
  let fixture: ComponentFixture<PdbCurveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdbCurveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdbCurveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
