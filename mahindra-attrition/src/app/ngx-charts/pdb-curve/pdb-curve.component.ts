import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { PredictiveService } from 'src/app/common/predictive.service';
import * as d3 from 'd3';
import * as _ from "lodash";
import * as d3Tip from 'd3-tip';

declare var $: any;

@Component({
  selector: 'app-pdb-curve',
  templateUrl: './pdb-curve.component.html',
  styleUrls: ['./pdb-curve.component.scss']
})
export class PdbCurveComponent implements OnInit {

  pdb_chart_data = [];
  table_filter: any = {}
  drop_down_data = [];
  data = []
  @Input() oprn_data: any;

  
  color_scale = {"Average_0":"#4ed54e","Average_1":"#FFD700","Average_2":"#FF8C00","Average3":"#ff2828"}
  new_data = [];
  @ViewChild('chartContainer',{static:true}) element: ElementRef;
  private elmWidth;
  y: any;
 

  private currElement: HTMLElement;
  private htmlElement: HTMLElement;
  constructor(private api: PredictiveService,private elRef: ElementRef) {
    this.currElement = elRef.nativeElement
  }
  tipObject;
  ngOnInit() {
// this.new_data = [{'name': 'Average_0', 'values': [{'Name': 'oprn_category_EHBC', 'Average': 0.31453444811340103}, {'Name': 'oprn_category_ENPC', 'Average': 0.19503107690817678}, {'Name': 'oprn_category_ESBC', 'Average': 0.5094248151751072}]}, {'name': 'Average_1', 'values': [{'Name': 'oprn_category_EHBC', 'Average': 0.3044087698741489}, {'Name': 'oprn_category_ENPC', 'Average': 0.11851650999909333}, {'Name': 'oprn_category_ESBC', 'Average': 0.21544041345326656}]}, {'name': 'Average_2', 'values': [{'Name': 'oprn_category_EHBC', 'Average': 0.24760322855638245}, {'Name': 'oprn_category_ENPC', 'Average': 0.12411525506453763}, {'Name': 'oprn_category_ESBC', 'Average': 0.1702216843512095}]}, {'name': 'Average_3', 'values': [{'Name': 'oprn_category_EHBC', 'Average': 0.29588555449258525}, {'Name': 'oprn_category_ENPC', 'Average': 0.7101788619712261}, {'Name': 'oprn_category_ESBC', 'Average': 0.25742751985789714}]}]

    this.elmWidth = this.element.nativeElement.offsetWidth;
    this.htmlElement = this.element.nativeElement;


    this.elmWidth = this.element.nativeElement.offsetWidth;
    

    this.tipObject = (<any>d3Tip)()
      .attr('class', 'd3-tip-pdb d3-tip-treemap')
      .offset([-10, 0])
      .html(function (d) {
        
        return '<p><span style = "color:red;"> ' + '</span>' +"Average  :"+ d.Average.toFixed(2) + '</p>'
      }
      );
      this.get_dropdown_data()
  }

  ngOnChanges(changes): void {
    let self = this


    // this.elmWidth = this.element.nativeElement.offsetWidth;
    // this.htmlElement = this.element.nativeElement;
    // this.elmWidth = this.element.nativeElement.offsetWidth;

    if (changes.oprn_data.currentValue != undefined) {

    this.get_dropdown_data()
      // Utils.setContainerState(self.currElement,'loading');
      self = this
      
    }
  }
  ngOnDestroy() {
   
    $(".d3-tip").remove()
  }

  get_dropdown_data() {

 

    let self = this
    this.api.get_pdb_dropdown().subscribe((res: any, ) => {
      
      this.table_filter.col_name = res.data[0]
    
        self.drop_down_data = res.data

      this.get_filter_data()
    })

  }

  get_filter_data() {
    // this.table_filter.oprn_cat = this.oprn_data["oprn_cat"][0]

    let self = this
    this.api.get_pdb_data({ "feature_col": this.table_filter.col_name}).subscribe((res: any, ) => {
      self.data = res.data
      this.new_data = res.line_data

      if (_.isEmpty(res.data)) {
        // Utils.setContainerState(self.currElement, "nodata");
        return;
      };
      // Utils.setContainerState(self.currElement, "done-loading");
      self.build_chart()
    })
  }
  build_chart() {
   
    var circleRadius = 3.7;
    
    var margin = { top: 20, right: 20, bottom: 30, left: 100 },
      width = this.elmWidth - margin.left - margin.right,
      height = 510 - margin.top - margin.bottom;
    var x = d3.scaleBand().rangeRound([0, width - 150]).padding(.7).paddingOuter(1);;
    var y = d3.scaleLinear().rangeRound([height, 0]);
    var y1 = d3.scaleLinear().range([height, 0]).domain([0, 1]);//marks can have min 0 and max 100

    var yAxisRight = d3.axisRight(y1)
    var color = d3.scaleOrdinal()
    var xAxis = d3.axisBottom(x)
    var yAxis = d3.axisLeft(y)
    $(".pdb_chart").html("");

 

    var sw = width + margin.left + margin.right
    var sh = height + margin.top + margin.bottom + 10
    var svg = d3.select(".pdb_chart").append("svg")
      // .attr("width", width + margin.left + margin.right)
      // .attr("height", height + margin.top + margin.bottom + 10)
      .attr("viewBox",`0 0  ${sw} ${sh}`)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    let self = this
    this.data.forEach(function (d) {
      //filter out name and average

      color.domain(d3.keys(self.data[0]).filter(function (key) { return key !== "Name" && key !== "Average"; }));
      self.data.forEach(function (d) {
        var y0 = 0;
        d.group = color.domain().map(function (name) { return { name: name, y0: y0, y1: y0 += +d[name] }; });
        d.count = d.count
      });
    })

    x.domain(self.data.map(function (d) { return d.Name; }));
    //stores toltal headcount
    y.domain([0, d3.max(self.data, function (d) { return d.count; })]);

    
    //this will make the y axis to teh right
    svg.append("g")
      .attr("class", "y axis")
      .attr("transform", "translate(" + (width - 105) + " ,0)")
      .style("color", "(0, 0, 0)")
      .style("font-family", "openSans_regular")
      .style("stroke", "lightsteelblue")
      .call(yAxisRight);


    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)

    svg.append("g")
      .attr("class", "y axis")
      .style("stroke", "steelblue")
      .style("fill", "(0, 0, 0)")
      .style("font-size", "10px")
      .style("font-family", "openSans_regular")
      .call(yAxis)
    
    var state = svg.selectAll(".state")
      .data(self.data)
      .enter().append("g")
      .attr("class", "g")
      .attr("transform", function (d) { 
        return "translate(" + x(d.Name) + ",0)"; 
      })
      
      

    //adding the rect for group chart
    state.selectAll("rect")
      .data(function (d) { return d.group; })
      .enter().append("rect")
      .attr("width", x.bandwidth()+10)
      .attr("y", function (d) {
       
        return y(d['y1']);
      })
      .attr("height", function (d) { return y(d['y0']) - y(d['y1']); })
      .style("fill", "steelblue");

  
     var averageline = d3.line()
      .x(function (d) { return x(d["Name"]) + x.bandwidth() / 2; })
      .y(function (d) { return y1(d["Average"]); });


let lines = svg.append('g')
  .attr('class', 'lines');

lines.selectAll('.line-group')
  .data(this.new_data).enter()
  .append('g')
  .attr('class', 'line-group') 
  
// .on("mouseout", function(d) {
//     svg.select(".title-text").remove();
//   }) 
  .append('path')
  .attr('class', 'line')  
  .attr("fill", "none")
  .attr('d',function(d){

    return averageline(d["values"])})
  .attr("stroke",function(d){
    
    
    if(d["name"] == "Average_0"){
      return "#4ed54e"
    }
   
  }) 

   
      svg.selectAll("circle-group")
        .data(this.new_data).enter()
        .append("g")
        .style("fill", function(d){
    
          if(d["name"] == "Average_0"){
            return "#4ed54e"
          }
        }
          )
        .selectAll("circle")
        .data(d => d.values).enter()
        .append("g")
        .attr("class", "circle")  
        .append("circle")
        .attr("cx", (d:any) => x(d.Name) +x.bandwidth() / 2)
        .attr("cy", (d:any) => y1(d.Average))
        .attr("r", circleRadius)
        .on('mouseover', self.tipObject.show)
        .on('mouseout', self.tipObject.hide);
        svg.call(self.tipObject);


   // if(!("Average" in this.data[0])){
    //   svg.append("path")
    //   .data([self.data])
    //   .attr("class", "line")
    //   .attr("d", averageline_0)
    //   .attr("stroke", "#4ed54e")
 
    svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", -68)
      .attr("x", 0 - (height / 2))
      .attr("dy", "1.5em")
      .style("font-size", "13px")
      .style("font-family", "openSans_regular")
      .style("text-anchor", "middle")
      .text("Count");

    svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", (width / 1.18) + margin.top + margin.bottom + 20)
      .attr("x", 0 - (height / 2))
      .attr("dy", "1.5em")
      .style("font-size", "13px")
      .style("font-family", "openSans_regular")
      .style("text-anchor", "middle")
      .text("Partial Dependency");

    svg.append("text")
      .attr("transform",
        "translate(" + (width / 2) + ", " + (height + margin.top + margin.bottom + 20) + ")")
      .style("text-anchor", "middle")
      .style("font-family", "openSans_regular")
      .attr("dy", "-3em")
      .style("font-size", "13px")
      .text("Dependency");
  };

}
