import { Component, OnInit, Input, OnDestroy, OnChanges, Output, EventEmitter } from '@angular/core';
import { CompileShallowModuleMetadata } from '@angular/compiler';
declare var $: any;

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit, OnChanges {

  @Input() mainGraphData: any;
  view: any;
  plotGraph: any = false;
  loaderactive: any = true;
  data: any[] = [];
  mycolorfn  = [];

  constructor() { }

  getPiechartData(data) {
    this.data = [];
    this.mycolorfn = [];
  
    
      if(data.name=='marital' || data.name=='voluntary_category'){
        for (let val of data.value) {
        this.provideColor(val);
        this.data.push(val);
      }
    }
      else{
      for (let val of data.value) {
      if (!isNaN(parseFloat(val.name)) || val.name === 'NA' || val.name === 'Resigned') {
        
        this.provideColor(val);
        this.data.push(val);
      }
      }
    }

  }

  onResize() {
    const width = $('#pie-chart-parent').width();
    this.view = [width, 400];
  }

  ngOnInit() {
    const width = $('#pie-chart-parent').width();
    this.view = [width, 400];
  }

  provideColor(obj) {
    const temObj: any = {};
    temObj['name'] = obj.name;
    if (obj.value <= 10) {
      temObj['value'] = "#216353";
    } else if (obj.value > 10 && obj.value <= 20) {
      temObj['value'] = "#ffe75e";
    } else if (obj.value > 20 && obj.value <= 30) {
      temObj['value'] = "#de7119";
    }else {
      temObj['value'] = "#e31837";
    }
    this.mycolorfn.push(temObj);
  }

  ngOnChanges() {
    if (this.mainGraphData !== undefined) {
      this.getPiechartData(this.mainGraphData);
      this.plotGraph = true;
      this.loaderactive = false;
    }
  }

}
