import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictiveNgxTreemapComponent } from './predictive-ngx-treemap.component';

describe('PredictiveNgxTreemapComponent', () => {
  let component: PredictiveNgxTreemapComponent;
  let fixture: ComponentFixture<PredictiveNgxTreemapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictiveNgxTreemapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictiveNgxTreemapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
