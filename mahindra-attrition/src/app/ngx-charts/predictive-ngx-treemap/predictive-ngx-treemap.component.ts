import { Component, OnInit, Input, ViewChild, ElementRef, Output } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import { PredictiveService } from 'src/app/common/predictive.service';
import { EventEmitter } from '@angular/core';
import { Utils } from 'src/app/common/shared/utility';
import * as _ from "lodash";

@Component({
  selector: 'app-predictive-ngx-treemap',
  templateUrl: './predictive-ngx-treemap.component.html',
  styleUrls: ['./predictive-ngx-treemap.component.scss']
})
export class PredictiveNgxTreemapComponent implements OnInit {
  data: any[] = [];
  subscription: any
  temp: any = {}
  percent_achivement: any = {}

  temp_dict = []
  tipObject: any;
  breadcrumb_data: any = {}
  chartData: any;
  filters: any = {}
  flatData: any = {}
  loaderactive: any = true;

  private currElement: HTMLElement;
  private htmlElement: HTMLElement;
  private elmWidth;
  view = []
  plotGraph=false
  mycolorfn = []
  onResize:any;

  @ViewChild('chartContainer',{static:true}) element: ElementRef;
  // private capitalize = CapitalizePipe.prototype.transform;

  constructor(private api: PredictiveService, private elRef: ElementRef) {
    this.currElement = elRef.nativeElement
  }

  @Output() filter_values = new EventEmitter<any>();
  @Output() predic_perf = new EventEmitter<any>();
  @Input() filter_data_tree_map :any;

  ngOnInit() {
    
    let self = this;
    this.elmWidth = this.element.nativeElement.offsetWidth;
    this.view = [ this.elmWidth, 400];
   
    // this.fetchChartData(this.filters)
    this.temp_dict = []
  }


  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    $(".d3-tip").remove()
  }

  ngOnChanges(changes): void {
    
    let self = this
    if (changes.filter_data_tree_map.currentValue != undefined) {

      this.fetchChartData(this.filter_data_tree_map)
    }
  }
  getPath(val) {
    return val.extra + ' ' + val.value.toFixed(2);
  }
  // fetchChartData(data): void {

  //     this.drawBreadcrumb(Object.keys(data["filters"]))
  //     this.chartData = data["data"]["performer_by_state"];
  //     this.flatData = [...data["data"]["performer_by_state"]]
  //     this.data = []
  //     if (this.flatData.length === 1 && this.flatData[0].percentage === 0) {
  //       this.flatData = [];
  //       this.loaderactive =false

  //       this.flatData['error_msg'] = { key: true, value: 'No data available for the selected sequence.' }
  //     }
  //     if (this.flatData.length < 1) {
  //       this.flatData = [];
  //       this.loaderactive =false

  //       this.flatData['error_msg'] = { key: true, value: 'Invalid sequence selection!!' }
  //     }
  //     if (this.flatData  < 1) {
  //       this.plotGraph = false;
  //     }
      
  //     else{
        
  //       for (let val of this.flatData ) {
  //       if (val.name !== '') {
  //         const tempObj = {};
  //         const grpah_value = val.name.split("-->");
  //         tempObj['name'] = grpah_value.pop();
  //         tempObj['value'] = val.value;
  //         tempObj['extra'] = val.hir_name;
  //         this.provideColor(tempObj);
  //         this.data.push(tempObj);
        
  //       }
  //     }
  //   }
    
  //     // this.assign_percent(res)

  //     // this.predic_perf.emit(this.percent_achivement)
  //     this.buildChart()
  //   }
  
  fetchChartData(data): void {

    this.drawBreadcrumb(Object.keys(data["filters"]))
    this.chartData = data["data"]["performer_by_state"];
    this.flatData = [...data["data"]["performer_by_state"]]
    this.data = []
    if (this.flatData.length === 1 && this.flatData[0].percentage === 0) {
      this.flatData = [];
      this.loaderactive =false
      this.plotGraph = false;
      this.flatData['error_msg'] = { key: true, value: 'No data available for the selected sequence.' }
    }
    if (this.flatData.length < 1) {
      this.flatData = [];
      this.loaderactive =false
      this.plotGraph = false;
      this.flatData['error_msg'] = { key: true, value: 'Invalid sequence selection!!' }
    }
    if (this.flatData  < 1) {
      this.plotGraph = false;
    }
    
    else{
      
      for (let val of this.flatData ) {
      if (val.name !== '') {
        const tempObj = {};
        const grpah_value = val.name.split("-->");
        tempObj['name'] = grpah_value.pop();
        tempObj['value'] = val.value;
        tempObj['extra'] = val.hir_name;
        this.provideColor(tempObj);
        this.data.push(tempObj);
        this.plotGraph = true;
        this.buildChart()
      
      }
    }
  }
    // this.assign_percent(res)

    // this.predic_perf.emit(this.percent_achivement)
  
  }


  

  buildChart() {
    var self = this

    // if (_.isEmpty(self.chartData.performer_by_state)) {
    //   Utils.setContainerState(self.currElement, "nodata");
    //   return;
    // };
    // Utils.setContainerState(self.currElement, "done-loading");
  
    var idKey = "name"

    var valueKey = 'total_count'
    var colorKey = 'performer'

    var margin = { top: 0, left: 0, bottom: 0, right: 0 },
      width = this.elmWidth - margin.left - margin.right,
      height = 470 - margin.top - margin.bottom;
      this.plotGraph = true;
      this.loaderactive =false

  }
  drawBreadcrumb(data) {
    // console.log(data)
    this.temp_dict = data
  }

  // assign_percent(data){

  //   this.percent_achivement.a = (data.avg_percent.active) 
  //   this.percent_achivement.b = (data.avg_percent.seperated) 
    

  // }
  
  provideColor(obj) {
    const temObj: any = {};
    temObj['name'] = obj.name;
    if (obj.value <= 60) {
      temObj['value'] = "#ff2828";
    } else if (obj.value >60) {
      temObj['value'] = "#4ed54e";
    } 
    this.mycolorfn.push(temObj);
  }


  
  
}
