import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictiveTreeMapComponent } from './predictive-tree-map.component';

describe('PredictiveTreeMapComponent', () => {
  let component: PredictiveTreeMapComponent;
  let fixture: ComponentFixture<PredictiveTreeMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictiveTreeMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictiveTreeMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
