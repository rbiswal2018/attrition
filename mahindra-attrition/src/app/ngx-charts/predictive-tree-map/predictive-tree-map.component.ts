import { Component, OnInit, Input, Output, ViewChild, ElementRef } from '@angular/core';
import * as d3 from 'd3';
import * as _ from "lodash";
import * as d3Tip from 'd3-tip';

import { PredictiveService } from 'src/app/common/predictive.service';
import { EventEmitter } from '@angular/core';
import { Utils } from 'src/app/common/shared/utility';

declare var $: any

@Component({
  selector: 'app-predictive-tree-map',
  templateUrl: './predictive-tree-map.component.html',
  styleUrls: ['./predictive-tree-map.component.scss']
})
export class PredictiveTreeMapComponent implements OnInit {

  subscription: any
  temp: any = {}
  percent_achivement: any = {}

  temp_dict = []
  tipObject: any;
  breadcrumb_data: any = {}
  chartData: any;
  filters: any = {}
  flatData: any = {}
  private currElement: HTMLElement;
  private htmlElement: HTMLElement;
  private elmWidth;

  @ViewChild('chartContainer',{static:true}) element: ElementRef;
  // private capitalize = CapitalizePipe.prototype.transform;

  constructor(private api: PredictiveService, private elRef: ElementRef) {
    this.currElement = elRef.nativeElement
  }

  @Output() filter_values = new EventEmitter<any>();
  @Output() predic_perf = new EventEmitter<any>();
  @Input() filter_data_tree_map :any;

  ngOnInit() {
    
    let self = this;
    this.elmWidth = this.element.nativeElement.offsetWidth;
    self.tipObject = (<any>d3Tip)()
      .attr('class', 'd3-tip d3-tip-treemap')
      .offset([-10, 0])
      .html(function (d) {
        return '<p><span class="title"> ' + '</span>' + d["data"].hir_name + '</p>'
          + '<p><span class="title">' + "Attrition Yes Percentage" + ': </span>' + d["data"].total_count + '</p>';
      });

    // this.fetchChartData(this.filters)
    this.temp_dict = []
  }


  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    $(".d3-tip").remove()
  }

  ngOnChanges(changes): void {
    
  
    let self = this
    if (changes.filter_data_tree_map.currentValue != undefined) {
      this.fetchChartData(this.filter_data_tree_map)
      // this.temp_dict = []
    }
  }
  fetchChartData(data): void {

    // this.api.predictive_tree_map_list(this.filter_data_tree_map).subscribe((res: any, ) => {
      // this.drawBreadcrumb(Object.keys(this.filter_data_tree_map))
      // this.chartData = res.data;
      this.drawBreadcrumb(Object.keys(data["filters"]))
      this.chartData =data["data"];
      this.flatData = [...this.chartData['performer_by_state']]
      // this.assign_percent(res)
      // this.predic_perf.emit(this.percent_achivement)
      this.buildChart()
    // })
  }

  buildChart() {
    var self = this

    if (_.isEmpty(self.chartData.performer_by_state)) {
      Utils.setContainerState(self.currElement, "nodata");
      return;
    };
  
    var idKey = "name"

    var valueKey = 'total_count'
    var colorKey = 'performer'

    var margin = { top: 0, left: 0, bottom: 0, right: 0 },
      width = this.elmWidth - margin.left - margin.right,
      height = 470 - margin.top - margin.bottom;

    var color = d3.scaleOrdinal(d3.schemeCategory20);
    $(".treemap").html("");

    this.flatData= _.remove( this.flatData, function(n) {
     
      return n[idKey] != null;;
    });
    var svg = d3.select(".treemap")
      .append("svg")
      .attr("width", this.elmWidth)
      .attr("height", height + margin.left + margin.right),
      chartLayer = svg.append("g").classed("chartLayer", true);

    _.each(this.flatData, function (d) {
      d.name = d[idKey];
      d.value = d.total_count;
      d.temp_data = d
      d.parent = 'Top Level';

    });

    this.flatData.unshift({ "name": "Top Level", "parent": null });

    // convert the flat data into a hierarchy 
    var treeData = d3.stratify()
      .id(function (d) { return d["name"]; })
      .parentId(function (d) { return d['parent']; })
      (this.flatData)
      .sum(function (d) { return d[valueKey]; });
    var treemap = d3.treemap()
      .size([width, height])
      .padding(1)
      .round(true);

    treemap(treeData);

    var root = treeData


    var self = this

    //data bind
    var node = chartLayer
      .selectAll(".node")
      .data(root.leaves(), function (d) { return d["id"] })

    node
      .selectAll("rect")
      .data(root.leaves(), function (d) { return d["id"] })

    node
      .selectAll("text")
      .data(root.leaves(), function (d) { return d["id"] })

    // enter                  
    var newNode = node.enter()
      .append("g")
      .attr("class", "node")

    newNode.append("rect")
    newNode.append("text")

    // update  

    chartLayer
      .selectAll(".node rect")
      .on('mouseover', self.tipObject.show)
      // .on('mousemove',
      // function(d){        
      
      // self.tipObject.style("left", d3.event.pageX-160 + "px")
      //         .style("top", d3.event.pageY-70+ "px")       
      // })

      .on('mouseout', self.tipObject.hide)
      .on("click", function (d) {
        // self.treeFilter(d["data"], event);
      })

      .style("cursor", "pointer")
      .attr("x", function (d) { return d["x0"] })
      .attr("y", function (d) { return d["y0"] })
      .attr("width", function (d) { return d["x1"] - d["x0"] })
      .attr("height", function (d) { return d["y1"] - d["y0"] })

      .attr("fill", (d) => {
     
       
        if (d["data"]["temp_data"]["total_count"] <=60) {
          return "#ff2828"
        }
        else if (d["data"]["temp_data"]["total_count"]>60) {
          return "#4ed54e"
        }
      })

    chartLayer
      .selectAll(".node text")
      .html((d: any) => `<tspan>` + d.data[idKey] + `</tspan><tspan x = "7" dy = "20">` + d.data.total_count + '</tspan>')

      .attr("y", "1.5em")
      .attr("x", "0.5em")
      .attr("font-size", "0.8em")
      .attr("transform", function (d) { return "translate(" + [d["x0"], d["y0"]] + ")" });
    svg.call(self.tipObject);
    Utils.setContainerState(self.currElement, "done-loading");

  }


  databack(data, name) {
  
  }


  drawBreadcrumb(data) {

    this.temp_dict = data
  
  }

  assign_percent(data){

    this.percent_achivement.a = (data.avg_percent.active) 
    this.percent_achivement.b = (data.avg_percent.seperated) 
    

  }



}





