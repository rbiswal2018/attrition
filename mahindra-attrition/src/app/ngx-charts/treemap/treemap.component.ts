import { Component, OnInit, AfterViewInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import { reduce } from 'rxjs/operators';
// import { ConsoleReporter } from 'jasmine';
declare var $: any;

@Component({
  selector: 'app-treemap',
  templateUrl: './treemap.component.html',
  styleUrls: ['./treemap.component.scss']
})
export class TreemapComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() mainGraphData: any;
  // @Output() sendTreemapData = new EventEmitter<object>();
  // @Output() sendBredacrumbData = new EventEmitter<object>();
  data: any[] = [];
  view: any;
  plotGraph: any = false;
  loaderactive: any = true;
  breadcrumbArr: any[] = [];
  treemapClickedDataArr: any = [];
  // stateClicked: boolean = false;
  // branchClicked: boolean = false;
  // functionClicked: boolean = false;
  // divisionClicked: boolean = false;
  // businessClicked: boolean = false;
  // depClicked: boolean = false;
  // subDepClicked: boolean = false;
  // costClicked: boolean = false;
  // workClicked: boolean = false;
  // bandClicked: boolean = false;
  // gradeClicked: boolean = false;
  isObjPresent: any[] = [];
  lastData: any;
  dataSentArr: any[] = [];
  curBreadcrumbData: any[] = [];
  handleDynamic: any = false;
  breadCrumbTracker: any[] = [];
  // selectionHierarchy: any[] = [
  //   {name: 'state' , value:'', index: 1 },
  //   {name: 'branch' , value:'', index: 2 },
  //   {name: 'function_name' , value:'', index: 3 },
  //   {name: 'division' , value:'', index: 4 },
  //   {name: 'business_unit' , value:'', index: 5 },
  //   {name: 'department' , value:'', index: 6 },
  //   {name: 'sub_department' , value:'', index: 7 },
  //   {name: 'cost_centre' , value:'', index: 8 },
  //   {name: 'work_contract' , value:'', index: 9 },
  //   {name: 'band' , value:'', index: 10 },
  //   {name: 'grade' , value:'', index: 11 }
  // ];

  onResize() {
    const width = $('#treemap-parent').width();
    this.view = [width, 400];
  }

  mycolorfn  = [];

  // onRectSelect(val) {
  //   this.dataSentArr = [];
  //   this.plotGraph = false;
  //   this.loaderactive = true;
  //   let mainData: any[] = [];
  //   if (this.handleDynamic) {
  //     mainData = this.mainGraphData.value;
  //   } else {
  //     mainData = this.mainGraphData;
  //   }
  //   const totalKeys = Object.keys(mainData[0]);
  //   for (let obj of mainData) {
  //     for (let value of totalKeys) {
  //       if (obj[value] === val.name && Math.trunc(obj.attrition_ratio) === Math.trunc(val.value)) {
  //           if (this.isObjPresent.includes(value) === false) {
  //             this.isObjPresent.push(value);
  //             for (let i of Object.keys(obj)) {
  //               if (i !== 'attrition_ratio') {
  //                 this.dataSentArr.push({'key': i, 'value': [obj[i]]});
  //               }
  //             }
  //           }
  //       }
  //     }
  //   }
  //   this.handleBreadcrumb();
  //   this.makeLayerTrue(totalKeys.length);
  //   this.sendTreemapData.emit(this.dataSentArr);
  //   this.createBreadcrumb(this.dataSentArr, val);
  // }

  // makeLayerTrue(val) {
  //   switch(val) {
  //     case 2:
  //     this.stateClicked = true;
  //     break;
  //     case 3:
  //     this.branchClicked = true;
  //     break;
  //     case 4:
  //     this.functionClicked = true;
  //     break;
  //     case 5:
  //     this.divisionClicked = true;
  //     break;
  //     case 6:
  //     this.businessClicked = true;
  //     break;
  //     case 7:
  //     this.depClicked = true;
  //     break;
  //     case 8:
  //     this.subDepClicked = true;
  //     break;
  //     case 9:
  //     this.costClicked = true;
  //     break;
  //     case 10:
  //     this.workClicked = true;
  //     break;
  //     case 11:
  //     this.bandClicked = true;
  //     break;
  //     case 12:
  //     this.gradeClicked = true;
  //     break;
  //   }
  // }

  // handleBreadcrumb() {
  //   this.stateClicked = false;
  //   this.branchClicked = false;
  //   this.functionClicked = false;
  //   this.divisionClicked = false;
  //   this.businessClicked = false;
  //   this.depClicked = false;
  //   this.subDepClicked = false;
  //   this.costClicked = false;
  //   this.workClicked = false;
  //   this.bandClicked = false;
  //   this.gradeClicked = false;
  //   this.plotGraph = false;
  // }

  // createBreadcrumb(val, clickedObj) {
  //   this.breadCrumbTracker.push(clickedObj.name);
  //   this.curBreadcrumbData = [];
  //   this.curBreadcrumbData = val;
  //   this.breadcrumbArr = [];
  //   for (let value of this.selectionHierarchy) {
  //     value.value = '';
  //   }
  //   for (let i = 0; i < this.selectionHierarchy.length; i++) {
  //     const cur_val = this.selectionHierarchy[i].name;
  //     for (let obj of val) {
  //       if (obj.key === cur_val && this.breadCrumbTracker.includes(obj.value[0])) {
  //           this.selectionHierarchy[i].value = obj.value;
  //       }
  //     }
  //   }
  //   for (let i = 0; i < this.selectionHierarchy.length; i++) {
  //     if (this.selectionHierarchy[i].value !== '') {
  //       this.breadcrumbArr.push(this.selectionHierarchy[i]);
  //     }
  //   }
  // }

  // breadCrumbClick(val) {
  //   this.plotGraph = false;
  //   this.loaderactive = true;
  //   const breadcrumbObj = {};
  //   const curFactor = val.value.split('^')[0];
  //   const curName = val.value.split('^')[1];
  //   const tempArr: any[] = [];
  //   let tempBreadcrumbArr = [];
  //   if (curFactor === 'state') {
  //     this.breadcrumbArr = [];
  //   }
  //   breadcrumbObj['cur_factor'] = curFactor;
  //   this.handleBreadcrumb();
  //   for (let i = 0; i < this.breadCrumbTracker.length; i++) {
  //     if (this.breadCrumbTracker[i] === curName) {
  //       if (this.breadCrumbTracker[i] !== 'state' && this.isObjPresent[0] === 'state') {
  //         this.isObjPresent.length = i;
  //         for (let j = 0; j < i; j++) {
  //           tempBreadcrumbArr.push(this.breadCrumbTracker[j]);
  //         }
  //         for (let val1 of this.curBreadcrumbData) {
  //           const tempVal = val1.key;
  //           for (let val2 of this.isObjPresent) {
  //             if (tempVal === val2) {
  //               tempArr.push(val1);
  //             }
  //           }
  //         }
  //         breadcrumbObj['cur_all_val'] = tempArr;
  //         this.makeLayerTrue(i + 1);
  //         this.breadcrumbArr.length = i;
  //       } else if (this.isObjPresent[0] !== 'state') {
  //         const tempArr: any[] = [];
  //         this.isObjPresent = [];
  //         for (let j = 0; j < i; j++) {
  //           tempBreadcrumbArr.push(this.breadCrumbTracker[j]);
  //         }
  //         for (let obj of this.selectionHierarchy) {
  //           if (obj.name === curFactor) {
  //             this.makeLayerTrue(obj.index);
  //             this.breadcrumbArr.length = ( obj.index - 1 );
  //             breadcrumbObj['cur_all_val'] = tempArr;
  //             break;
  //           }
  //           const tempObj = {};
  //           tempObj['key'] = obj.name;
  //           tempObj['value'] = [this.mainGraphData[0][obj.name]];
  //           tempArr.push(tempObj);
  //         }
  //       } else if (this.breadCrumbTracker[i] === 'state') {
  //         this.isObjPresent = [];
  //         this.breadCrumbTracker.length = 1;
  //         this.makeLayerTrue(NaN);
  //       }
  //     }
  //   }
  //   this.breadCrumbTracker.length = tempBreadcrumbArr.length;
  //   this.sendBredacrumbData.emit(breadcrumbObj);
  // }

  plotTreemap(mainGraphData) {
    this.data = [];
    if (mainGraphData.length < 1) {
      this.plotGraph = false;
    } else {
      for (let val of mainGraphData) {
        if (val.name !== '') {
          const tempObj = {};
          const grpah_value = val.name.split("-->");
          tempObj['name'] = grpah_value.pop();
          tempObj['value'] = val.percentage;
          
          tempObj['extra'] = val.name;
          this.provideColor(tempObj);
          this.data.push(tempObj);
        }
      }
    }
  }

  getPath(val) {
    return val.extra + ' ' + val.value.toFixed(2);
  }

  provideColor(obj) {
    const temObj: any = {};
    temObj['name'] = obj.name;
    if (obj.value <= 10) {
      temObj['value'] = "#216353";
    } else if (obj.value > 10 && obj.value <= 20) {
      temObj['value'] = "#ffe75e";
    } else if (obj.value > 20 && obj.value <= 30) {
      temObj['value'] = "#de7119";
    }else {
      temObj['value'] = "#e31837";
    }
    this.mycolorfn.push(temObj);
  }

  constructor(private _snackBar: MatSnackBar) {
  }

  ngOnInit() {
    const width = $('#treemap-parent').width();
    this.view = [width, 400];
  }

  ngAfterViewInit() {
  }

  ngOnChanges() {
    // console.log(this.mainGraphData)
    if (this.mainGraphData !== undefined) {
      if (this.mainGraphData.index !== undefined) {
        this.breadcrumbArr = [];
        this.mycolorfn = [];
        // this.handleBreadcrumb();
        // this.makeLayerTrue(parseFloat(this.mainGraphData.index) + 2);
        this.plotTreemap(this.mainGraphData.value);
        if (parseFloat(this.mainGraphData.index) !== 11) {
          this.plotGraph = true;
          this.loaderactive = false;
        }
        this.handleDynamic = true;
        this.isObjPresent = [];
        this.breadCrumbTracker = [];
      } else if (this.mainGraphData.index === null) {
        this.breadcrumbArr = [];
        this.mycolorfn = [];
        // this.handleBreadcrumb();
        // this.makeLayerTrue(2);
        this.plotTreemap(this.mainGraphData.value);
        this.plotGraph = true;
        this.loaderactive = false;
        this.handleDynamic = true;
        this.isObjPresent = [];
        this.breadCrumbTracker = [];
      } else {
        this.handleDynamic = false;
        this.mycolorfn = [];
        this.plotTreemap(this.mainGraphData);
        if (this.data.length > 0) {
          this.plotGraph = true;
          this.loaderactive = false;
        }
      }
    }
  }

}
