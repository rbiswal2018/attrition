import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelhealthComponent } from './modelhealth.component';

describe('ModelhealthComponent', () => {
  let component: ModelhealthComponent;
  let fixture: ComponentFixture<ModelhealthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelhealthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelhealthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
