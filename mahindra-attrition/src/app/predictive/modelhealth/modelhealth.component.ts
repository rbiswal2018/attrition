import { Component, OnInit, Input } from '@angular/core';
import { PredictiveService } from 'src/app/common/predictive.service';
import { Utils } from 'src/app/common/shared/utility';

@Component({
  selector: 'app-modelhealth',
  templateUrl: './modelhealth.component.html',
  styleUrls: ['./modelhealth.component.scss']
})
export class ModelhealthComponent implements OnInit {
  

  training_rows: number;
  training_accuracy: any;
  test_rows: number;
  test_accuracy: any;
  recall: any;
  precision: any;
  accuracy: any;
  f_score: any;
  line_chart_data:any

  trained_year_quarter = ""
  prediction_year_quarter = ""

  constructor(private api: PredictiveService) { }

  ngOnInit() {
    // this.insert_model_health_data()
    this.get_model_health_data()
    this.fetch_line_chart_data()
  }

  // insert_model_health_data(){
  //   this.api.insert_model_health_data().subscribe((res: any) => {
      
  //   })
  // }

  fetch_line_chart_data() {
    let self = this
    this.api.get_line_chart_data().subscribe((res: any) => {
      var temp_object = {}

      temp_object["line_data"] = res.data_A
      temp_object["auc_value"]  = res.Auc_A
      temp_object["key"] = "model_health"
      this.line_chart_data = temp_object
    
    });

  }
  get_model_health_data() {
    let self = this
    this.api.get_model_health_data().subscribe((res: any) => {

      self.training_rows = res.data.training_rows
      self.training_accuracy = (res.data.train_accuracy * 100).toFixed(2)
      self.test_rows = res.data.testing_rows
      self.test_accuracy = (res.data.test_accuracy * 100).toFixed(2)
      self.recall = (res.data.test_recall * 100).toFixed(2)
      self.precision = (res.data.test_precision * 100).toFixed(2)
      self.accuracy = (res.data.auc_test * 100).toFixed(2)
      self.f_score = (res.data.test_f1 * 100).toFixed(2)
      self.trained_year_quarter = (res.data.train_quarter.toString() + " " + res.data.train_year).replace(/[&\/\\#+()$~%.'":*?<>{}]/g, '');
      self.prediction_year_quarter = res.data.pred_quarter + " " + res.data.pred_year
      localStorage.setItem("pre_q_year", self.prediction_year_quarter)
    })
  }
  dom_to_img(event, chart_name) {
  
    Utils.dom_to_img_predict(event.target, chart_name);
  }
}


