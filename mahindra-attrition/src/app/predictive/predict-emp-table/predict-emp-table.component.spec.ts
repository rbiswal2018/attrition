import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictEmpTableComponent } from './predict-emp-table.component';

describe('PredictEmpTableComponent', () => {
  let component: PredictEmpTableComponent;
  let fixture: ComponentFixture<PredictEmpTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PredictEmpTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictEmpTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
