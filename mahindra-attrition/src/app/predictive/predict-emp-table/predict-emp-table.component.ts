import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { PredictiveService } from 'src/app/common/predictive.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { CsvDownloaderService } from 'src/app/common/services/csv-downloader.service';
import { Utils } from 'src/app/common/shared/utility';

@Component({
  selector: 'app-predict-emp-table',
  templateUrl: './predict-emp-table.component.html',
  styleUrls: ['./predict-emp-table.component.scss']
})
export class PredictEmpTableComponent implements OnInit {
  @Input() filter: any;
  @ViewChild(DataTableDirective,{static:true})
  dtElement: DataTableDirective;
  dtOptions: any = {};
  dtRendered = true;
  dtTrigger: any = new Subject();
  data = [];
  private currElement: HTMLElement;
  private htmlElement: HTMLElement;
  emp_list: any = [];
  dtColumns :any = []
  temp_data:any
  csv_data: any;
  table_filter: any = {};
  tabTitreColonnesXLS = []

  @ViewChild('chartContainer',{static:true}) element: ElementRef;
  constructor(private api: PredictiveService, private elRef: ElementRef,private csv_object: CsvDownloaderService) {
    this.currElement = this.elRef.nativeElement;
  }

  ngOnInit() {

    //this.predictive_emp_data(this.filter)
  }

  ngOnChanges(changes): void {


    if (changes.filter.currentValue != undefined) {
     
      $('#audits').empty();
      this.plot_userTable()
      this.predictive_emp_data(this.filter)
    }
  }


  rerender(): void {
    let self = this

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      
      // console.log("destroy")
      // console.log(dtInstance)
      dtInstance.destroy();
      // console.log("sjhsd")
      this.dtTrigger.next();
      // Utils.setContainerState(self.currElement, "done-loading");
    });
  }
  predictive_emp_data(emp_data) {
    let self = this
    Utils.setContainerState(self.currElement, "loading");
      this.csv_data = emp_data
      this.emp_list.length = 0
      for (let value of emp_data) {
        this.emp_list.push(value);
      }
      if (!this.dtElement.dtInstance) {  
        this.dtTrigger.next();
        console.log("Hi")
        Utils.setContainerState(self.currElement, "done-loading");
      } else {
      
        this.plot_userTable()
        Utils.setContainerState(self.currElement, "done-loading");
        this.rerender()
      };
    };
  

  ngOnDestroy(): void {
    //unsubscribe the event
    // this.dtTrigger.unsubscribe();
  }
  plot_userTable() {

      this.dtOptions = {
        data: this.emp_list,
        processing: true,
        deferRender: true,
        columns: [
          {
            title: "Employee ID",
            data: "employee_id",
          },
          {
            title: 'Employee Name',
            data: 'empname',
          },
          {
            title: 'Predicted Attrition(%)',
            data: 'probability',
          },
          {
            title: 'Status',
            data: 'grade',
          }
        ],
      }

    }
  

  download($event) {
    this.csv_object.download_predictive_user_data($event, this.csv_data)
  }
}
