import { Component, OnInit, Input } from '@angular/core';
import { PredictiveService } from 'src/app/common/predictive.service';
import { Utils } from 'src/app/common/shared/utility';
import { CsvDownloaderService } from 'src/app/common/services/csv-downloader.service';
import { Observable, forkJoin } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-predictve-part',
  templateUrl: './predictve-part.component.html',
  styleUrls: ['./predictve-part.component.scss']
})
export class PredictvePartComponent implements OnInit {

  @Input() filter_data : any;
  

  tree_map_filter:any;
  prediction_year_quarter: any = "";
  parameters_data: any;
  public childData: any = { high_perform_percent: "", low_perform_percent: "" };
  filters: any = {};
  percent_achivement: any = {};
  get_parameter_service_subscribe: any;
  tier_data: any;
  tier_percent_data: any;
  emp_table_filters;
  chart_height: any = 385;
  treeMapData :any
  options: any = "true";
  barFilter: any = {}
  csv_percent_data = []
  csv_count_data = []
  serieClickEvent:any;
  pred_emp_temp:any;

  constructor(private api: PredictiveService, private modelhealth_api: PredictiveService,private csv_object: CsvDownloaderService,private _snackBar: MatSnackBar) { };
  


  ngOnDestroy() {
    if (this.get_parameter_service_subscribe) {
      this.get_parameter_service_subscribe.unsubscribe()
    }
    // this.serieClickEvent.unbind();
  }

  ngOnInit() {
  
    
  }
  ngOnChanges(changes): void {
    let self = this;    
    if (changes.filter_data && changes.filter_data.currentValue !== undefined) {
      // console.log(this.filter_data)
      // this.tree_map_filter = this.filter_data
      this.emp_table_filters = this.filter_data
      // this.fetch_parameters_data(this.filter_data)
      // this.fetch_emp_data(this.filter_data)
      // this.fetch_treemap_data(this.filter_data)
      this.get_forkjoin_data(this.filter_data)
      // Utils.setContainerState(self.currElement,'loading');
      // setTimeout(function () {
      //   self.normStackedBar();
      // }, 500);
    }
  
  }


  get_forkjoin_data(data){

   let resource1 = this.api.get_predictive_parameter_service(data)
    let resource2 =  this.api.predictive_emp_list(data)
    let resource3 = this.api.predictive_tree_map_list(data)
    let self = this
    forkJoin([resource1, resource2,resource3]).subscribe(results => {
    
      // console.log(results )
      self.parameters_data = results[0]["data"]
      self.emp_table_filters=  results[1]["data"]
      let tempTreemapdata = {}
      tempTreemapdata["filters"] = data
      tempTreemapdata["data"] = results[2]["data"]
      self.treeMapData = tempTreemapdata
      self.percent_achivement["a"] = results[2]["avg_percent"]["active"];
      self.percent_achivement["b"] = results[2]["avg_percent"]["seperated"];

      this._snackBar.open('Data Updated Successfully', 'Close', {
        duration: 3000
      });
    });
  }


  return_emp_table_details(){

    return this.tree_map_filter
  }
  
  pred_perfom_data(data) {
    this.percent_achivement["a"] = data.a;
    this.percent_achivement["b"] = data.b;
    // console.log(this.percent_achivement)
    
  }

 


  dom_to_img(event, chart_name,value_name,key,per_value) {
    
    var data = {}
    if (per_value == "true") {
      data["tier_data_predictive"] = this.tier_percent_data
    } else {
      data['tier_data_predictive'] = this.tier_data
    }  
    // Utils.dom_to_img(event.target, chart_name,value_name, key, data, per_value);
  }
  
  download($event, value_name, range_value, per_value, file_name, data) {
    var data:any
    if (per_value == "true") {
      data = this.csv_percent_data
    } else {
      data = this.csv_count_data
    }

    // this.csv_object.download_csv($event, value_name,range_value,per_value,file_name,data)
  }

  download_parameters_data(event){

    let temp_csv_parameter_data = []

    this.parameters_data.forEach(element => {

      if(element['value']<0){
        temp_csv_parameter_data.push({
        "positive_parameter":" ",
        "negative_parameter" :element['name'],
        "value":element['value']
        })
      }
      if(element['value']>=0){
        temp_csv_parameter_data.push({
          "positive_parameter": element['name'],
          "negative_parameter" :" ",
          "value":element['value']
          })
      }
    });
    
     this.csv_object.download_parameter_csv(temp_csv_parameter_data)
  }
  dom_to_image_non_stack(event, chart_name){

    Utils.dom_to_img_predict(event.target, chart_name);
  }
  // Data Inserting to modelhealth table for predictive(will change functionality in future)
  insert_model_health_data(){
    this.modelhealth_api.insert_model_health_data().subscribe((res: any) => {
      
    })
  }


}
