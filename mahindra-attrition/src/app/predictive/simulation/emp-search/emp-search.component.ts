import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { PredictiveService } from 'src/app/common/predictive.service';

@Component({
  selector: 'app-emp-search',
  templateUrl: './emp-search.component.html',
  styleUrls: ['./emp-search.component.scss']
})
export class EmpSearchComponent implements OnInit {

  constructor(private service: PredictiveService,
    public httpClient: HttpClient) { }
  model_value: any = "";

  model: any = {
    employee_id: ""
  };
  success_message: any = "";
  empname: any = {}
  emp_data: any;
  emp_details: any = {};
  employee_list: any = [];
  emp_arr: any = [];
  dtOptions: any;
  dtTrigger: any;
  @Output() public childEvent = new EventEmitter;
  ngOnInit() {

  }


  simulateSubmit() {
    // this.plot_roleTable()
    this.model = { employee_id: "" };
    this.employee_list.length = 0
    $(".text-danger").hide()

      this.model.employee_id = this.model_value
      this.model.empname = ''
      this.getEmployeeService(this.model)
    
  }
  getEmployeeService(data) {

    this.service.employee_data(data).subscribe((res: any) => {
      this.success_message = res.message      
      if (res.message) {
        $(".text-danger").show()
      } else {
        this.emp_data = res
        this.emp_data.extension_of_probation

        if (this.emp_data["manager_net_mcares_score"] !== null) {
          this.emp_details["mcares_score"] = this.emp_data["manager_net_mcares_score"]
        } else {
          this.emp_details["mcares_score"] = 15
        }

        if (this.emp_data["band"] !== null) {
          this.emp_details["band"] = this.emp_data["band"]
        } else {
          this.emp_details["band"] = 15
        }
        
        if (this.emp_data["emp_age"] !== null) {
          this.emp_details["emp_age"] = this.emp_data["age"]
        } else {
          this.emp_details["emp_age"] = 15
        }
        
        if (this.emp_data.reporting_office_age !== null) {
          
          this.emp_details["repo_age"] = this.emp_data.reporting_office_age
        } else {
          this.emp_details["repo_age"] = 15
        }

        if (this.emp_data.experience_in_current_role_in_months !== null) {
                   
          this.emp_details["experience_in_current_role"] = (this.emp_data.experience_in_current_role_in_months / 12)
        } else {
          this.emp_details["experience_in_current_role"] = 0
        }

        if (this.emp_data.total_experience_months_in_mf  !== null) {
          
          this.emp_details["total_experience_in_mf"] = (this.emp_data.total_experience_year_in_mf)
        } else {
          this.emp_details["total_experience_in_mf"] = 0
        }

        if (this.emp_data.reporting_officer_total_experience_months_in_mf !== null) {
          this.emp_details["repo_total_experience_in_mf"] = (this.emp_data.reporting_officer_total_experience_year_in_mf)
        } else {
          this.emp_details["repo_total_experience_in_mf"] = 0
        }

        if (this.emp_data.reporting_officer_experience_in_current_role_in_months !== null) {
          this.emp_details["repo_experience_in_current_role"] = (this.emp_data.reporting_officer_experience_in_current_role_in_months / 12)
        } else {
          this.emp_details["repo_experience_in_current_role"] = 0
        }

        if (this.emp_data.no_of_days_previous_quarter !== null) {
          this.emp_details["no_leaves_taken"] = this.emp_data.no_of_days_previous_quarter
        } else {
          this.emp_details["no_leaves_taken"] = 0
        }

        if (this.emp_data.no_of_days_prev_to_prev_quarter !== null) {
          this.emp_details["no_leaves_previous_taken"] = this.emp_data.no_of_days_prev_to_prev_quarter
        } else {
          this.emp_details["no_leaves_previous_taken"] = 0
        }

        if (this.emp_data.emp_lang_count !== null) {
          this.emp_details["no_lang_known"] = this.emp_data.emp_lang_count
        } else {
          this.emp_details["no_lang_known"] = 0
        }

        if (this.emp_data.repo_lang_count !== null) {
          this.emp_details["repo_no_lang_known"] = this.emp_data.repo_lang_count
        } else {
          this.emp_details["repo_no_lang_known"] = 0
        }


        if (this.emp_data.monthly_compensation_gross_previous_quarter !== null) {
          this.emp_details["monthly_compens_prev_gross"] = this.emp_data.monthly_compensation_gross_prev_to_prev_quarter
        } else {
          this.emp_details["monthly_compens_prev_gross"] = 0
        }

        if (this.emp_data.monthly_compensation_gross_previous_quarter !== null) {
          this.emp_details["monthly_compens_gross"] = this.emp_data.monthly_compensation_gross_previous_quarter
        } else {
          this.emp_details["monthly_compens_gross"] = 0
        }

        if (this.emp_data.training_previous_quarter !== null) {
          this.emp_details["training_done"] = this.emp_data.training_previous_quarter
        } else {
          this.emp_details["training_done"] = 0
        }
        if (this.emp_data.training_prev_to_prev_quarter !== null) {
          this.emp_details["training_prev_done"] = this.emp_data.training_prev_to_prev_quarter
        } else {
          this.emp_details["training_prev_done"] = 0
        }

      

        if (this.emp_data.gem_previous_quarter !== null) {
          this.emp_details["gem_performer"] = this.emp_data.gem_previous_quarter
        } else {
          this.emp_details["gem_performer"] = 0
        }
        
        if (this.emp_data.gem_prev_to_prev_quarter !== null) {
          this.emp_details["gem_prev_performer"] = this.emp_data.gem_prev_to_prev_quarter
        } else {
          this.emp_details["gem_prev_performer"] = 0
        }
        
        if (this.emp_data.star_previous_quarter !== null) {
          this.emp_details["star_performer"] = this.emp_data.star_previous_quarter
        } else {
          this.emp_details["star_performer"] = 0
        }
        if (this.emp_data.star_prev_to_prev_quarter !== null) {
          this.emp_details["star_prev_performer"] = this.emp_data.star_prev_to_prev_quarter
        } else {
          this.emp_details["star_prev_performer"] = 0
        }

        if (this.emp_data.variable_component_previous_quarter !== null) {
          this.emp_details["vari_comp"] = this.emp_data.variable_component_previous_quarter
        } else {
          this.emp_details["vari_comp"] = 0
        }

        if (this.emp_data.variable_component_prev_to_prev_quarter !== null) {
          this.emp_details["vari_prev_comp"] = this.emp_data.variable_component_prev_to_prev_quarter
        } else {
          this.emp_details["vari_prev_comp"] = 0
        }

        if (this.emp_data.annual_hike_percentage_previous_quarter !== null) {
          this.emp_details["annual_hike_percentage"] = this.emp_data.annual_hike_percentage_previous_quarter*100
        } else {
          this.emp_details["annual_hike_percentage"] = 0
        }

        if (this.emp_data.annual_hike_percentage_prev_to_prev_quarter !== null) {
          this.emp_details["annual_hike_prev_percentage"] = this.emp_data.annual_hike_percentage_prev_to_prev_quarter*100
        } else {
          this.emp_details["annual_hike_prev_percentage"] = 0
        }

        if (this.emp_data.pip_previous_quarter !== null) {
          this.emp_details["pip_act"] = this.emp_data.pip_previous_quarter
        } else {
          this.emp_details["pip_act"] = 0
        }
        if (this.emp_data.pip_prev_to_prev_quarter !== null) {
          this.emp_details["pip_prev_act"] = this.emp_data.pip_prev_to_prev_quarter
        } else {
          this.emp_details["pip_prev_act"] = 0
        }
        if (this.emp_data.fixed_component_previous_quarter !== null) {
          this.emp_details["fixed_component"] = this.emp_data.fixed_component_previous_quarter
        } else {
          this.emp_details["fixed_component"] = 0
        }

        if (this.emp_data.promotion_category =='NA' ) {
          this.emp_details["promotion"] = "promotion_category_NA"
        } if(this.emp_data.promotion_category =='<=2years') {
          this.emp_details["promotion"] = "promotion_category_2years"
        }if(this.emp_data.promotion_category == '<=4years') {
          this.emp_details["promotion"] = "promotion_category_4years"
        }if(this.emp_data.promotion_category =='<=9years') {
          this.emp_details["promotion"] = "promotion_category_9years"
          
        }

        this.emp_details["gender"] = this.emp_data.emp_gender
        this.emp_details["marital_status"] = this.emp_data.emp_marital_status
        this.emp_details["emp_qualif"] = this.emp_data.emp_qualif_cat
        this.emp_details["repo_qualif"] = this.emp_data.repo_qualif_cat
        this.emp_details["emp_repo_location"] = this.emp_data.location_emp_rep
        this.emp_details["employee_id"] = this.model.employee_id
        
        if (this.emp_data.dic_previous_quarter !== null) {
          this.emp_details["disp_act"] = this.emp_data.dic_previous_quarter
        } else {
          this.emp_details["disp_act"] = 0
        }
        if (this.emp_data.dic_prev_to_prev_quarter !== null) {
          this.emp_details["disp_prev_act"] = this.emp_data.dic_prev_to_prev_quarter
        } else {
          this.emp_details["disp_prev_act"] = 0
        }

        // console.log(this.emp_details)
        // this.emp_details["disp_act"] = this.emp_data.dic_previous_quarter

        this.childEvent.emit(this.emp_details)
      }
    })
  }

  // ngOnDestroy(): void {
  //   //unsubscribe the event
  //   this.dtTrigger.unsubscribe();
  // }

  // rerender(): void {
  //   this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //     // Destroy the table first
  //     dtInstance.destroy();
  //     // Call the dtTrigger to rerender again
  //     this.dtTrigger.next();
  //   });
  // }

  // plot_roleTable() {
  //   let self = this;
  //   this.dtOptions = {
  //     data: this.emp_arr,
  //     columns: [{
  //       title: 'Employee Id',
  //       data: 'empid',
  //       className: "employee_id"
  //     },
  //     {
  //       title: 'Name',
  //       data: 'empname',

  //     },
  //     {
  //       title: 'Grade',
  //       data: 'grade_name',
  //     },
  //     {
  //       title: 'Operational Category',
  //       data: 'oprn_category',
  //     }],
  //     rowCallback: (row: Node, data: any[] | Object, index: number) => {
  //       $('td', row).unbind('click');
  //       $('td', row).bind('click', () => {
  //         $(".text-danger").hide()
  //         this.model.employee_id = data["empid"]
  //         this.getEmployeeService(this.model)
  //       });
  //     }
  //   };
  // }

}
