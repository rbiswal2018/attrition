import { Component, OnInit } from '@angular/core';
import { PredictiveService } from 'src/app/common/predictive.service';
import { Utils } from 'src/app/common/shared/utility';
import { CsvDownloaderService } from 'src/app/common/services/csv-downloader.service';

@Component({
  selector: 'app-simulation',
  templateUrl: './simulation.component.html',
  styleUrls: ['./simulation.component.scss']
})
export class SimulationComponent implements OnInit {
  line_chart_data:any

  monthly_compens_gross_percentage =0
  fixed_component_percentage=0 
  component_percentage_values = [0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100]

  pred_quarter = ""
  model: any = {
    employee_id:"",
    emp_age: 15,
    repo_age: 15,
    mcares_score:0,
    band:"",
    fixed_component:0,
    total_experience_in_mf: 0,
    repo_total_experience_in_mf: 0,
    no_leaves_taken: 0,
    no_leaves_previous_taken: 0,
    no_lang_known: 1,
    star_performer: 0,
    star_prev_performer:0,
    gem_performer: 0,
    gem_prev_performer :0,
    promotion: "promotion_category_2years",
    training_done: 0,
    training_prev_done:0,
    marital_status: "",
    emp_qualif: "",
    repo_qualif : "",
    emp_repo_location: "",
    disp_act: 0,
    pip_act : 0,
    monthly_compens_gross: 5000,
    annual_hike_percentage: 0,
    gender:0,
    repo_no_lang_known : 1,
  };

  temp_fixed_component = this.model.fixed_component
  temp_monthly_compensation  = this.model.monthly_compens_gross
  cal_temp_fixed_component = this.model.fixed_component
  cal_temp_monthly_compensation = this.model.monthly_compens_gross

  public childData: any;
  dur_mf: number = 0;
  ageList: any = [15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
    40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
    60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70];
  ageLabel: any = [15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70];
  exp_curnt_role_list: any = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
  exp_curnt_role_label_list: any = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
  totalExpList: any = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
  totalExpList_label: any = [0, 5, 10, 15, 20, 25, 30];
  langKnwList: any = [1, 2, 3, 4, 5];
  mcares_score_list: any = [1, 2, 3, 4, 5];
  hikePerList: any = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
    40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
    60];
  hikePerLabel: any = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60]
  state_list: any = [];
  selected_language: any = [];
  state_data: any;
  simulate_res: any = {
    data: {},
  };

  limeChart_data_list: any ;
  limeChart_data_list_par: any ;

  marked: boolean = false;
  isChecked: boolean = false;
  survival_data:any;

  constructor(private service: PredictiveService,private csv_object: CsvDownloaderService) { }
  ngOnInit() {
    // this.getStateLangData();
    this.pred_quarter = localStorage.getItem("pre_q_year")
   
    $(".form-check-box").prop("checked", false);

  }

  // ngOnChanges(changes:void) {
    
  //   let self = this
  //   if (changes.globalFilter.currentValue != undefined) {

  // }

  monthly_compens_change(event){
    // console.log(event.target["value"])
    this.temp_monthly_compensation = this.cal_temp_monthly_compensation+ (( this.cal_temp_monthly_compensation/100)*event.target["value"])
  // console.log(this.temp_monthly_compensation)
  }
  fixed_component_change(event){
    // console.log(event.target["value"])

    this.temp_fixed_component =this.cal_temp_fixed_component+ ((this.cal_temp_fixed_component/100)*event.target["value"])
  }

  emp_details(data){
   
    this.model = data

    this.model.oprn_category = data.oprn_category

    this.temp_fixed_component = this.model.fixed_component
    this.temp_monthly_compensation = this.model.monthly_compens_gross
    this.cal_temp_fixed_component = this.model.fixed_component
    this.cal_temp_monthly_compensation = this.model.monthly_compens_gross
    this.model.oprn_category = data.oprn_category
    this.monthly_compens_gross_percentage =0
    this.fixed_component_percentage=0

    
    
    if (data.gender=='Male'){
      this.model.gender =0
     

    }
    
    if (data.gender=='Female'){
      this.model.gender =1
  
    }

   

  }
  

  

 yvalue(){


}
  reset() {
    this.model.emp_age = 15;
    this.model.total_experience_in_mf = 0;
    this.model.no_leaves_taken = 0;
    this.model.no_lang_known = 1;
    this.model.star_performer = 0;
    this.model.gem_performer = 0;
    this.model.promotion = "promotion_category_2years";
    this.model.training_done = 0;
    this.model.marital_status = "";
    this.model.emp_qualif = "";
    this.model.repo_qualif  = "";
    this.model.emp_repo_location = "";
    this.model.emp_repo_depart = "";
    this.model.mother_tongue = "";
    this.model.prev_q_perform1 = "";
    this.model.prev_q_perform2 = "";
    this.model.repo_total_experience_in_mf = 0;
    this.model.repo_age = 0;
    this.model.disp_act = 0;
    this.model.pip_act  = 0;
    this.model.gender = 0;
    this.model.monthly_compens_gross = 5000
    this.model.annual_hike_percentage = 0,
    this.model.fixed_component= 0,
    this.model.repo_no_lang_known = 1;
    this.monthly_compens_gross_percentage = 0
    this.fixed_component_percentage = 0
    this.temp_fixed_component = this.model.fixed_component
    this.temp_monthly_compensation  = this.model.monthly_compens_gross
    this.cal_temp_fixed_component = this.model.fixed_component
    this.cal_temp_monthly_compensation = this.model.monthly_compens_gross

    
  }


  yearData  = ["ESBC","EHBC"]
  // getStateLangData() {
  //   this.service.state_list().subscribe((data: any) => {
  //     this.state_data = data
  //     this.state_list = Object.keys(this.state_data.data);
  //     this.selected_language = Object.values(this.state_data.data);
  //     this.selected_language = _.unionBy(this.selected_language);
  //   })
  // }
  toggleVisibility(e) {
    this.marked = e.target.checked;
    if (this.marked === true) {
      this.model.prev_q_perform1 = ""
      this.model.prev_q_perform2 = ""
    }
  }
  toggleVisibility_repo(e) {
    this.isChecked = e.target.checked;
    if (this.isChecked === true) {
      this.model.repo_total_experience_in_mf = 0;
      this.model.repo_experience_in_current_role = 0;
      this.model.repo_age = 0;
      this.model.repo_qualif  = "";
      this.model.repo_no_lang_known = 1
    }
  }
  simulateSubmit(value) {
    this.simulate_res["message"] = 'none'
    let data_list = []
    this.temp_monthly_compensation = this.cal_temp_monthly_compensation+ (( this.cal_temp_monthly_compensation/100)*this.monthly_compens_gross_percentage)
    this.temp_fixed_component =this.cal_temp_fixed_component+ ((this.cal_temp_fixed_component/100)*this.fixed_component_percentage)

    this.model.monthly_compens_gross =this.temp_monthly_compensation
    this.model.fixed_component =this.temp_fixed_component

    var newModel = Object.assign({}, this.model);
    newModel.experience_in_current_role = (this.model.experience_in_current_role) ;
    newModel.total_experience_in_mf = (this.model.total_experience_in_mf) ;
    newModel.repo_experience_in_current_role = (this.model.repo_experience_in_current_role) ;
    newModel.repo_total_experience_in_mf = (this.model.repo_total_experience_in_mf) ;
    this.service.simulation_data(newModel).subscribe((res: any) => {    
      //Lime chart response
      this.simulate_res["input_prediction"] = res["input_prediction"];
      // predictive data response 
      // Employee Demographic Details 
      this.simulate_res["data"].marital_status_Single = res["data"].emp_marital_status_Single;
      this.simulate_res["data"].marital_status_Married = res["data"].emp_marital_status_Married;
      this.simulate_res["data"].emp_qualif_cat_post_grad = res["data"].emp_qualif_cat_post_grad;
      this.simulate_res["data"].emp_qualif_cat_under_grad = res["data"].emp_qualif_cat_under_grad;
      this.simulate_res["data"].emp_qualif_cat_grad = res["data"].emp_qualif_cat_grad;
      this.simulate_res["data"].emp_lang_count = res["data"].emp_lang_count;
      this.simulate_res["data"].emp_age = res["data"].age;

      // Employee Professional Details      
      this.simulate_res["data"].total_experience_months_in_mf = (res["data"].total_experience_year_in_mf).toFixed(1);
      // this.simulate_res["data"].experience_in_current_role_in_months = (res["data"].experience_in_current_role_in_months / 12).toFixed(1);
      this.simulate_res["data"].location_emp_rep_same = res["data"].location_emp_rep_same;
      this.simulate_res["data"].location_emp_rep_different = res["data"].location_emp_rep_different;
      // this.simulate_res["data"].department_emp_rep_same = res["data"].department_emp_rep_same;
      // this.simulate_res["data"].department_emp_rep_different = res["data"].department_emp_rep_different;
 
         
   
      // console.log(this.simulate_res["data"])
      // RBMI Factors
    
      
      // Reporting Officer Details
      this.simulate_res["data"].repo_lang_count = res["data"].repo_lang_count;
      this.simulate_res["data"].repo_office_age = res["data"].reporting_office_age;
      this.simulate_res["data"].repo_qualif_cat_post_grad = res["data"].repo_qualif_cat_post_grad;
      this.simulate_res["data"].repo_qualif_cat_under_grad = res["data"].repo_qualif_cat_under_grad;
      this.simulate_res["data"].repo_qualif_cat_grad = res["data"].repo_qualif_cat_grad;
      // this.simulate_res["data"].reporting_officer_experience_in_current_role_in_months = (res["data"].reporting_officer_experience_in_current_role_in_months / 12).toFixed(1);
      this.simulate_res["data"].reporting_officer_total_experience_months_in_mf = (res["data"].reporting_officer_total_experience_year_in_mf).toFixed(1);
      
      // Rewards Details
      this.simulate_res["data"].gem_performer = res["data"].gem_previous_quarter;
      this.simulate_res["data"].star_performer = res["data"].star_previous_quarter;
      this.simulate_res["data"].gem_prev_performer = res["data"].gem_prev_to_prev_quarter;
      this.simulate_res["data"].star_prev_performer = res["data"].star_prev_to_prev_quarter;

      // band    
      this.simulate_res["data"].band_depertment = res["data"]["band_DEPATMENT HEAD"];
      this.simulate_res["data"].band_EXECUTIVE = res["data"].band_EXECUTIVE;  
      this.simulate_res["data"].band_MANAGERIAL = res["data"].band_MANAGERIAL; 
      this.simulate_res["data"].band_OPERATIONAL= res["data"].band_OPERATIONAL;   
      this.simulate_res["data"].oprn_STRATEGIC = res["data"].band_STRATEGIC; 

      // Organisational Changes
      this.simulate_res["data"]["promotion_category_2years"] = res["data"]["promotion_category_<=2years"];
      this.simulate_res["data"]["promotion_category_4years"] = res["data"]["promotion_category_<=4years"];
      this.simulate_res["data"]["promotion_category_9years"] = res["data"]["promotion_category_<=9years"];
      this.simulate_res["data"]["promotion_category_NA"] = res["data"]["promotion_category_NA"];
      

      // Others
      this.simulate_res["data"].pip_previous_quarter = res["data"].pip_previous_quarter;
      // this.simulate_res["data"].pip_prev_to_prev_quarter = res["data"].pip_prev_to_prev_quarter;


      this.simulate_res["data"].no_of_days_previous_quarter = res["data"].no_of_days_previous_quarter;
      this.simulate_res["data"].no_of_days_prev_to_prev_quarter = res["data"].no_of_days_prev_to_prev_quarter;


      // this.simulate_res["data"].variable_component_previous_quarter = res["data"].variable_component_previous_quarter;  
      // this.simulate_res["data"].variable_component_prev_to_prev_quarter= res["data"].variable_component_prev_to_prev_quarter;      
    
      // this.simulate_res["data"].dic_2018_q3_0 = res["data"].dic_previous_quarter;
      // this.simulate_res["data"].dic_prev_to_prev_quarter = res["data"].dic_prev_to_prev_quarter;
      this.simulate_res["data"].dic_previous_quarter = res["data"].dic_previous_quarter;


      this.simulate_res["data"].training_done = res["data"].training_previous_quarter;
      this.simulate_res["data"].training_prev_done = res["data"].training_prev_to_prev_quarter;

    
      this.simulate_res['data'].monthly_compensation_gross_previous_quarter = res["data"].monthly_compensation_gross_previous_quarter;
      // this.simulate_res['data'].monthly_compensation_gross_prev_to_prev_quarter = res["data"].monthly_compensation_gross_prev_to_prev_quarter;

      this.simulate_res["data"].annual_hike_percentage = res["data"].annual_hike_percentage_previous_quarter;
      // this.simulate_res["data"].annual_hike_percentage_prev_to_prev_quarter = res["data"].annual_hike_percentage_prev_to_prev_quarter;
      this.simulate_res["data"].fixed_component = res["data"].fixed_component_previous_quarter;


      if (res["data"].emp_gender_Female==1){
        this.simulate_res["data"].emp_gender_Female =1
        this.simulate_res["data"].emp_gender_Male = 0

      }
      
      if (res["data"].emp_gender_Male==1){
        this.simulate_res["data"].emp_gender_Female =0
        this.simulate_res["data"].emp_gender_Male = 1

      }
      
      this.simulate_res["message"] = res["message"];
    
    })
  

    // lime graph api
    let temp={}
    temp["type"] = "simulation"
    temp["parameters"] = newModel
    this.limeChart_data_list_par = temp
    // console.log(this.limeChart_data_list_par)
    this.service.lime_explainer_graph_data(newModel).subscribe((res: any) => {
      let self = this
      self.limeChart_data_list = res.data
            
    })
    this.service.survival_curve_data(newModel).subscribe((res: any) => {
      let self = this
      let temp_object = {}
      temp_object["line_data"] = res.data
      temp_object["key"] = "simulation"
      this.line_chart_data = temp_object
 
    })

    // let self = this
    // let temp_object = {}
    // temp_object["line_data"] = newModel
    // temp_object["key"] = "simulation"
    // self.line_chart_data = temp_object

  }

  dom_to_img(event, chart_name) {
   
    Utils.dom_to_img_predict(event.target, chart_name);
  }

  download_parameters_data(event){

    let temp_csv_parameter_data = []

    this.limeChart_data_list.forEach(element => {

      if(element['value']<0){
        temp_csv_parameter_data.push({
        "positive_parameter":" ",
        "negative_parameter" :element['name'],
        "value":element['value']
        })
      }
      if(element['value']>=0){
        temp_csv_parameter_data.push({
          "positive_parameter": element['name'],
          "negative_parameter" :" ",
          "value":element['value']
          })
      }
    });
    this.csv_object.download_parameter_csv(temp_csv_parameter_data)
  }

}
