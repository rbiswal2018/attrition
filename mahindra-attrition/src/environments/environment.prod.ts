const testIP = 'http://68.183.80.87:9000/';
const serverIP = 'http://172.30.23.101:9000/';
const localIP = 'http://127.0.0.1:8000/';

export const environment = {
  production: true,
  apiUrl: serverIP
};
