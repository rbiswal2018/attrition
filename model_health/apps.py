from django.apps import AppConfig


class ModelHealthConfig(AppConfig):
    name = 'model_health'
