from django.db import models

# Create your models here.
from django.db import models


class kedro_modelhealth_output(models.Model):

    training_rows = models.IntegerField()
    testing_rows = models.IntegerField()
    test_accuracy = models.FloatField()
    train_accuracy = models.FloatField()
    test_recall = models.FloatField()
    train_recall = models.FloatField()
    train_precision = models.FloatField()
    test_precision = models.FloatField()
    train_f1 = models.FloatField()
    test_f1 = models.FloatField()
    # auc_train = models.FloatField()
    auc_test = models.FloatField()
    pred_quarter =  models.TextField()
    pred_year =models.IntegerField()
    train_year = models.IntegerField()
    train_quarter = models.TextField()
    model_version = models.TextField()
    task_status = models.TextField()

    class Meta():
        managed = True
        db_table = 'predictive\".\"modelhealth_output_kedro'


class modelhealth_output(models.Model):

    training_rows = models.IntegerField()
    testing_rows = models.IntegerField()
    test_accuracy = models.FloatField()
    train_accuracy = models.FloatField()
    test_recall = models.FloatField()
    train_recall = models.FloatField()
    train_precision = models.FloatField()
    test_precision = models.FloatField()
    train_f1 = models.FloatField()
    test_f1 = models.FloatField()
    # auc_train = models.FloatField()
    auc_test = models.FloatField()
    pred_quarter =  models.TextField()
    pred_year =models.IntegerField()
    train_year = models.IntegerField()
    train_quarter = models.TextField()

    class Meta():
        managed = True
        db_table = 'predictive\".\"modelhealth_output'

