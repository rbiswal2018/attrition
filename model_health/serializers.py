from rest_framework import serializers
from model_health.models import *


class model_health_Serializer(serializers.ModelSerializer):
    class Meta:
        model = modelhealth_output
        #fields = ('model_version','task_status',)
        fields = '__all__'

class model_health_put_Serializer(serializers.ModelSerializer):
    class Meta:
        model = kedro_modelhealth_output
        fields ='__all__'
