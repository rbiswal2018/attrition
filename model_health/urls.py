from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from model_health import views


urlpatterns = [
    path('roc_chart/', views.ModelHealth.get_roc_curve_mf),
    path('insights/', views.ModelHealth.get_model_health_output),
    # path('load_insights/', views.ModelHealth.model_health),
    # path('feature_importance/', views.ModelHealth.class_feature_importance),
    # path('parameters_data/', views.ModelHealth.get_parameters_data),
    path('insertmodelhealth/', views.insert_modelhealth_output),
    path('get_model_observation/', views.get_model_observation)
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

