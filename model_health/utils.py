from authentication.util import get_cursor
import pdb

def format_query_output():
   
    try:
        result = []
        final_query = "select * from predictive.modelhealth_output"
        query_obj = get_cursor(final_query)
        query_data = query_obj.fetchall()
        query_columns = [col[0] for col in query_obj.description]
        for row in query_data:
            temp_disc = dict(set(zip(query_columns, row)))
            result.append({"ModelVersion":  temp_disc["model_version"],
    # "AucTrain":temp_disc["auc_train"],
    # "TrainPrecision": temp_disc["train_precision"],
    "TestPrecision": temp_disc["test_precision"],
    "TestingRows": temp_disc["testing_rows"],
    "TestRecall": temp_disc["test_recall"],
    "TrainYear":  temp_disc["train_year"],
    "TrainQuarter": temp_disc["train_quarter"],
    # "TestAccuracy":  temp_disc["test_accuracy"],
    # "TrainF1":temp_disc["train_f1"],
    # "AucTest": temp_disc["auc_test"],
    "PredQuarter":  temp_disc["pred_quarter"],
    # "TrainRecall": temp_disc["train_recall"],
    "TrainingRows":  temp_disc["training_rows"],
    "TrainAccuracy": temp_disc["train_accuracy"],
    "TestF1":temp_disc["test_f1"],
    "TaskStatus": temp_disc["task_status"],
    # "PredYear": temp_disc["pred_year"]
            })
        return result
    except Exception as e:
        print(e)