from decimal import Decimal

from django.http import JsonResponse

from sklearn.preprocessing import label_binarize
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score, accuracy_score, recall_score, precision_score, roc_curve, auc
from rest_framework.response import Response
import pdb
import re
# from model_health.querys import pred_year_month_query

from rest_framework import status
from authentication.util import get_cursor
from model_health.utils import format_query_output

from model_health.models import modelhealth_output
from rest_framework.decorators import api_view, permission_classes

from authentication.permissions import IsAuthenticatedUser
from load_default.views import load_default_files

import logging
from model_health.serializers import model_health_Serializer

# from model_health.serializers import model_health_Serializer

logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)


class ModelHealth:
    final_data = load_default_files.model_health_data
    target_col = 'employee_status'
    model = load_default_files.model_data

    def split_data(final_data, target_col):
        try:
            if target_col in final_data.columns:
                X = final_data.drop(labels=target_col, axis=1)
                y = final_data[target_col]
                X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=101)
                return X, y, X_train, X_test, y_train, y_test
        except Exception as e:
            logger.info("exception occurred in split_data: ", e)

    def roc_curve_mf(final_data, model, target_col):
        try:
            roc_vals = dict()
            fpr = dict()
            tpr = dict()
            X, y, X_train, X_test, y_train, y_test = ModelHealth.split_data(final_data.drop('employee_id', axis=1), target_col)
            y_pred_proba = model.predict_proba(X_test)
            y1 = label_binarize(y_test, classes=['Active', 'Separated'])
            n_classes = y1.shape[1]
            y_preds = y_pred_proba[:, 1]
            fpr, tpr, _ = roc_curve(y1, y_preds)
            roc_vals['auc'] = auc(fpr, tpr)
            roc_vals['false_pos'] = fpr
            roc_vals['true_pos'] = tpr

            return roc_vals
        except Exception as e:
            # print (e)
            logger.info("exception occurred in roc_curve_mf: ", e)
            pass

    @api_view(['GET'])
    # @permission_classes((IsAuthenticatedUser,))
    def get_roc_curve_mf(request):
        try:
            # target_col = ModelHealth.target_col

            list_data_A = []
            roc_vals = ModelHealth.roc_curve_mf(ModelHealth.final_data, ModelHealth.model, ModelHealth.target_col)
            for i in range(0, roc_vals["true_pos"].size):
                data = dict()
                data["value"] = round(roc_vals["true_pos"][i], 2)
                data["name"] = round(roc_vals["false_pos"][i], 2)
                list_data_A.append(data)
            return Response({"message": "Success", "data_A": list_data_A, "Auc_A": round(roc_vals['auc'] * 100, 2)})
        except Exception as e:
            # print(e)
            logger.error("Error from get_roc_curve_mf: %s" % e)
            # logger.info("exception occurred3: ", e)
    def month2quarter_convert():
        try:
            month_to_quarter_list = [(["1", "2", "3"], 4), (["4", "5", "6"], 1), (["7", "8", "9"], 2),
                                     (["10", "11", "12"], 2)]
            pred_quarter_list = {"Q1": "Q2", "Q2": "Q3", "Q3": "Q4", "Q4": "Q1"}
            temp_dict = {}
            query_obj = get_cursor(pred_year_month_query)
            query_data = query_obj.fetchall()
            query_columns = [col[0] for col in query_obj.description]
            for row in query_data:
                temp_dict = dict(set(zip(query_columns, row)))
            yearquarter = temp_dict["year_quarter"]
            train_quarter = ""
            pred_year = ""
            train_year = str(yearquarter)[0:4]
            pred_quarter = ""
            for months, quarter in month_to_quarter_list:
                if str(yearquarter[-2:]).strip("0") in months:
                    train_quarter = "Q" + str(quarter)
            if train_quarter == "Q1" or train_quarter == "Q2":
                pred_year = int(train_year)
                train_year = int(train_year)
                pred_quarter = pred_quarter_list[train_quarter]
            if train_quarter == "Q4":
                train_year = int(train_year) - 1
                pred_year = train_year + 1
                pred_quarter = pred_quarter_list[train_quarter]
            if train_quarter == "Q3":
                train_year = int(train_year)
                pred_year = train_year
                pred_quarter = pred_quarter_list[train_quarter]

            return train_year, train_quarter, pred_year, pred_quarter
        except Exception as e:
            # print('in month2quarter_convert',e)
            logger.info("exception occurred in month2quarter_convert: ", e)

    @staticmethod
    def model_health(model, final_data, target_col):
        """
        {
        params::
            model: class pickle file
            final_data: csv file shared
            target_col: 'performance_category_2018_q4' (persent in csv file shared)
        output::
            output_dict: 'accuracy,precision,recall, f1 score,
                          prediction/train quarter and year for train & test'
        }
        """
        try:
            output_dict = dict()
            if 'employee_id' in final_data.columns:
                final_data = final_data.drop('employee_id', axis=1)
            X, y, X_train, X_test, y_train, y_test = ModelHealth.split_data(final_data, target_col)
            y_train_pred = model.predict(X_train)
            y_test_pred = model.predict(X_test)
            output_dict['training_rows'] = X_train.shape[0]
            output_dict['testing_rows'] = X_test.shape[0]
            output_dict['train_accuracy'] = round(accuracy_score(y_train, y_train_pred), 3)
            output_dict['test_accuracy'] = round(accuracy_score(y_test, y_test_pred), 3)
            output_dict['train_precision'] = round(precision_score(y_train, y_train_pred, average='weighted'), 3)
            output_dict['test_precision'] = round(precision_score(y_test, y_test_pred, average='weighted'), 3)
            output_dict['train_recall'] = round(recall_score(y_train, y_train_pred, average='weighted'), 3)
            output_dict['test_recall'] = round(recall_score(y_test, y_test_pred, average='weighted'), 3)
            output_dict['train_f1'] = round(recall_score(y_train, y_train_pred, average='weighted'), 3)
            output_dict['test_f1'] = round(recall_score(y_test, y_test_pred, average='weighted'), 3)
            output_dict['auc_test'] = round(recall_score(y_test, y_test_pred, average='weighted'), 3)
            all_quarters = []
            for i in final_data.columns:
                all_quarters.extend(re.findall('Q+.', i.upper()))
                all_quarters.extend(re.findall('[0-9]+', i.upper()))
            
            # train_year, train_quarter, pred_year, pred_quarter = ModelHealth.month2quarter_convert()
            output_dict['train_year'] = 2020
            output_dict['train_quarter'] = 'q1'
            output_dict['pred_quarter'] = 'q2'
            output_dict['pred_year'] = 2020

            # pdb.set_trace()
            serializer = model_health_Serializer(data=output_dict)
            if serializer.is_valid():
                temp_rows = modelhealth_output.objects.filter(pred_year=output_dict['pred_year'])
                if len(temp_rows) == 0:
                    serializer.save()
                else:
                    pred_quarters = modelhealth_output.objects.filter(pred_year=output_dict['pred_year'],
                                                                      pred_quarter=output_dict['pred_quarter'])
                    if len(pred_quarters) > 0:
                        if abs((Decimal(pred_quarters[0].test_accuracy) * 100 - Decimal(
                                output_dict['test_accuracy']) * 100)) > 5:
                            serializer = model_health_Serializer(data=output_dict)
                            serializer.update(pred_quarters[0], output_dict)
                    else:
                        serializer.save()
            else:
                return Response({"message": "failed, not a valid serializer", "error": serializer.errors})
            return Response({"message": "Success", "data": output_dict})

        except Exception as e:
            logger.info("exception occurred5: ", e)
            return Response({"message": "failed"})

    @api_view(['GET'])
    @permission_classes((IsAuthenticatedUser,))
    def get_model_health_output(request):
        # to get the data from modelhealth_db
        try:
            # pdb.set_trace()
            queryset = modelhealth_output.objects.latest("pred_year", "pred_quarter")
            partner_serializer_data = model_health_Serializer(queryset)
            return Response({"message": "Success", "data": partner_serializer_data.data}, status=status.HTTP_200_OK)
        except Exception as e:
            logger.info("exception occurred6: ", e)
            return Response({"message": "failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# ModelHealth.model_health
@api_view(['GET'])
@permission_classes((IsAuthenticatedUser,))
def insert_modelhealth_output(request):
    try:
        ModelHealth.model_health(ModelHealth.model, ModelHealth.final_data, ModelHealth.target_col)
        return JsonResponse({"message": "Successfully data inserted"}, status=status.HTTP_200_OK)

    except Exception as e:
        return JsonResponse({"message": "failed"}, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
@permission_classes((IsAuthenticatedUser,))
def get_model_observation(request):
    try:
        result = {}
        result["data"] = format_query_output()
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)
