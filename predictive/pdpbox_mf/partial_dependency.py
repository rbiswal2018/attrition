from sklearn.model_selection import train_test_split
from pdpbox_mf import info_plots_mf
import pandas as pd
import pickle


final_data = pd.read_csv('final_mf_data.csv')
final_data  = final_data[final_data.columns[1:]]
rf_model = pickle.load(open('mf_rfmodel.pkl','rb'))


def split_data(final_data,target_col):
    try:
        if target_col in final_data.columns:
            X =  final_data.drop(labels=target_col,axis=1)
            pred_quarter = re.search('q.',target_col)[0].upper()
            pred_year = re.search('[0-9]\d+',target_col)[0]
            y = final_data[target_col]
            X_train,X_test,y_train,y_test = train_test_split(X,y,random_state=101)
            return X,y, X_train,X_test,y_train,y_test,pred_quarter,pred_year
    except Exception as e:
        print (e)



def pdp_plot(data,target_col,model,feature_col):
	X,y, X_train,X_test,y_train,y_test,pred_quarter,pred_year = split_data(data,target_col)
	try:
	    ignore_keys = ['actual_prediction_q1','actual_prediction_q2']
	    plot_data = info_plots_mf.actual_plot(figsize=(20,10),
	    model=model, X=data, feature=feature_col, feature_name= feature_col)
	    op_dict =  {key: value for key,value in plot_data.items() if key not in ignore_keys}
	    op_dict.update({'pred_year':pred_year})
	    return op_dict

	except Exception as e:
        print (e)