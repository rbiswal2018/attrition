predict_query = """select  distinct employee_id, COALESCE(first_name, '') || ' ' || COALESCE(last_name, '') as empname,
round(probability::decimal,2)*100 as probability,	Grade as grade from predictive.predictive_output 
where {clause}  order by employee_id  """

state_query = "select distinct state from predictive.predictive_treemap_output order by state"

branch_query = "select distinct branch from predictive.predictive_treemap_output order by branch"

function_query = "select distinct function_name from predictive.predictive_treemap_output where function_name not in ('MAHINDRA ASSET MANAGEMENT CO. PVT. LTD.')"

business_unit_query = 'select distinct business_unit from predictive.predictive_treemap_output order by business_unit'
division_query = 'select distinct division from predictive.predictive_treemap_output order by division'
department_query = 'select distinct department from predictive.predictive_treemap_output order by department'
sub_department_query = 'select distinct sub_department from predictive.predictive_treemap_output order by sub_department'
band_query = "select distinct band from predictive.predictive_treemap_output"
cost_centre_query = 'select distinct cost_centre from predictive.predictive_treemap_output order by cost_centre'
work_contract_query = 'select distinct work_contract from predictive.predictive_treemap_output'
grade_query = "select distinct grade_designation from predictive.predictive_treemap_output where grade_designation not in ('A1','A2','A4')"

tree_map_query = """select coalesce(name,'N_A') as name,case when coalesce(active,0)+seperated = 0 then 0 else
seperated/((coalesce(active,0)+seperated) ::float)*100::numeric end  as
total_count from (
select {select} as name,count(distinct  case when preds = 'Active'  then employee_id else null end
) as active ,
count(distinct  case when  preds = 'Seperated'  then employee_id else null end
) as seperated 
from predictive.predictive_treemap_output
where {where} group by {select} ) Inner_query """


active_seperated_query = """select case when coalesce(active,0)+seperated = 0 then 0 else
active/((coalesce(active,0)+seperated) ::float)*100::numeric end  as
active,case when coalesce(active,0)+seperated = 0 then 0 else
seperated/((coalesce(active,0)+seperated) ::float)*100::numeric end  as
seperated from (
select count(distinct  case when preds = 'Active'  then employee_id else null end
) as active ,
count(distinct  case when  preds = 'Seperated'  then employee_id else null end
) as seperated 
from predictive.predictive_treemap_output
where {where} ) Inner_query 
"""

quater_year_query = """select distinct pred_quarter ,pred_year ,train_year ,train_quarter  
from predictive.modelhealth_output """
