from django.urls import path, include
from predictive import views


urlpatterns = [
    path('quater_year_api/', views.quater_year_api),
    path('state_predictive/', views.get_tree_map_data),
    path('pdb_curve/', views.get_pdb_data),
    path('pdb_dropdown/', views.StatePredictive.get_pdb_dropdown),
    path('parameter_predict_data/', views.class_group_importance),
    path('get_emp_predict/', views.get_emp_predict_data),
    path('predicitve_drop_down_data/', views.get_predictive_drop_down_data)

]
