import pickle
import pandas as pd
import pdb
import numpy as np
from load_default.views import load_default_files
from sklearn.preprocessing import scale
import logging
import re
from authentication.util import get_cursor
import warnings
from predictive.pdpbox_mf import info_plots_mf
from authentication.models import UserProfile, CustomPermissions, Group
from analytics.utils import where_condition
import  pdb
from predictive.query import *
import copy
logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)
dict_feat_clarity = {'band_DEPATMENT HEAD': 'Emp. band - Department Head',
 'band_EXECUTIVE': 'Emp. band - Executive',
 'band_MANAGERIAL': 'Emp. band - Managerial',
 'band_OPERATIONAL': 'Emp. band - Operational',
 'band_STRATEGIC': 'Emp. band - Strategic',
 'total_experience_year_in_mf': 'Total Experience of Employee in MF(years)',
 'reporting_officer_total_experience_year_in_mf': 'Total Experience of Reporting officer in MF(years)',
 'manager_net_mcares_score': 'Managers MCARES Score',
 'age': 'Employee Age',
 'reporting_office_age': 'Reporting officer Age',
 'gem_prev_to_prev_quarter': 'Gem Award in Previous to Previous Quarter',
 'gem_previous_quarter': 'Gem Award in Previous Quarter',
 'star_prev_to_prev_quarter': 'Star Award in Previous to Previous Quarter',
 'star_previous_quarter': 'Star Award in Previous Quarter',
 'monthly_compensation_gross_previous_quarter': 'Emp. Monthly Compensation',
 'fixed_component_previous_quarter': 'Emp. Fixed Component',
 'annual_hike_percentage_previous_quarter': 'Emp. Annual Hike(%)',
 'no_of_days_prev_to_prev_quarter': 'Leaves in Previous to Previous Quarter(days)',
 'no_of_days_previous_quarter': 'Leaves in Previous Quarter(days)',
 'emp_lang_count': 'Employee Languages Count',
 'repo_lang_count': 'Reporting Officer Languages Count',
 'emp_marital_status_Married': 'Emp. Marital Status - Married',
 'emp_marital_status_Single': 'Emp. Marital Status - Single',
 'emp_gender_Female': 'Emp. Gender - Female',
 'emp_gender_Male': 'Emp. Gender - Male',
 'emp_qualif_cat_grad': 'Emp. Qualification - Graduate',
 'emp_qualif_cat_post_grad': 'Emp. Qualification - Post Graduate',
 'emp_qualif_cat_under_grad': 'Emp. Qualification - Under Graduate',
 'repo_qualif_cat_grad': 'Reporting Officer Qualification - Graduate',
 'repo_qualif_cat_post_grad': 'Reporting Officer Qualification - Post Graduate',
 'repo_qualif_cat_under_grad': 'Reporting Officer Qualification - Under Graduate',
 'training_prev_to_prev_quarter': 'Emp. Training in Previous to Previous Quarter',
 'training_previous_quarter': 'Emp. Training in Previous Quarter',
 'dic_previous_quarter': 'Disc. in Previous Quarter',
 'pip_previous_quarter': 'PIP in Previous Quarter',
 'location_emp_rep_same': 'Location of Emp. & Reporting Officer - Same',
 'location_emp_rep_different': 'Location of Emp. & Reporting Officer - Different',
 'promotion_category_<=2years': 'Emp. Promoted within 2 years',
 'promotion_category_<=4years': 'Emp. Promoted within 4 years',
 'promotion_category_<=9years': 'Emp. Promoted within 9 years',
 'promotion_category_NA': 'Emp. Promotion Not Given/Available'}


def predict_table_where_condition(request):
    try:
        params = {}
        params["clause"] = ""
        user_id = request.pop("user_id")
        role_name = UserProfile.objects.get(user_id=user_id).role
        group_id = Group.objects.get(name=role_name).id
        custom_perm_list = CustomPermissions.objects.get(group_id=group_id).permission_list
        band_drop_down = set(["OBand", "MBand", "OBand(HO)", "DH(Excluding HO)"]) & set(custom_perm_list)
        for j, i in enumerate(request.keys()):
            if i == "band":
                if len(band_drop_down) > 0:
                    request[i] = request[i].replace("Department Head", "DH(Excluding HO)")
                if len(set(["OBand", "MBand", "OBand(HO)", "DH(Excluding HO)"]) & set(
                        request[i][2:-2].replace("'", '').split(','))) > 0:
                    continue
                elif request[i] != "":
                    if j == 0:
                        params["clause"] = params["clause"] + " {} in ('{}')".format(i, str(request[i])[2:-2])
                    else:
                        params["clause"] = params["clause"] + "and {} in ('{}')".format(i, str(request[i])[2:-2])
            elif request[i] != "":
                if j == 0:
                    params["clause"] = params["clause"] + " {} in ('{}')".format(i, str(request[i])[2:-2])
                else:
                    params["clause"] = params["clause"] + "and {} in ('{}')".format(i, str(request[i])[2:-2])

        state_drop_down = set([str(i) for i in execute_function(state_query)]) & set(custom_perm_list)

        if 'state' in request.keys():
            if len(state_drop_down) > 0 and ("Maharashtra" in state_drop_down or "Maharashtra" in request["state"]):
                params["clause"] = params["clause"] + "and {} not in ('{}')".format('branch', 'HO-HEAD OFFICE')

        if 'state' not in request.keys() and len(state_drop_down) > 0:
            params["clause"] = params["clause"] + "and {} in ('{}')".format('state', str(state_drop_down)[2:-2])
            if len(state_drop_down) > 0 and "Maharashtra" in state_drop_down:
                params["clause"] = params["clause"] + "and {} not in ('{}')".format('branch', 'HO-HEAD OFFICE')
            else:
                pass
        if 'business_unit' not in request.keys():
            business_unit_drop_down = set([str(i) for i in execute_function(business_unit_query)]) & set(
                custom_perm_list)
            # params["clause"] = params["clause"] + "and {} in ('{}')".format('business_unit',
            #                                                                 str(business_unit_drop_down)[2:-2])
            if len(business_unit_drop_down) > 0:
                params["clause"] = params["clause"] + "and {} in ('{}')".format('business_unit',
                                                                                str(business_unit_drop_down)[2:-2])
            else:
                pass
        if band_drop_down and 'band' in request.keys():
            band_drop_down = request['band']
        if "OBand" in band_drop_down and "MBand" in band_drop_down and "DH(Excluding HO)" in band_drop_down:
            params['clause'] = params[
                                   'clause'] + "and (band in ('OPERATIONAL','MANAGERIAL') or (band in ('DEPARTMENT HEAD') and state not in ('Head Office')))"
        elif "OBand" in band_drop_down and "MBand" in band_drop_down:
            params['clause'] = params['clause'] + "and band in ('OPERATIONAL','MANAGERIAL')"
        elif "OBand" in band_drop_down and "DH(Excluding HO)" in band_drop_down:
            params['clause'] = params[
                                   'clause'] + "and (band in ('OPERATIONAL') or (band in ('DEPARTMENT HEAD') and state not in ('Head Office')))"
        elif "MBand" in band_drop_down and "DH(Excluding HO)" in band_drop_down:
            params['clause'] = params[
                                   'clause'] + "and (band in ('MANAGERIAL') or (band in ('DEPARTMENT HEAD') and state not in ('Head Office')))"
        elif "OBand" in band_drop_down:
            params['clause'] = params['clause'] + "and band in ('OPERATIONAL')"
        elif "MBand" in band_drop_down:
            params['clause'] = params['clause'] + "and band in ('MANAGERIAL')"
        elif "DH(Excluding HO)" in band_drop_down:
            params['clause'] = params['clause'] + "and (band in ('DEPARTMENT HEAD') and state not in ('Head Office'))"

        if params['clause'][:3] == "and":
            params['clause'] = params['clause'][3:]

        return params
    except Exception as e:
        logger.error("Error from predictive2: %s" % e)


def str2percent(x):
    try:
        if type(x) is str:
            y = float(x.strip('%')) / 100
        else:
            y = x
        return y
    except Exception as e:
        logger.error("error in str2percent", e)

def groupby_state(cat_df, cat_input, **kwargs):
    try:
        final_tree = dict()
        parameters = {i: v[2:-2].strip('][').replace("'", "").split(",") for i, v in kwargs.items() if v}
        df_1 = cat_df
        if parameters:
            for i, v in parameters.items():
                df_1 = df_1.loc[cat_df[i].isin(v)]
            df_values = df_1['preds'].value_counts()
            for j in v:
                filter_df = df_1[df_1[i] == j]['preds']
                if filter_df.empty:
                    pass
                elif 'Active' in filter_df.value_counts().index:
                    criteria = {j: {'Active': filter_df.value_counts()['Active'],
                                    'Separated': filter_df.shape[0] - filter_df.value_counts()['Active'],
                                    'Total': filter_df.shape[0]}}
                    final_tree.setdefault(i, []).append(criteria)
                else:
                    criteria = {j: {'Active': 0, 'Separated': filter_df.value_counts()['Seperated'],
                                    'Total': filter_df.shape[0]}}
                    final_tree.setdefault(i, []).append(criteria)
            df_values['Active'] = 0 if 'Active' not in df_values.index else df_values['Active']

            final_tree.update({'Overall': {'Active': df_values['Active'],
                                           'Separated': df_1.shape[0] - df_values['Active'], 'Total': df_1.shape[0]}})
        else:
            final_tree = {'states': {i[0]: cat_input[i] for i in cat_input if len(i) == 1}}
            df_values = df_1['preds'].value_counts()
            final_tree.update({'Overall': {'Active': df_values['Active'],
                                           'Separated': df_1.shape[0] - df_values['Active'], 'Total': df_1.shape[0]}})
        return final_tree
    except Exception as e:
        logger.error("Error from groupby_state: %s" % e)
        final_tree = "No"
    return final_tree


def data_manip_simul_graph(data):
    try:

        comp_cols = ['desig_name', 'reporting_office_name', 'divisions', 'circle', 'area', 'branch_name',
                     'emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3',
                     'emp_lang_known4', 'repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4',
                     'state', 'grade_name', 'emp_location', 'repo_location', 'emp_highest_qualification',
                     'repo_highest_qualification', 'repo_department']
        under_grad = dict.fromkeys(list(map(str.capitalize,
                                            ['Diploma in business administration', '<xth', 'XIIth Standard / PUC',
                                             'Puc', 'Xth Standard', 'ITI', '< Xth', 'Diploma',
                                             'Diploma in Computer Application', 'Diploma in Mechanical Engineering',
                                             'Diploma in Electrical Engineering', 'Advance Diploma'])), 'under_grad')
        grad = dict.fromkeys(list(map(str.capitalize,
                                      ['Hotel management', 'Mathematics', 'Ca (inter)', 'Business management', 'Cpa',
                                       'C.i.a. - internal audit', 'Ba,bl', 'B.m.s.', 'B.arch.', 'B.SC', 'B.com',
                                       'B.Com', 'BA', 'BA, BL', 'BIT', 'BL', 'B.Pharm', 'Others',
                                       'Armed Forces Services', 'B.A.', 'B.B.M', 'LLB', 'B.B.A', 'B.C.A', 'B.Ed.',
                                       'B.P.E.', 'BCS', 'B.Tech', 'B.Tech.', 'Bachelor of Journalism',
                                       'Bachelor of Social Works', 'BE', 'Graduate', 'BL', 'B.Sc. (Tech)', 'BHM',
                                       'BA, BL' 'B.Pharm', 'Bachelor of Social, Legal Sciences', 'BDS'])), 'grad')
        post_grad = dict.fromkeys(list(map(str.capitalize,
                                           ['M.a.m.', 'M.p.m.', 'Pgpm', 'Pgdpe', 'Pg programme in media management',
                                            'Mhrm', 'M.phil.', 'M.tech.', 'MA', 'MMM', 'MBA', 'MSW', 'MFM', 'PGDBA',
                                            'LLM', 'Post Diploma', 'Executive Diploma in HR Management', 'ME', 'MCA',
                                            'PG Diploma', 'M.Sc.', 'M.Com.', 'MscIT', 'ML', 'Post Graduate', 'PGDM',
                                            'PGDHRM', 'PGDBM', 'ML', 'M.Ed.'])), 'post_grad')

        f_data = dict(data[
                          ['employee_id', 'state', 'branch', 'division', 'function_name', 'business_unit', 'department',
                           'sub_department',
                           'cost_centre', 'band', 'work_contract', 'total_experience_year_in_mf',
                           'reporting_officer_total_experience_year_in_mf',
                           'age', 'reporting_office_age', 'emp_gender', 'emp_lang_known1', 'emp_lang_known2',
                           'emp_lang_known3',
                           'emp_lang_known4', 'repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3',
                           'repo_lang_known4',
                           'emp_marital_status', 'emp_highest_qualification', 'repo_highest_qualification',
                           'manager_net_mcares_score',
                           'gem_previous_quarter', 'gem_prev_to_prev_quarter', 'star_previous_quarter',
                           'star_prev_to_prev_quarter', 'training_previous_quarter',
                           'training_prev_to_prev_quarter', 'dic_previous_quarter',
                           'monthly_compensation_gross_previous_quarter', 'fixed_component_previous_quarter',
                           'annual_hike_percentage_previous_quarter', 'no_of_days_prev_to_prev_quarter',
                           'no_of_days_previous_quarter','emp_location','repo_location',
                           'pip_previous_quarter', 'promotion_count_year', 'employee_status']])


        f_data['emp_qualif_cat'] = f_data['emp_highest_qualification'].apply(
            lambda x: x.capitalize() if not pd.isna(x) else x)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(under_grad)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(grad)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(post_grad)
        f_data['repo_qualif_cat'] = f_data['repo_highest_qualification'].apply(
            lambda x: x.capitalize() if not pd.isna(x) else x)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(under_grad)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(grad)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(post_grad)

        cleaned_df = pd.DataFrame(f_data)
        binInterval = [0, 24, 48, 108]


        binLabels = ['<=2years', '<=4years', '<=9years']
        cleaned_df['promotion_category'] = pd.cut(cleaned_df['promotion_count_year'], bins=binInterval,
                                                  labels=binLabels)

        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].cat.add_categories('NA')
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].fillna('NA')
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].astype(str)
        #         cleaned_df['emp_qualif_cat']=cleaned_df.apply(
        #         lambda row: 'under_grad' if pd.isna(row['emp_qualif_cat']) and row['grade_name'] =="L10C- Operational" else row['emp_qualif_cat'],axis=1 )
        cleaned_df['emp_qualif_cat'].fillna(cleaned_df['emp_qualif_cat'].value_counts().index[0], inplace=True)
        cleaned_df['repo_qualif_cat'].fillna(cleaned_df['repo_qualif_cat'].value_counts().index[0], inplace=True)
        cleaned_df.loc[:, 'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Divorcee', 'Single')
        cleaned_df.loc[:, 'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Widower', 'Single')
        cleaned_df.loc[:, 'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Widow', 'Single')
        cleaned_df.loc[:, 'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Separated', 'Single')
        cleaned_df.loc[:, 'employee_status'] = cleaned_df['employee_status'].str.replace('Suspended', 'Active')
        cleaned_df.loc[:, 'employee_status'] = cleaned_df['employee_status'].str.replace('Unpaid Leave', 'Active')
        cleaned_df['location_emp_rep'] = np.where(cleaned_df['emp_location'].str.lower() == cleaned_df['repo_location'].str.lower(), 'same', 'different') 

        cleaned_df[['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']] = cleaned_df[
            ['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']].replace(r'^\s*$', np.nan,
                                                                                                  regex=True)
        cleaned_df['emp_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in cleaned_df[
            ['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']].values]
        cleaned_df[['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']] = cleaned_df[
            ['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']].replace(r'^\s*$', np.nan,
                                                                                                      regex=True)
        cleaned_df['emp_lang_count'] = cleaned_df[
            ['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']].count(axis=1)
        cleaned_df['repo_lang_count'] = cleaned_df[
            ['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']].count(axis=1)
        cleaned_df['repo_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in cleaned_df[
            ['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']].values]
        cleaned_df['emp_lang_count'] = cleaned_df['emp_lang_count'].replace(0, 1)
        cleaned_df['repo_lang_count'] = cleaned_df['repo_lang_count'].replace(0, 1)
        cleaned_df['training_previous_quarter'] = cleaned_df['training_previous_quarter'].apply(
            lambda x: 1 if x >= 1 else 0)
        cleaned_df['training_previous_quarter'].fillna(cleaned_df['training_previous_quarter'].value_counts().index[0],
                                                       inplace=True)
        cleaned_df['training_prev_to_prev_quarter'] = cleaned_df['training_prev_to_prev_quarter'].apply(
            lambda x: 1 if x >= 1 else 0)
        cleaned_df['training_prev_to_prev_quarter'].fillna(
            cleaned_df['training_prev_to_prev_quarter'].value_counts().index[0], inplace=True)
        cleaned_df.update(cleaned_df[['gem_previous_quarter', 'gem_prev_to_prev_quarter', 'star_previous_quarter',
                                      'star_prev_to_prev_quarter', 'training_previous_quarter',
                                      'training_prev_to_prev_quarter', 'dic_previous_quarter',
                                      'annual_hike_percentage_previous_quarter',
                                      'no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter',
                                      'pip_previous_quarter']].fillna(0))
        cleaned_df['gem_prev_to_prev_quarter'] = cleaned_df['gem_prev_to_prev_quarter'].apply(
            lambda x: 1 if x >= 1 else 0)
        cleaned_df['star_prev_to_prev_quarter'] = cleaned_df['star_prev_to_prev_quarter'].apply(
            lambda x: 1 if x >= 1 else 0)
        cleaned_df['gem_previous_quarter'] = cleaned_df['gem_previous_quarter'].apply(lambda x: 1 if x >= 1 else 0)
        cleaned_df['star_previous_quarter'] = cleaned_df['star_previous_quarter'].apply(lambda x: 1 if x >= 1 else 0)
        cleaned_df['dic_previous_quarter'] = cleaned_df['dic_previous_quarter'].apply(lambda x: 1 if x >= 1 else 0)
        cleaned_df['pip_previous_quarter'] = cleaned_df['pip_previous_quarter'].apply(lambda x: 1 if x >= 1 else 0)
        cleaned_df['reporting_officer_total_experience_year_in_mf'] = cleaned_df[
            'reporting_officer_total_experience_year_in_mf'].fillna(
            cleaned_df['reporting_officer_total_experience_year_in_mf'].mean())
        cleaned_df['manager_net_mcares_score'] = cleaned_df['manager_net_mcares_score'].fillna(
            cleaned_df['manager_net_mcares_score'].mean())
        cleaned_df['reporting_office_age'] = cleaned_df['reporting_office_age'].fillna(
            cleaned_df['reporting_office_age'].mean())
        cleaned_df['monthly_compensation_gross_previous_quarter'] = cleaned_df[
            'monthly_compensation_gross_previous_quarter'].fillna(
            cleaned_df['monthly_compensation_gross_previous_quarter'].median())
        cleaned_df['annual_hike_percentage_previous_quarter'] = cleaned_df[
            'annual_hike_percentage_previous_quarter'].apply(str2percent)
        cleaned_df['fixed_component_previous_quarter'] = cleaned_df['fixed_component_previous_quarter'].fillna(
            cleaned_df['fixed_component_previous_quarter'].median())
        cleaned_df['annual_hike_percentage_previous_quarter'] = cleaned_df[
            'annual_hike_percentage_previous_quarter'].fillna(
            cleaned_df['annual_hike_percentage_previous_quarter'].median())
        #         print (cleaned_df[['gem_previous_quarter','star_previous_quarter','pip_previous_quarter']])
        cat_cols = [col for col in cleaned_df.columns if cleaned_df[col].dtypes == 'object' and col not in comp_cols]
        cat_cols_df = pd.get_dummies(cleaned_df[cat_cols])  # ,drop_first=True)
        cleaned_df.drop(labels=['emp_highest_qualification', 'repo_highest_qualification'], axis=1, inplace=True)
        final_data = pd.concat([cleaned_df, cat_cols_df], axis=1)
        cols_by_oprn = ['employee_id', 'band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL', 'band_OPERATIONAL',
                        'band_STRATEGIC',
                        'total_experience_year_in_mf', 'reporting_officer_total_experience_year_in_mf',
                        'manager_net_mcares_score',
                        'age', 'reporting_office_age', 'gem_prev_to_prev_quarter', 'gem_previous_quarter',
                        'star_prev_to_prev_quarter', \
                        'star_previous_quarter', 'monthly_compensation_gross_previous_quarter',
                        'fixed_component_previous_quarter', \
                        'annual_hike_percentage_previous_quarter', 'no_of_days_prev_to_prev_quarter',
                        'no_of_days_previous_quarter', \
                        'emp_lang_count', 'repo_lang_count', 'emp_marital_status_Married', 'emp_marital_status_Single',
                        'emp_gender_Female', 'emp_gender_Male', \
                        'emp_qualif_cat_grad', 'emp_qualif_cat_post_grad', 'emp_qualif_cat_under_grad',
                        'repo_qualif_cat_grad', 'repo_qualif_cat_post_grad', 'repo_qualif_cat_under_grad', \
                        'training_prev_to_prev_quarter', 'training_previous_quarter', 'dic_previous_quarter', \
                        'pip_previous_quarter', 'location_emp_rep_same','location_emp_rep_different','promotion_category_<=2years', 'promotion_category_<=4years',
                        'promotion_category_<=9years',
                        'promotion_category_NA', 'employee_status']

        final_data = final_data.reindex(columns=cols_by_oprn).fillna(0)
        return final_data.drop(['employee_status', 'employee_id'], axis=1)
    except Exception as e:
        logger.error("Error from data_manip_simul_graph: %s" % e)



# def class_feature_importance(final_data, model,threshold):
#     try:
#         class_imp_dict ={}
#         X,y = final_data,np.array(['Separated' if round(i[1],3)>threshold else 'Active' for i in model.predict_proba(final_data)])
#         feature_importances = model.feature_importances_
#         N = len(feature_importances)
#         X_data = scale(X)
#         class_importance = {}
#         sort_y = sorted(set(y))
#         for c in sort_y:
#             if 'Active' not in sort_y:
#                 class_importance['Active'] = dict(zip(range(N),np.zeros(N)))
#             if 'Separated' not in sort_y:
#                 class_importance['Separated'] = dict(zip(range(N),np.zeros(N)))
#             class_importance[c] = dict(zip(range(N), np.mean(X_data[y==c, :], axis=0)*feature_importances))
#         class_importance_df = pd.concat([pd.DataFrame.from_dict(class_importance['Active'],orient='index'),
#                                      pd.DataFrame.from_dict(class_importance['Separated'],orient='index')],axis=1)
# 
#         class_importance_df.columns = ['Active','Separated']
#         class_importance_df['features'] = X.columns.tolist()
#         class_importance_df.sort_values(by='Active',inplace=True)
#         class_imp_dict['high_attirtion'] = dict(zip(class_importance_df['features'][:5],class_importance_df['Active'][:5]*-1))
#         class_imp_dict['high_attirtion'] = {k: v for k, v in
#                                             sorted(class_imp_dict['high_attirtion'].items(), key=lambda item: item[1])}
#         class_imp_dict['low_attirtion'] = dict(zip(class_importance_df['features'][-5:],class_importance_df['Active'][-5:]*-1))
#         return class_imp_dict
#     except Exception as e:
#         logger.error("Error from class_feature_importance: %s" % e)
#         class_imp_dict = {}


def class_feature_importance(final_data, model, threshold):
    try:
        feat_dict = {'Gem Award in Previous to Previous Quarter': ['Gem Award in Previous to Previous Quarter - Yes',
                                                                   'Gem Award in Previous to Previous Quarter - No'],
                     'Gem Award in Previous Quarter': ['Gem Award in Previous Quarter - Yes',
                                                       'Gem Award in Previous Quarter - No'],
                     'Star Award in Previous Quarter': ['Star Award in Previous Quarter - Yes',
                                                        'Star Award in Previous Quarter - No'],
                     'Star Award in Previous to Previous Quarter': ['Star Award in Previous to Previous Quarter - Yes',
                                                                    'Star Award in Previous to Previous Quarter - No'],
                     'Star Award in Previous to Previous Quarter': ['Star Award in Previous to Previous Quarter - Yes',
                                                                    'Star Award in Previous to Previous Quarter - No'],
                     'Emp. Training in Previous to Previous Quarter': [
                         'Emp. Training in Previous to Previous Quarter - Yes',
                         'Emp. Training in Previous to Previous Quarter - No'],
                     'Emp. Training in Previous Quarter': ['Emp. Training in Previous Quarter - Yes',
                                                           'Emp. Training in Previous Quarter - No'],
                     'Disc. in Previous Quarter': ['Disc. in Previous Quarter - No', 'Disc. in Previous Quarter - Yes'],
                     'PIP in Previous Quarter': ['PIP in Previous Quarter - No', 'PIP in Previous Quarter - Yes'],
                     }
        class_imp_dict = {}
        X, y = final_data, np.array(
            ['Seperated' if round(i[1], 3) > threshold else 'Active' for i in model.predict_proba(final_data)])
        feature_importances = model.feature_importances_

        N = len(feature_importances)
        X_data = scale(X)
        class_importance = {}
        sort_y = sorted(set(y))
        features_with_names = [dict_feat_clarity[i] for i in X.columns.tolist()]
        for c in sort_y:
            if 'Active' not in sort_y:
                class_importance['Active'] = dict(zip(range(N), np.zeros(N)))
            if 'Separated' not in sort_y:
                class_importance['Separated'] = dict(zip(range(N), np.zeros(N)))
            class_importance[c] = dict(zip(range(N), np.mean(X_data[y == c, :], axis=0) * feature_importances))
        class_importance_df = pd.concat([pd.DataFrame.from_dict(class_importance['Active'], orient='index'),
                                         pd.DataFrame.from_dict(class_importance['Separated'], orient='index')], axis=1)
        class_importance_df.columns = ['Active', 'Separated']
        class_importance_df['features'] = features_with_names
        class_importance_df.sort_values(by='Active', inplace=True)
        high_fact = [feat_dict[i][1] if i in feat_dict.keys() else i for i in
                     class_importance_df['features'][:5].values]
        low_fact = [feat_dict[i][0] if i in feat_dict.keys() else i for i in
                    class_importance_df['features'][-5:].values]
        class_imp_dict['high_attirtion'] = dict(zip(high_fact, class_importance_df['Active'][:5] * -1))
        class_imp_dict['low_attirtion'] = dict(zip(low_fact, class_importance_df['Active'][-5:] * -1))

        return class_imp_dict
    except Exception as e:
        logger.error("Error from class_feature_importance: %s" % e)
        class_imp_dict = {}


# def pdp_plot(data, model, feature_col):
#     """{input: 
#             data: (attrition_pred_data.csv)
#             model: model
#             feature_col: 'age'}
#         sample_input: pdp_plot(data,model_with_rbmi,'emp_qualif_cat_grad')
#                       pdp_plot(data,model_with_rbmi,'age')"""
#     try:
#         
#         f_data = data_manip_simul_graph(data)
#         data = f_data
#     
#         if feature_col == 'emp_qualif_cat_post_grad' or feature_col == 'emp_qualif_cat_under_grad' or feature_col == 'emp_qualif_cat_grad':
#             plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
#                                                   model=model, X=data,
#                                                   feature=['emp_qualif_cat_post_grad', 'emp_qualif_cat_under_grad',
#                                                            'emp_qualif_cat_grad'], feature_name=feature_col)
#             op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
#             return op_dict
#     
#         elif feature_col == 'band_DEPATMENT HEAD' or feature_col == 'band_EXECUTIVE' or feature_col == 'band_MANAGERIAL' or feature_col == 'band_OPERATIONAL' or feature_col == 'band_STRATEGIC':
#             plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
#                                                   model=model, X=data,
#                                                   feature=['band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL',
#                                                            'band_OPERATIONAL', 'band_STRATEGIC'], feature_name=feature_col)
#             op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
#             return op_dict
#     
#         elif feature_col == 'emp_gender_Male' or feature_col == 'emp_gender_Female':
#             plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
#                                                   model=model, X=data, feature=['emp_gender_Male', 'emp_gender_Female'],
#                                                   feature_name=feature_col)
#             op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
#             return op_dict
#     
#         elif feature_col == 'emp_marital_status_Single' or feature_col == 'emp_marital_status_Married':
#             plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
#                                                   model=model, X=data,
#                                                   feature=['emp_marital_status_Single', 'emp_marital_status_Married'],
#                                                   feature_name=feature_col)
#             op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
#             return op_dict
#     
#         elif feature_col == 'promotion_category_<=2years' or feature_col == 'promotion_category_<=4years' or feature_col == 'promotion_category_<=9years' or feature_col == 'promotion_category_NA':
#             plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
#                                                   model=model, X=data,
#                                                   feature=['promotion_category_<=2years', 'promotion_category_<=4years',
#                                                            'promotion_category_<=9years', 'promotion_category_NA'],
#                                                   feature_name=feature_col)
#             op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
#             return op_dict
#         
#         elif feature_col == 'location_emp_rep_different' or feature_col == 'location_emp_rep_same':
#             plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
#                                                   model=model, X=data,
#                                                   feature=['location_emp_rep_different', 'location_emp_rep_same'],
#                                                   feature_name=feature_col)
#             op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
#             return op_dict
#     
#         elif feature_col == 'experience_in_current_role_in_months':
#             data['total_experience_year_in_mf'] = data['total_experience_year_in_mf'] * 12
#             plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
#                                                   model=model, X=data, feature='total_experience_year_in_mf',
#                                                   feature_name=feature_col)
#             op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
#             return op_dict
#     
#         elif feature_col == 'reporting_officer_experience_in_current_role_in_months':
#             data['reporting_officer_total_experience_year_in_mf'] = data[
#                                                                         'reporting_officer_total_experience_year_in_mf'] * 12
#             plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
#                                                   model=model, X=data,
#                                                   feature='reporting_officer_total_experience_year_in_mf',
#                                                   feature_name=feature_col)
#             op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
#             return op_dict
#     
#         else:
#             plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
#                                                   model=model, X=data, feature=feature_col, feature_name=feature_col)
#             op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
#             return op_dict
#     except Exception as e:
#         logger.error("Error from pdp_plot: %s" % e)


def pdp_plot(data, model, feature_col):
    """{input: 
            data: (attrition_pred_data.csv)
            model: model
            feature_col: 'age'}
        sample_input: pdp_plot(data,model_with_rbmi,'emp_qualif_cat_grad')
                      pdp_plot(data,model_with_rbmi,'age')"""

    f_data = data_manip_simul_graph(data)
    data = f_data

    all_features = {
        **dict.fromkeys(['emp_qualif_cat_post_grad', 'emp_qualif_cat_under_grad', 'emp_qualif_cat_grad'],
                        ['emp_qualif_cat_post_grad', 'emp_qualif_cat_under_grad', 'emp_qualif_cat_grad']),
        **dict.fromkeys(['repo_qualif_cat_post_grad', 'repo_qualif_cat_under_grad', 'repo_qualif_cat_grad'],
                        ['repo_qualif_cat_post_grad', 'repo_qualif_cat_under_grad', 'repo_qualif_cat_grad']),
        **dict.fromkeys(
            ['band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL', 'band_OPERATIONAL', 'band_STRATEGIC'],
            ['band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL', 'band_OPERATIONAL', 'band_STRATEGIC']),
        **dict.fromkeys(['emp_gender_Male', 'emp_gender_Female'], ['emp_gender_Male', 'emp_gender_Female']),
        **dict.fromkeys(['emp_marital_status_Single', 'emp_marital_status_Married'],
                        ['emp_marital_status_Single', 'emp_marital_status_Married']),
        **dict.fromkeys(['promotion_category_<=2years', 'promotion_category_<=4years', 'promotion_category_<=9years',
                         'promotion_category_NA'],
                        ['promotion_category_<=2years', 'promotion_category_<=4years', 'promotion_category_<=9years',
                         'promotion_category_NA'])
    }
    if feature_col in all_features.keys():
        plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
                                              model=model, X=data, feature=all_features[feature_col],
                                              feature_name=feature_col)
        op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
        op_dict['display_column'] = {k: dict_feat_clarity[v] for k, v in op_dict['display_column'].items()}
        return op_dict

    elif feature_col == 'experience_in_current_role_in_months':
        data['total_experience_year_in_mf'] = data['total_experience_year_in_mf'] * 12
        plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
                                              model=model, X=data, feature='total_experience_year_in_mf',
                                              feature_name=feature_col)
        op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
        return op_dict

    elif feature_col == 'reporting_officer_experience_in_current_role_in_months':
        data['reporting_officer_total_experience_year_in_mf'] = data[
                                                                    'reporting_officer_total_experience_year_in_mf'] * 12
        plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
                                              model=model, X=data,
                                              feature='reporting_officer_total_experience_year_in_mf',
                                              feature_name=feature_col)
        op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
        return op_dict

    else:
        plot_data = info_plots_mf.actual_plot(figsize=(20, 10),
                                              model=model, X=data, feature=feature_col, feature_name=feature_col)
        op_dict = {key: value for key, value in plot_data.items() if not re.findall('q1|q2', key)}
        return op_dict


def execute_function(query):
    data = get_cursor(query)
    data = data.fetchall()
    data = list(map(lambda x: x[0], data))
    return data


def order_band_grade_data(order_list,temp_data):
    temp_order_dict = []
    for key in order_list:
        if key in temp_data:
            temp_data.remove(key)
            temp_order_dict.append(key)
    temp_order_dict.extend(temp_data)
    return temp_order_dict


def fetch_dropdown_data(request):
    try:
        print("2222222222",request.user.id)
        role_name = UserProfile.objects.get(user_id=request.user.id).role
        print("3333333333")
        group_id = Group.objects.get(name=role_name).id
        print("4444444444")
        custom_perm_list = CustomPermissions.objects.get(group_id=group_id).permission_list
        print("55555555555555", custom_perm_list)
        temp_dict = {}
        print(".1111111122222222",state_query)
        state_drop_down = set([str(i) for i in execute_function(state_query)]) & set(custom_perm_list)
        print(".............",state_drop_down)
        business_unit_drop_down = set([str(i) for i in execute_function(business_unit_query)]) & set(custom_perm_list)
        print("..........11111111111",business_unit_drop_down)
        if len(state_drop_down) > 0:
            temp_dict["state_list"] = state_drop_down
        else:
            temp_dict["state_list"] = execute_function(state_query)
        if len(business_unit_drop_down) > 0:
            temp_dict["business_unit"] = business_unit_drop_down
        else:
            temp_dict["business_unit"] = execute_function(business_unit_query)
        temp_dict["branch_list"] = execute_function(branch_query)
        temp_dict["function_name"] = execute_function(function_query)
        temp_dict["division_list"] = execute_function(division_query)
        temp_dict["department"] = execute_function(department_query)
        temp_dict["sub_department"] = execute_function(sub_department_query)
        temp_dict["cost_centre"] = execute_function(cost_centre_query)
        temp_dict["work_contract"] = execute_function(work_contract_query)
        temp_band_data = execute_function(band_query)
        temp_grade_data = execute_function(grade_query)
        grade_order_list = ['L10C- Operational', 'L10B- Operational', 'L10A- Operational', 'L10-Operational',
                            'L9-Operational', 'L8-Operational', 'L7-Operational',
                            'L7-Managerial', 'L6-Managerial', 'L5-Managerial', 'L5-Department Head',
                            'L4-Department Head', 'L3-Department Head',
                            'L3-Executive', 'L2-Executive', 'L2-Strategic', 'L1-Strategic']
        band_order_list = ['OPERATIONAL', 'MANAGERIAL', 'DEPATMENT HEAD', 'EXECUTIVE', 'STRATEGIC']
        temp_dict["grade_designation"] = order_band_grade_data(grade_order_list, temp_grade_data)
        band_drop_down = set(["OBand","MBand",'DH(Excluding HO)'])&set(custom_perm_list)
        if len(band_drop_down)>0:
            band_drop_down = ['Department Head' if i=="DH(Excluding HO)" else i for i in band_drop_down]
            temp_dict["band"] = band_drop_down
        else:
            temp_dict["band"] = order_band_grade_data(band_order_list,temp_band_data)
        return temp_dict
    except Exception as e:
        print("............error",e)
        logger.error("Error from pdp_plot: %s" % e)


def tree_map_output(request):
    """
    Return tree map detail.
    :param reporting_officer_sap_code:
    :param reporting_office_name:
    :param branch:
    :param oprn_cat_tree:
    :param oprn_category:
    :param year:
    :param quarter:
    :param func:
    :param circle:
    :param divisions:
    :param area:
    :return:
    """
    try:
        temp_dict_tree_map = {}
        params = {}
        temp =copy.deepcopy(request)
        temp["start_date"]= ""
        temp["end_date"]= ""
        params["where"] = where_condition(temp)["clause"]
        user_id = request.pop("user_id")
        params["select"] = "concat("
        count = 0
        length = len(request.keys())
        for i in request.keys():
            count = count + 1
            if length == 1:
                params["select"] = params["select"] + i + ")"
            else:
                if count == length:
                    params["select"] = params["select"] + "{})".format(i)
                else:
                    params["select"] = params["select"] + "{},'-->',".format(i)
        temp_dict_tree_map["performer_by_state"] = []
        final_query = tree_map_query.format(**params)
        query_obj = get_cursor(final_query)
        query_data = query_obj.fetchall()
        query_columns = [col[0] for col in query_obj.description]
        for row in query_data:
            temp_disc = dict(set(zip(query_columns, row)))
            temp_dict_tree_map["performer_by_state"].append({"hir_name":temp_disc["name"],"name":temp_disc["name"].split("-->")[-1],"value":round(temp_disc["total_count"],2)})
        final_query = active_seperated_query.format(**params)
        query_obj = get_cursor(final_query)
        query_data = query_obj.fetchall()
        query_columns = [col[0] for col in query_obj.description]
        for row in query_data:
            temp_perecent = {}
            temp_disc = dict(set(zip(query_columns, row)))
            temp_perecent["active"] = round(temp_disc["active"],2)
            temp_perecent["seperated"] = round(temp_disc["seperated"],2)
        return temp_dict_tree_map,temp_perecent
    except Exception as e:
        logger.error("Error from tree_map_output: %s" % e)

def format_query(query_obj):
    """
    :param query_obj:
    :return:
    """
    # logger.info("Control entered in format_query function of app_kpi")
    try:
        result = []
        query_data = query_obj.fetchall()
        query_columns = [col[0] for col in query_obj.description]
        for row in query_data:
            temp_disc = dict(set(zip(query_columns, row)))
            result.append(temp_disc)
        return result
    except Exception as e:
        print("error", e)
        # logger.error("Error in format_query function of app_kpi :%s" % e)
        return None




