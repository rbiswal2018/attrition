from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from authentication.permissions import IsAuthenticatedUser
from collections import Counter
from collections import Counter
from authentication.decorators import role_permission
import sys
import pandas as pd
from authentication.util import get_cursor
import logging
import pdb
from rest_framework import status
from predictive.utils import *
from load_default.views import load_default_files

from predictive.query import *

import warnings

logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)

from rest_framework import status

@api_view(["GET"])
# @permission_classes((IsAuthenticatedUser,))
def quater_year_api(request):
    # Desired O/P : {"data": ['IB Vertical']}
    logger.info("Control entered in quater_year_api api")
    data = []
    status_code, msg, flag = status.HTTP_200_OK, "Distinct sub-department generated successfully", "success"
    try:
        query = quater_year_query
        print("...............query", query)
        query_obj = get_cursor(query)
        result = format_query(query_obj)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in quater_year_api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)

class StatePredictive:
    """ to getpredict_query the necessary data for predictive operations """
    model = load_default_files.model_data
    data = load_default_files.pred_data
    data['state'] = data['state'].apply(lambda x: x.split('-', 1)[0] if not pd.isna(x) else x).fillna('N_A')
    split_cols = ['branch', 'division', 'function_name', 'business_unit', 'department', 'sub_department',    'cost_centre']
    data[split_cols] = data[split_cols].fillna('N_A')
    @api_view(['GET'])
    @permission_classes((IsAuthenticatedUser,))
    def get_pdb_dropdown(request):
        try:
            # pdb drowpdown list
            temp = ["Band", "Employee Qualification", "Employee Age in Years", "Reporting Officer Age in Years",
                    "Emp. Total Experience in Company(Years)",
                    "Emp. Experience in Current Role in Months", "Manager MCARES Score",
                    "Repo. Total Experience in Company(Years)", "Repo. Experience in Current Role in Months",
                    "Gem Performer in Previous Quarter",
                    "Trainings Done in Previous Quarter",
                    "Star Performer in Previous Quarter", 
                    "Employee Monthly Compensation Gross",
                    "Epdb.semployee Monthly Compensation Gross",
                    "Fixed Component Previous Quarter",
                     "Annual Hike Percentage", "No. of Leaves taken in Previous Quarter",
                    "Gender", "No of Languages Known", "DIC in Previous Quarter",
                    "PIP in Previous Quarter"]
            return Response({"data": temp}, status=status.HTTP_200_OK)
        except Exception as e:
            logger.error("Error from get_pdb_dropdown: %s" % e)
            return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((IsAuthenticatedUser,))
def get_emp_predict_data(request):
    try:
        request.data["user_id"] =request.user.id
        params = predict_table_where_condition(request.data)
        query = predict_query.format(**params)
        query_obj = get_cursor(query)
        query_data = query_obj.fetchall()
        temp_list = []
        query_columns = [col[0] for col in query_obj.description]
        for row in query_data:
            temp_dict = dict(set(zip(query_columns, row)))
            temp_list.append(temp_dict)
        return Response({"data": temp_list}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("Error from get_rbmi_data: %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
# @role_permission
# @permission_classes((IsAuthenticatedUser,))
def get_tree_map_data(request):
    try:
        result = {}
        print("11111")
        # request.data["user_id"] = request.user.id
        request.data["user_id"] = 1
        print("33333333")
        result,percent_data = tree_map_output(request.data)
        return Response({"data":result, "avg_percent": percent_data},status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes((IsAuthenticatedUser,))
def class_group_importance(request):
    """params {inp_data: attrition_pred_data.csv}"""
    try:
        inp_data = StatePredictive.data
        threshold = 0.6
        kwargs = request.data
        model = StatePredictive.model
        parameters = {i: v[2:-2].strip('][').replace("'", "").split(",") for i, v in kwargs.items() if v}
        if parameters:
            for i, v in parameters.items():
                df_1 = inp_data.loc[inp_data[i].isin(v)]
            df_class = data_manip_simul_graph(df_1)
        else:
            df_class = data_manip_simul_graph(inp_data)
        recv_data = class_feature_importance(df_class, model, threshold)
        temp_list = []
        for i, j in recv_data.items():
            for new_key in recv_data[i].keys():
                if recv_data[i][new_key] != 0:
                    temp_dict = {}
                    temp_dict["name"] = new_key
                    temp_dict["value"] = recv_data[i][new_key] * 100
                    temp_list.append(temp_dict)
    except Exception as e:
        temp_list = []
    return Response({"data": temp_list}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticatedUser,))
def get_pdb_data(request):
    try:
        data = StatePredictive.data
        model = StatePredictive.model
        # oprn_category = request.data['oprn_cat']
        dropdown_key_values = {"Band": "band_DEPATMENT HEAD", \
                               "Employee Qualification": "emp_qualif_cat_grad", \
                               "Employee Age in Years": "age", \
                               "Reporting Officer Age in Years": "reporting_office_age", \
                               "Emp. Total Experience in Company(Years)": "total_experience_year_in_mf", \
                               "Emp. Experience in Current Role in Months": "experience_in_current_role_in_months", \
                               "Manager MCARES Score": "manager_net_mcares_score", \
                               "Repo. Total Experience in Company(Years)": "reporting_officer_total_experience_year_in_mf", \
                               "Repo. Experience in Current Role in Months": "reporting_officer_experience_in_current_role_in_months", \
                               "Trainings Done in Previous Quarter": "training_previous_quarter", \
                               "Gem Performer in Previous Quarter": "gem_previous_quarter", \
                               "Star Performer in Previous Quarter": "star_previous_quarter", \
                               "Employee Monthly Compensation Gross": "monthly_compensation_gross_previous_quarter", \
                               "Employee Variable Component": "variable_component_previous_quarter", \
                               "Fixed Component Previous Quarter":"fixed_component_previous_quarter",\
                               "Annual Hike Percentage": "annual_hike_percentage_previous_quarter", \
                               "No. of Leaves taken in Previous Quarter": "no_of_days_previous_quarter", \
                               "Gender": "emp_gender_Male", "Maritial Status": "emp_marital_status_Single", \
                               "No of Languages Known": "emp_lang_count",
                               "DIC in Previous Quarter": "dic_previous_quarter", \
                               "PIP in Previous Quarter": "pip_previous_quarter"}
        req_column = request.data['feature_col']
        feature_col = dropdown_key_values[req_column]

        result = pdp_plot(data, model, feature_col)

        temp_list = []
        for i, j in enumerate(result["display_column"]):
            temp_dict = {}

            temp_dict["Name"] = result["display_column"][j]
            temp_dict["count"] = result["count"][j]
            temp_dict["Average_0"] = result["actual_prediction_q3"][j]
            if 'performance_category_prev_to_prev_quarter' in temp_dict['Name']:
                temp_dict['Name'] = temp_dict['Name'][42:len(temp_dict['Name'])]
            if 'performance_category_previous_quarter' in temp_dict['Name']:
                temp_dict['Name'] = temp_dict['Name'][38:len(temp_dict['Name'])]
            temp_list.append(temp_dict)
        temp_dict_line = {}
        temp_dict_line["Average_0"] = {"name": "Average_0", "values": []}

        temp_list_line = ["Average_0"]

        for i in temp_list:
            for j in temp_list_line:
                temp_dict_line[j]["values"].append({"Name": i["Name"], "Average": i[j]})
        final_list = []
        for s in temp_dict_line.values():
            final_list.append(s)

        return Response({"data": temp_list, "line_data": final_list}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("Error from get_pdb_data: %s" % e)


@api_view(["GET"])
@permission_classes((IsAuthenticatedUser,))
def get_predictive_drop_down_data(request):
    try:
        print("11111111111")
        result = fetch_dropdown_data(request)
        return Response({"message": "success", "data": result}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("error in emp_detail_from_manager api %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)
