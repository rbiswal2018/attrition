import pandas as pd
import re
from sklearn.model_selection import train_test_split
from lime.lime_tabular import LimeTabularExplainer
import numpy as np
from attrition import settings
from load_default.views import load_default_files
import logging
import operator
import pdb

logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)

# from lifelines import CoxPHFilter


dict_feat_clarity = {'band_DEPATMENT HEAD': 'Emp. band - Department Head',
                     'band_EXECUTIVE': 'Emp. band - Executive',
                     'band_MANAGERIAL': 'Emp. band - Managerial',
                     'band_OPERATIONAL': 'Emp. band - Operational',
                     'band_STRATEGIC': 'Emp. band - Strategic',
                     'total_experience_year_in_mf': 'Total Experience of Employee in MF(years)',
                     'reporting_officer_total_experience_year_in_mf': 'Total Experience of Reporting officer in MF(years)',
                     'manager_net_mcares_score': 'Managers MCARES Score',
                     'age': 'Employee Age',
                     'reporting_office_age': 'Reporting officer Age',
                     'gem_prev_to_prev_quarter': 'Gem Award in Previous to Previous Quarter',
                     'gem_previous_quarter': 'Gem Award in Previous Quarter',
                     'star_prev_to_prev_quarter': 'Star Award in Previous to Previous Quarter',
                     'star_previous_quarter': 'Star Award in Previous Quarter',
                     'monthly_compensation_gross_previous_quarter': 'Emp. Monthly Compensation',
                     'fixed_component_previous_quarter': 'Emp. Fixed Component',
                     'annual_hike_percentage_previous_quarter': 'Emp. Annual Hike(%)',
                     'no_of_days_prev_to_prev_quarter': 'Leaves in Previous to Previous Quarter(days)',
                     'no_of_days_previous_quarter': 'Leaves in Previous Quarter(days)',
                     'emp_lang_count': 'Employee Languages Count',
                     'repo_lang_count': 'Reporting Officer Languages Count',
                     'emp_marital_status_Married': 'Emp. Marital Status - Married',
                     'emp_marital_status_Single': 'Emp. Marital Status - Single',
                     'emp_gender_Female': 'Emp. Gender - Female',
                     'emp_gender_Male': 'Emp. Gender - Male',
                     'emp_qualif_cat_grad': 'Emp. Qualification - Graduate',
                     'emp_qualif_cat_post_grad': 'Emp. Qualification - Post Graduate',
                     'emp_qualif_cat_under_grad': 'Emp. Qualification - Under Graduate',
                     'repo_qualif_cat_grad': 'Reporting Officer Qualification - Graduate',
                     'repo_qualif_cat_post_grad': 'Reporting Officer Qualification - Post Graduate',
                     'repo_qualif_cat_under_grad': 'Reporting Officer Qualification - Under Graduate',
                     'training_prev_to_prev_quarter': 'Emp. Training in Previous to Previous Quarter',
                     'training_previous_quarter': 'Emp. Training in Previous Quarter',
                     'dic_previous_quarter': 'Disc. in Previous Quarter',
                     'pip_previous_quarter': 'PIP in Previous Quarter',
                     'location_emp_rep_same': 'Location of Emp. & Reporting Officer - Same',
                     'location_emp_rep_different': 'Location of Emp. & Reporting Officer - Different',
                     'promotion_category_<=2years': 'Emp. Promoted within 2 years',
                     'promotion_category_<=4years': 'Emp. Promoted within 4 years',
                     'promotion_category_<=9years': 'Emp. Promoted within 9 years',
                     'promotion_category_NA': 'Emp. Promotion Not Given/Available'}


def str2percent(x):
    if type(x) is str:
        y = float(x.strip('%')) / 100
    else:
        y = x
    return y


def simulation(model, total_exp_in_mf, repo_total_exp_in_mf, no_leaves_prev_taken, no_leaves_taken, \
               gem_prev_performer, gem_performer, star_prev_performer, star_performer, training_prev_done, \
               training_done, emp_age, repo_age, emp_no_lang_known, mcares_score, monthly_compens_gross, \
               fixed_comp, annual_hike_percentage, repo_no_lang_known, location_emp_rep, promotion=None, \
               band=None, gender=None, marital_status=None, emp_qualif=None, repo_qualif=None, disp_act=None,
               pip_act=None):
    prediction = dict()
    pred_max = ""
    try:
        cat_cols_lst = ['band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL', 'band_OPERATIONAL',
                        'band_STRATEGIC',
                        'emp_gender_Female', 'emp_gender_Male', 'emp_marital_status_Married',
                        'emp_marital_status_Single', 'emp_qualif_cat_grad',
                        'emp_qualif_cat_post_grad', 'emp_qualif_cat_under_grad', 'repo_qualif_cat_grad',
                        'repo_qualif_cat_post_grad', 'repo_qualif_cat_under_grad']
        all_cols = ['total_experience_year_in_mf', 'reporting_officer_total_experience_year_in_mf',
                    'manager_net_mcares_score', 'age', 'reporting_office_age', 'gem_prev_to_prev_quarter',
                    'gem_previous_quarter',
                    'star_prev_to_prev_quarter', 'star_previous_quarter', 'monthly_compensation_gross_previous_quarter',
                    'fixed_component_previous_quarter',
                    'annual_hike_percentage_previous_quarter', 'no_of_days_prev_to_prev_quarter',
                    'no_of_days_previous_quarter', 'location_emp_rep_same', 'location_emp_rep_different',
                    'emp_lang_count', 'repo_lang_count', 'training_prev_to_prev_quarter', 'training_previous_quarter',
                    'dic_previous_quarter',
                    'pip_previous_quarter', 'promotion_category_<=2years', 'promotion_category_<=4years',
                    'promotion_category_<=9years', 'promotion_category_NA']
        all_cols.extend(cat_cols_lst)
        final_df = {i: int() for i in all_cols}
        final_df.update({i: None for i in cat_cols_lst})
        if band is None:
            final_df.update({'band_DEPATMENT HEAD': 0, 'band_EXECUTIVE': 0, 'band_MANAGERIAL': 0, 'band_OPERATIONAL': 0,
                             'band_STRATEGIC': 0})
            final_df.update({a: 0 for a in all_cols})
        else:
            final_df.update({a: 1 if '_' + band in a else 0 for a in all_cols})
        if gender is None:
            final_df.update({'emp_gender_Female': 0, 'emp_gender_Male': 0})
        else:
            final_df.update({a: 1 for a in all_cols if '_' + gender in a})
        if marital_status is None:
            final_df.update({'emp_marital_status_Married': 1, 'emp_marital_status_Single': 0})
        else:
            final_df.update({a: 1 for a in all_cols if '_' + marital_status in a})
        if promotion is None:
            final_df.update(
                {'promotion_category_<=2years': 0, 'promotion_category_<=4years': 0, 'promotion_category_<=9years': 0,
                 'promotion_category_NA': 1})
        else:
            final_df.update({a: 1 for a in all_cols if '_' + promotion in a})
        if location_emp_rep is None:
            final_df.update({'location_emp_rep_same': 0, 'location_emp_rep_different': 0})
        else:
            final_df.update({a: 1 for a in all_cols if '_' + location_emp_rep in a})
        final_df.update(
            {'age': emp_age, 'reporting_office_age': repo_age, 'total_experience_year_in_mf': total_exp_in_mf, \
             'reporting_officer_total_experience_year_in_mf': repo_total_exp_in_mf,
             'manager_net_mcares_score': mcares_score, \
             'training_previous_quarter': training_done, 'gem_previous_quarter': gem_performer, \
             'star_previous_quarter': star_performer, 'training_prev_to_prev_quarter': training_prev_done,
             'gem_prev_to_prev_quarter': gem_prev_performer, \
             'star_prev_to_prev_quarter': star_prev_performer, 'fixed_component_previous_quarter': fixed_comp, \
             'monthly_compensation_gross_previous_quarter': monthly_compens_gross,
             'no_of_days_prev_to_prev_quarter': no_leaves_prev_taken, \
             'annual_hike_percentage_previous_quarter': annual_hike_percentage, "pip_previous_quarter": pip_act, \
             'no_of_days_previous_quarter': no_leaves_taken, 'emp_lang_count': emp_no_lang_known,
             'repo_lang_count': repo_no_lang_known})
        if emp_qualif is None:
            final_df.update({'emp_qualif_cat_grad': 0, 'emp_qualif_cat_post_grad': 0, 'emp_qualif_cat_under_grad': 0})
        elif emp_qualif == 'post_grad':
            final_df.update({'emp_qualif_cat_grad': 0, 'emp_qualif_cat_post_grad': 1, 'emp_qualif_cat_under_grad': 0})
        elif emp_qualif == 'under_grad':
            final_df.update({'emp_qualif_cat_grad': 0, 'emp_qualif_cat_post_grad': 0, 'emp_qualif_cat_under_grad': 1})
        elif emp_qualif == 'grad':
            final_df.update({'emp_qualif_cat_grad': 1, 'emp_qualif_cat_post_grad': 0, 'emp_qualif_cat_under_grad': 0})
        if repo_qualif is None:
            final_df.update(
                {'repo_qualif_cat_grad': 0, 'repo_qualif_cat_post_grad': 0, 'repo_qualif_cat_under_grad': 0})
        elif repo_qualif == 'post_grad':
            final_df.update(
                {'repo_qualif_cat_grad': 0, 'repo_qualif_cat_post_grad': 1, 'repo_qualif_cat_under_grad': 0})
        elif repo_qualif == 'under_grad':
            final_df.update(
                {'repo_qualif_cat_grad': 0, 'repo_qualif_cat_post_grad': 0, 'repo_qualif_cat_under_grad': 1})
        elif repo_qualif == 'grad':
            final_df.update(
                {'repo_qualif_cat_grad': 1, 'repo_qualif_cat_post_grad': 0, 'repo_qualif_cat_under_grad': 0})
        final_df = pd.DataFrame.from_dict(final_df, orient='index').T
        final_df = final_df[
            ['band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL', 'band_OPERATIONAL', 'band_STRATEGIC',
             'total_experience_year_in_mf', 'reporting_officer_total_experience_year_in_mf', 'manager_net_mcares_score',
             'age', 'reporting_office_age',
             'gem_prev_to_prev_quarter', 'gem_previous_quarter', 'star_prev_to_prev_quarter', 'star_previous_quarter',
             'monthly_compensation_gross_previous_quarter', 'fixed_component_previous_quarter',
             'annual_hike_percentage_previous_quarter',
             'no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter', 'emp_lang_count', 'repo_lang_count',
             'emp_marital_status_Married',
             'emp_marital_status_Single', 'emp_gender_Female', 'emp_gender_Male', 'emp_qualif_cat_grad',
             'emp_qualif_cat_post_grad',
             'emp_qualif_cat_under_grad', 'repo_qualif_cat_grad', 'repo_qualif_cat_post_grad',
             'repo_qualif_cat_under_grad',
             'training_prev_to_prev_quarter', 'training_previous_quarter', 'dic_previous_quarter',
             'pip_previous_quarter', 'location_emp_rep_same', 'location_emp_rep_different',
             'promotion_category_<=2years', 'promotion_category_<=4years', 'promotion_category_<=9years',
             'promotion_category_NA']]
        n = np.array(list(final_df.values))
        probs = model.predict_proba(n)[0]
        prediction = {i: j for i, j in zip(['Low Attrition', 'High Attrition'], probs)}
        pred_max = 'High Attrition' if round(prediction['High Attrition'], 3) > 0.6 else 'Low Attrition'
        return prediction, final_df.to_dict('index')[0], pred_max
    except Exception as e:
        # print("...................",e)
        logger.error("Error from simulation: %s" % e)


def lime_explainer(data, final_df, model):
    try:
        inf_fact = dict()
        final_data = data_manip_simul_graph(data)
        X = final_data
        X.columns = final_df.keys()
        predict_fn_rfc = lambda x: model_rbmi.predict_proba(x).astype(float)
        mod_inf_fact = dict()
        feature_names_cat = ['emp_marital_status_Married', 'emp_marital_status_Single', 'training_previous_quarter', \
                             'training_prev_to_prev_quarter', 'gem_previous_quarter', 'gem_prev_to_prev_quarter',
                             'star_prev_to_prev_quarter', \
                             'star_previous_quarter', 'emp_gender_Female', 'emp_gender_Male', 'emp_qualif_cat_grad',
                             'emp_qualif_cat_post_grad', \
                             'emp_qualif_cat_under_grad', 'repo_qualif_cat_grad', 'repo_qualif_cat_post_grad',
                             'repo_qualif_cat_under_grad', \
                             'dic_previous_quarter', 'pip_previous_quarter', 'band_DEPATMENT HEAD', 'band_EXECUTIVE',
                             'band_MANAGERIAL', \
                             'band_OPERATIONAL', 'band_STRATEGIC', 'promotion_category_<=2years',
                             'promotion_category_<=4years', 'promotion_category_<=9years', 'promotion_category_NA']

        explainer = LimeTabularExplainer(X.values,
                                         feature_names=X.columns.tolist(),
                                         class_names=['Active', 'Separated'],
                                         categorical_features=feature_names_cat,
                                         categorical_names=feature_names_cat,
                                         kernel_width=3,
                                         mode='classification', random_state=42)

        exp = explainer.explain_instance(pd.Series(final_df), predict_fn_rfc, num_features=6).as_list()

        inf_fact = dict([(re.findall('prom.*years', i[0])[0], i[1]) if 'promotion_category' in i[0] else
                         (re.sub('[<=0-9>.]', '', i[0]).strip(), i[1])
                         for i in exp])
        contin_vars = ['total_experience_year_in_mf', 'reporting_officer_total_experience_year_in_mf',
                       'manager_net_mcares_score', \
                       'age', 'reporting_office_age', 'monthly_compensation_gross_previous_quarter',
                       'fixed_component_previous_quarter', \
                       'annual_hike_percentage_previous_quarter', 'no_of_days_prev_to_prev_quarter',
                       'no_of_days_previous_quarter', \
                       'emp_lang_count', 'repo_lang_count']

        for k, v in inf_fact.items():
            if k not in contin_vars:
                if final_df[k] <= 1 or 'promotion_category' not in k:
                    if k == 'location_emp_rep_same' or k == 'location_emp_rep_different':
                        if final_df['location_emp_rep_different'] > final_df['location_emp_rep_same']:
                            mod_inf_fact['location_emp_rep_different'] = v
                        else:
                            mod_inf_fact['location_emp_rep_same'] = v

                    elif 'promotion_category' in k:
                        if final_df['promotion_category_<=2years'] == 1:
                            mod_inf_fact['promotion_category_<=2years'] = v
                        elif final_df['promotion_category_<=4years'] == 1:
                            mod_inf_fact['promotion_category_<=4years'] = v
                        else:
                            mod_inf_fact['promotion_category_NA'] = v

                    elif final_df[k] == 0:
                        mod_inf_fact[k + '_no'] = v
                    elif final_df[k] == 1:
                        mod_inf_fact[k + '_yes'] = v
            else:
                mod_inf_fact[k] = v

        return mod_inf_fact
    except Exception as e:
        logger.error("Error from lime_explianer: %s" % e)


def data_manip_simul_graph(data):
    try:
        comp_cols = ['desig_name', 'reporting_office_name', 'divisions', 'circle', 'area', 'branch_name',
                     'emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3',
                     'emp_lang_known4', 'repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4',
                     'state', 'grade_name', 'emp_location', 'repo_location', 'emp_highest_qualification',
                     'repo_highest_qualification', 'repo_department']
        under_grad = dict.fromkeys(list(map(str.capitalize,
                                            ['Diploma in business administration', '<xth', 'XIIth Standard / PUC',
                                             'Puc', 'Xth Standard', 'ITI', '< Xth', 'Diploma',
                                             'Diploma in Computer Application', 'Diploma in Mechanical Engineering',
                                             'Diploma in Electrical Engineering', 'Advance Diploma'])), 'under_grad')
        grad = dict.fromkeys(list(map(str.capitalize,
                                      ['Hotel management', 'Mathematics', 'Ca (inter)', 'Business management', 'Cpa',
                                       'C.i.a. - internal audit', 'Ba,bl', 'B.m.s.', 'B.arch.', 'B.SC', 'B.com',
                                       'B.Com', 'BA', 'BA, BL', 'BIT', 'BL', 'B.Pharm', 'Others',
                                       'Armed Forces Services', 'B.A.', 'B.B.M', 'LLB', 'B.B.A', 'B.C.A', 'B.Ed.',
                                       'B.P.E.', 'BCS', 'B.Tech', 'B.Tech.', 'Bachelor of Journalism',
                                       'Bachelor of Social Works', 'BE', 'Graduate', 'BL', 'B.Sc. (Tech)', 'BHM',
                                       'BA, BL' 'B.Pharm', 'Bachelor of Social, Legal Sciences', 'BDS'])), 'grad')
        post_grad = dict.fromkeys(list(map(str.capitalize,
                                           ['M.a.m.', 'M.p.m.', 'Pgpm', 'Pgdpe', 'Pg programme in media management',
                                            'Mhrm', 'M.phil.', 'M.tech.', 'MA', 'MMM', 'MBA', 'MSW', 'MFM', 'PGDBA',
                                            'LLM', 'Post Diploma', 'Executive Diploma in HR Management', 'ME', 'MCA',
                                            'PG Diploma', 'M.Sc.', 'M.Com.', 'MscIT', 'ML', 'Post Graduate', 'PGDM',
                                            'PGDHRM', 'PGDBM', 'ML', 'M.Ed.'])), 'post_grad')

        f_data = dict(data[
                          ['employee_id', 'state', 'branch', 'division', 'function_name', 'business_unit', 'department',
                           'sub_department',
                           'cost_centre', 'band', 'work_contract', 'total_experience_year_in_mf',
                           'reporting_officer_total_experience_year_in_mf',
                           'age', 'reporting_office_age', 'emp_gender', 'emp_lang_known1', 'emp_lang_known2',
                           'emp_lang_known3',
                           'emp_lang_known4', 'repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3',
                           'repo_lang_known4',
                           'emp_marital_status', 'emp_highest_qualification', 'repo_highest_qualification',
                           'manager_net_mcares_score', 'emp_location', 'repo_location',
                           'gem_previous_quarter', 'gem_prev_to_prev_quarter', 'star_previous_quarter',
                           'star_prev_to_prev_quarter', 'training_previous_quarter',
                           'training_prev_to_prev_quarter', 'dic_previous_quarter',
                           'monthly_compensation_gross_previous_quarter', 'fixed_component_previous_quarter',
                           'annual_hike_percentage_previous_quarter', 'no_of_days_prev_to_prev_quarter',
                           'no_of_days_previous_quarter',
                           'pip_previous_quarter', 'promotion_count_year', 'employee_status']])

        f_data['emp_qualif_cat'] = f_data['emp_highest_qualification'].apply(
            lambda x: x.capitalize() if not pd.isna(x) else x)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(under_grad)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(grad)
        f_data['emp_qualif_cat'] = f_data['emp_qualif_cat'].replace(post_grad)
        f_data['repo_qualif_cat'] = f_data['repo_highest_qualification'].apply(
            lambda x: x.capitalize() if not pd.isna(x) else x)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(under_grad)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(grad)
        f_data['repo_qualif_cat'] = f_data['repo_qualif_cat'].replace(post_grad)
        cleaned_df = pd.DataFrame(f_data)
        binInterval = [0, 24, 48, 108]
        binLabels = ['<=2years', '<=4years', '<=9years']
        cleaned_df['promotion_category'] = pd.cut(cleaned_df['promotion_count_year'], bins=binInterval,
                                                  labels=binLabels)
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].cat.add_categories('NA')
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].fillna('NA')
        cleaned_df['promotion_category'] = cleaned_df['promotion_category'].astype(str)
        #         cleaned_df['emp_qualif_cat']=cleaned_df.apply(
        #         lambda row: 'under_grad' if pd.isna(row['emp_qualif_cat']) and row['grade_name'] =="L10C- Operational" else row['emp_qualif_cat'],axis=1 )
        cleaned_df['emp_qualif_cat'].fillna(cleaned_df['emp_qualif_cat'].value_counts().index[0], inplace=True)
        cleaned_df['repo_qualif_cat'].fillna(cleaned_df['repo_qualif_cat'].value_counts().index[0], inplace=True)
        cleaned_df.loc[:, 'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Divorcee', 'Single')
        cleaned_df.loc[:, 'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Widower', 'Single')
        cleaned_df.loc[:, 'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Widow', 'Single')
        cleaned_df.loc[:, 'emp_marital_status'] = cleaned_df['emp_marital_status'].str.replace('Separated', 'Single')
        cleaned_df.loc[:, 'employee_status'] = cleaned_df['employee_status'].str.replace('Suspended', 'Active')
        cleaned_df.loc[:, 'employee_status'] = cleaned_df['employee_status'].str.replace('Unpaid Leave', 'Active')
        cleaned_df['location_emp_rep'] = np.where(
            cleaned_df['emp_location'].str.lower() == cleaned_df['repo_location'].str.lower(), 'same', 'different')

        cleaned_df[['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']] = cleaned_df[
            ['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']].replace(r'^\s*$', np.nan,
                                                                                                  regex=True)
        cleaned_df['emp_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in cleaned_df[
            ['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']].values]
        cleaned_df[['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']] = cleaned_df[
            ['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']].replace(r'^\s*$', np.nan,

                                                                                                      regex=True)
        cleaned_df['emp_lang_count'] = cleaned_df[
            ['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']].count(axis=1)
        cleaned_df['repo_lang_count'] = cleaned_df[
            ['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']].count(axis=1)
        cleaned_df['repo_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in cleaned_df[
            ['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']].values]
        cleaned_df['emp_lang_count'] = cleaned_df['emp_lang_count'].replace(0, 1)
        cleaned_df['repo_lang_count'] = cleaned_df['repo_lang_count'].replace(0, 1)
        cleaned_df['training_previous_quarter'] = cleaned_df['training_previous_quarter'].apply(
            lambda x: 1 if x >= 1 else 0)
        cleaned_df['training_previous_quarter'].fillna(cleaned_df['training_previous_quarter'].value_counts().index[0],
                                                       inplace=True)
        cleaned_df['training_prev_to_prev_quarter'] = cleaned_df['training_prev_to_prev_quarter'].apply(
            lambda x: 1 if x >= 1 else 0)
        cleaned_df['training_prev_to_prev_quarter'].fillna(
            cleaned_df['training_prev_to_prev_quarter'].value_counts().index[0], inplace=True)
        cleaned_df.update(cleaned_df[['gem_previous_quarter', 'gem_prev_to_prev_quarter', 'star_previous_quarter',
                                      'star_prev_to_prev_quarter', 'training_previous_quarter',
                                      'training_prev_to_prev_quarter', 'dic_previous_quarter',
                                      'annual_hike_percentage_previous_quarter',
                                      'no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter',
                                      'pip_previous_quarter']].fillna(0))
        cleaned_df['gem_prev_to_prev_quarter'] = cleaned_df['gem_prev_to_prev_quarter'].apply(
            lambda x: 1 if x >= 1 else 0)
        cleaned_df['star_prev_to_prev_quarter'] = cleaned_df['star_prev_to_prev_quarter'].apply(
            lambda x: 1 if x >= 1 else 0)
        cleaned_df['gem_previous_quarter'] = cleaned_df['gem_previous_quarter'].apply(lambda x: 1 if x >= 1 else 0)
        cleaned_df['star_previous_quarter'] = cleaned_df['star_previous_quarter'].apply(lambda x: 1 if x >= 1 else 0)
        cleaned_df['dic_previous_quarter'] = cleaned_df['dic_previous_quarter'].apply(lambda x: 1 if x >= 1 else 0)
        cleaned_df['pip_previous_quarter'] = cleaned_df['pip_previous_quarter'].apply(lambda x: 1 if x >= 1 else 0)
        cleaned_df['reporting_officer_total_experience_year_in_mf'] = cleaned_df[
            'reporting_officer_total_experience_year_in_mf'].fillna(
            cleaned_df['reporting_officer_total_experience_year_in_mf'].mean())
        cleaned_df['manager_net_mcares_score'] = cleaned_df['manager_net_mcares_score'].fillna(
            cleaned_df['manager_net_mcares_score'].mean())
        cleaned_df['reporting_office_age'] = cleaned_df['reporting_office_age'].fillna(
            cleaned_df['reporting_office_age'].mean())
        cleaned_df['monthly_compensation_gross_previous_quarter'] = cleaned_df[
            'monthly_compensation_gross_previous_quarter'].fillna(
            cleaned_df['monthly_compensation_gross_previous_quarter'].median())
        cleaned_df['annual_hike_percentage_previous_quarter'] = cleaned_df[
            'annual_hike_percentage_previous_quarter'].apply(str2percent)
        cleaned_df['fixed_component_previous_quarter'] = cleaned_df['fixed_component_previous_quarter'].fillna(
            cleaned_df['fixed_component_previous_quarter'].median())
        cleaned_df['annual_hike_percentage_previous_quarter'] = cleaned_df[
            'annual_hike_percentage_previous_quarter'].fillna(
            cleaned_df['annual_hike_percentage_previous_quarter'].median())
        #         print (cleaned_df[['gem_previous_quarter','star_previous_quarter','pip_previous_quarter']])
        cat_cols = [col for col in cleaned_df.columns if cleaned_df[col].dtypes == 'object' and col not in comp_cols]
        cat_cols_df = pd.get_dummies(cleaned_df[cat_cols])  # ,drop_first=True)
        cleaned_df.drop(labels=['emp_highest_qualification', 'repo_highest_qualification'], axis=1, inplace=True)
        final_data = pd.concat([cleaned_df, cat_cols_df], axis=1)
        cols_by_oprn = ['employee_id', 'band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL', 'band_OPERATIONAL',
                        'band_STRATEGIC',
                        'total_experience_year_in_mf', 'reporting_officer_total_experience_year_in_mf',
                        'manager_net_mcares_score',
                        'age', 'reporting_office_age', 'gem_prev_to_prev_quarter', 'gem_previous_quarter',
                        'star_prev_to_prev_quarter', \
                        'star_previous_quarter', 'monthly_compensation_gross_previous_quarter',
                        'fixed_component_previous_quarter', \
                        'annual_hike_percentage_previous_quarter', 'no_of_days_prev_to_prev_quarter',
                        'no_of_days_previous_quarter', \
                        'emp_lang_count', 'repo_lang_count', 'emp_marital_status_Married', 'emp_marital_status_Single',
                        'emp_gender_Female', 'emp_gender_Male', \
                        'emp_qualif_cat_grad', 'emp_qualif_cat_post_grad', 'emp_qualif_cat_under_grad',
                        'repo_qualif_cat_grad', 'repo_qualif_cat_post_grad', 'repo_qualif_cat_under_grad', \
                        'training_prev_to_prev_quarter', 'training_previous_quarter', 'dic_previous_quarter', \
                        'pip_previous_quarter', 'location_emp_rep_same', 'location_emp_rep_different',
                        'promotion_category_<=2years', 'promotion_category_<=4years',
                        'promotion_category_<=9years',
                        'promotion_category_NA', 'employee_status']

        final_data = final_data.reindex(columns=cols_by_oprn).fillna(0)
        return final_data.drop(['employee_status', 'employee_id'], axis=1)
    except Exception as e:
        logger.error("Error from data_manip_simul_graph: %s" % e)


def emp_wise_data_sim(data, all_data):
    try:
        all_data = all_data[(all_data['employee_status'] == 'Active') | (all_data['employee_status'] == 'Suspended') | (
                all_data['employee_status'] == 'Unpaid Leave')]
        qualif = {'under_grad': list(map(str.capitalize,
                                         ['Diploma in business administration', '<xth', 'XIIth Standard / PUC', 'Puc',
                                          'Xth Standard', 'ITI', \
                                          '< Xth', 'Diploma', 'Diploma in Computer Application',
                                          'Diploma in Mechanical Engineering', 'Diploma in Electrical Engineering',
                                          'Advance Diploma'])), \
                  'grad': list(map(str.capitalize,
                                   ['Hotel management', 'Mathematics', 'Ca (inter)', 'Business management', 'Cpa',
                                    'C.i.a. - internal audit', \
                                    'Ba,bl', 'B.m.s.', 'B.arch.', 'B.SC', 'B.com', 'B.Com', 'BA', 'BA, BL', 'BIT', 'BL',
                                    'B.Pharm', 'Others', 'Armed Forces Services', 'B.A.', \
                                    'B.B.M', 'LLB', 'B.B.A', 'B.C.A', 'B.Ed.', 'B.P.E.', 'BCS', 'B.Tech', 'B.Tech.',
                                    'Bachelor of Journalism', 'Bachelor of Social Works', 'BE', \
                                    'Graduate', 'BL', 'B.Sc. (Tech)', 'BHM', 'BA, BL' 'B.Pharm',
                                    'Bachelor of Social, Legal Sciences', 'BDS'])),
                  'post_grad': list(map(str.capitalize,
                                        ['M.a.m.', 'M.p.m.', 'Pgpm', 'Pgdpe', 'Pg programme in media management',
                                         'Mhrm', 'M.phil.', 'M.tech.', 'MA', \
                                         'MMM', 'MBA', 'MSW', 'MFM', 'PGDBA', 'LLM', 'Post Diploma',
                                         'Executive Diploma in HR Management', 'ME', 'MCA', 'PG Diploma', 'M.Sc.',
                                         'M.Com.', \
                                         'MscIT', 'ML', 'Post Graduate', 'PGDM', 'PGDHRM', 'PGDBM', 'ML', 'M.Ed.']))}

        if pd.isna(data['emp_highest_qualification']) or data['emp_highest_qualification'].capitalize() not in sum(
                list(qualif.values()), []):
            data['emp_qualif_cat'] = 'grad'
        else:
            data['emp_qualif_cat'] = \
                [i for i, d in qualif.items() if data['emp_highest_qualification'].capitalize() in d][0]

        if pd.isna(data['repo_highest_qualification']) or data['repo_highest_qualification'].capitalize() not in sum(
                list(qualif.values()), []):
            data['repo_qualif_cat'] = 'grad'
        else:
            data['repo_qualif_cat'] = \
                [i for i, d in qualif.items() if data['repo_highest_qualification'].capitalize() in d][0]

        final_data = pd.DataFrame.from_dict(data, orient='index').T
        final_data[['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']] = final_data[
            ['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']].replace(r'^\s*$', np.nan,
                                                                                                  regex=True)
        final_data[['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']] = final_data[
            ['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']].replace(r'^\s*$', np.nan,
                                                                                                      regex=True)
        final_data['emp_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in final_data[
            ['emp_lang_known1', 'emp_lang_known2', 'emp_lang_known3', 'emp_lang_known4']].values]
        final_data['repo_lang_count'] = [len(set(v[pd.notna(v)].tolist())) for v in final_data[
            ['repo_lang_known1', 'repo_lang_known2', 'repo_lang_known3', 'repo_lang_known4']].values]
        final_data['emp_lang_count'] = final_data['emp_lang_count'].apply(lambda x: 1 if x <= 0 else x)
        final_data['repo_lang_count'] = final_data['repo_lang_count'].apply(lambda x: 1 if x <= 0 else x)

        if final_data['promotion_count_year'].iloc[0] is None:
            final_data['promotion_category'] = 'NA'
        else:
            final_data['promotion_category'] = final_data['promotion_count_year'].apply(
                lambda x: '<=2years' if x <= 24 else '<=4years' if x <= 48 else '<=9years' if x < 108 else 'NA')
        final_data['annual_hike_percentage_previous_quarter'] = final_data[
            'annual_hike_percentage_previous_quarter'].apply(str2percent)
        # Filling with mean/min
        final_data['location_emp_rep'] = np.where(
            final_data['emp_location'].str.lower() == final_data['repo_location'].str.lower(), 'same', 'different')
        final_data['reporting_officer_total_experience_year_in_mf'] = final_data[
            'reporting_officer_total_experience_year_in_mf'].fillna(
            all_data['reporting_officer_total_experience_year_in_mf'].mean())
        final_data['reporting_office_age'] = final_data['reporting_office_age'].fillna(
            all_data['reporting_office_age'].mean())
        final_data['monthly_compensation_gross_previous_quarter'] = final_data[
            'monthly_compensation_gross_previous_quarter'].fillna(
            all_data['monthly_compensation_gross_previous_quarter'].median())
        final_data['fixed_component_previous_quarter'] = final_data['fixed_component_previous_quarter'].fillna(
            all_data['fixed_component_previous_quarter'].median())
        final_data['manager_net_mcares_score'] = final_data['manager_net_mcares_score'].fillna(
            all_data['manager_net_mcares_score'].mean())
        final_data['annual_hike_percentage_previous_quarter'] = final_data[
            'annual_hike_percentage_previous_quarter'].fillna(0)
        final_data[['no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter', 'manager_net_mcares_score', \
                    'star_previous_quarter', 'gem_previous_quarter', 'star_prev_to_prev_quarter',
                    'gem_prev_to_prev_quarter', 'training_prev_to_prev_quarter', \
                    'training_previous_quarter']] = final_data[
            ['no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter', 'manager_net_mcares_score', \
             'star_previous_quarter', 'gem_previous_quarter', 'star_prev_to_prev_quarter', 'gem_prev_to_prev_quarter',
             'training_prev_to_prev_quarter', \
             'training_previous_quarter']].fillna(0)

        final_data = final_data[['employee_id', 'employee_name', 'department', 'branch', 'division', 'business_unit',
                                 'band',
                                 'total_experience_year_in_mf', 'reporting_officer_total_experience_year_in_mf', \
                                 'no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter',
                                 'gem_prev_to_prev_quarter', \
                                 'gem_previous_quarter', 'star_prev_to_prev_quarter', 'star_previous_quarter',
                                 'training_prev_to_prev_quarter', 'training_previous_quarter', \
                                 'age', 'reporting_office_age', 'emp_lang_count', 'manager_net_mcares_score',
                                 'monthly_compensation_gross_previous_quarter', \
                                 'fixed_component_previous_quarter', 'annual_hike_percentage_previous_quarter', \
                                 'repo_lang_count', 'location_emp_rep', 'promotion_category', 'band', 'emp_gender',
                                 'emp_marital_status', 'emp_qualif_cat', 'repo_qualif_cat', \
                                 'dic_previous_quarter', 'pip_previous_quarter']]
        return final_data
    except Exception as e:
        logger.error("Error from emp_wise_simulation: %s" % e)
        pass


def survival_curve(indv_data, cph):
    try:
        data = pd.DataFrame.from_dict(indv_data, orient='index').T
        data = data[['band_OPERATIONAL', 'total_experience_year_in_mf',
                     'reporting_officer_total_experience_year_in_mf', 'age',
                     'reporting_office_age', 'gem_prev_to_prev_quarter',
                     'gem_previous_quarter', 'star_previous_quarter',
                     'monthly_compensation_gross_previous_quarter',
                     'fixed_component_previous_quarter',
                     'annual_hike_percentage_previous_quarter',
                     'no_of_days_prev_to_prev_quarter', 'no_of_days_previous_quarter',
                     'emp_lang_count', 'repo_lang_count', 'emp_gender_Male',
                     'emp_qualif_cat_grad', 'repo_qualif_cat_grad',
                     'training_prev_to_prev_quarter', 'training_previous_quarter',
                     'dic_previous_quarter', 'pip_previous_quarter',
                     'location_emp_rep_different', 'promotion_category_NA']]

        plot_dict = cph.predict_survival_function(data).to_dict()[0]
        survival_plot_dict = []
        for i, j in plot_dict.items():
            temp_data = {}
            temp_data["value"] = j
            temp_data["name"] = i
            survival_plot_dict.append(temp_data)

            # survival_plot_dict = {'x':list(plot_dict.keys()),'y':list(plot_dict.values())}
        return survival_plot_dict
    except Exception as e:
        logger.error("Error from survival_curve: %s" % e)
