from django.urls import path
from simulation import views

urlpatterns = [

    # path('data_load/', views.lang_state_data),
    path('lime_explainer/', views.lime_explainer_view),
    path('emp_detail/', views.employee_wise_simulation),
    path('get_surv_curve_data/', views.get_survival_data),
    path('get_simulation_data/', views.get_simulation_data),
    path('get_quarterly_simulation_data/', views.get_quarterly_simulation_data)

]

