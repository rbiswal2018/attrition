
from django.shortcuts import render

# Create your views here.
from lime.lime_tabular import LimeTabularExplainer
from pandas import DataFrame
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.response import Response
from authentication.permissions import IsAuthenticatedUser
from simulation.simulation import *
from authentication.util import get_cursor
from load_default.views import load_default_files
from datetime import datetime
import re
import logging
import pdb
import pandas as pd



logger = logging.getLogger(__name__)
previous_level = logger.getEffectiveLevel()
logger.setLevel(logging.ERROR)
logger.setLevel(previous_level)


class Simulation_load_class:
    model = load_default_files.model_data
    data = load_default_files.simulatio_data
    surv_model = load_default_files.surv_model


@api_view(["POST"])
# @permission_classes((IsAuthenticatedUser,))
def employee_wise_simulation(request):
    try:
        new_dic = []
        # employee_id = request.data["employee_id"]
        employee_id = request.data["employee_id"]
        query = """select employee_id,
            first_name || ' ' || last_name employee_name,department,branch,division,business_unit,
        total_experience_year_in_mf::double precision,experience_in_current_role_in_months::double precision,
        reporting_officer_total_experience_year_in_mf::double precision, reporting_officer_experience_in_current_role_in_months::double precision,
        age::int,reporting_office_age::int, emp_gender,        
        emp_lang_known1,emp_lang_known2,emp_lang_known3,emp_lang_known4,repo_lang_known1,repo_lang_known2,
        repo_lang_known3,repo_lang_known4,emp_marital_status,emp_highest_qualification,
        repo_highest_qualification,repo_location,emp_location,
        gem_previous_quarter,gem_prev_to_prev_quarter,star_previous_quarter,
        star_prev_to_prev_quarter,band,
        promotion_previous_quarter,training_previous_quarter,training_prev_to_prev_quarter,
        promotion_prev_to_prev_quarter,manager_net_mcares_score,
        dic_prev_to_prev_quarter,dic_previous_quarter,monthly_compensation_gross_previous_quarter::float8,
        monthly_compensation_gross_prev_to_prev_quarter::float8,fixed_component_previous_quarter::float8,promotion_count_year,
        variable_component_previous_quarter::float8,variable_component_prev_to_prev_quarter::float8,annual_hike_percentage_prev_to_prev_quarter,
        annual_hike_percentage_previous_quarter,no_of_days_prev_to_prev_quarter,no_of_days_previous_quarter,
        pip_prev_to_prev_quarter,pip_previous_quarter,promotion_year         
        from  buisness_suite.prediction_data_view  where employee_id= '{0}'""".format(employee_id)
        query_obj = get_cursor(query)
        query_data = query_obj.fetchall()
        # print(".................",query)
        if len(query_data) == 0:
            return Response({"message": "Employee id doesn't exist"}, status=status.HTTP_200_OK)
        else:
            query_columns = [col[0] for col in query_obj.description]
            result = dict(zip(query_columns, query_data[0]))
            manipulated_data = emp_wise_data_sim(result, Simulation_load_class.data)
            manipulated_data = manipulated_data.to_dict()
            new_dic_temp = []
            for key, value in manipulated_data.items():
                key, value = key, value.get(0)
                if value == False:
                    value = 0
                if value == True:
                    value = 1
                new_dic_temp.append(key)
                new_dic_temp.append(value)

            keys = []
            values = []
            for i, j in enumerate(new_dic_temp):
                if i % 2 == 0:
                    keys.append(j)
                else:
                    values.append(j)
            result = dict(zip(keys, values))
            return Response(result, status=status.HTTP_200_OK)
    except Exception as e:
        print("error",e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes((IsAuthenticatedUser,))
def lime_explainer_view(request):
    try:
        inf_fact = dict()
        model = Simulation_load_class.model
        data = Simulation_load_class.data
        pred_max = ''
        error_type = "Field is mandatory"
        total_exp_in_mf = request.data.get('total_experience_in_mf', None)
        if total_exp_in_mf is None:
            raise Exception(error_type, "total_exp_in_mf is mandatory")
        repo_total_exp_in_mf = request.data.get('repo_total_experience_in_mf', None)
        if repo_total_exp_in_mf is None:
            raise Exception(error_type, "repo_total_exp_in_mf is mandatory")
        # exp_in_cur_role = request.data.get('experience_in_current_role', None)
        # if exp_in_cur_role is None:
        #     raise Exception(error_type, "exp_in_cur_role is mandatory")
        no_leaves_taken = request.data.get('no_leaves_taken', None)
        if no_leaves_taken is None:
            raise Exception(error_type, "no_leaves_taken is mandatory")
        # repo_exp_in_cur_role = request.data.get('repo_experience_in_current_role', None)
        # if repo_exp_in_cur_role is None:
        #     raise Exception(error_type, "repo_exp_in_cur_role is mandatory")
        gem_performer = request.data.get('gem_performer', None)
        if gem_performer is None:
            raise Exception(error_type, "gem_performer is mandatory")
        star_performer = request.data.get('star_performer', None)
        if star_performer is None:
            raise Exception(error_type, "star_performer is mandatory")
        promotion = request.data.get('promotion', None)
        if promotion == "promotion_category_2years":
            promotion = "<=2years"
        elif promotion == "promotion_category_4years":
            promotion = "<=4years"
        elif promotion == "promotion_category_9years":
            promotion = "<=9years"
        elif promotion == "promotion_category_NA":
            promotion = "NA"
        if promotion is None:
            raise Exception(error_type, "promotion is mandatory")
        training_done = request.data.get('training_done', None)
        if training_done is None:
            raise Exception(error_type, "training_done is mandatory")
        emp_age = int(request.data.get('emp_age', None))
        if emp_age is None:
            raise Exception(error_type, "emp_age is mandatory")
        repo_age = int(request.data.get('repo_age', None))
        emp_repo_location = request.data.get('emp_repo_location', None)
        if emp_repo_location == "":
            emp_repo_location = None
        if repo_age is None:
            raise Exception(error_type, "repo_age is mandatory")
        emp_no_lang_known = request.data.get('no_lang_known', None)
        if emp_no_lang_known is None:
            raise Exception(error_type, "emp_no_lang_known is mandatory")
        monthly_compens_gross = request.data.get('monthly_compens_gross', None)
        if monthly_compens_gross is None:
            raise Exception(error_type, "monthly_compens_gross is mandatory")
        # vari_comp = request.data.get('vari_comp', None)
        # if vari_comp is None:
        #     raise Exception(error_type, "vari_comp is mandatory")
        annual_hike_percentage = request.data.get('annual_hike_percentage', None)
        if annual_hike_percentage is None:
            raise Exception(error_type, "annual_hike_percentage is mandatory")
        repo_no_lang_known = request.data.get('repo_no_lang_known', None)
        if repo_no_lang_known is None:
            raise Exception(error_type, "repo_no_lang_known is mandatory")
        gender = request.data.get('gender', None)
        if gender == 1:
            gender = "Female"
        if gender == 0:
            gender = 'Male'
        if gender == "":
            gender = None
        marital_status = request.data.get('marital_status', None)
        if marital_status == "":
            marital_status = None
        emp_qualif = request.data.get('emp_qualif', None)
        if emp_qualif == "":
            emp_qualif = None
        repo_qualif = request.data.get('repo_qualif', None)
        if repo_qualif == "":
            repo_qualif = None
        prev_q_perform1 = request.data.get('prev_q_perform1', None)
        if prev_q_perform1 == "":
            prev_q_perform1 = None
        prev_q_perform2 = request.data.get('prev_q_perform2', None)
        if prev_q_perform2 == "":
            prev_q_perform2 = None
        disp_act = request.data.get('disp_act', None)
        if disp_act == "":
            disp_act = None
        pip_act = request.data.get('pip_act', None)
        if pip_act == "":
            pip_act = None
        gem_prev_performer = request.data.get('gem_prev_performer', None)
        if gem_prev_performer == "":
            gem_prev_performer = None
        star_prev_performer = request.data.get('star_prev_performer', None)
        if star_prev_performer == "":
            star_prev_performer = None
        no_leaves_prev_taken = request.data.get('no_leaves_previous_taken', None)
        if no_leaves_prev_taken == "":
            no_leaves_prev_taken = None
        training_prev_done = request.data.get('training_prev_done', None)
        if training_prev_done == "":
            training_prev_done = None
        mcares_score = request.data.get('mcares_score', None)
        if mcares_score == "":
            mcares_score = None
        monthly_compens_prev_gross = request.data.get('monthly_compens_prev_gross', None)
        if monthly_compens_prev_gross == "":
            monthly_compens_prev_gross = None
        vari_prev_comp = request.data.get('vari_prev_comp', None)
        if vari_prev_comp == "":
            vari_prev_comp = None
        annual_hike_prev_percentage = request.data.get('annual_hike_prev_percentage', None)
        if annual_hike_prev_percentage == "":
            annual_hike_prev_percentage = None
        disp_prev_act = request.data.get('disp_prev_act', None)
        if disp_prev_act == "":
            disp_prev_act = None
        pip_prev_act = request.data.get('pip_prev_act', None)
        if pip_prev_act == "":
            pip_prev_act = None
        band = request.data.get('band', None)
        if band == "":
            band = None
        fixed_comp = request.data.get('fixed_component', None)
        if fixed_comp == "":
            fixed_comp = None
        prediction, final_df, pred_max = simulation(model, total_exp_in_mf, repo_total_exp_in_mf, no_leaves_prev_taken,
                                                    no_leaves_taken,
                                                    gem_prev_performer, gem_performer, star_prev_performer,
                                                    star_performer, training_prev_done,
                                                    training_done, emp_age, repo_age, emp_no_lang_known, mcares_score,
                                                    monthly_compens_gross,
                                                    fixed_comp, annual_hike_percentage, repo_no_lang_known,
                                                    emp_repo_location, promotion,
                                                    band, gender, marital_status, emp_qualif, repo_qualif, disp_act,
                                                    pip_act)

        final_data = data_manip_simul_graph(data)
        X = final_data
        X.columns = final_df.keys()
        predict_fn_rfc = lambda x: model.predict_proba(x).astype(float)
        mod_inf_fact = dict()
        feature_names_cat = ['emp_marital_status_Married', 'emp_marital_status_Single', 'training_previous_quarter', \
                             'training_prev_to_prev_quarter', 'gem_previous_quarter', 'gem_prev_to_prev_quarter',
                             'star_prev_to_prev_quarter', \
                             'star_previous_quarter', 'emp_gender_Female', 'emp_gender_Male', 'emp_qualif_cat_grad',
                             'emp_qualif_cat_post_grad', \
                             'emp_qualif_cat_under_grad', 'repo_qualif_cat_grad', 'repo_qualif_cat_post_grad',
                             'repo_qualif_cat_under_grad', \
                             'dic_previous_quarter', 'pip_previous_quarter', 'band_DEPATMENT HEAD', 'band_EXECUTIVE',
                             'band_MANAGERIAL', \
                             'band_OPERATIONAL', 'band_STRATEGIC', 'promotion_category_<=2years',
                             'promotion_category_<=4years', 'promotion_category_<=9years', 'promotion_category_NA']

        explainer = LimeTabularExplainer(X.values,
                                         feature_names=X.columns.tolist(),
                                         class_names=['Active', 'Separated'],
                                         categorical_features=feature_names_cat,
                                         categorical_names=feature_names_cat,
                                         kernel_width=3,
                                         mode='classification', random_state=42)

        exp = explainer.explain_instance(pd.Series(final_df), predict_fn_rfc, num_features=6).as_list()

        inf_fact = dict([(re.findall('prom.*years', i[0])[0], i[1]) if 'promotion_category' in i[0] else
                         (re.sub('[<=0-9>.]', '', i[0]).strip(), i[1])
                         for i in exp])
        contin_vars = ['total_experience_year_in_mf', 'reporting_officer_total_experience_year_in_mf',
                       'manager_net_mcares_score', \
                       'age', 'reporting_office_age', 'monthly_compensation_gross_previous_quarter',
                       'fixed_component_previous_quarter', \
                       'annual_hike_percentage_previous_quarter', 'no_of_days_prev_to_prev_quarter',
                       'no_of_days_previous_quarter', \
                       'emp_lang_count', 'repo_lang_count', 'band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL',
                       'band_OPERATIONAL', \
                       'band_STRATEGIC', 'emp_marital_status_Married', 'emp_marital_status_Single', 'emp_gender_Female',
                       'emp_gender_Male', \
                       'emp_qualif_cat_grad', 'emp_qualif_cat_post_grad', 'emp_qualif_cat_under_grad',
                       'repo_qualif_cat_grad', 'repo_qualif_cat_post_grad',
                       'repo_qualif_cat_under_grad', 'location_emp_rep_same', 'location_emp_rep_different',
                       'promotion_category_<=2years', \
                       'promotion_category_<=4years', 'promotion_category_<=9years', 'promotion_category_NA']

        for k, v in inf_fact.items():
            if k not in contin_vars:
                if final_df[k] <= 1:
                    if final_df[k] == 0:
                        mod_inf_fact[dict_feat_clarity[k] + ' - No'] = v
                    elif final_df[k] == 1:
                        mod_inf_fact[dict_feat_clarity[k] + ' - Yes'] = v
            else:
                mod_inf_fact[dict_feat_clarity[k]] = v
        temp_list = []
        for i in mod_inf_fact.keys():
            temp_dict = {}
            temp_dict["name"] = i
            temp_dict["value"] = mod_inf_fact[i]
            temp_list.append(temp_dict)

        return Response({"message": "success", "data": temp_list}, status=status.HTTP_200_OK)

    except Exception as e:
        logger.error("Error from lime_explainer_view: %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes((IsAuthenticatedUser,))
def get_simulation_data(request):
    try:
        model = Simulation_load_class.model
        pred_max = ''
        error_type = "Field is mandatory"
        total_exp_in_mf = request.data.get('total_experience_in_mf', None)
        if total_exp_in_mf is None:
            raise Exception(error_type, "total_exp_in_mf is mandatory")
        repo_total_exp_in_mf = request.data.get('repo_total_experience_in_mf', None)
        if repo_total_exp_in_mf is None:
            raise Exception(error_type, "repo_total_exp_in_mf is mandatory")
        # exp_in_cur_role = request.data.get('experience_in_current_role', None)
        # if exp_in_cur_role is None:
        #     raise Exception(error_type, "exp_in_cur_role is mandatory")
        no_leaves_taken = request.data.get('no_leaves_taken', None)
        if no_leaves_taken is None:
            raise Exception(error_type, "no_leaves_taken is mandatory")
        # repo_exp_in_cur_role = request.data.get('repo_experience_in_current_role', None)
        # if repo_exp_in_cur_role is None:
        #     raise Exception(error_type, "repo_exp_in_cur_role is mandatory")
        gem_performer = request.data.get('gem_performer', None)
        if gem_performer is None:
            raise Exception(error_type, "gem_performer is mandatory")
        star_performer = request.data.get('star_performer', None)
        if star_performer is None:
            raise Exception(error_type, "star_performer is mandatory")
        promotion = request.data.get('promotion', None)
        if promotion == "promotion_category_2years":
            promotion = "<=2years"
        elif promotion == "promotion_category_4years":
            promotion = "<=4years"
        elif promotion == "promotion_category_9years":
            promotion = "<=9years"
        elif promotion == "promotion_category_NA":
            promotion = "NA"
        if promotion is None:
            raise Exception(error_type, "promotion is mandatory")
        training_done = request.data.get('training_done', None)
        if training_done is None:
            raise Exception(error_type, "training_done is mandatory")
        emp_age = request.data.get('emp_age', None)
        if emp_age is None:
            raise Exception(error_type, "emp_age is mandatory")
        repo_age = request.data.get('repo_age', None)
        if repo_age is None:
            raise Exception(error_type, "repo_age is mandatory")
        emp_no_lang_known = request.data.get('no_lang_known', None)
        if emp_no_lang_known is None:
            raise Exception(error_type, "emp_no_lang_known is mandatory")
        monthly_compens_gross = request.data.get('monthly_compens_gross', None)
        if monthly_compens_gross is None:
            raise Exception(error_type, "monthly_compens_gross is mandatory")
        # vari_comp = request.data.get('vari_comp', None)
        # if vari_comp is None:
        #     raise Exception(error_type, "vari_comp is mandatory")
        annual_hike_percentage = request.data.get('annual_hike_percentage', None)
        if annual_hike_percentage is None:
            raise Exception(error_type, "annual_hike_percentage is mandatory")
        repo_no_lang_known = request.data.get('repo_no_lang_known', None)
        if repo_no_lang_known is None:
            raise Exception(error_type, "repo_no_lang_known is mandatory")
        gender = request.data.get('gender', None)
        if gender == 1:
            gender = "Female"
        if gender == 0:
            gender = 'Male'
        if gender == "":
            gender = None
        emp_repo_location = request.data.get('emp_repo_location', None)
        if emp_repo_location == "":
            emp_repo_location = None
        marital_status = request.data.get('marital_status', None)
        if marital_status == "":
            marital_status = None
        emp_qualif = request.data.get('emp_qualif', None)
        if emp_qualif == "":
            emp_qualif = None
        repo_qualif = request.data.get('repo_qualif', None)
        if repo_qualif == "":
            repo_qualif = None
        disp_act = request.data.get('disp_act', None)
        if disp_act == "":
            disp_act = None
        pip_act = request.data.get('pip_act', None)
        if pip_act == "":
            pip_act = None
        gem_prev_performer = request.data.get('gem_prev_performer', None)
        if gem_prev_performer == "":
            gem_prev_performer = None
        star_prev_performer = request.data.get('star_prev_performer', None)
        if star_prev_performer == "":
            star_prev_performer = None
        no_leaves_prev_taken = request.data.get('no_leaves_previous_taken', None)
        if no_leaves_prev_taken == "":
            no_leaves_prev_taken = None
        training_prev_done = request.data.get('training_prev_done', None)
        if training_prev_done == "":
            training_prev_done = None
        monthly_compens_prev_gross = request.data.get('monthly_compens_prev_gross', None)
        # if monthly_compens_prev_gross == "":
        #     monthly_compens_prev_gross = None
        mcares_score = request.data.get('mcares_score', None)
        if mcares_score == "":
            mcares_score = None
        # vari_prev_comp = request.data.get('vari_prev_comp', None)
        # if vari_prev_comp == "":
        #     vari_prev_comp = None
        annual_hike_prev_percentage = request.data.get('annual_hike_prev_percentage', None)
        # if annual_hike_prev_percentage == "":
        #     annual_hike_prev_percentage = None
        disp_prev_act = request.data.get('disp_prev_act', None)
        # if disp_prev_act == "":
        #     disp_prev_act = None
        # pip_prev_act = request.data.get('pip_prev_act', None)
        # if pip_prev_act == "":
        #     pip_prev_act = None
        band = request.data.get('band', None)
        if band == "":
            band = None
        fixed_comp = request.data.get('fixed_component', None)
        if fixed_comp == "":
            fixed_comp = None

        prediction, final_df, pred_max = simulation(model, total_exp_in_mf, repo_total_exp_in_mf, no_leaves_prev_taken,
                                                    no_leaves_taken, \
                                                    gem_prev_performer, gem_performer, star_prev_performer,
                                                    star_performer, training_prev_done, \
                                                    training_done, emp_age, repo_age, emp_no_lang_known, mcares_score,
                                                    monthly_compens_gross, \
                                                    fixed_comp, annual_hike_percentage, repo_no_lang_known,
                                                    emp_repo_location, promotion, \
                                                    band, gender, marital_status, emp_qualif, repo_qualif, disp_act,
                                                    pip_act)

        searched_empid = request.data.get('employee_id')
        if searched_empid == "":
            searched_empid = None
        if searched_empid is not None:
            # data1 = {'total_exp_in_mf': total_exp_in_mf, 'exp_in_cur_role': exp_in_cur_role, 'repo_total_exp_in_mf': repo_total_exp_in_mf,
            #          'no_leaves_taken': no_leaves_taken, 'repo_exp_in_cur_role': repo_exp_in_cur_role, 'promotion': promotion, 'emp_age': emp_age,
            #          'repo_age': repo_age, 'extension_of_probation': extension_of_probation,
            #          'monthly_compens_gross': monthly_compens_gross, 'vari_comp': vari_comp,
            #          'oprn_category': oprn_category, 'department': department,
            #          'gender': gender, 'marital_status': marital_status
            #          }
            data1 = {
                     'monthly_compens_gross': monthly_compens_gross,
                # 'vari_comp': vari_prev_comp,
                # 'oprn_category': oprn_category, 'department': department,
                'age': emp_age
                     }
            df1 = pd.DataFrame(data1,columns= data1.keys(), index=[0])
            # query1 = """select  total_experience_months_in_mf::int as total_exp_in_mf,experience_in_current_role_in_months::int as exp_in_cur_role,
            # reporting_officer_total_experience_months_in_mf::int as repo_total_exp_in_mf, no_of_days_previous_quarter::int as no_leaves_taken,
            # reporting_officer_experience_in_current_role_in_months::int as repo_exp_in_cur_role, promotion_year as promotion,emp_age,
            # reporting_office_age as repo_age,extension_of_probation_prev_quarter as extension_of_probation,
            # monthly_compensation_gross_previous_quarter::numeric::integer as monthly_compens_gross,
            # variable_component_previous_quarter::numeric::integer as vari_comp,oprn_category,department,
            # emp_gender as gender,emp_marital_status as marital_status
            # from buisness_suite.prediction_data_view  where empid ='{0}' ;""".format(searched_empid)

            query1 = """select monthly_compensation_gross_previous_quarter::numeric::float as monthly_compens_gross,
                        age::numeric::integer from buisness_suite.prediction_data_view  where employee_id ='{0}' ;"""\
                .format(searched_empid)
            query_obj = get_cursor(query1)
            df2 = DataFrame(query_obj.fetchall(),columns= data1.keys(), index=[0])
            mismatch = False
            for i in df1.keys():
                if df1['monthly_compens_gross'].values[0] != 0 and df1['age'].values[0] !=0:
                    if not df1[i].equals(df2[i]):
                        mismatch = True
            if not mismatch:
                pred_max_query = """select coalesce(grade,'N/A') as perfomance_cat from predictive.predictive_output where employee_id ='{0}' ;""".format(searched_empid)
                query_obj2 = get_cursor(pred_max_query)
                pred_max = query_obj2.fetchone()[0] + ' Attrition'

        return Response({"message": "success", "data": final_df, 'input_prediction': pred_max,
                         'prediction performance': "prediction_performance"}, status=status.HTTP_200_OK)
    except Exception as e:
        print("error", e)
        logger.error("Error from get_simulation_data: %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["POST"])
@permission_classes((IsAuthenticatedUser,))
def get_survival_data(request):
    try:
        surv_model = Simulation_load_class.surv_model
        model = Simulation_load_class.model
        pred_max = ''
        error_type = "Field is mandatory"
        total_exp_in_mf = request.data.get('total_experience_in_mf', None)
        if total_exp_in_mf is None:
            raise Exception(error_type, "total_exp_in_mf is mandatory")
        repo_total_exp_in_mf = request.data.get('repo_total_experience_in_mf', None)
        if repo_total_exp_in_mf is None:
            raise Exception(error_type, "repo_total_exp_in_mf is mandatory")
        # exp_in_cur_role = request.data.get('experience_in_current_role', None)
        # if exp_in_cur_role is None:
        #     raise Exception(error_type, "exp_in_cur_role is mandatory")
        no_leaves_taken = request.data.get('no_leaves_taken', None)
        if no_leaves_taken is None:
            raise Exception(error_type, "no_leaves_taken is mandatory")
        # repo_exp_in_cur_role = request.data.get('repo_experience_in_current_role', None)
        # if repo_exp_in_cur_role is None:
        #     raise Exception(error_type, "repo_exp_in_cur_role is mandatory")
        gem_performer = request.data.get('gem_performer', None)
        if gem_performer is None:
            raise Exception(error_type, "gem_performer is mandatory")
        star_performer = request.data.get('star_performer', None)
        if star_performer is None:
            raise Exception(error_type, "star_performer is mandatory")
        promotion = request.data.get('promotion', None)
        if promotion == "promotion_category_2years":
            promotion = "<=2years"
        elif promotion == "promotion_category_4years":
            promotion = "<=4years"
        elif promotion == "promotion_category_9years":
            promotion = "<=9years"
        elif promotion == "promotion_category_NA":
            promotion = "NA"
        if promotion is None:
            raise Exception(error_type, "promotion is mandatory")

        emp_repo_location = request.data.get('emp_repo_location', None)
        if emp_repo_location == "":
            emp_repo_location = None
        training_done = request.data.get('training_done', None)

        if training_done is None:
            raise Exception(error_type, "training_done is mandatory")

        emp_age = request.data.get('emp_age', None)
        if emp_age is None:
            raise Exception(error_type, "emp_age is mandatory")
        repo_age = request.data.get('repo_age', None)
        if repo_age is None:
            raise Exception(error_type, "repo_age is mandatory")
        emp_no_lang_known = request.data.get('no_lang_known', None)
        if emp_no_lang_known is None:
            raise Exception(error_type, "emp_no_lang_known is mandatory")
        monthly_compens_gross = request.data.get('monthly_compens_gross', None)
        if monthly_compens_gross is None:
            raise Exception(error_type, "monthly_compens_gross is mandatory")
        # vari_comp = request.data.get('vari_comp', None)
        # if vari_comp is None:
        #     raise Exception(error_type, "vari_comp is mandatory")
        annual_hike_percentage = request.data.get('annual_hike_percentage', None)
        if annual_hike_percentage is None:
            raise Exception(error_type, "annual_hike_percentage is mandatory")
        repo_no_lang_known = request.data.get('repo_no_lang_known', None)
        if repo_no_lang_known is None:
            raise Exception(error_type, "repo_no_lang_known is mandatory")
        gender = request.data.get('gender', None)
        if gender == 1:
            gender = "Female"
        if gender == 0:
            gender = 'Male'
        if gender == "":
            gender = None
        marital_status = request.data.get('marital_status', None)
        if marital_status == "":
            marital_status = None
        emp_qualif = request.data.get('emp_qualif', None)
        if emp_qualif == "":
            emp_qualif = None
        repo_qualif = request.data.get('repo_qualif', None)
        if repo_qualif == "":
            repo_qualif = None
        disp_act = request.data.get('disp_act', None)
        if disp_act == "":
            disp_act = None
        pip_act = request.data.get('pip_act', None)
        if pip_act == "":
            pip_act = None
        gem_prev_performer = request.data.get('gem_prev_performer', None)
        if gem_prev_performer == "":
            gem_prev_performer = None
        star_prev_performer = request.data.get('star_prev_performer', None)
        if star_prev_performer == "":
            star_prev_performer = None
        no_leaves_prev_taken = request.data.get('no_leaves_previous_taken', None)
        if no_leaves_prev_taken == "":
            no_leaves_prev_taken = None
        training_prev_done = request.data.get('training_prev_done', None)
        if training_prev_done == "":
            training_prev_done = None
        monthly_compens_prev_gross = request.data.get('monthly_compens_prev_gross', None)
        # if monthly_compens_prev_gross == "":
        #     monthly_compens_prev_gross = None
        mcares_score = request.data.get('mcares_score', None)
        if mcares_score == "":
            mcares_score = None
        # vari_prev_comp = request.data.get('vari_prev_comp', None)
        # if vari_prev_comp == "":
        #     vari_prev_comp = None
        annual_hike_prev_percentage = request.data.get('annual_hike_prev_percentage', None)
        # if annual_hike_prev_percentage == "":
        #     annual_hike_prev_percentage = None
        disp_prev_act = request.data.get('disp_prev_act', None)
        # if disp_prev_act == "":
        #     disp_prev_act = None
        # pip_prev_act = request.data.get('pip_prev_act', None)
        # if pip_prev_act == "":
        #     pip_prev_act = None
        band = request.data.get('band', None)
        if band == "":
            band = None
        fixed_comp = request.data.get('fixed_component', None)
        if fixed_comp == "":
            fixed_comp = None

        prediction, final_df, pred_max = simulation(model, total_exp_in_mf, repo_total_exp_in_mf, no_leaves_prev_taken,
                                                    no_leaves_taken, \
                                                    gem_prev_performer, gem_performer, star_prev_performer,
                                                    star_performer, training_prev_done, \
                                                    training_done, emp_age, repo_age, emp_no_lang_known, mcares_score,
                                                    monthly_compens_gross, \
                                                    fixed_comp, annual_hike_percentage, repo_no_lang_known,
                                                    emp_repo_location, promotion, \
                                                    band, gender, marital_status, emp_qualif, repo_qualif, disp_act,
                                                    pip_act)
        data = survival_curve(final_df, surv_model)
        return Response({"message": "success", "data": data}, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("Error from get_survival_data: %s" % e)
        return Response({"message": "Failed", "error": e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# @api_view(["GET"])
# @permission_classes((IsAuthenticatedUser,))
def get_quarterly_simulation_data():
    try:
        start_time = datetime.now()
        print("start_time",start_time)
        truncate_query = """truncate table predictive.simulation_data_for_download"""
        query_obj = get_cursor(truncate_query)
        emp_list_query = """select distinct employee_id from predictive.predictive_output"""
        query_obj = get_cursor(emp_list_query)
        query_data = query_obj.fetchall()
        emp_list = [i[0] for i in query_data]
        # emp_list = [23111278]
        for emp in emp_list:
            # employee detail function.......................
            employee_id = emp
            query = """select employee_id,
            first_name || ' ' || last_name employee_name,department,branch,division,business_unit, 
            total_experience_year_in_mf::double precision,experience_in_current_role_in_months::double precision,
            reporting_officer_total_experience_year_in_mf::double precision, 
            reporting_officer_experience_in_current_role_in_months::double precision,
            age::int,reporting_office_age::int, emp_gender,        
            emp_lang_known1,emp_lang_known2,emp_lang_known3,emp_lang_known4,repo_lang_known1,repo_lang_known2,
            repo_lang_known3,repo_lang_known4,emp_marital_status,emp_highest_qualification,
            repo_highest_qualification,repo_location,emp_location,
            gem_previous_quarter,gem_prev_to_prev_quarter,star_previous_quarter,
            star_prev_to_prev_quarter,band,
            promotion_previous_quarter,training_previous_quarter,training_prev_to_prev_quarter,
            promotion_prev_to_prev_quarter,manager_net_mcares_score,
            dic_prev_to_prev_quarter,dic_previous_quarter,monthly_compensation_gross_previous_quarter::float8,
            monthly_compensation_gross_prev_to_prev_quarter::float8,fixed_component_previous_quarter::float8,
            promotion_count_year, variable_component_previous_quarter::float8,
            variable_component_prev_to_prev_quarter::float8, annual_hike_percentage_prev_to_prev_quarter,
            annual_hike_percentage_previous_quarter,no_of_days_prev_to_prev_quarter,no_of_days_previous_quarter,
            pip_prev_to_prev_quarter,pip_previous_quarter,promotion_year         
            from  buisness_suite.prediction_data_view  where employee_id= '{0}'""".format(employee_id)
            query_obj = get_cursor(query)
            query_data = query_obj.fetchall()
            # print(".................", query)
            if len(query_data) == 0:
                return Response({"message": "Employee id doesn't exist"}, status=status.HTTP_200_OK)
            else:
                try:
                    query_columns = [col[0] for col in query_obj.description]
                    result = dict(zip(query_columns, query_data[0]))
                    manipulated_data = emp_wise_data_sim(result, Simulation_load_class.data)
                    manipulated_data = manipulated_data.to_dict()
                    new_dic_temp = []
                except Exception as e1:
                    # print(manipulated_data)
                    print("==========================================================================")
                    print(emp)
                    continue
                for key, value in manipulated_data.items():
                    key, value = key, value.get(0)
                    if value is False:
                        value = 0
                    if value is True:
                        value = 1
                    new_dic_temp.append(key)
                    new_dic_temp.append(value)

                keys = []
                values = []
                for i, j in enumerate(new_dic_temp):
                    if i % 2 == 0:
                        keys.append(j)
                    else:
                        values.append(j)
                result = dict(zip(keys, values))

                # simulation function..................................................
                model = Simulation_load_class.model
                pred_max = ''
                error_type = "Field is mandatory"
                total_exp_in_mf = result.get('total_experience_year_in_mf')
                if total_exp_in_mf is None:
                    raise Exception(error_type, "total_exp_in_mf is mandatory")
                repo_total_exp_in_mf = result.get('reporting_officer_total_experience_year_in_mf', None)
                if repo_total_exp_in_mf is None:
                    raise Exception(error_type, "repo_total_exp_in_mf is mandatory")
                no_leaves_taken = result.get('no_of_days_previous_quarter', None)
                if no_leaves_taken is None:
                    raise Exception(error_type, "no_leaves_taken is mandatory")
                gem_performer = result.get('gem_previous_quarter', None)
                if gem_performer is None:
                    raise Exception(error_type, "gem_performer is mandatory")
                star_performer = result.get('star_previous_quarter', None)
                if star_performer is None:
                    raise Exception(error_type, "star_performer is mandatory")
                promotion = result.get('promotion_category', None)
                if promotion == "promotion_category_2years":
                    promotion = "<=2years"
                elif promotion == "promotion_category_4years":
                    promotion = "<=4years"
                elif promotion == "promotion_category_9years":
                    promotion = "<=9years"
                elif promotion == "promotion_category_NA":
                    promotion = "NA"
                if promotion is None:
                    raise Exception(error_type, "promotion is mandatory")
                training_done = result.get('training_previous_quarter', None)
                if training_done is None:
                    raise Exception(error_type, "training_done is mandatory")
                emp_age = result.get('age', None)
                if emp_age is None:
                    raise Exception(error_type, "emp_age is mandatory")
                repo_age = result.get('reporting_office_age', None)
                if repo_age is None:
                    raise Exception(error_type, "repo_age is mandatory")
                emp_no_lang_known = result.get('emp_lang_count', None)
                if emp_no_lang_known is None:
                    raise Exception(error_type, "emp_no_lang_known is mandatory")
                monthly_compens_gross = result.get('monthly_compensation_gross_previous_quarter', None)
                if monthly_compens_gross is None:
                    raise Exception(error_type, "monthly_compens_gross is mandatory")
                annual_hike_percentage = result.get('annual_hike_percentage_previous_quarter', None)
                if annual_hike_percentage is None:
                    raise Exception(error_type, "annual_hike_percentage is mandatory")
                repo_no_lang_known = result.get('repo_lang_count', None)
                if repo_no_lang_known is None:
                    raise Exception(error_type, "repo_no_lang_known is mandatory")
                gender = result.get('emp_gender', None)
                if gender == 1:
                    gender = "Female"
                if gender == 0:
                    gender = 'Male'
                if gender == "":
                    gender = None
                emp_repo_location = result.get('location_emp_rep', None)
                if emp_repo_location == "":
                    emp_repo_location = None
                marital_status = result.get('emp_marital_status', None)
                if marital_status == "":
                    marital_status = None
                emp_qualif = result.get('emp_qualif_cat', None)
                if emp_qualif == "":
                    emp_qualif = None
                repo_qualif = result.get('repo_qualif_cat', None)
                if repo_qualif == "":
                    repo_qualif = None
                disp_act = result.get('dic_previous_quarter', None)
                if disp_act == "":
                    disp_act = None
                if disp_act is None:
                    disp_act = 0
                pip_act = result.get('pip_previous_quarter', None)
                if pip_act == "":
                    pip_act = None
                if pip_act is None:
                    pip_act = 0
                gem_prev_performer = result.get('gem_previous_quarter', None)
                if gem_prev_performer == "":
                    gem_prev_performer = None
                star_prev_performer = result.get('star_previous_quarter', None)
                if star_prev_performer == "":
                    star_prev_performer = None
                no_leaves_prev_taken = result.get('no_of_days_prev_to_prev_quarter', None)
                if no_leaves_prev_taken == "":
                    no_leaves_prev_taken = None
                training_prev_done = result.get('training_prev_to_prev_quarter', None)
                if training_prev_done == "":
                    training_prev_done = None
                monthly_compens_prev_gross = result.get('monthly_compensation_gross_previous_quarter', None)
                mcares_score = result.get('manager_net_mcares_score', None)
                if mcares_score == "":
                    mcares_score = None
                annual_hike_prev_percentage = result.get('annual_hike_percentage_previous_quarter', None)
                disp_prev_act = result.get('disp_prev_act', None)
                band = result.get('band', None)
                if band == "":
                    band = None
                fixed_comp = result.get('fixed_component_previous_quarter', None)
                if fixed_comp == "":
                    fixed_comp = None

                prediction, final_df, pred_max = simulation(model, total_exp_in_mf, repo_total_exp_in_mf,
                                                            no_leaves_prev_taken, no_leaves_taken, gem_prev_performer,
                                                            gem_performer, star_prev_performer, star_performer,
                                                            training_prev_done, training_done, emp_age, repo_age,
                                                            emp_no_lang_known, mcares_score, monthly_compens_gross,
                                                            fixed_comp, annual_hike_percentage, repo_no_lang_known,
                                                            emp_repo_location, promotion, band, gender, marital_status,
                                                            emp_qualif, repo_qualif, disp_act, pip_act)
                searched_empid = result.get('employee_id')
                if searched_empid == "":
                    searched_empid = None
                if searched_empid is not None:
                    data1 = {
                        'monthly_compens_gross': monthly_compens_gross,
                        # 'vari_comp': vari_prev_comp,
                        # 'oprn_category': oprn_category, 'department': department,
                        'age': emp_age
                    }
                    df1 = pd.DataFrame(data1, columns=data1.keys(), index=[0])
                    query1 = """select monthly_compensation_gross_previous_quarter::numeric::float 
                    as monthly_compens_gross, age::numeric::integer from buisness_suite.prediction_data_view  
                    where employee_id ='{0}' ;""" \
                        .format(searched_empid)
                    query_obj = get_cursor(query1)
                    df2 = DataFrame(query_obj.fetchall(), columns=data1.keys(), index=[0])
                    mismatch = False
                    for i in df1.keys():
                        if df1['monthly_compens_gross'].values[0] != 0 and df1['age'].values[0] != 0:
                            if not df1[i].equals(df2[i]):
                                mismatch = True
                    if not mismatch:
                        pred_max_query = """select coalesce(grade,'N/A') as perfomance_cat 
                        from predictive.predictive_output where employee_id ='{0}' ;""".format(searched_empid)
                        query_obj2 = get_cursor(pred_max_query)
                        pred_max = query_obj2.fetchone()[0] + ' Attrition'
                # lime graph function.........................................................................

                inf_fact = dict()
                model = Simulation_load_class.model
                data = Simulation_load_class.data
                pred_max = ''
                error_type = "Field is mandatory"
                total_exp_in_mf = result.get('total_experience_year_in_mf', None)
                if total_exp_in_mf is None:
                    raise Exception(error_type, "total_exp_in_mf is mandatory")
                repo_total_exp_in_mf = result.get('reporting_officer_total_experience_year_in_mf', None)
                if repo_total_exp_in_mf is None:
                    raise Exception(error_type, "repo_total_exp_in_mf is mandatory")
                no_leaves_taken = result.get('no_of_days_previous_quarter', None)
                if no_leaves_taken is None:
                    raise Exception(error_type, "no_leaves_taken is mandatory")
                gem_performer = result.get('gem_previous_quarter', None)
                if gem_performer is None:
                    raise Exception(error_type, "gem_performer is mandatory")
                star_performer = result.get('star_previous_quarter', None)
                if star_performer is None:
                    raise Exception(error_type, "star_performer is mandatory")
                promotion = result.get('promotion_category', None)
                if promotion == "promotion_category_2years":
                    promotion = "<=2years"
                elif promotion == "promotion_category_4years":
                    promotion = "<=4years"
                elif promotion == "promotion_category_9years":
                    promotion = "<=9years"
                elif promotion == "promotion_category_NA":
                    promotion = "NA"
                if promotion is None:
                    raise Exception(error_type, "promotion is mandatory")
                training_done = result.get('training_previous_quarter', None)
                if training_done is None:
                    raise Exception(error_type, "training_done is mandatory")
                emp_age = int(result.get('age', None))
                if emp_age is None:
                    raise Exception(error_type, "emp_age is mandatory")
                repo_age = int(result.get('reporting_office_age', None))
                emp_repo_location = result.get('emp_repo_location', None)
                if emp_repo_location == "":
                    emp_repo_location = None
                if repo_age is None:
                    raise Exception(error_type, "repo_age is mandatory")
                emp_no_lang_known = result.get('emp_lang_count', None)
                if emp_no_lang_known is None:
                    raise Exception(error_type, "emp_no_lang_known is mandatory")
                monthly_compens_gross = result.get('monthly_compensation_gross_previous_quarter', None)
                if monthly_compens_gross is None:
                    raise Exception(error_type, "monthly_compens_gross is mandatory")
                annual_hike_percentage = result.get('annual_hike_percentage_previous_quarter', None)
                if annual_hike_percentage is None:
                    raise Exception(error_type, "annual_hike_percentage is mandatory")
                repo_no_lang_known = result.get('repo_lang_count', None)
                if repo_no_lang_known is None:
                    raise Exception(error_type, "repo_no_lang_known is mandatory")
                gender = result.get('gender', None)
                if gender == 1:
                    gender = "Female"
                if gender == 0:
                    gender = 'Male'
                if gender == "":
                    gender = None
                marital_status = result.get('marital_status', None)
                if marital_status == "":
                    marital_status = None
                emp_qualif = result.get('emp_qualif', None)
                if emp_qualif == "":
                    emp_qualif = None
                repo_qualif = result.get('repo_qualif', None)
                if repo_qualif == "":
                    repo_qualif = None
                prev_q_perform1 = result.get('prev_q_perform1', None)
                if prev_q_perform1 == "":
                    prev_q_perform1 = None
                prev_q_perform2 = result.get('prev_q_perform2', None)
                if prev_q_perform2 == "":
                    prev_q_perform2 = None
                if disp_act == "":
                    disp_act = None
                if disp_act is None:
                    disp_act = 0
                pip_act = result.get('pip_previous_quarter', None)
                if pip_act == "":
                    pip_act = None
                if pip_act is None:
                    pip_act = 0
                gem_prev_performer = result.get('gem_prev_to_prev_quarter', None)
                if gem_prev_performer == "":
                    gem_prev_performer = None
                star_prev_performer = result.get('star_prev_to_prev_quarter', None)
                if star_prev_performer == "":
                    star_prev_performer = None
                no_leaves_prev_taken = result.get('no_of_days_prev_to_prev_quarter', None)
                if no_leaves_prev_taken == "":
                    no_leaves_prev_taken = None
                training_prev_done = result.get('training_prev_to_prev_quarter', None)
                if training_prev_done == "":
                    training_prev_done = None
                mcares_score = result.get('manager_net_mcares_score', None)
                if mcares_score == "":
                    mcares_score = None
                monthly_compens_prev_gross = result.get('monthly_compens_prev_gross', None)
                if monthly_compens_prev_gross == "":
                    monthly_compens_prev_gross = None
                vari_prev_comp = result.get('vari_prev_comp', None)
                if vari_prev_comp == "":
                    vari_prev_comp = None
                annual_hike_prev_percentage = result.get('annual_hike_prev_percentage', None)
                if annual_hike_prev_percentage == "":
                    annual_hike_prev_percentage = None
                disp_prev_act = result.get('disp_prev_act', None)
                if disp_prev_act == "":
                    disp_prev_act = None
                pip_prev_act = result.get('pip_prev_act', None)
                if pip_prev_act == "":
                    pip_prev_act = None
                band = result.get('band', None)
                if band == "":
                    band = None
                fixed_comp = result.get('fixed_component_previous_quarter', None)
                if fixed_comp == "":
                    fixed_comp = None
                prediction, final_df, pred_max = simulation(model, total_exp_in_mf, repo_total_exp_in_mf,
                                                            no_leaves_prev_taken,
                                                            no_leaves_taken,
                                                            gem_prev_performer, gem_performer, star_prev_performer,
                                                            star_performer, training_prev_done,
                                                            training_done, emp_age, repo_age, emp_no_lang_known,
                                                            mcares_score,
                                                            monthly_compens_gross,
                                                            fixed_comp, annual_hike_percentage, repo_no_lang_known,
                                                            emp_repo_location, promotion,
                                                            band, gender, marital_status, emp_qualif, repo_qualif,
                                                            disp_act,
                                                            pip_act)

                final_data = data_manip_simul_graph(data)
                X = final_data
                X.columns = final_df.keys()
                predict_fn_rfc = lambda x: model.predict_proba(x).astype(float)
                mod_inf_fact = dict()
                feature_names_cat = ['emp_marital_status_Married', 'emp_marital_status_Single',
                                     'training_previous_quarter', 'training_prev_to_prev_quarter',
                                     'gem_previous_quarter', 'gem_prev_to_prev_quarter', 'star_prev_to_prev_quarter',
                                     'star_previous_quarter', 'emp_gender_Female', 'emp_gender_Male',
                                     'emp_qualif_cat_grad', 'emp_qualif_cat_post_grad', 'emp_qualif_cat_under_grad',
                                     'repo_qualif_cat_grad', 'repo_qualif_cat_post_grad', 'repo_qualif_cat_under_grad',
                                     'dic_previous_quarter', 'pip_previous_quarter', 'band_DEPATMENT HEAD',
                                     'band_EXECUTIVE', 'band_MANAGERIAL', 'band_OPERATIONAL', 'band_STRATEGIC',
                                     'promotion_category_<=2years', 'promotion_category_<=4years',
                                     'promotion_category_<=9years', 'promotion_category_NA']

                explainer = LimeTabularExplainer(X.values,
                                                 feature_names=X.columns.tolist(),
                                                 class_names=['Active', 'Separated'],
                                                 categorical_features=feature_names_cat,
                                                 categorical_names=feature_names_cat,
                                                 kernel_width=3,
                                                 mode='classification', random_state=42)

                exp = explainer.explain_instance(pd.Series(final_df), predict_fn_rfc, num_features=6).as_list()

                inf_fact = dict([(re.findall('prom.*years', i[0])[0], i[1]) if 'promotion_category' in i[0] else
                                 (re.sub('[<=0-9>.]', '', i[0]).strip(), i[1])
                                 for i in exp])
                contin_vars = ['total_experience_year_in_mf', 'reporting_officer_total_experience_year_in_mf',
                               'manager_net_mcares_score', 'age', 'reporting_office_age',
                               'monthly_compensation_gross_previous_quarter', 'fixed_component_previous_quarter',
                               'annual_hike_percentage_previous_quarter', 'no_of_days_prev_to_prev_quarter',
                               'no_of_days_previous_quarter', 'emp_lang_count', 'repo_lang_count',
                               'band_DEPATMENT HEAD', 'band_EXECUTIVE', 'band_MANAGERIAL', 'band_OPERATIONAL',
                               'band_STRATEGIC', 'emp_marital_status_Married', 'emp_marital_status_Single',
                               'emp_gender_Female', 'emp_gender_Male', 'emp_qualif_cat_grad',
                               'emp_qualif_cat_post_grad', 'emp_qualif_cat_under_grad', 'repo_qualif_cat_grad',
                               'repo_qualif_cat_post_grad', 'repo_qualif_cat_under_grad', 'location_emp_rep_same',
                               'location_emp_rep_different', 'promotion_category_<=2years',
                               'promotion_category_<=4years', 'promotion_category_<=9years', 'promotion_category_NA']

                for k, v in inf_fact.items():
                    if k not in contin_vars:
                        if final_df[k] <= 1:
                            if final_df[k] == 0:
                                mod_inf_fact[dict_feat_clarity[k] + ' - No'] = v
                            elif final_df[k] == 1:
                                mod_inf_fact[dict_feat_clarity[k] + ' - Yes'] = v
                    else:
                        mod_inf_fact[dict_feat_clarity[k]] = v
                temp_list = []
                for i in mod_inf_fact.keys():
                    temp_dict = {}
                    temp_dict["name"] = i
                    temp_dict["value"] = mod_inf_fact[i]
                    temp_list.append(temp_dict)
            temp_list_df = pd.DataFrame(temp_list)
            new_df = temp_list_df.sort_values(by='value',ascending=False)
            final_temp_list = new_df.head(3).to_dict('records')
            for i in final_temp_list:
                insert_query = """INSERT INTO predictive.simulation_data_for_download
                (employee_id, employee_name, department, branch, division, business_unit, band,
                attrition_factor, attrition_factor_value, attrition_type)
                VALUES({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', {8}, '{9}')""".\
                    format(result.get('employee_id'), result.get('employee_name'), result.get('department'),
                           result.get('branch'), result.get('division'), result.get('business_unit'), result.get('band'),
                           i.get('name'), i.get('value'), pred_max)
                query_obj = get_cursor(insert_query)
        end_time = datetime.now()
        print("total_time", (end_time - start_time).total_seconds() / 60.0)
        return Response({"message": "success", "data": final_df, 'input_prediction': pred_max,
                         'temp_list': temp_list, "result": result}, status=status.HTTP_200_OK)
    except Exception as e:
        print(emp)
        logger.error("Error from get_survival_data: %s" % e)
        pass
        # return Response({"message": "Failed", "error": e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



